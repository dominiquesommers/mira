from mira.prelim.multiset import Multiset
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import Arc, InhibitorArc
from mira.nets.firing import Firing
from mira.nets.marking import Marking, NuMarking

from typing import Optional, Iterable
from copy import copy
from collections import defaultdict
import tempfile
from graphviz import Digraph


class Net:
  firing_type = Firing
  marking_type = Marking
  def __init__(self, places: Optional[Iterable[Place]]=None, transitions: Optional[Iterable[Transition]]=None,
               arcs: Optional[Iterable[Arc]]=None, inhibitor_arcs: Optional[Iterable[InhibitorArc]]=None):
    self.__places = [] if places is None else list(places)
    self.__transitions = [] if transitions is None else list(transitions)
    self.__arcs = set() # if arcs is None else set(arc for arc in arcs if self._check_arc(arc))
    self.__element_in_arcs = defaultdict(self.arc_multiset_initializer)
    self.__element_out_arcs = defaultdict(self.arc_multiset_initializer)
    if arcs is not None:
      [self.add_arc(arc) for arc in arcs]
    self.inhibitor_arcs = set()
    self.__transition_inhibitor_arcs = defaultdict(list)
    if inhibitor_arcs is not None:
      [self.add_arc(inhibitor_arc) for inhibitor_arc in inhibitor_arcs]

  @staticmethod
  def arc_multiset_initializer():
    return Multiset(Arc)

  @property
  def places(self):
    return self.__places

  @places.setter
  def places(self, places):
    self.__places = places

  @property
  def transitions(self):
    return self.__transitions

  @transitions.setter
  def transitions(self, transitions):
    self.__transitions = transitions

  @property
  def arcs(self):
    return self.__arcs

  @arcs.setter
  def arcs(self, arcs):
    self.__arcs = arcs

  @property
  def element_in_arcs(self):
    return self.__element_in_arcs

  @property
  def element_out_arcs(self):
    return self.__element_out_arcs

  @property
  def transition_inhibitor_arcs(self):
    return self.__transition_inhibitor_arcs

  def __add__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")
    net = type(self)(copy(self.places), copy(self.transitions), copy(self.arcs), copy(self.inhibitor_arcs))
    net.places.extend(other.places)
    net.transitions.extend(other.transitions)
    [net.add_arc(arc) for arc in other.arcs]
    [net.add_arc(arc) for arc in other.inhibitor_arcs]
    return net

  def get_incidence_matrix(self):
    matrix = [[0 for _ in range(len(self.transitions))] for _ in range(len(self.places))]
    for arc in self.arcs:
      if arc.source == arc.place:
        matrix[self.places.index(arc.place)][self.transitions.index(arc.transition)] -= arc.weight
      else:
        matrix[self.places.index(arc.place)][self.transitions.index(arc.transition)] += arc.weight
    return matrix

  def _check_arc(self, arc):
    error_message = ''
    if arc.source not in (self.places if isinstance(arc.source, Place) else self.transitions):
      error_message += f'Source {arc.source} of type {type(arc.source).__name__} not in net.'
    if arc.target not in (self.places if isinstance(arc.target, Place) else self.transitions):
      error_message += f'{"T" if error_message == "" else "And t"}arget {arc.target} of type {type(arc.target).__name__} not in net.'
    if error_message != '':
      raise ValueError(error_message)
    return True

  def add_place(self, place:Place):
    self.places.append(place)
    return place

  def add_transition(self, transition:Transition):
    self.transitions.append(transition)
    return transition

  def remove_place(self, place:Place):
    if place in self.places:
      self.places.remove(place)

  def remove_transition(self, transition:Transition):
    if transition in self.transitions:
      self.transitions.remove(transition)

  def add_arc(self, arc:Arc, verbose:bool=False):
    if isinstance(arc, InhibitorArc):
      self.add_inhibitor_arc(arc)
      return
    if not self._check_arc(arc):
      return
    for existing_arc in self.arcs:
      if existing_arc == arc:
        existing_arc.weight += arc.weight
        self.__element_out_arcs[arc.source][arc] += arc.weight
        self.__element_in_arcs[arc.target][arc] += arc.weight
        break
    else:
      self.arcs.add(arc)
      self.__element_out_arcs[arc.source][arc] += arc.weight
      self.__element_in_arcs[arc.target][arc] += arc.weight

  def remove_arc(self, arc):
    if isinstance(arc, InhibitorArc):
      self.remove_inhibitor_arc(arc)
      return
    self.arcs.remove(arc)
    del self.__element_out_arcs[arc.source][arc]
    del self.__element_in_arcs[arc.target][arc]

  def add_inhibitor_arc(self, arc:InhibitorArc):
    if not (isinstance(arc.source, Place) and self._check_arc(arc)):
      return
    self.__transition_inhibitor_arcs[arc.target].append(arc)
    self.inhibitor_arcs.add(arc)

  def remove_inhibitor_arc(self, arc:InhibitorArc):
    self.inhibitor_arcs.remove(arc)
    self.__transition_inhibitor_arcs[arc.target].remove(arc)

  def merge_transitions(self, transitions, new_name=None, new_label=None):
    transitions[0].name = transitions[0].name if new_name is None else new_name
    transitions[0].label = transitions[0].label if new_label is None else new_label
    for transition in transitions[1:]:
      for arc, weight in self.__element_out_arcs[transition].items():
        arc.source = transitions[0]
        self.__element_out_arcs[transitions[0]][arc] = weight
      for arc, weight in self.__element_in_arcs[transition].items():
        arc.target = transitions[0]
        self.__element_in_arcs[transitions[0]][arc] = weight
      for arc in self.__transition_inhibitor_arcs[transition]:
        arc.target = transitions[0]
        self.__transition_inhibitor_arcs[transitions[0]].append(arc)
      del self.__element_out_arcs[transition]
      del self.__element_in_arcs[transition]
      del self.__transition_inhibitor_arcs[transition]
      self.remove_transition(transition)
    return transitions[0]


  def create_marking_from_mode(self, arcs: dict, mode: dict):
    return Marking({arc.place: weight for arc, weight in arcs.items()})

  def transition_enabled(self, transition: Transition, marking: Marking, mode: dict=None, verbose=False):
    inh_marking = self.create_marking_from_mode({arc: 1 for arc in self.__transition_inhibitor_arcs[transition]}, mode)
    if not all(marking[place][token] == 0 for place, inh_place_marking in inh_marking.items() for token in inh_place_marking.keys()):
      if verbose: print(f'Not enabled due to inhibitors {transition.name} {mode}')
      return False, None, None

    try:
      if (sub_marking := self.create_marking_from_mode(self.__element_in_arcs[transition], mode)) > marking:
        if verbose: print(f'Not enabled due to sub_marking > marking\n{sub_marking}\n>\n{marking}')
        return False, sub_marking, None
    except ValueError as e:
      if verbose: print(f'Not enabled due to cannot make sub_marking: {e}')
      return False, None, None

    try: add_marking = self.create_marking_from_mode(self.element_out_arcs[transition], mode)
    except ValueError as e:
      if verbose: print(f'Not enabled due to cannot make add_marking: {e}')
      return False, sub_marking, None
    return True, sub_marking, add_marking

  def enabled_firings(self, marking, skip=None, verbose=False):
    skip = [] if skip is None else skip
    firings = []
    nr = 0
    for transition in self.transitions:
      enabled, sub_marking, add_marking = self.transition_enabled(transition, marking, verbose=verbose)
      nr += 1
      if enabled:
        firings.append([Firing(transition, sub_marking, add_marking)])
    return firings, nr

  def fire_transition(self, marking: Marking, transition: Transition, sub_marking: NuMarking, add_marking: NuMarking, mode: dict=None):
    new_marking = marking - sub_marking + add_marking
    firing = Firing(transition, sub_marking, add_marking)
    return new_marking, firing

  def view(self, initial_marking=None, final_marking=None, debug=False, name=''):
    m_i = initial_marking if initial_marking is not None else Marking()
    m_f = final_marking if final_marking is not None else Marking()

    font_size = 12
    viz = Digraph(name, engine='dot', graph_attr={'bgcolor': 'transparent', 'rankdir': 'LR', 'overlap': 'false'}, format='png')
    nodes = {}
    self._view_transitions(nodes, viz, font_size+6, debug)
    self._view_places(nodes, viz, m_i, m_f, font_size, debug)
    self._view_arcs(nodes, viz, font_size, debug)
    filename = tempfile.mktemp('.gv')
    viz.view(filename=filename, cleanup=True)
    # time.sleep(2)
    # os.remove(f'{filename}.{viz.format}')

  def _view_transitions(self, nodes, viz, font_size, debug):
    nodes.update({transition: transition.view(viz, font_size, debug) for transition in self.transitions})

  def _view_places(self, nodes, viz, m_i, m_f, font_size, debug):
    nodes.update({place: place.view(viz, m_i[place], m_f[place], font_size, debug) for place in self.places})

  def _view_arcs(self, nodes, viz, font_size, debug):
    [arc.view(nodes[arc.source], nodes[arc.target], viz, font_size, debug) for arc in self.arcs]
    [arc.view(nodes[arc.source], nodes[arc.target], viz, font_size, debug) for arc in self.inhibitor_arcs]

