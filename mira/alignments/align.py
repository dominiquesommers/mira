from mira.alignments.alignable_intervals import get_minimal_alignable_intervals
from mira.alignments.search import Search
from mira.alignments import utils
from mira.alignments.importer import import_individual_alignment_from_file
from mira.alignments.splitter import Splitter
from mira.alignments.trace_net import construct_trace_net
from mira.alignments.synchronous_product import construct_synchronous_product
from mira.rc_nu_net.nu_net import NuMarking, GeneralizedNuMarking
from mira.rc_nu_net import nu_net_utils
from mira.prelim.poset import POSet

from copy import deepcopy
from itertools import chain
import time
import tqdm
import numpy as np


def align(event_log, nu_net, m_i, m_f, parameters=None):
  t = time.time()
  mira_alignment = MiraAlignment(event_log, nu_net, m_i, m_f, parameters=parameters)
  alignment = mira_alignment.align()
  mira_alignment.info['t']['total'] = time.time() - t
  return alignment, mira_alignment.info


class MiraAlignment:
  def __init__(self, event_log, nu_net, m_m_i, m_m_f, parameters=None):
    self.event_log = event_log
    self.nu_net = nu_net
    self.m_m_i = m_m_i
    self.m_m_f = m_m_f
    self.parameters = {} if parameters is None else parameters
    self.info = {'t': {}}
    self.check()

  def check(self):
    if self.parameters.get('skip_easy_soundness_check', False) != False:
      # TODO Check easy soundness of the nu_net
      return ...

  def align(self):
    if self.parameters.get('approximation', False) != False:
      return self.align_approximation()

    trace_net, t_m_i, t_m_f = construct_trace_net(self.event_log)
    sync_net, s_m_i, s_m_f = construct_synchronous_product(trace_net, t_m_i, t_m_f, self.nu_net, self.m_m_i, self.m_m_f, utils.SKIP, relax=False)

    if self.parameters.get('visualize_sync_net', False):
      sync_net.view(s_m_i)

    searcher = Search(sync_net, s_m_i, s_m_f)
    search_result = searcher.compute_cheapest_run()
    self.info['search'] = search_result['stats']
    if 'error' in search_result:
      self.info['search_error'] = search_result['error']

    if 'error' in search_result:
      return None

    alignment, cost = self.__reconstruct_alignment(search_result['state'])
    self.info['cost'] = cost

    return self.__to_poset(alignment)

  def align_approximation(self):
    self.idr_capacities = {}
    self.idrs = set()
    for place, token_count in self.m_m_i.items():
      for (idc, idr), count in token_count.items():
        if idr != '_':
          self.idr_capacities[idr] = count
          self.idrs.add(idr)

    trace_parameters = deepcopy(self.parameters)
    del trace_parameters['approximation']
    trace_parameters['skip_easy_soundness_check'] = True

    t = time.time()
    if (individual_prefix := self.parameters.get('individual_prefix', None)) is None:
      traces = self.event_log.decompose_on_attribute('case_id')
      self.info['t']['decompose'] = time.time() - t
      t = time.time()
      alignments = {case_id: align(trace, self.nu_net, self.__isolate_case_marking(self.m_m_i, case_id), self.__isolate_case_marking(self.m_m_f, case_id), trace_parameters)
                    for case_id, trace in tqdm.tqdm(sorted(traces.items(), reverse=True), postfix=f'{"Aligning individual cases":>40}')}
      for al, _ in alignments.values():
        al.check_loops()
      self.info['individual'] = {case_id: info for case_id, (_, info) in alignments.items()}
    else:
      case_ids = set([event.get('case_id', None) for event in self.event_log])
      alignments, self.info['individual'] = {}, {}
      for case_id in case_ids:
        alignments[case_id] = import_individual_alignment_from_file(case_id, f'{individual_prefix}{case_id}.json', self.nu_net, self.event_log)
        self.info['individual'][case_id] = alignments[case_id][1]

    self.info['t']['individual_alignments'] = time.time() - t
    t = time.time()

    composed_alignment = self.__compose_alignments([al for al, _ in alignments.values()])
    # composed_alignment.view(element_str=utils.MOVE_STR, colors=utils.MOVE_COLOR)
    composed_alignment.check_loops()
    self.info['composed_alignment_cost'] = self.__compute_cost(composed_alignment)
    self.info['t']['compose'] = time.time() - t
    t = time.time()

    splitter = Splitter(composed_alignment, self.idr_capacities)

    splitters = splitter.split(self.parameters.get('split', 'inf'))
    splitter.export()

    self.info['t']['split'] = time.time() - t
    t = time.time()

    self.info['ilp'] = []
    extra_partial_order, intervals = [], []
    for i, splitter in enumerate(splitters):
      splitter_indices = [composed_alignment.indices[m] for m in splitter]
      pres_indices = set(np.where(np.logical_or.reduce(np.array(composed_alignment.r)[:, splitter_indices].T))[0])
      pres = [composed_alignment[i] for i in pres_indices - set(splitter_indices)]
      m_m_i = self.__cut_marking(self.m_m_i, pres)
      sub_extra_partial_order, sub_intervals, ilp_info = get_minimal_alignable_intervals(splitter, m_m_i, self.idr_capacities, name=f'{i+1}/{len(splitters)}')
      extra_partial_order.extend(sub_extra_partial_order)
      intervals.extend(sub_intervals)
      self.info['ilp'].append(ilp_info)
    self.info['t']['ilps'] = time.time() - t
    t = time.time()

    interval_elements = [el for interval in intervals for el in interval]

    extras = [(a, b) for a, b in extra_partial_order if a not in interval_elements and b not in interval_elements]
    for a, b in extras:
      composed_alignment.r[composed_alignment.indices[a],composed_alignment.indices[b]] = 1

    self.info['realign_intervals'] = []
    nu_net_utils.decorate_transitions_prepostset(self.nu_net)
    interval_alignments = []
    for index, interval2 in enumerate(tqdm.tqdm(intervals, postfix=f'{"Realigning violating intervals":>40}')):
      print(f'realigning {index+1}/{len(intervals)}')
      [print(move.short_str(18)) for move in interval2]
      interval = composed_alignment.projection(lambda x: x in interval2)
      interval_event_log = self.__extract_interval_event_log(interval)

      interval_indices = [composed_alignment.indices[move] for move in interval]
      pres_indices = set(np.where(np.logical_or.reduce(np.array(composed_alignment.r)[:, interval_indices].T))[0])
      pres = [composed_alignment[i] for i in pres_indices - set(interval_indices)]
      m_i = self.__cut_marking(self.m_m_i, pres)
      m_f = self.__cut_marking(m_i, interval)

      interval_alignment, realign_info = align(interval_event_log, self.nu_net, m_i, m_f, trace_parameters) # TODO implement relaxed alignment here.
      for move in interval_alignment:
        move.transition.reference.incoming_arc_labels = {arc.source: arc.labels for arc in move.transition.reference.in_arcs}
        move.transition.reference.outgoing_arc_labels = {arc.target: arc.labels for arc in move.transition.reference.out_arcs}

      self.info['realign_intervals'].append(realign_info)
      interval_alignments.append(interval_alignment)

    for index, (interval, interval_alignment) in enumerate(tqdm.tqdm(zip(intervals, interval_alignments), total=len(intervals), postfix=f'{"Inserting realigned intervals":>40}')):
      composed_alignment = self.__insert_interval_alignment(composed_alignment, interval, interval_alignment)
    self.info['t']['realign'] = time.time() - t

    self.info['cost'] = self.__compute_cost(composed_alignment)

    nu_net_utils.undecorate_transitions_prepostset(self.nu_net)

    return composed_alignment

  def __isolate_case_marking(self, m, case_id):
    m_isolated = deepcopy(m)
    for place, token_count in m.items():
      for token, count in token_count.items():
        if token[0] != '_' and token[0] != case_id:
          m_isolated[place][token] = 0
    return m_isolated

  def __cut_marking(self, m_start, sub_alignment) -> NuMarking:
    m = deepcopy(m_start).generalize()

    for move in sub_alignment:
      if not utils.is_log_move(move.transition):
        sub_marking_m, add_marking_m = GeneralizedNuMarking(), GeneralizedNuMarking()
        for sync_product_place, token_count in move.sub_marking.items():
          if sync_product_place.name[1] != '>>':  # consider only places from the process model
            sub_marking_m[sync_product_place.reference] = token_count
        for sync_product_place, token_count in move.add_marking.items():
          if sync_product_place.name[1] != '>>':  # consider only places from the process model
            add_marking_m[sync_product_place.reference] = token_count

        m = m - sub_marking_m + add_marking_m

    return m.degeneralize()

  def __reconstruct_alignment(self, state, return_type='firing'):
    def mode_string(mode):
      return ','.join([f'{k}:{mode[k]}' for k in sorted(mode.keys())])

    def element(firing):
      return {'name_label': (firing.transition.name, firing.transition.label, firing.mode),
              'label':      (*firing.transition.label, mode_string(firing.mode)),
              'transition': (firing.transition, firing.mode, firing.sub_marking),
              'firing':     firing}[return_type]

    alignment = list()
    if state.parent is not None and state.last_firing is not None:
      parent = state.parent
      alignment = [element(state.last_firing)]
      while parent.parent is not None:
        alignment = [element(parent.last_firing)] + alignment
        parent = parent.parent

    return alignment, state.costs['c1']

  def __to_poset(self, alignment):
    r = np.matrix(np.zeros((len(alignment), len(alignment)), dtype=int))
    for i, firing in enumerate(alignment):
      mode_keys = set(firing.mode.keys())
      incoming_places = set(firing.transition.incoming_arc_labels.keys())
      for j, other_firing in enumerate(alignment[:i]):
        other_outgoing_places = set(other_firing.transition.outgoing_arc_labels.keys())
        overlap_places = incoming_places.intersection(other_outgoing_places)
        if len(overlap_places) == 0:
          continue
        if any(overlap_place.name[0] != utils.SKIP for overlap_place in overlap_places) or \
          any(firing.mode[key] == other_firing.mode[key] for key in mode_keys.intersection(set(other_firing.mode.keys()))):
          r[j,i] = 1
    return POSet(alignment, r)

  def __compose_alignments(self, alignments):
    composed_alignment = POSet()
    for alignment in alignments:
      composed_alignment = composed_alignment.union(alignment)

    r = np.array(composed_alignment.r)
    event_log_r = np.array(self.event_log.r)
    # extend partial order with <_L
    events = {move.transition.event: move for move in composed_alignment if not utils.is_model_move(move.transition)}

    move_indices = [composed_alignment.indices[events[event]] for event in self.event_log]
    for event_index in tqdm.tqdm(self.event_log.traverse_yield(forward=False, return_indices=True), total=len(self.event_log), postfix=f'{"Extending partial order with <_L":>40}'):
      move_index = move_indices[event_index]
      child_indices = event_log_r[event_index].nonzero()[0]
      move_child_indices = [move_indices[i] for i in child_indices]
      grandchildren = np.where(np.logical_or.reduce(r[move_child_indices]))[0]
      move_child_indices = list(set(move_child_indices).union(grandchildren))
      r[np.ix_([move_index], move_child_indices)] = 1

    for move_index in tqdm.tqdm(composed_alignment.traverse_yield(forward=False, return_indices=True), total=len(composed_alignment), postfix=f'{"Extending partial order with <_L (2)":>40}'):
      move = composed_alignment[move_index]
      if not utils.is_model_move(move.transition):
        continue
      r[move_index] = np.logical_or.reduce(r[[move_index] + list(r[move_index].nonzero()[0])])

    composed_alignment.r = np.matrix(r)

    composed_alignment.check_loops()
    return composed_alignment

  def __extract_interval_event_log(self, interval) -> POSet:
    events = [move.transition.event for move in interval if not utils.is_model_move(move.transition)]
    return self.event_log.projection(lambda e: e in events)

  def __insert_interval_alignment(self, composed_alignment, interval, alignment):
    r = np.array(composed_alignment.r)
    interval_indices = [composed_alignment.indices[move] for move in interval]
    outside_interval_ones = np.ones(len(composed_alignment), dtype=int)
    outside_interval_ones[interval_indices] = 0

    before_interval = np.logical_and(np.logical_or.reduce(r[:,interval_indices].T), outside_interval_ones)
    before_interval = composed_alignment.projection(lambda el: before_interval[composed_alignment.indices[el]] == 1, is_already_closed=True)

    after_interval = [composed_alignment[i] for i in np.where(np.logical_and(np.logical_or.reduce(r[interval_indices]), outside_interval_ones))[0]]

    composed_alignment = composed_alignment.projection(lambda el: outside_interval_ones[composed_alignment.indices[el]] == 1, is_already_closed=True)
    composed_alignment = composed_alignment.union(alignment)

    r = np.array(composed_alignment.r)
    for i, left_move in enumerate(chain(alignment.traverse_yield(forward=False), before_interval.traverse_yield(forward=False))):
      relation_indices = []
      rel = r[composed_alignment.indices[left_move]]
      for right_move in (after_interval if i < len(alignment) else alignment):
        if self.__connected(left_move, right_move):
          relation_indices.append(composed_alignment.indices[right_move])
      rel[relation_indices] = 1
      composed_alignment.r[composed_alignment.indices[left_move]] = np.logical_or(rel, np.logical_or.reduce(r[relation_indices]))
    composed_alignment.check_loops()

    return composed_alignment

  def __connected(self, a, b):
    a_transition, b_transition = a.transition, b.transition
    if not (utils.is_model_move(a_transition) or utils.is_model_move(b_transition)):
      return True

    a_out = set([p.name for p in a_transition.reference.outgoing_arc_labels.keys()])
    b_in  = set([p.name for p in b_transition.reference.incoming_arc_labels.keys()])

    return len(a_out.intersection(b_in)) > 0 and \
           any(a.mode[key] == b.mode[key] for key in set(a.mode.keys()).intersection(set(b.mode.keys())))

  def __compute_cost(self, alignment):
    return sum([utils.STD_TAU_COST if utils.is_model_move(t.transition) and t.transition.label[1] is None else
                utils.STD_MODEL_LOG_MOVE_COST if utils.is_model_move(t.transition) or utils.is_log_move(t.transition) else
                utils.STD_SYNC_COST
                for t in alignment])


