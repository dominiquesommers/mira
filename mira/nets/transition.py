from mira.prelim.multiset import Multiset
from mira.nets.marking import Marking
from mira.prelim.colors import Color

from typing import Optional, Union


class Transition:
  def __init__(self, name: Union[float, str], label: Optional[Union[float, str]]=None, move_type='model', **parameters):
    self.name = name
    self.label = label
    self.move_type = move_type
    self.parameters = parameters

  def __repr__(self):
    if self.name == self.label:
      return f'{self.name}'
    return f'{self.name}({self.label})'

  def __str__(self):
    return self.__repr__()

  def __hash__(self):
    return hash(self.name) + hash(self.label)

  def __eq__(self, other):
    return type(self) == type(other) and self.name == other.name and self.label == other.label

  def minimum_marking(self):
    # TODO Figure out a way to not always recompute minimum_marking
    return

  def enabled(self, marking: Marking):
    return self.minimum_marking() <= marking

  def view(self, viz, font_size='16', debug=False):
    viz.attr('node', shape='box')
    transition_id = str(id(self))
    # label = self.name if (debug and self.label is None) else self.label
    label = self.name if debug else self.label
    if label is not None and '_' in label:
      main = label.split('_')[0]
      subscript = '_'.join(label.split('_')[1:]).split('^')[0]
      superscript = '^'.join(label.split('^')[1:])
      # label = f'{main}<sub>{subscript}</sub><sup>{superscript}</sup>'
      label = f'{main}<sub>{subscript}</sub>{superscript}'
    # TODO process superscript.

    silent = label is None
    label = '' if label is None and not debug else f'<{label}>'

    # TODO change color based on model/log/sync move.
    colors = {'synchronous': Color.GREEN, 'log': Color.YELLOW, 'model': Color.PURPLE}
    args = {'style': 'filled', 'penwidth': '2', 'fillcolor': Color.BLACK if silent else Color.WHITE, 'border': '1',
            'fontcolor': Color.WHITE if silent and not debug else Color.BLACK, 'fontsize': str(font_size),
            'color': colors[self.move_type]}
    viz.node(transition_id, label, **args)
    return transition_id


if __name__ == '__main__':
  t = Transition('t', 't', move_type='log')

  m = Marking({'p': 3})
  print(m)

