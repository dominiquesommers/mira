# from sage.combinat.combinat import CombinatorialClass
import sys

from copy import copy


class DAG:
  def __init__(self, elements, partial_order):
    self.nodes = elements
    self.partial_order = partial_order
    self.R = []

  def num_verts(self):
    return len(self.nodes)

  def min(self):
    minimial_elements = []
    for node in self.vertices():
      if len(self.incoming_edges(node)) == 0:
        minimial_elements.append(node)
    return minimial_elements

  def vertices(self):
    return self.nodes

  def incoming_edges(self, node):
    incoming_nodes = []
    for a, b in self.partial_order:
      if b == node:
        incoming_nodes.append(a)
    return incoming_nodes

  def delete_vertex(self, node):
    self.nodes.remove(node)
    remove_edges = [(a, b) for a, b in self.partial_order if a == node or b == node]
    for r_edge in remove_edges:
      self.partial_order.remove(r_edge)

  def __repr__(self):
    return str(self.nodes)

  def __copy__(self):
    return DAG(copy(self.nodes), copy(self.partial_order))


class LinearExtensions:
  def __init__(self, dag):
    from copy import copy
    dag_copy = copy(dag)

    le = []
    a = []
    b = []

    while dag_copy.num_verts() != 0:
      minimial_elements = dag_copy.min()
      if len(minimial_elements) == 1:
        le.append(minimial_elements[0])
        dag_copy.delete_vertex(minimial_elements[0])
      else:
        ap = minimial_elements[0]
        bp = minimial_elements[1]
        a.append(ap)
        b.append(bp)
        le.append(ap)
        le.append(bp)
        dag_copy.delete_vertex(ap)
        dag_copy.delete_vertex(bp)

    self.max_pair = len(a) - 1

    self.le = le
    self.a = a
    self.b = b
    self.dag = dag
    self.mrb = 0
    self.mra = 0
    self.is_plus = True
    self.linear_extensions = None
    self._name = f'Linear extensions of {dag}'

  def switch(self, i):
    if i == -1:
      self.is_plus = not self.is_plus
    if i >= 0:
      a_index = self.le.index(self.a[i])
      b_index = self.le.index(self.b[i])
      self.le[a_index] = self.b[i]
      self.le[b_index] = self.a[i]

      self.b[i], self.a[i] = self.a[i], self.b[i]

    if self.is_plus:
      self.linear_extensions.append(self.le[:])

  def move(self, element, direction):
    index = self.le.index(element)
    if direction == "right":
      self.le[index] = self.le[index + 1]
      self.le[index + 1] = element
    elif direction == "left":
      self.le[index] = self.le[index - 1]
      self.le[index - 1] = element
    else:
      print("Bad direction!")
      sys.exit()
    if self.is_plus:
      self.linear_extensions.append(self.le[:])

  def right(self, i, letter):
    if letter == "a":
      x = self.a[i]
      yindex = self.le.index(x) + 1
      if yindex >= len(self.le):
        return False
      y = self.le[yindex]
      if self.incomparable(x, y) and y != self.b[i]:
        return True
      return False
    elif letter == "b":
      x = self.b[i]
      yindex = self.le.index(x) + 1
      if yindex >= len(self.le):
        return False
      y = self.le[yindex]
      if self.incomparable(x, y):
        return True
      return False
    else:
      raise ValueError("Bad letter!")

  def generate_linear_extensions(self, i):
    if i >= 0:
      self.generate_linear_extensions(i - 1)
      mrb = 0
      typical = False
      while self.right(i, "b"):
        mrb += 1
        self.move(self.b[i], "right")
        self.generate_linear_extensions(i - 1)
        mra = 0
        if self.right(i, "a"):
          typical = True
          cont = True
          while cont:
            mra += 1
            self.move(self.a[i], "right")
            self.generate_linear_extensions(i - 1)
            cont = self.right(i, "a")
        if typical:
          self.switch(i - 1)
          self.generate_linear_extensions(i - 1)
          if mrb % 2 == 1:
            mla = mra - 1
          else:
            mla = mra + 1
          for x in range(mla):
            self.move(self.a[i], "left")
            self.generate_linear_extensions(i - 1)

      if typical and (mrb % 2 == 1):
        self.move(self.a[i], "left")
      else:
        self.switch(i - 1)
      self.generate_linear_extensions(i - 1)
      for x in range(mrb):
        self.move(self.b[i], "left")
        self.generate_linear_extensions(i - 1)

  def list(self):
    if self.linear_extensions is not None:
      return self.linear_extensions[:]

    self.linear_extensions = []
    self.linear_extensions.append(self.le[:])
    self.generate_linear_extensions(self.max_pair)
    self.switch(self.max_pair)
    self.generate_linear_extensions(self.max_pair)
    # self.linear_extensions.sort()
    return self.linear_extensions[:]

  def incomparable(self, x, y):
    return not self.dag.get_R_value(x, y) and not self.dag.get_R_value(y, x)
