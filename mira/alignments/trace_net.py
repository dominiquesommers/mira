from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.rc_nu_net import nu_net_utils
from mira.prelim.poset import POSet

from pm4py.util import xes_constants as xes_util


def construct_trace_net(event_log:POSet, activity_key:str=xes_util.DEFAULT_NAME_KEY) -> (NuPetriNet, NuMarking, NuMarking):
  nu_net = NuPetriNet()
  m_i, m_f = NuMarking(), NuMarking()

  transition_map = {}
  for index, event in enumerate(event_log):

    t = NuPetriNet.Transition(f'{event[activity_key]}_{event["case_id"]}_{index}', event[activity_key])
    t.event = event
    transition_map[event] = t
    nu_net.transitions.add(t)

    for resource_instance in event.get('resources', []):
      p = __add_place(nu_net, None, transition_map[event], ('_', resource_instance.rtype), postfix=resource_instance.rtype)
      p.properties['resource'] = resource_instance.name
      p.properties['resource_type'] = resource_instance.rtype
      m_i[p][('_', resource_instance.name)] += 1


  case_ids = list(set([event['case_id'] for event in event_log]))
  traces = event_log.decompose_on_attribute('case_id')
  for case_id, trace in traces.items():
    for i, j in trace.get_R_indices(trace.get_minimal_partial_order()):
      __add_place(nu_net, transition_map[trace[i]], transition_map[trace[j]], ('c', '_'))

    for event in trace.min():
      p = __add_place(nu_net, None, transition_map[event], ('c', '_'))
      m_i[p][(case_id, '_')] = 1
    for event in trace.max():
      p = __add_place(nu_net, transition_map[event], None, ('c', '_'))
      m_f[p][(case_id, '_')] = 1

  for i, j in event_log.get_R_indices(event_log.get_minimal_partial_order()):
    if event_log[i]['case_id'] != event_log[j]['case_id']:
      __add_place(nu_net, transition_map[event_log[i]], transition_map[event_log[j]], ('_', '_'))

  return nu_net, m_i, m_f


def __add_place(nu_net:NuPetriNet, transition1:[NuPetriNet.Transition,None], transition2:NuPetriNet.Transition,
                labels:(str, str), postfix:str='') -> NuPetriNet.Place:
  p = NuPetriNet.Place(f'p_{len(nu_net.places)}')
  nu_net.places.add(p)
  if transition1 is not None:
    nu_net_utils.add_arc_from_to(transition1, p, nu_net, labels=labels)
  if transition2 is not None:
    nu_net_utils.add_arc_from_to(p, transition2, nu_net, labels=labels)
  return p
