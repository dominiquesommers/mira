from mira.alignments.relaxations import Relaxations
from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.rc_nu_net.nu_net_utils import add_arc_from_to

from pm4py.objects.petri_net import properties


def construct_synchronous_product(pn1:NuPetriNet, im1:NuMarking, fm1:NuMarking,
                                  pn2:NuPetriNet, im2:NuMarking, fm2:NuMarking, skip:str, relax=False):
  sync_net = NuPetriNet(f'synchronous_product_net of {pn1.name} and {pn2.name}')
  t1_map, p1_map = __copy_into(pn1, sync_net, True, skip)
  t2_map, p2_map = __copy_into(pn2, sync_net, False, skip)

  for t1 in pn1.transitions:
    for t2 in pn2.transitions:
      if t1.label == t2.label:
        sync = NuPetriNet.Transition((t1.name, t2.name), (t1.label, t2.label))
        sync.event = t1.event
        sync.reference = t2
        sync_net.transitions.add(sync)
        # copy the properties of the transitions inside the transition of the sync net
        for p1 in t1.properties:
          sync.properties[p1] = t1.properties[p1]
        for p2 in t2.properties:
          sync.properties[p2] = t2.properties[p2]
        for a in t1.in_arcs:
          add_arc_from_to(p1_map[a.source], sync, sync_net, labels=a.labels)
        for a in t2.in_arcs:
          add_arc_from_to(p2_map[a.source], sync, sync_net, labels=a.labels)
        for a in t1.out_arcs:
          add_arc_from_to(sync, p1_map[a.target], sync_net, labels=a.labels)
        for a in t2.out_arcs:
          add_arc_from_to(sync, p2_map[a.target], sync_net, labels=a.labels)

  sync_im = NuMarking({**{p1_map[place]: token_count for place, token_count in im1.items()},
                       **{p2_map[place]: token_count for place, token_count in im2.items()}})
  sync_fm = NuMarking({**{p1_map[place]: token_count for place, token_count in fm1.items()},
                       **{p2_map[place]: token_count for place, token_count in fm2.items()}})

  sync_net.properties[properties.IS_SYNC_NET] = True

  if relax:
    relaxations = Relaxations(sync_net)
    relaxations.add_relaxations()

  return sync_net, sync_im, sync_fm


def construct_cost_aware(pn1:NuPetriNet, im1:NuMarking, fm1:NuMarking,
                         pn2:NuPetriNet, im2:NuMarking, fm2:NuMarking, skip:str, pn1_costs:dict, pn2_costs:dict, sync_costs:dict):
  sync_net = NuPetriNet('synchronous_product_net of %s and %s' % (pn1.name, pn2.name))
  t1_map, p1_map = __copy_into(pn1, sync_net, True, skip)
  t2_map, p2_map = __copy_into(pn2, sync_net, False, skip)
  costs = dict()

  for t1 in pn1.transitions:
    costs[t1_map[t1]] = pn1_costs[t1]
  for t2 in pn2.transitions:
    costs[t2_map[t2]] = pn2_costs[t2]

  for t1 in pn1.transitions:
    for t2 in pn2.transitions:
      if t1.label == t2.label:
        sync = NuPetriNet.Transition((t1.name, t2.name), (t1.label, t2.label))
        sync_net.transitions.add(sync)
        costs[sync] = sync_costs[(t1, t2)]
        # copy the properties of the transitions inside the transition of the sync net
        for p1 in t1.properties:
          sync.properties[p1] = t1.properties[p1]
        for p2 in t2.properties:
          sync.properties[p2] = t2.properties[p2]
        for a in t1.in_arcs:
          add_arc_from_to(p1_map[a.source], sync, sync_net, labels=a.labels)
        for a in t2.in_arcs:
          add_arc_from_to(p2_map[a.source], sync, sync_net, labels=a.labels)
        for a in t1.out_arcs:
          add_arc_from_to(sync, p1_map[a.target], sync_net, labels=a.labels)
        for a in t2.out_arcs:
          add_arc_from_to(sync, p2_map[a.target], sync_net, labels=a.labels)

  sync_im = NuMarking()
  sync_fm = NuMarking()
  for p in im1:
    # sync_im[p] = im1[p]
    sync_im[p1_map[p]] = im1[p]
  for p in im2:
    # sync_im[p] = im2[p]
    sync_im[p2_map[p]] = im2[p]
  for p in fm1:
    # sync_fm[p] = fm1[p]
    sync_fm[p1_map[p]] = fm1[p]
  for p in fm2:
    # sync_fm[p] = fm2[p]
    sync_fm[p2_map[p]] = fm2[p]

  sync_net.properties[properties.IS_SYNC_NET] = True

  return sync_net, sync_im, sync_fm, costs


def __copy_into(source_net:NuPetriNet, target_net:NuPetriNet, upper:bool, skip:str):
  t_map = {}
  p_map = {}
  for t in source_net.transitions:
    name = (t.name, skip) if upper else (skip, t.name)
    label = (t.label, skip) if upper else (skip, t.label)
    t_map[t] = NuPetriNet.Transition(name, label)
    t_map[t].reference = t
    try:
      t_map[t].event = t.event
    except AttributeError:
      pass
    if properties.TRACE_NET_TRANS_INDEX in t.properties:
      t_map[t].properties[properties.TRACE_NET_TRANS_INDEX] = t.properties[properties.TRACE_NET_TRANS_INDEX]
    target_net.transitions.add(t_map[t])

  for p in source_net.places:
    name = (p.name, skip) if upper else (skip, p.name)
    p_map[p] = NuPetriNet.Place(name)
    p_map[p].reference = p
    if properties.TRACE_NET_PLACE_INDEX in p.properties:
      p_map[p].properties[properties.TRACE_NET_PLACE_INDEX] = p.properties[properties.TRACE_NET_PLACE_INDEX]
    target_net.places.add(p_map[p])

  for t in source_net.transitions:
    for a in t.in_arcs:
      add_arc_from_to(p_map[a.source], t_map[t], target_net, labels=a.labels)
    for a in t.out_arcs:
      add_arc_from_to(t_map[t], p_map[a.target], target_net, labels=a.labels)

  return t_map, p_map
