from mira.prelim.colors import Color, ResourceColor, CaseColor

from copy import deepcopy


SKIP = '>>'
STD_MODEL_LOG_MOVE_COST = 10000
STD_TAU_COST = 1
STD_SYNC_COST = 0


def MOVE_STR(m):
  transition, mode = m.transition, m.mode
  activity = transition.label[0] if is_log_move(transition) else 'τ' if transition.label[1] is None else transition.label[1]
  if '_' in activity:
    activity = f'{activity.split("_")[0]}<SUB>{"_".join(activity.split("_")[1:])}</SUB>'

  color = lambda s, c: f'<FONT COLOR="{c}">{s}</FONT>'
  resource_string = ','.join([color(rtype, ResourceColor[rinstance])
                              for rtype, rinstance in mode.items() if rtype != 'c' and rinstance in ResourceColor])
  resource_string = f'<SUP>{resource_string}</SUP>' if resource_string != '' else ''

  return f'<{color(activity, CaseColor.get(mode.get("c", "_"), Color.BLACK))}{resource_string}>'

TRANSITION_COLOR = lambda t: Color.PURPLE if is_model_move(t) else Color.YELLOW if is_log_move(t) else Color.GREEN
MOVE_COLOR = lambda m: TRANSITION_COLOR(m.transition)


def is_model_move(t, skip:str=SKIP):
  if isinstance(t.label, tuple):
    return t.label[0] == skip and t.label[1] != skip
  return False


def is_log_move(t, skip:str=SKIP):
  if isinstance(t.label, tuple):
    return t.label[0] != skip and t.label[1] == skip
  return False


def create_model_markings(m_i, m_f, event_log):
  case_ids = sorted(list(set(event['case_id'] for event in event_log)))
  m_m_i, m_m_f = deepcopy(m_i), deepcopy(m_f)
  for token_count in m_m_i.values():
    if ('_', '_') in token_count:
      for case_id in case_ids:
        token_count[(case_id, '_')] += 1
  for token_count in m_m_f.values():
    if ('_', '_') in token_count:
      for case_id in case_ids:
        token_count[(case_id, '_')] += 1

  return m_m_i, m_m_f
