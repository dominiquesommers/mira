# from enum import Enum

class Color:
  BLACK      = '#000000'
  WHITE      = '#FFFFFF'
  ORANGE     = '#F47F4C'
  GREY       = '#B8B8B8' #'#505050'
  BLUE       = '#1C7AA2'
  LIGHT_BLUE = '#4CC1F4'
  PURPLE     = '#9378E3'
  PINK       = '#FC8EAC'
  YELLOW     = '#F3BA38'
  GREEN      = '#00944C'


CaseColor = {
  '_': Color.GREY,
  '1': Color.BLACK,
  '2': Color.ORANGE,
  '3': Color.BLUE,
  'c1': Color.BLACK,
  'c2': Color.ORANGE,
  'c3': Color.BLUE,
  '652823628': Color.BLACK,
  '1691306052': Color.ORANGE,
}

ResourceColor = {
  '_': Color.GREY,
  'r1': Color.BLUE,
  'r2': Color.LIGHT_BLUE,
  'r3': Color.GREEN,
  'd1': Color.BLUE,
  'd2': Color.LIGHT_BLUE,
  'a1': Color.PURPLE,
  'a2': Color.PINK,
  'g1': Color.BLUE,
  'g2': Color.LIGHT_BLUE,
  's1': Color.GREEN,
  'w1': Color.YELLOW,
  **{f'c{i}': Color.PURPLE for i in range(1, 8)},
  **{f'User_{i}': Color.LIGHT_BLUE for i in [1,10,100,101,102,103,104,105,106,107,108,109,11,110,111,112,113,114,115,116,117,118,119,12,120,121,122,123,124,125,126,127,128,129,13,130,131,132,133,134,135,136,137,139,14,140,141,142,145,146,15,16,17,18,19,2,20,21,22,23,24,25,26,27,28,29,3,30,31,32,33,34,35,36,37,38,39,4,40,41,42,43,44,45,46,47,48,49,5,50,51,52,53,54,55,56,57,58,59,6,60,61,62,63,64,65,66,67,68,69,7,70,71,72,73,74,75,76,77,78,79,8,80,81,82,83,84,85,86,87,88,89,9,90,91,92,93,94,95,96,97,98,99]},
  **{f'User_{i}': Color.GREEN for i in [138,143,144]},
  **{f'User_{i}': Color.PURPLE for i in [11,43]}
}

ResourceTypeColor = {
  'r': Color.BLUE,
  'd': Color.BLUE,
  'a': Color.PURPLE,
  'g': Color.BLUE,
  's': Color.GREEN,
  'w': Color.YELLOW,

  'r1': Color.BLUE,
  'r2': Color.LIGHT_BLUE,
  'r3': Color.GREEN,
  **{f'c{i}': Color.PURPLE for i in range(1, 8)},

  'clerk': Color.LIGHT_BLUE,
  'fraud analyst': Color.GREEN,
  'manager': Color.PURPLE
}
