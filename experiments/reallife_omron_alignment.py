from mira.nets.system import System
from mira.nets.transition import Transition
from mira.nets.place import Place
from mira.nets.firing import MultiNuFiring
from mira.nets.marking import MultiNuMarking
from mira.nets.token import ColorToken, MultiColorToken
from mira.logs.event_log import Log
from mira.logs.event import Event
from mira.alignment.align import align
from mira.alignment.cost import Cost
from mira.prelim.colors import Color
from mira.prelim.poset import POSet
from mira.prelim.multiset import Multiset

import json
import os
import numpy as np
from copy import deepcopy
from collections import defaultdict
from mira.alignments.exporter import NoIndent, MyEncoder


class Stages:
  operators = {
    'o1': ['CBP-WPPc', 'CBP-FS', 'CBP-AP', 'CBP-Lf1', 'CBP-STL', 'CBP-WSFs', '453',
           'P-WSc', 'P-AP', 'P-Lt2', 'P-Lt1', 'P-STL', 'P-WSFs'],
    'o2': ['CB-WSc', 'CB-AP', 'CB-STL', 'CB-WCSs', 'CB-WCSc', 'CB-WSFs',
           'FC-WPPc', 'FC-AP', 'FC-DUT', 'FC-WSFs'],
    'o3': ['IA-WSc', 'IA-AP', 'IA-STL', 'IA-WCSs', 'IA-WCSc', 'IA-WSFs', '226',
           'OFC-WPPc', 'OFC-AP', 'OFC-DUT', 'OFC-WSFs', 'OFC-WSFc',
           'OA-WSc', 'OA-AP', 'OA-Lf1', 'OA-STL', 'OA-WSFs']
  }
  capacities = {
    'c1': ['CBP-WPPs', 'CBP-WPPc', 'CBP-FS', 'CBP-AP', 'CBP-Lf1', 'CBP-STL', 'CBP-WSFs', 'CBP-WSFc', '453'],
    'c2': ['CB-WSs', 'CB-WSc', 'CB-AP', 'CB-STL', 'CB-WCSs', 'CB-WCSc', 'CB-WSFs', 'CB-WSFc'],
    'c3': ['IA-WSs', 'IA-WSc', 'IA-AP', 'IA-STL', 'IA-WCSs', 'IA-WCSc', 'IA-WSFs', '226', 'IA-WSFc'],
    'c4': ['OFC-WPPs', 'OFC-WPPc', 'OFC-AP', 'OFC-DUT', 'OFC-WSFs', 'OFC-WSFc', 'OFC-GT'],
    'c5': ['OA-WSs', 'OA-WSc', 'OA-AP', 'OA-Lf1', 'OA-STL', 'OA-WSFs', 'OA-WSFc'],
    'c6': ['FC-WPPs', 'FC-WPPc', 'FC-AP', 'FC-DUT', 'FC-WSFs', 'FC-WSFc', 'FC-GT'],
    'c7': ['P-WSs', 'P-WSc', 'P-AP', 'P-Lt2', 'P-Lt1', 'P-STL', 'P-WSFs', 'P-WSFc']
  }
  operators_by_activity = {activity: operator for operator, activities in operators.items() for activity in activities}
  capacities_by_activity = {activity: capacity for capacity, activities in capacities.items() for activity in activities}

  @staticmethod
  def operator(activity):
    return Stages.operators_by_activity.get(activity, None)

  @staticmethod
  def capacity(activity):
    return Stages.capacities_by_activity.get(activity, None)


def view_alignment(alignment):
  TRANSITION_COLOR = lambda m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN
  # alignment = alignment.projection(lambda x: not (x.transition.move_type == 'log' and x.transition.label is None))
  marking_string = lambda x: '' if (str(x.sub_marking) == '[]' and str(x.add_marking) == '[]') else f'\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
  move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label}\n[{", ".join(x.mode.values())}]{marking_string(x)}'


  fill_color = lambda x: 'gray' if x.transition.label is None else 'white'
  alignment.view(element_str=move_str, colors=TRANSITION_COLOR, fill_colors=fill_color)


def run_all(visualize=None, relax=False, verbose=False):
  visualize = [] if visualize is None else visualize
  log_indices = [int(filename.split('_')[1].split('.')[0])
                 for filename in os.listdir('datasets/logs/omron') if filename.startswith('omron_')]
  for log_index in sorted(log_indices):
    print(f'\nOmron {log_index}.')
    # TODO log_index=8, 9, 14.2, 14.3, 22, 25, 31.2, 32, 33, 38, 39.3, 42, 43.3, 43.4, 44
    if log_index <= 44:
      continue
    run(log_index, relax=relax, visualize=visualize, verbose=verbose)


def importeer(data, system):
  mode_fixer = {'c': 'p1'}
  info = data['info']
  moves = []
  x = None
  for (t_name, t_label), mode, sub_marking, add_marking in data['M'][:x]:
    move_type = 'log' if t_name[1] == '>>' else 'model' if t_name[0] == '>>' else 'synchronous'
    transition = Transition(t_name[0 if move_type == 'log' else 1], t_label[0 if move_type == 'log' else 1])
    transition.move_type = move_type
    transition.vars = {'x': ['x']}

    mode = {key: mode_fixer.get(key, key) for key in mode} #__parse_mode(mode, transition, sub_marking)

    places = {place: Place(place) for place in list(sub_marking.keys()) + list(add_marking.keys())}
    sub_marking = MultiNuMarking(args={
      places[place]: MultiNuMarking.MultiColorTokenMultiset(
        args={MultiColorToken(multisets=[Multiset(key_type=ColorToken, args={ColorToken(v[1:] if len(v) > 2 else v): 1}) for v in token.split(',') if v != '_']): n for token, n in tokens.items()})
      for place, tokens in sub_marking.items()
    })

    activity = t_name[1] if move_type != 'log' else t_name[0].split('_')[0]
    objects = set()
    for place_tokens in sub_marking.values():
      for tokens in place_tokens.keys():
        for token in tokens:
          o = list(token.keys())[0].id
          if o[0] != 'r':
            objects.add(o)
    objects.update({Stages.operator(activity), Stages.capacity(activity)})
    # objects = {list(token.keys())[0].id for place_tokens in sub_marking.values() for tokens in place_tokens.keys() for token in tokens}.union({Stages.operator(activity), Stages.capacity(activity)})
    objects.discard(None)
    # print(activity, objects, 's', Stages.capacity(activity))

    add_marking = MultiNuMarking(args={
      places[place]: MultiNuMarking.MultiColorTokenMultiset(
        args={MultiColorToken(multisets=[Multiset(key_type=ColorToken, args={ColorToken(v): 1}) for v in token.split(',') if v != '_']): n for token, n in tokens.items()})
      for place, tokens in add_marking.items()
    })
    firing = MultiNuFiring(transition, mode, sub_marking, add_marking)
    firing.objects = objects
    firing.event = None if move_type == 'model' else Event(t_label[0], Multiset(args={object: 1 for object in objects}, key_type=str))
    moves.append(firing)

  prec = np.matrix(data['r'])[:x,:x]
  alignment = POSet(moves, prec)
  return alignment, info


def __cut_marking(system, m_start, sub_alignment):
  m = deepcopy(m_start)

  for move in sub_alignment:
    if move.transition.move_type == 'log':
      continue

    # Create firing for the model based on move.transition and move.mode
    variable = lambda o: o[0] if len(o) == 2 else 'p'
    mode = {variable(object): ColorToken(object) for object in move.objects if variable(object) in ['c', 'p', 'o']}
    # if '400016703_0012' in move.objects:
    #   print(move.transition, mode)
    sub_marking = system.net.create_marking_from_mode(system.net.element_in_arcs[move.transition], mode)
    add_marking = system.net.create_marking_from_mode(system.net.element_out_arcs[move.transition], mode)
    m = m - sub_marking + add_marking

  return m

def run(log_index=1, relax=False, visualize=None, verbose=False):
  visualize = [] if visualize is None else visualize

  with open(f'./datasets/logs/omron/omron_{log_index}.json', 'r') as json_file:
    data = json.load(json_file)
    products = set()
    events = []
    for activity, product, resources in data['events']:
      events.append([activity, f'[{", ".join([product, *resources, *[x for x in [Stages.operator(activity), Stages.capacity(activity)] if x is not None]])}]'])
      products.add(product)
    data['events'] = events
    data['O'] = {'P': list(products), 'O': ['o1', 'o2', 'o3'], 'C':  ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7']}
    log = Log.from_json_dict(data)
    if 'log' in visualize:
      log.view()

  net_filename = f'./datasets/nets/omron{"_relaxed" if relax else ""}.json'
  with open(net_filename, 'r') as json_file:
    data = json.load(json_file)
    data['m_i'] = f'{data["m_i"][:-1]}, [{",".join("([" + p + "])" for p in log.object_types.get("P", set()))}]·i{data["m_i"][-1]}'
    data['m_f'] = f'{data["m_f"][:-1]}, [{",".join("([" + p + "])" for p in log.object_types.get("P", set()))}]·f{data["m_f"][-1]}'

    system = System.from_json_dict(data, silent=lambda x: x.isnumeric(), relaxed=relax)
    if len(log.object_types.keys()) > 0:
      system.net.object_types = log.object_types
    if 'net' in visualize:
      system.net.transitions = {t for t in system.transitions if t.name.startswith('IA-WSc')}
      places = [a.source for t in system.transitions for a in system.net.element_in_arcs[t]] + [a.target for t in system.transitions for a in system.net.element_out_arcs[t]]
      system.net.places = set(places)
      system.net.arcs = {a for a in system.arcs if a.place in system.places and a.transition in system.transitions}
      system.view(debug=True)
      exit()

  if relax:
    n_overflow = 2
    with open(f'./examples/alignments/omron/omron_{log_index}.json', 'r') as json_file:
      data = json.load(json_file)
      al, info = importeer(data, system)
      # view_alignment(al)
      suppers = POSet()
      has_devs = False
      for f in al:
        if Cost.cost(f.transition) <= Cost.epsilon2:
          continue
        has_devs = True
        from_a = prev_antichain(al, [f], n_overflow)
        right = al.postfixopen(from_a) if len(from_a) > 0 else al
        to_a = next_antichain(al, [f], n_overflow)
        left = al.prefixopen(to_a) if len(to_a) > 0 else al
        suppers = suppers.union(right.intersection(left))
      print(f'{has_devs=}')
      # view_alignment(suppers)

      suppers = suppers.detach_unconnected_subposets()
      print(f'{len(suppers)=}')
      for index, supper in enumerate(suppers):
        # view_alignment(supper)
        # exit()
        object_types = defaultdict(set)
        for move in supper:
          for object in move.objects:
            object_types[object[0].upper() if len(object) == 2 else 'P'].add(object)
        recorded_objects = set.union(*[objs for objs in object_types.values()])

        m_i_original, m_f_original = deepcopy(system.m_i), deepcopy(system.m_f)
        m_i = __cut_marking(system, system.m_i, al.prefixopen(supper.min())) #.projection(recorded_objects, object_types)
        m_f = __cut_marking(system, system.m_i, al.prefix(supper.max())) #.projection(recorded_objects, object_types)
        for place in list(m_i.keys()) + list(m_f.keys()):
          place.alpha = next(p for p in system.places if p == place).alpha
        m_i = m_i.projection(recorded_objects, object_types)
        m_f = m_f.projection(recorded_objects, object_types)
        print(f'{m_i == m_f=}')


        log_supper = supper.projection(lambda f: f.event is not None)
        sublog = Log([f.event for f in log_supper], log_supper.r, is_already_closed=True, object_types=dict(object_types))
        print(f'Sublog contains {len(sublog)} events.')
        system.m_i, system.m_f = m_i, m_f
        system.marking = m_i
        system.net.object_types = log.object_types
        # sublog.view()
        # exit()

        cost = lambda transition: Cost.deviation*100 if transition.name in ['309', '312', '315', '317', '318', '319', '320'] else Cost.cost_relaxed(transition)
        run, info = align(sublog, system, {'relax': relax, 'cost': cost,
                                           'visualize_sync_net': ('syncnet' in visualize) or ('sync' in visualize),
                                           'visualize_log_net': 'lognet' in visualize,
                                           'max_search_time': np.inf, 'min_memory_available': 1})
        run = POSet() if run is None else run
        alignment_json = run.to_json()
        alignment_json['M'] = [NoIndent(move) for move in alignment_json['M']]
        alignment_json['r'] = [NoIndent(row) for row in alignment_json['r']]

        data = {'N': net_filename, 'L': log_index, 'info': info, 'exact': True, 'relaxed': True,
                'n_overflow': n_overflow, 'm_i': str(system.m_i), 'm_f': str(system.m_f), **alignment_json}

        with open(f'./examples/relaxed_alignments/omron/omron_{log_index}_n{n_overflow}_i{index}_{index+1}_of_{len(suppers)}.json', 'w') as json_file:
          json.dump(data, json_file, cls=MyEncoder, sort_keys=False, indent=2)

        if len(run) > 0:
          print(f'Cost: {info["cost"]:_}')
          if ('alignment' in visualize) or ('run' in visualize):
            view_alignment(run)
        system.m_i, system.m_f = m_i_original, m_f_original
        # print(fdsafdsa)
      return

  run, info = align(log, system, {'relax': relax, 'cost': Cost.cost_relaxed if relax else Cost.cost,
                                  'visualize_sync_net': ('syncnet' in visualize) or ('sync' in visualize),
                                  'visualize_log_net': 'lognet' in visualize})
  print(f'Cost: {info["cost"]:_}')
  if ('alignment' in visualize) or ('run' in visualize):
    view_alignment(run)


def prev_antichain(poset, antichain, n):
  if n < 0:
    return antichain
  return prev_antichain(poset, poset.prefixopen(antichain).max(), n-1)

def next_antichain(poset, antichain, n):
  if n < 0:
    return antichain
  return next_antichain(poset, poset.postfixopen(antichain).min(), n-1)

if __name__ == '__main__':
  import argparse
  #
  # poset = POSet(['a', 'b', 'c', 'd', 'e'], np.matrix([[0,1,1,1,1], [0,0,1,1,0], [0,0,0,1,0], [0,0,0,0,0], [0,0,1,1,0]]))
  # poset = POSet(['a', 'b', 'c', 'd', 'e'], np.matrix([[0,1,1,1,1], [0,0,1,1,1], [0,0,0,1,1], [0,0,0,0,1], [0,0,0,0,0]]))
  # # poset.view()
  # suppers = POSet()
  # for el in ['b', 'c']:
  #   print(el)
  #   n = 2
  #   from_a = prev_antichain(poset, [el], n)
  #   right = poset.postfixopen(from_a) if len(from_a) > 0 else poset
  #   to_a = next_antichain(poset, [el], n)
  #   left = poset.prefixopen(to_a) if len(to_a) > 0 else poset
  #   suppers = suppers.union(right.intersection(left))
  # suppers.view()
  #
  # suppers = suppers.detach_unconnected_subposets()
  #
  # el = poset[2]
  # print(el)
  #
  # print()
  #
  # # poset.prefixopen(el).view()
  # exit()

  if False:
    all_moves = []
    all_prec = None
    for filename in os.listdir(f'./examples/relaxed_alignments/omron/'):
      print(filename)
      with open(f'./examples/relaxed_alignments/omron/{filename}', 'r') as json_file:
        data = json.load(json_file)
        moves = []
        for t_name, t_label, move_type, mode, sub_marking, add_marking in data['M']:
          transition = Transition(t_name, t_label)
          transition.move_type = move_type
          firing = MultiNuFiring(transition, mode, MultiNuMarking.from_str(''), MultiNuMarking.from_str(''))  # TODO parse sub/add markings
          firing.objects = set(mode.values())
          firing.event = None if move_type == 'model' else Event(t_label, Multiset(args={object: 1 for object in firing.objects}, key_type=str))
          moves.append(firing)
        prec = np.matrix(data['r'])
        if len(moves) <= 40 and any('↾' in move.transition.label for move in moves if move.transition.label is not None):
          all_moves.extend(moves)
          all_prec = prec if all_prec is None else np.matrix(np.asarray(np.bmat([[all_prec, np.zeros((all_prec.shape[0], prec.shape[1]))], [np.zeros((prec.shape[0], all_prec.shape[1])), prec]])))
        print(all_prec)
        alignment = POSet(moves, prec)
        info = data['info']

      # view_alignment(alignment.projection(lambda f: not (f.transition.label is None and f.transition.move_type == 'log')))
      # print(info)
    alignment = POSet(all_moves, all_prec)
    view_alignment(alignment.projection(lambda f: not (f.transition.label is None and f.transition.move_type == 'log')))
    exit()


  parser = argparse.ArgumentParser()
  parser.add_argument('-i', '--log_index', help='', type=int)
  parser.add_argument('-r', '--relax', help='Relaxed alignment.', action='store_true')
  parser.add_argument('-vis', '--visualize', help='Comma separated visualization (log,net,sync/syncnet,alignment/run)', type=str)
  parser.add_argument('-v', '--verbose', help='Verbose.', action='store_true')
  args = parser.parse_args()

  args.visualize = [] if args.visualize is None else args.visualize.split(',')

  vargs = {k: v for k, v in vars(args).items() if v is not None}
  if args.log_index is None:
    run_all(**vargs)
  else:
    run(**vargs)
