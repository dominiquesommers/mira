from mira.nets.system import System

import json


def main():
  with open('./tests/nets/delivery_extended.json', 'r') as json_file:
    data = json.load(json_file)
    system = None
    for net_data in data:
      system_ = System.from_json_dict(net_data)
      # system_.view()
      if system is None:
        system = system_
      else:
        system = system.union(system_)
    system.view(debug=True)


if __name__ == '__main__':
  main()
