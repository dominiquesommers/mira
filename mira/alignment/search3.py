from mira.prelim.poset import POSet
from mira.nets.system import System
from mira.nets.marking import Marking, MultiNuMarking
from mira.nets.firing import Firing
from mira.nets.place import Place

from pm4py.util.lp import solver as lp_solver
from bidict import bidict
from collections import defaultdict
from cvxopt import matrix
from typing import Union, List
import numpy as np
import heapq
import time
import sys
import psutil
from datetime import datetime
from typing import Callable


class State:
  def __init__(self, markings:frozenset, costs:dict, last_firing:Union[Firing,None], T_vec:Union[List[float],None],
               trustable:bool, parent, enabled_firings:List[Firing]):
    self.markings = markings
    self.costs = {'c': costs['c1'] + costs['c2'], **costs} # m_i -c1-> m -c2-> m_f
    self.last_firing = last_firing
    self.T_vec = T_vec
    self.trustable = trustable
    self.parent = parent
    self.enabled_firings = enabled_firings

  def __lt__(self, other):
    # TODO prefer unpack transitions, (pack transitions), unprefer non-unpack void transitions.
    if self.costs['c'] != other.costs['c']:
      return self.costs['c'] < other.costs['c']
    if all(t for t in self.trustable.values()) and not all(t for t in other.trustable.values()):
      return True
    return self.costs['c2'] < other.costs['c2']

  def __get_firing_sequence(self):
    sequence = []
    if self.parent is not None:
      sequence.extend(self.parent.__get_firing_sequence())
    if self.last_firing is not None:
      sequence.append((self.last_firing.transition, self.last_firing.mode))
    return sequence

  def get_possible_next_transitions(self):
    # TODO implement
    ...
  #   if self.last_firing is None:
  #     return list({p_out_arc.target for p in self.marking for p_out_arc in p.out_arcs})
  #   return list({p_out_arc.target for t_out_arc in self.last_firing.transition.out_arcs for p_out_arc in t_out_arc.target.out_arcs})

  def update_cost_to_m_f(self, cost_to_m_f):
    self.costs['c'] = self.costs['c'] - self.costs['c2'] + cost_to_m_f
    self.costs['c2'] = cost_to_m_f

  def __repr__(self):
    return f'STATE\n{self.markings}\nm_i -{self.costs["c1"]}-> m -{self.costs["c2"]}-> m_f ({"" if all(t for t in self.trustable.values()) else "not "}trustable)\n{self.last_firing}\n' + \
           f'prev firing: {self.parent.last_firing if self.parent is not None else "None"}\n'

  def __hash__(self):
    return hash(self.markings)

  def __eq__(self, other):
    return self.markings == other.markings

class PrioritySet(object):
  def __init__(self):
    self.heap = []
    heapq.heapify(self.heap)
    self.set_ = set()

  def __len__(self):
    return len(self.heap)

  def add(self, d):
    # heapq.heappush(self.heap, d)
    if not d in self.set_:
      heapq.heappush(self.heap, d)
      self.set_.add(d)

  def pop(self):
    d = heapq.heappop(self.heap)
    self.set_.remove(d)
    return d

class Search:
  def __init__(self, system:System, costs:Callable=None, variant='a_star'):
    self.system = system
    self.black_nets = {}
    for role, role_objects in system.net.object_types.items():
      self.black_nets[role] = self.system.net.projection(role, self.system.net.object_types, self.system.net.variable_types).blacken()
    # self.black_system = self.system.blacken()
    self.costs = costs
    self.__vectorize()

  def __vectorize(self):
    self.incidence_matrices = {role: black_net.get_incidence_matrix() for role, black_net in self.black_nets.items()}

    for place in list(self.system.m_i.keys()) + list(self.system.m_f.keys()):
      # for place_ in self.system.places:
      #   if place_ == place:
      place.alpha = next(p for p in self.system.places if p == place).alpha

    self.__m_i_vecs = {o: self.system.m_i.projection({o}, self.system.net.object_types).blacken().vectorize(self.black_nets[role].places)
                       for role, role_objects in self.system.net.object_types.items() for o in role_objects}
    self.__m_f_vecs = {o: self.system.m_f.projection({o}, self.system.net.object_types).blacken().vectorize(self.black_nets[role].places)
                       for role, role_objects in self.system.net.object_types.items() for o in role_objects}
    self.__cost_vecs = {role: matrix([self.costs(transition) * 1.0 for transition in black_net.transitions]) for role, black_net in self.black_nets.items()}

    self.__a_matrices = {role: matrix(np.asmatrix(incidence_matrix).astype(np.float64)) for role, incidence_matrix in self.incidence_matrices.items()}
    self.__g_matrices = {role: matrix(-np.eye(len(black_net.transitions))) for role, black_net in self.black_nets.items()}
    self.__h_cvxes = {role: matrix(np.matrix(np.zeros(len(black_net.transitions))).transpose()) for role, black_net in self.black_nets.items()}

    self.__use_cvxopt = lp_solver.DEFAULT_LP_SOLVER_VARIANT in [lp_solver.CVXOPT_SOLVER_CUSTOM_ALIGN, lp_solver.CVXOPT_SOLVER_CUSTOM_ALIGN_ILP]

  def check(self):
    # TODO check for easy soundness.
    ...

  def compute_cheapest_run(self, max_search_time:float=np.inf, min_memory_available:float=0):
    self.start_time = time.time()
    print(f'Starting at {datetime.now()} search 3.')
    self.max_search_time = max_search_time
    self.min_memory_available = min_memory_available
    self.process = psutil.Process()
    closed_markings = set()

    cost_to_m_f = 0
    T_vecs = {}
    trustables = {}
    for role, role_objects in self.system.net.object_types.items():
      for o in role_objects:
        o_cost_to_m_f, T_vec = self.__compute_exact_heuristic_new_version(role, o, self.system.m_i.projection({o}, self.system.net.object_types).blacken())
        cost_to_m_f += o_cost_to_m_f
        T_vecs[o] = T_vec
        trustables[o] = True

    print(f'aligning from\n{self.system.m_i}\nto\n{self.system.m_f}')

    for place in self.system.m_i.keys():
      place.alpha = self.system.net.alpha(place, self.system.net.variable_types)

    self.submarkings = bidict(enumerate(self.system.m_i.split_tokens()))

    open_set = PrioritySet()
    open_set.add(State(frozenset(self.submarkings.keys()), {'c1': 0, 'c2': cost_to_m_f}, last_firing=None, T_vec=T_vecs, trustable=trustables, parent=None, enabled_firings=[]))

    self.search_stats = {'visited': 0, 'queued': 0, 'traversed': 0, 'lp_solved': 1, 'enabled_check': 0}
    while len(open_set) > 0:
      print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({(time.time() - self.start_time)/60:.2f}) min ({self.process.memory_info().rss / 1024**3:.2f}GB)', end='\r')
      current_state = open_set.pop()
      current_state, break_reason = self.__get_trustable(current_state, closed_markings, open_set)
      if break_reason is not None:
        print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({(time.time() - self.start_time) / 60:.2f}min, {self.process.memory_info().rss / 1024 ** 3:.2f}GB)')
        return self.get_run(current_state), {'error': break_reason, 'cost': None, 'stats': {**self.search_stats, 'duration': time.time() - self.start_time}}

      if current_state.costs['c2'] > lp_solver.MAX_ALLOWED_HEURISTICS:
        print(f'continue because of max allowed heuristics {current_state.costs["c2"]}.')
        continue
      if current_state.markings in closed_markings:
        continue

      self.system.marking = sum([self.submarkings[i] for i in current_state.markings], start=MultiNuMarking())
      if current_state.costs['c2'] < 0.01:
        if self.system.marking == self.system.m_f:
          print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({(time.time() - self.start_time) / 60:.2f}min, {self.process.memory_info().rss / 1024 ** 3:.2f}GB)')
          print('found it')
          return self.get_run(current_state), {'cost': current_state.costs['c1'], **self.search_stats, 'duration': time.time() - self.start_time}

      closed_markings.add(current_state.markings)
      self.search_stats['visited'] += 1

      parent_enabled_firings = None if current_state.parent is None else current_state.parent.enabled_firings
      enabled_firings, nr = self.system.enabled_firings(parent_enabled_firings=parent_enabled_firings, last_firing=current_state.last_firing)

      self.search_stats['enabled_check'] += nr

      current_state.enabled_firings = enabled_firings

      a = time.time()
      for enabled_firing in enabled_firings:
        self.search_stats['traversed'] += 1
        new_marking = enabled_firing.new_marking(self.system.marking)
        if new_marking in closed_markings:
          continue

        T_vecs = {}
        trustables = {}
        for role, role_objects in self.system.net.object_types.items():
          for obj in role_objects:
            cost_to_m_f, T_vec = self.__derive_heuristic(role, current_state.T_vec[obj],  enabled_firing.transition, current_state.costs['c2'])
            T_vecs[obj] = T_vec
            trustables[obj] = self.__trust_solution(T_vec)

        new_submarkings = set()
        for submarking in new_marking.split_tokens():
          if (index := self.submarkings.inverse.get(submarking, None)) is None:
            index = len(self.submarkings.keys())
            self.submarkings[index] = submarking
          new_submarkings.add(index)

        new_state = State(frozenset(new_submarkings), {'c1': current_state.costs['c1'] + self.costs(enabled_firing.transition), 'c2': cost_to_m_f},
                          enabled_firing, T_vecs, trustables, current_state, enabled_firings=[])

        open_set.add(new_state)
        self.search_stats['queued'] += 1

    print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({(time.time() - self.start_time) / 60:.2f}min, {self.process.memory_info().rss / 1024 ** 3:.2f}GB)')
    raise ValueError('Alignment failed due to unknown reason.')

  def get_run(self, state):
    if state is None:
      return None, None
    self.system.marking = self.system.m_i
    if state.last_firing is None:
      return POSet()
    reversed_firings = [(state.last_firing, state.markings)]
    if state.parent is not None and state.last_firing is not None:
      parent = state.parent
      while parent.parent is not None:
        reversed_firings.append((parent.last_firing, parent.markings))
        parent = parent.parent
    for firing, new_marking in reversed(reversed_firings):
      self.system.fire(firing, new_marking)
    return self.system.firings

  def __get_trustable(self, state, closed_markings, open_set):
    while not all(t for t in state.trustable.values()):
      if (break_reason := self.__check_break()) is not None:
        return state, break_reason

      if state.markings in closed_markings:
        state = open_set.pop()
        continue

      m = sum([self.submarkings[i] for i in state.markings], start=MultiNuMarking())
      cost_to_m_f = 0
      for role, role_objects in self.system.net.object_types.items():
        for o in role_objects:
          cost_to_m_f_, T_vec = self.__compute_exact_heuristic_new_version(role, o, m.projection({o}, self.system.net.object_types).blacken())
          cost_to_m_f += cost_to_m_f_
          state.trustable[o] = True

      self.search_stats['lp_solved'] += 1
      state.update_cost_to_m_f(cost_to_m_f)

      open_set.add(state)
      state = open_set.pop()
    return state, None

  def __check_break(self):
    if (seconds := time.time() - self.start_time) >= self.max_search_time:
      return f'Out of time, took {seconds:.2f} seconds.'
    if psutil.virtual_memory().available / 1024**3 < self.min_memory_available:
      return f'Out of memory (min: {self.min_memory_available}GB), used {self.process.memory_info().rss / 1024**3:.2f}GB of memory.'
    return None

  def __check_still_enabled(self, marking, firing):
    return firing.sub_marking <= marking

  def __compute_exact_heuristic_new_version(self, role, o, marking:Marking, strict:bool=True):
    m_vec = marking.vectorize(self.black_nets[role].places)
    b_term = [i - j for i, j in zip(self.__m_f_vecs[o], m_vec)]
    b_term = np.matrix([x * 1.0 for x in b_term]).transpose()

    if not strict:
      self.__g_matrices[role] = np.vstack([self.__g_matrices[role], self.__a_matrices[role]])
      self.__h_cvxes[role] = np.vstack([self.__h_cvxes[role], b_term])
      self.__a_matrices[role] = np.zeros((0, self.__a_matrices[role].shape[1]))
      b_term = np.zeros((0, b_term.shape[1]))

    if self.__use_cvxopt:
      b_term = matrix(b_term)

    sol = lp_solver.apply(self.__cost_vecs[role], self.__g_matrices[role], self.__h_cvxes[role], self.__a_matrices[role], b_term,
                          parameters={"solver": "glpk"}, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)
    prim_obj = lp_solver.get_prim_obj_from_sol(sol, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)
    points = lp_solver.get_points_from_sol(sol, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)

    prim_obj = prim_obj if prim_obj is not None else sys.maxsize
    points = points if points is not None else [0.0] * len(self.black_nets[role].transitions)

    return prim_obj, points

  def __derive_heuristic(self, role, x, t, h):
    x_prime = x.copy()
    transition_index = next((i for i, x in enumerate(self.black_nets[role].transitions) if x.name == t.name), None) #  self.black_nets[role].transitions.index(t)
    if transition_index is None:
      return h, x_prime
    x_prime[transition_index] -= 1
    return max(0, h - self.__cost_vecs[role][transition_index]), x_prime

  def __trust_solution(self, x):
    for v in x:
      if v < -0.001:
        return False
    return True
