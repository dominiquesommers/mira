from mira.simulation.ground_truth_simulation import GroundTruthSimulation
from mira.nets.system import System
import json
from datetime import timedelta

with open('./tests/nets/inhibitor_test.json', 'r') as json_file:
  data = json.load(json_file)
  system = System.from_json_dict(data)
  # system.view(debug=True)

transition_weights = {
  t.name: (lambda dt: 1)
  for t in system.transitions
}
transition_weights = {'a': lambda dt: 0, 'b': lambda dt: 1}

durations = {(arc.source.name, arc.target.name): (lambda: 0)
             for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}

sim = GroundTruthSimulation(system, {}, transition_weights, durations, {}, {})

log = sim.simulate_log(start_time='2024-04-08T10:00:00', min_log_length=1, max_log_length=60, verbose=1)

log = log.projection(lambda f: f.transition.label is not None, is_already_closed=True)
[print(f'{str(sim.start_time + timedelta(seconds=firing.time))}, {firing.short_str()}') for firing in log]

move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label} @ {int(x.time / 3600)}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
log.view(element_str=move_str)
