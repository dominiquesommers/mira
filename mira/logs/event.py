from mira.prelim.multiset import Multiset

from datetime import datetime
from typing import Union, Optional


class Event:
  def __init__(self, activity:str, identifiers:Multiset, timestamp:Optional[Union[float,datetime]]=None):
    self.activity = activity
    self.identifiers = identifiers
    self.timestamp = timestamp

  def __repr__(self):
    return f'{self.activity}\n{self.identifiers}'

  def __str__(self):
    return self.__repr__()

# class NuEvent(Event):
#   def __init__(self):
#     super().__init__()
#
#
# class MultiNuEvent(Event):
#   def __init__(self):
#     super().__init__()
