from mira.logs.event_log import Log
from mira.logs.event import Event
from mira.prelim.multiset import Multiset

import numpy as np
import pandas as pd
import ast
import pickle

def get(example=1, view=False):
  with open('./mira/examples/logs/bpi17_variants.pkl', 'rb') as file:
    variants = pickle.load(file)

  examples = []
  for variant_key, variant in variants.items():
    events = [Event(activity, Multiset(args={entity: 1 for entity in entities}), timestamp=dt) for activity, entities, dt in variant]
    entity_set = set([entity for _, entities, _ in variant for entity in entities])
    identifiers = tuple([{entity for entity in entity_set if entity[:len(object)] == object} for object in ['Application', 'Offer']])
    examples.append({'events': events, 'identifiers': identifiers, 'variant_key': variant_key})

  default_example = {'events': [], 'identifiers': tuple([set()]*len(examples[0]['identifiers'])), 'variant_key': example}
  example = examples[example] if isinstance(example, int) else next((x for x in examples if x['variant_key'] == example), default_example)

  print(f"{example['variant_key']}: {len(example['events'])} events, {sum(len(v) for v in example['identifiers'])} objects.")


  # # TODO extract from csv?
  # # with open('./mira/examples/logs/BPI2017-Final.csv', 'r') as csv_file:
  # #   data = pd.read_csv(csv_file)
  # #   col = 'offer'
  # #   data.dropna(subset=[col], inplace=True)
  # #   data[col] = data[col].apply(lambda x: ast.literal_eval(x))
  # #   print(data[['event_activity', col]].head())
  # #   print(set(data.where(data[col].apply(len) > 1).dropna(subset=[col])['event_activity']))
  # examples = {1: {'events':
  # [
  #   Event('Create application', Multiset(args={'a1': 1})),
  #   Event('Accept', Multiset(args={'a1': 1})),
  #   Event('Create offer', Multiset(args={'a1': 1, 'o1': 1})),
  #   Event('Create offer', Multiset(args={'a1': 1, 'o2': 1})),
  #   Event('Create offer', Multiset(args={'a1': 1, 'o3': 1})),
  #   Event('Create offer', Multiset(args={'a1': 1, 'o4': 1})),
  #
  #   Event('Send (mail and online)', Multiset(args={'o3': 1})),
  #   Event('Call', Multiset(args={'a1': 1, 'o3': 1})),
  #   Event('Send (mail and online)', Multiset(args={'o4': 1})),
  #
  #   Event('Send (mail and online)', Multiset(args={'o1': 1})),
  #   Event('Send (mail and online)', Multiset(args={'o2': 1})),
  #
  #   Event('Create offer', Multiset(args={'a1': 1, 'o5': 1})),
  #   Event('Create offer', Multiset(args={'a1': 1, 'o6': 1})),
  #
  #   Event('Send (mail and online)', Multiset(args={'o5': 1})),
  #   Event('Send (mail and online)', Multiset(args={'o6': 1})),
  #
  #   Event('Create offer', Multiset(args={'a1': 1, 'o7': 1})),
  #   Event('Create offer', Multiset(args={'a1': 1, 'o8': 1})),
  #
  #   Event('Send (mail and online)', Multiset(args={'o7': 1})),
  #   Event('Send (mail and online)', Multiset(args={'o8': 1})),
  #
  #   Event('Validate', Multiset(args={'a1': 1})),
  #   Event('Validate', Multiset(args={'a1': 1})),
  #   Event('Validate', Multiset(args={'a1': 1})),
  #   Event('Validate', Multiset(args={'a1': 1})),
  # ], 'identifiers': ({'a1'}, set([f'o{i}' for i in range(1, 9)]))},
  # 2: {'events': # (Create offer 349351 @ 09-09-2016 18:51) -> (Create offer 349354 @ 09-09-2016 19:16) -> (Validate 349360 @ 14-09-2016 15:48)
  #       [
  #         Event('Create application', Multiset(args={'a1': 1})),
  #         Event('Accept', Multiset(args={'a1': 1})),
  #         Event('Create offer', Multiset(args={'a1': 1, 'o2': 1})), #o2 finito
  #         Event('Create offer', Multiset(args={'a1': 1, 'o4': 1})),
  #         Event('Send (mail and online)', Multiset(args={'o4': 1})),
  #         Event('Call', Multiset(args={'a1': 1, 'o4': 1})), #o4 finito
  #
  #         Event('Create offer', Multiset(args={'a1': 1, 'o5': 1})), #o5 finito
  #         Event('Create offer', Multiset(args={'a1': 1, 'o6': 1})),
  #         Event('Send (mail and online)', Multiset(args={'o6': 1})), #o6 finito
  #         Event('Validate', Multiset(args={'a1': 1})),
  #
  #         Event('Create offer', Multiset(args={'a1': 1, 'o7': 1})),
  #         Event('Send (mail and online)', Multiset(args={'o7': 1})),  # o7 finito
  #         Event('Create offer', Multiset(args={'a1': 1, 'o8': 1})),
  #         Event('Send (mail and online)', Multiset(args={'o8': 1})),  # o8 finito
  #         Event('Create offer', Multiset(args={'a1': 1, 'o9': 1})),
  #         Event('Send (mail and online)', Multiset(args={'o9': 1})),  # o9 finito
  #         Event('Validate', Multiset(args={'a1': 1})),
  #         Event('Validate', Multiset(args={'a1': 1})),
  #         Event('Validate', Multiset(args={'a1': 1})),
  #
  #         Event('Create offer', Multiset(args={'a1': 1, 'o1': 1})),
  #         Event('Send (mail and online)', Multiset(args={'o1': 1})),  # o1 finito
  #         Event('Create offer', Multiset(args={'a1': 1, 'o3': 1})),
  #         Event('Validate', Multiset(args={'a1': 1})),
  #
  #       ], 'identifiers': ({'a1'}, set([f'o{i}' for i in range(1, 10)]))}
  # }

  events, identifiers = example['events'], example['identifiers']
  log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, identifiers)
  if view:
    log.view()
  return log


if __name__ == '__main__':
  get()
