from mira.nets.transition import Transition
from mira.nets.marking import Marking, NuMarking, MultiNuMarking

import numpy as np


class Firing:
  def __init__(self, transition: Transition, sub_marking: Marking, add_marking: Marking, timestamp=None):
    self.transition = transition
    self.sub_marking = sub_marking
    self.add_marking = add_marking
    self.id = np.random.rand()
    self.timestamp = timestamp

  def get_new_marking(self, marking):
    return marking - self.sub_marking + self.add_marking

  def __repr__(self):
    return f'{self.transition} m-({self.sub_marking}) m+({self.add_marking})'

  def __str__(self):
    return self.__repr__()

  def short_str(self):
    return f'{self.transition}'

  def __hash__(self):
    return hash(self.id)

  def new_marking(self, marking):
    return marking - self.sub_marking + self.add_marking


class NuFiring(Firing):
  def __init__(self, transition: Transition, mode: dict, sub_marking: NuMarking, add_marking: NuMarking, timestamp=None):
    self.mode = mode
    super().__init__(transition, sub_marking, add_marking)

  def __repr__(self):
    return f'{self.transition} {self.mode} m-({self.sub_marking}) m+({self.add_marking})'


class MultiNuFiring(Firing):
  def __init__(self, transition: Transition, mode: dict, sub_marking: MultiNuMarking, add_marking: MultiNuMarking, timestamp=None):
    self.mode = mode
    super().__init__(transition, sub_marking, add_marking)

  def __repr__(self):
    return f'{self.transition} {self.mode} m-({self.sub_marking}) m+({self.add_marking})'

  def short_str(self):
    return f'{self.transition} {self.mode}'

  def objects(self):
    return sorted([v for k, v in self.mode.items() if k != '_'])

  def to_json(self):
    return [self.transition.name, self.transition.label, self.transition.move_type, {k: str(v) for k, v in self.mode.items() if k != '_'},
            str(self.sub_marking), str(self.add_marking)] + ([] if self.timestamp is None else [str(self.timestamp)])
