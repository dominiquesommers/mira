from mira.logs.event_log import Log
from mira.logs.event import Event
from mira.prelim.multiset import Multiset

import numpy as np


def get(example=1, view=False):
  events = [
    Event('create', Multiset(args={'o1': 1})),
    Event('split', Multiset(args={'o1': 1, 'p1': 1, 'p2': 1})),
    Event('notify', Multiset(args={'o1': 1})),

    Event('load', Multiset(args={'d1': 1, 'p1': 1, 'p2': 1})),
    Event('deliver', Multiset(args={'d1': 1, 'p1': 1})),
    Event('next', Multiset(args={'d1': 1})),
    Event('deliver', Multiset(args={'d1': 1, 'p2': 1})),
    Event('finish', Multiset(args={'d1': 1})),

    Event('bill', Multiset(args={'o1': 1, 'p1': 1, 'p2': 1})),
  ]
  log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'o1'}, {'p1', 'p2'}, {'d1'},))
  if view:
    log.view()

  return log
