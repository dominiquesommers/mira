import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--i', help='', type=int)
parser.add_argument('-n', '--n', help='', type=int)
args = parser.parse_args()

fns = sorted(os.listdir('./examples/logs/bpi_17/individual/'), reverse=True)
if args.i is not None or args.n is not None:
  nn = len(fns)
  x = int(nn / args.n)
  if args.i == args.n:
    print((args.i - 1) * x, nn, nn)
    fns = fns[(args.i - 1) * x:]
  else:
    print((args.i - 1) * x, args.i * x, nn)
    fns = fns[(args.i - 1) * x:args.i * x]

for fn in fns: #os.listdir('./examples/logs/bpi_17/individual/'):
  print(fn)
  if os.path.exists(f'./examples/alignments/bpi_17/individual/{fn}'):
    print('already done')
    continue
  command = f'python3 -m mira -en bpi_17 -el bpi_17/individual/{fn} -e bpi_17/individual/{fn[:-5]}'
  print(command)
  os.system(command)
  # break


# pm4py 2.2.1
# dgl 0.4.3post2
# 167,182,198,224,252,256

# import time
# import datetime
# def check():
#   n = len(os.listdir('.'))
#   start_n = n
#   st = datetime.datetime.now()
#   count = 0
#   while True:
#     time.sleep(2)
#     x = len(os.listdir('.'))
#     if x != n:
#       n = x
#       count = n - start_n
#       average_time = (datetime.datetime.now() - st) / count
#       count_to_do = 26000 - n
#       print(f'{str(datetime.datetime.now())}: {x}/26000 ({x / 26000 * 100:.3f}%) (avg {str(average_time)} over {count}) (until finish {str(average_time * count_to_do)})')