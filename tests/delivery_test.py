from mira.nets.system import System
from mira.logs.event_log import Log
from mira.alignment.align import align
from mira.alignment.cost import Cost
from mira.prelim.colors import Color

import json
import os


def run_all(visualize=None, relax=False, verbose=False):
  visualize = [] if visualize is None else visualize
  log_indices = [int(filename.split('_')[1].split('.')[0])
                 for filename in os.listdir('tests/logs') if filename.startswith('delivery_')]
  for log_index in sorted(log_indices):
    print(f'Test {log_index}.')
    run(log_index, relax=relax, visualize=visualize, verbose=verbose)

def run(log_index=0, relax=False, visualize=None, verbose=False):
  visualize = [] if visualize is None else visualize

  with open(f'./tests/logs/delivery_{log_index}.json', 'r') as json_file:
    data = json.load(json_file)
    log = Log.from_json_dict(data)
    if 'log' in visualize:
      log.view()

  with open('./tests/nets/delivery.json', 'r') as json_file:
    data = json.load(json_file)
    system = System.from_json_dict(data)
    if relax:
      system.net = system.net.relax()
    system.net.object_types = log.object_types
    if 'net' in visualize:
      system.view()

  run, info = align(log, system, {'relax': relax, 'cost': Cost.cost_relaxed if relax else Cost.cost,
                                  'visualize_sync_net': ('syncnet' in visualize) or ('sync' in visualize),
                                  'visualize_log_net': 'lognet' in visualize})
  print(f'Cost: {info["cost"]:_}')
  if ('alignment' in visualize) or ('run' in visualize):
    TRANSITION_COLOR = lambda m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN
    run = run.projection(lambda x: not (x.transition.move_type == 'log' and x.transition.label is None))
    move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
    fill_color = lambda x: 'gray' if x.transition.label is None else 'white'
    run.view(element_str=move_str, colors=TRANSITION_COLOR, fill_colors=fill_color)



if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('-i', '--log_index', help='', type=int)
  parser.add_argument('-r', '--relax', help='Relaxed alignment.', action='store_true')
  parser.add_argument('-vis', '--visualize', help='Comma separated visualization (log,net,sync/syncnet,alignment/run)', type=str)
  parser.add_argument('-v', '--verbose', help='Verbose.', action='store_true')
  args = parser.parse_args()

  visualize = [] if args.visualize is None else args.visualize.split(',')

  vargs = {k: v for k, v in vars(args).items() if v is not None}
  if args.log_index is None:
    run_all(**vargs)
  else:
    run(**vargs)
