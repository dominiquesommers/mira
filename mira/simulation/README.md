This repository contains the source code for:

## Assessing Process Mining Techniques: a Ground Truth Approach
#### By Dominique Sommers, Natalia Sidorova, Boudewijn van Dongen (accepted ICPM 2024)

and

## Assessing Process Mining Techniques: a Ground Truth Approach (extended version)
#### By Dominique Sommers, Natalia Sidorova, Boudewijn van Dongen (submitted ICPM 2024 Process Science Collection)

This document contains usage for the methods described in the paper.

## Usage
### Simulation
`experiments.synthetic_<process>_simulation` for `process ∈ {package, energy, omron}` contains example usage as the method is used in the paper for creating the datasets with ground truth.

Run `python3 -m experiments.synthetic_<process>_simulation` from the base directory of this repository with --help to show the usage.
This script is used to produce the process model with incorporated patterns, and the simulated event log as described in the paper.

The arguments to provide to the script control how the `create_synthetic_data` method is called.
These arguments are:
- `no_objects`: (only for `process=energy` and `process=omron`) number of spontaneous objects to simulate. 
- `export`: filename to export the simulated event log.
- `visualize`: whether to visualize the simulated event log.
- `deviation_index`: (only for `process=package`) simulate the log for a specific deviation from 0 to 5 for recording errors and 6 to 11 for behavioral deviations.


### Alignments
`experiments.synthetic_package_alignments` contains the code for using the different alignment methods on the model-log pairs produced
by method above, as described in the paper.

Run `python3 -m experiments.synthetic_package_alignments` from the base directory of this repository with --help to show the usage.
This script is used to produce alignments between provided model and log, with the provided method, 
i.e., `systemic` (default), `trace` (`--single_object`), and `relaxed systemic` (`--relax`).
