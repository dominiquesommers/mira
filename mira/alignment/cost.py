

class Cost:
  deviation = 1_000_000
  epsilon = 1_000
  epsilon2 = 1

  @staticmethod
  def cost(transition):
    V = len(set().union(*transition.vars.values()))
    if transition.move_type == 'synchronous':
      return 0 #10000
    if transition.label is None:
      if transition.move_type == 'log':
        return 0
      return int(Cost.epsilon / 1000) * V
    else:
      return int(Cost.deviation / 1000) * V
    # if transition.move_type == 'log':
    #   return int(Cost.deviation / 1000)
    # if transition.move_type == 'model':
    #   if transition.label is None:
    #     return int(Cost.epsilon / 1000)
    #   else:
    #     return int(Cost.deviation / 1000)

  @staticmethod
  def cost_relaxed(transition):
    if transition.name.startswith('t_create^') or transition.name.startswith('t_destroy^'):
      return Cost.epsilon2

    if transition.name == 'create' and transition.move_type == 'model':
      return Cost.deviation**2

    V = len(set().union(*transition.vars.values()))
    Vart = len(set().union(*transition.vars_original.values()))

    if transition.label is None:
      return int((Vart - V + int(transition.move_type == 'model')) * Cost.epsilon)
    if transition.move_type == 'synchronous':
      return int((Vart - V) * Cost.epsilon)
    else:
      return int(V * Cost.deviation + ((Vart - V) * Cost.epsilon))
