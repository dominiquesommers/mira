from mira.prelim.poset import POSet
from mira.nets.system import System
from mira.nets.marking import Marking, MultiNuMarking
from mira.nets.firing import Firing
from mira.nets.place import Place

from pm4py.util.lp import solver as lp_solver
from bidict import bidict
from collections import defaultdict
from cvxopt import matrix
from typing import Union, List
import numpy as np
import heapq
import time
import sys
import psutil
from datetime import datetime, timedelta
from typing import Callable
from copy import deepcopy


class State:
  def __init__(self, markings:frozenset, costs:dict, last_firing:Union[Firing,None], T_vec:Union[List[float],None],
               trustable:bool, parent, enabled_firings:List[Firing]):
    self.markings = markings
    self.costs = {'c': costs['c1'] + costs['c2'], **costs} # m_i -c1-> m -c2-> m_f
    self.last_firing = last_firing
    self.T_vec = T_vec
    self.trustable = trustable
    self.parent = parent
    self.enabled_firings = enabled_firings

  def __lt__(self, other):
    # TODO prefer unpack transitions, (pack transitions), unprefer non-unpack void transitions.
    if self.costs['c'] != other.costs['c']:
      return self.costs['c'] < other.costs['c']
    if self.trustable and not other.trustable:
      return True
    return self.costs['c2'] < other.costs['c2']

  def __get_firing_sequence(self):
    sequence = []
    if self.parent is not None:
      sequence.extend(self.parent.__get_firing_sequence())
    if self.last_firing is not None:
      sequence.append((self.last_firing.transition, self.last_firing.mode))
    return sequence

  def get_possible_next_transitions(self):
    # TODO implement
    ...
  #   if self.last_firing is None:
  #     return list({p_out_arc.target for p in self.marking for p_out_arc in p.out_arcs})
  #   return list({p_out_arc.target for t_out_arc in self.last_firing.transition.out_arcs for p_out_arc in t_out_arc.target.out_arcs})

  def update_cost_to_m_f(self, cost_to_m_f):
    self.costs['c'] = self.costs['c'] - self.costs['c2'] + cost_to_m_f
    self.costs['c2'] = cost_to_m_f

  def __repr__(self):
    return f'STATE\n{self.markings}\nm_i -{self.costs["c1"]}-> m -{self.costs["c2"]}-> m_f ({"" if self.trustable else "not "}trustable)\n{self.last_firing}\n' + \
           f'prev firing: {self.parent.last_firing if self.parent is not None else "None"}\n'

  def __hash__(self):
    return hash(self.markings) + hash(self.costs["c"] + int(self.trustable))

  def __eq__(self, other):
    return self.markings == other.markings

class PrioritySet(object):
  def __init__(self):
    self.heap = []
    heapq.heapify(self.heap)
    self.set_ = set()

  def __len__(self):
    return len(self.heap)

  def add(self, d, verbose=False):
    # heapq.heappush(self.heap, d)

    # TODO temp fix is to encode cost with marking of state, but this potentially introduces many additional unnecessary states in the set.
    #      Better is to only keep the cheapest one.
    # if d in self.set_:
      # d_ = next(x for x in self.set_ if x == d)
      # if d < d_:
      #   # TODO replace d_ with d, \ie remove d_ and add d.
      #   self.set_.remove(d_)
      #   self.heap.remove(d_)
      #   heapq.heappush(self.heap, d)
      #   self.set_.add(d)
    if d not in self.set_:
      heapq.heappush(self.heap, d)
      self.set_.add(d)

  def pop(self):
    d = heapq.heappop(self.heap)
    self.set_.remove(d)
    return d

class Search:
  def __init__(self, system:System, costs:Callable=None, variant='a_star'):
    self.system = system
    self.black_system = self.system.blacken()
    self.costs = costs
    self.variant = variant
    self.__vectorize()

  def __vectorize(self):
    self.incidence_matrix = self.black_system.net.get_incidence_matrix()
    self.__m_i_vec = self.black_system.m_i.vectorize(self.black_system.places)
    self.__m_f_vec = self.black_system.m_f.vectorize(self.black_system.places)
    self.__cost_vec = [0] * len(self.system.transitions)
    for index, transition in enumerate(self.black_system.transitions):
      self.__cost_vec[index] = self.costs(transition) * 1.0

    self.__a_matrix = np.asmatrix(self.incidence_matrix).astype(np.float64)
    self.__g_matrix = -np.eye(len(self.system.transitions))
    self.__h_cvx = np.matrix(np.zeros(len(self.system.transitions))).transpose()

    self.__use_cvxopt = lp_solver.DEFAULT_LP_SOLVER_VARIANT in \
                        [lp_solver.CVXOPT_SOLVER_CUSTOM_ALIGN, lp_solver.CVXOPT_SOLVER_CUSTOM_ALIGN_ILP]

    if self.__use_cvxopt:
      self.__a_matrix = matrix(self.__a_matrix)
      self.__g_matrix = matrix(self.__g_matrix)
      self.__h_cvx = matrix(self.__h_cvx)
      self.__cost_vec = matrix(self.__cost_vec)

  def check(self):
    # TODO check for easy soundness.
    ...

  def compute_cheapest_run(self, max_search_time:float=np.inf, min_memory_available:float=0):
    self.start_time = time.time()
    print(f'Starting at {datetime.now()}')
    self.max_search_time = max_search_time
    self.min_memory_available = min_memory_available
    self.process = psutil.Process()
    closed_markings = set()

    cost_to_m_f, T_vec = self.__compute_exact_heuristic_new_version(self.black_system.m_i)

    print(f'aligning from\n{self.system.m_i}\nto\n{self.system.m_f}')

    for place in self.system.m_i.keys():
      place.alpha = self.system.net.alpha(place, self.system.net.variable_types)

    self.submarkings = bidict(enumerate(self.system.m_i.split_tokens()))

    open_set = PrioritySet()
    open_set.add(State(frozenset(self.submarkings.keys()), {'c1': 0, 'c2': cost_to_m_f}, last_firing=None, T_vec=T_vec, trustable=True, parent=None, enabled_firings=[]))

    self.search_stats = {'visited': 0, 'queued': 0, 'traversed': 0, 'lp_solved': 1, 'enabled_check': 0}
    while len(open_set) > 0:
      print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({timedelta(seconds=time.time() - self.start_time)}, {self.process.memory_info().rss / 1024**3:.2f}GB)', end='\r')
      current_state = open_set.pop()
      current_state, break_reason = self.__get_trustable(current_state, closed_markings, open_set)
      if break_reason is not None:
        print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({timedelta(seconds=time.time() - self.start_time)}, {self.process.memory_info().rss / 1024 ** 3:.2f}GB)')
        return self.get_run(current_state), {'error': break_reason, 'cost': None, 'stats': {**self.search_stats, 'duration': time.time() - self.start_time}}

      # print(f'Select {current_state.last_firing}')
      if current_state.costs['c2'] > lp_solver.MAX_ALLOWED_HEURISTICS:
        print(f'continue because of max allowed heuristics {current_state.costs["c2"]}.')
        continue
      if current_state.markings in closed_markings:
        # print('skip since marking is already seen before.')
        continue

      self.system.marking = sum([self.submarkings[i] for i in current_state.markings], start=MultiNuMarking())
      if current_state.costs['c2'] < 0.01:
        if self.system.marking == self.system.m_f:
          print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({timedelta(seconds=time.time() - self.start_time)}, {self.process.memory_info().rss / 1024 ** 3:.2f}GB)')
          print('found it')
          return self.get_run(current_state), {'cost': current_state.costs['c1'], **self.search_stats, 'duration': time.time() - self.start_time}

      closed_markings.add(current_state.markings)
      self.search_stats['visited'] += 1

      parent_enabled_firings = None if current_state.parent is None else current_state.parent.enabled_firings
      enabled_firings, nr = self.system.enabled_firings(parent_enabled_firings=parent_enabled_firings, last_firing=current_state.last_firing)
      self.search_stats['enabled_check'] += nr

      current_state.enabled_firings = enabled_firings

      for enabled_firing in enabled_firings:
        self.search_stats['traversed'] += 1
        new_marking = enabled_firing.new_marking(self.system.marking)
        if new_marking in closed_markings:
          continue

        cost_to_m_f, T_vec = self.__derive_heuristic(current_state.T_vec,  enabled_firing.transition, current_state.costs['c2'])
        trustable = True if self.variant == 'dijkstra' else self.__trust_solution(T_vec)

        new_submarkings = set()
        for submarking in new_marking.split_tokens():
          if (index := self.submarkings.inverse.get(submarking, None)) is None:
            index = len(self.submarkings.keys())
            self.submarkings[index] = submarking
          new_submarkings.add(index)

        new_state = State(frozenset(new_submarkings), {'c1': current_state.costs['c1'] + self.costs(enabled_firing.transition), 'c2': cost_to_m_f},
                          enabled_firing, T_vec, trustable, current_state, enabled_firings=[])
        open_set.add(new_state)
        self.search_stats['queued'] += 1

    print(f'Open {len(open_set):>5}, closed {len(closed_markings):>5} ({(time.time() - self.start_time) / 60:.2f}min, {self.process.memory_info().rss / 1024 ** 3:.2f}GB)')
    raise ValueError('Alignment failed due to unknown reason.')

  def get_states(self, statee):
    state = deepcopy(statee)
    states = [state]
    while state.parent is not None:
      states.append(state.parent)
      state = state.parent
    return list(reversed(states))

  def get_run(self, state):
    if state is None:
      return None, None
    self.system.marking = self.system.m_i
    if state.last_firing is None:
      return POSet()
    reversed_firings = [(state.last_firing, state.markings)]
    if state.parent is not None and state.last_firing is not None:
      parent = state.parent
      while parent.parent is not None:
        reversed_firings.append((parent.last_firing, parent.markings))
        parent = parent.parent
    for firing, new_marking in reversed(reversed_firings):
      self.system.fire(firing, new_marking)
    return self.system.firings

  def __get_trustable(self, state, closed_markings, open_set):
    while not state.trustable:
      if (break_reason := self.__check_break()) is not None:
        return state, break_reason

      if state.markings in closed_markings:
        state = open_set.pop() #heapq.heappop(open_set)
        continue

      m = sum([self.submarkings[i] for i in state.markings], start=MultiNuMarking())
      cost_to_m_f, T_vec = self.__compute_exact_heuristic_new_version(m.blacken())
      self.search_stats['lp_solved'] += 1

      state.trustable = True
      state.update_cost_to_m_f(cost_to_m_f)

      open_set.add(state)
      state = open_set.pop()
      # state = heapq.heappushpop(open_set, state)
    return state, None

  def __check_break(self):
    if (seconds := time.time() - self.start_time) >= self.max_search_time:
      return f'Out of time, took {seconds:.2f} seconds.'
    if psutil.virtual_memory().available / 1024**3 < self.min_memory_available:
      return f'Out of memory (min: {self.min_memory_available}GB), used {self.process.memory_info().rss / 1024**3:.2f}GB of memory.'
    return None

  def __check_still_enabled(self, marking, firing):
    return firing.sub_marking <= marking

  def __compute_exact_heuristic_new_version(self, marking:Marking, strict:bool=True):
    if self.variant == 'dijkstra':
      return 0, None
    # TODO compute heuristic for each individual object in marking and its projection system (see search3).
    # print(np.matrix(self.incidence_matrix))
    m_vec = marking.vectorize(self.black_system.places)
    b_term = [i - j for i, j in zip(self.__m_f_vec, m_vec)]
    b_term = np.matrix([x * 1.0 for x in b_term]).transpose()

    if not strict:
      self.__g_matrix = np.vstack([self.__g_matrix, self.__a_matrix])
      self.__h_cvx = np.vstack([self.__h_cvx, b_term])
      self.__a_matrix = np.zeros((0, self.__a_matrix.shape[1]))
      b_term = np.zeros((0, b_term.shape[1]))

    if self.__use_cvxopt:
      b_term = matrix(b_term)

    sol = lp_solver.apply(self.__cost_vec, self.__g_matrix, self.__h_cvx, self.__a_matrix, b_term,
                          parameters={"solver": "glpk"}, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)
    prim_obj = lp_solver.get_prim_obj_from_sol(sol, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)
    points = lp_solver.get_points_from_sol(sol, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)

    prim_obj = prim_obj if prim_obj is not None else sys.maxsize
    points = points if points is not None else [0.0] * len(self.black_system.transitions)

    return prim_obj, points

  def __derive_heuristic(self, x, t, h):
    if self.variant == 'dijkstra':
      return 0, None
    x_prime = x.copy()
    transition_index = self.black_system.transitions.index(t)
    x_prime[transition_index] -= 1
    return max(0, h - self.__cost_vec[transition_index]), x_prime

  def __trust_solution(self, x):
    for v in x:
      if v < -0.001:
        return False
    return True















def main():
  from mira.nets.system import System
  from mira.alignment.log_net import create_log_system
  from mira.alignment.synchronous_product import tPNIDSynchronousProductSystem
  from mira.alignment.cost import Cost
  from mira.logs.event_log import Log
  from mira.prelim.colors import Color
  import json

  with open('./tests/nets/search.json', 'r') as json_file:
    data = json.load(json_file)
    system = System.from_json_dict(data)
    # system.view()
  with open(f'./tests/logs/search_1.json', 'r') as json_file:
    data = json.load(json_file)
    log = Log.from_json_dict(data)
    # log.view()

  log_system = create_log_system(log, type(system.net))
  sync_product = tPNIDSynchronousProductSystem(log_system, system)

  search = Search(sync_product, Cost.cost)
  run, info = search.compute_cheapest_run()

  for state in run:
    print(state)

  print(info)
  print(f'Cost: {info["cost"]:_}')
  view = True
  if view:
    TRANSITION_COLOR = lambda m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN
    # run = run.projection(lambda x: not (x.transition.move_type == 'log' and x.transition.label is None))
    move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
    fill_color = lambda x: 'gray' if x.transition.label is None else 'white'
    run.view(element_str=move_str, colors=TRANSITION_COLOR, fill_colors=fill_color)






















if __name__ == '__main__':
  main()
  exit()
  from mira.nets.petri_net import Net, NuNet, MultiNuNet, System
  from mira.nets.marking import Marking, NuMarking, MultiNuMarking
  from mira.nets.place import Place
  from mira.nets.transition import Transition
  from mira.nets.token import MultiColorToken, ColorToken
  from mira.nets.arc import Arc, NuArc, MultiNuArc, InhibitorArc, VariableMultiNuArc

  test = 'multi4'
  if test == 'nu':
    ps = [Place(name) for name in ['pi', 'p1', 'pf']]
    ts = [Transition(name, name) for name in ['a', 'b', 'c']]
    net = NuNet(ps, ts)
    [net.add_arc(NuArc(s, t, 'x')) for s, t in [(ps[0], ts[0]), (ps[0], ts[1]), (ts[0], ps[2]), (ts[1], ps[1]), (ps[1], ts[2]), (ts[2], ps[2])]]

    m_i = NuMarking({'pi': {ColorToken('a'): 1}})
    m_f = NuMarking({'pf': {ColorToken('a'): 1}})
    costs = {ts[0]: 3, ts[1]: 1, ts[2]: 1}

  elif test == 'multi1':
    ps = [Place(name) for name in ['p1', 'p2', 'p3']]
    ts = [Transition(name, name) for name in ['t', 't_']]
    net = MultiNuNet(ps, ts)
    net.add_arc(MultiNuArc(ps[0], ts[0], ('X', '_')))
    net.add_arc(MultiNuArc(ps[1], ts[0], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[0], ('X - {x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[2], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ps[0], ts[1], ('_', '_')))

    m_i = MultiNuMarking({
      ps[0]: {MultiColorToken(2, [{'a': 1, 'b': 1}, {}]): 1},
      ps[1]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    m_f = MultiNuMarking({
      ps[2]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    costs = {ts[0]: 1, ts[1]: 1}
  elif test == 'nu2':
    ps = [Place(name) for name in ['pi', 'p1', 'pf']]
    ts = [Transition(name, name) for name in ['a', 'b', 'c']]
    net = NuNet(ps, ts)
    [net.add_arc(NuArc(s, t, 'x')) for s, t in [(ps[0], ts[0]), (ps[0], ts[1]), (ts[0], ps[2]), (ts[1], ps[1]), (ps[1], ts[2]), (ts[2], ps[2])]]

    m_i = NuMarking({'pi': {ColorToken('a'): 1}})
    m_f = NuMarking({'pf': {ColorToken('a'): 1}})
    costs = {ts[0]: 3, ts[1]: 1, ts[2]: 1}

  elif test == 'multi1':
    ps = [Place(name) for name in ['p1', 'p2', 'p3']]
    ts = [Transition(name, name) for name in ['t', 't_']]
    net = MultiNuNet(ps, ts)
    net.add_arc(MultiNuArc(ps[0], ts[0], ('X', '_')))
    net.add_arc(MultiNuArc(ps[1], ts[0], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[0], ('X - {x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[2], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ps[0], ts[1], ('_', '_')))

    m_i = MultiNuMarking({
      ps[0]: {MultiColorToken(2, [{'a': 1, 'b': 1}, {}]): 1},
      ps[1]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    m_f = MultiNuMarking({
      ps[2]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    costs = {ts[0]: 1, ts[1]: 1}
  elif test == 'multi2':
    ps = {name: Place(name) for name in ['pi', 'p1', 'p2', 'p10', 'pf']}
    ts = {name: Transition(name, name) for name in ['t_c', 't_sp', 't_p', 'a']}
    net = MultiNuNet(list(ps.values()), list(ts.values()))
    [net.add_arc(MultiNuArc(source, target, ('X',))) for source, target in [(ps['pi'], ts['t_c']), (ts['t_c'], ps['p1']),
                                                                                (ps['p1'], ts['t_p']), (ps['p1'], ts['t_sp']),
                                                                                (ts['a'], ps['pf']), (ps['p10'], ts['a']),
                                                                                (ps['p2'], ts['a']), (ts['t_sp'], ps['p2'])
                                                                            ]]
    net.add_arc(MultiNuArc(ps['p2'], ts['t_p'], ('Y',)))
    net.add_arc(MultiNuArc(ts['t_p'], ps['p2'], ('X + Y',)))
    [net.add_arc(InhibitorArc(ps[place], ts[transition])) for place, transition in [('p2', 't_c'), ('p2', 't_sp'), ('p1', 'a')]]

    m_i = MultiNuMarking({
      ps['pi']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1},
      ps['p10']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1},
    })
    m_f = MultiNuMarking({
      ps['pf']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1},
    })
    costs = {t: 1 for t in ts.values()}

  elif test == 'multi3':
    ps = {name: Place(name) for name in ['pi', 'p1', 'p2', 'p10', 'pf']}
    ts = {name: Transition(name, name) for name in ['t_c', 't_ep', 't_p', 'a']}
    net = MultiNuNet(list(ps.values()), list(ts.values()))
    [net.add_arc(MultiNuArc(source, target, ('X',))) for source, target in [(ps['pi'], ts['t_c']), (ts['t_c'], ps['p1']),
                                                                                (ps['p1'], ts['t_p']),
                                                                                (ts['a'], ps['pf']), (ps['p10'], ts['a']),
                                                                                (ps['p2'], ts['a'])
                                                                            ]]
    net.add_arc(MultiNuArc(ts['t_ep'], ps['p2'], ('_',)))
    net.add_arc(MultiNuArc(ps['p2'], ts['t_p'], ('Y',)))
    net.add_arc(MultiNuArc(ts['t_p'], ps['p2'], ('X + Y',)))
    [net.add_arc(InhibitorArc(ps[place], ts[transition])) for place, transition in [('p2', 't_c'), ('p2', 't_ep'), ('p1', 'a')]]

    m_i = MultiNuMarking({
      ps['pi']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1},
      ps['p10']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{}]): 1},
    })
    m_f = MultiNuMarking({
      ps['pf']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{}]): 1},
    })
    costs = {t: 1 for t in ts.values()}
  elif test == 'multi4':
    # TODO MONDAY, FIX FOR THIS.

    ps = {name: Place(name) for name in ['i', 'f']}
    t = Transition('a', 'a')
    net = MultiNuNet(list(ps.values()), [t])
    net.add_variable_arc(VariableMultiNuArc(ps['i'], t, ('X',), (1, np.inf)))
    net.add_variable_arc(VariableMultiNuArc(t, ps['f'], ('X',), None))

    print('places')
    for place in net.places:
      print(place)
    print('\ntransitions')
    for transition in net.transitions:
      print(transition)
    print('\narcs')
    for arc in net.arcs:
      print(arc)
    print('\ninhibit arcs')
    for arc in net.inhibitor_arcs:
      print(arc)
    # print(buh)

    m_i = MultiNuMarking({
      ps['i']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1}
    })
    m_f = MultiNuMarking({
      ps['f']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1}
    })
    cost_function = lambda t: 10000 if t.label is not None else 1

  else:
    raise ValueError()

  system = System(net, m_i, m_f)

  search = Search(system, cost_function)
  run, stats = search.compute_cheapest_run()

  print('cost', stats['cost'])
  print('run', run)
  run.view(element_str=lambda x: f'{x.transition}')