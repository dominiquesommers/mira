# from mira.rc_nu_net.visualize import visualize
from mira.alignments.utils import TRANSITION_COLOR, is_model_move, is_log_move
from mira.prelim.colors import Color, CaseColor, ResourceColor, ResourceTypeColor

from pm4py.objects.petri_net.obj import Marking, PetriNet
from pm4py.objects.petri_net.utils import petri_utils

from collections import Counter, defaultdict
from copy import deepcopy
import tempfile
from graphviz import Digraph


class NuMarking(defaultdict):
  '''
  Example Marking:
  { place1: 2, place2, 1 }
  Example NuMarking:
  {
    place1: {('c1', 'r1'): 1, ('c2', 'r2'): 2},
    place2: ...
  }
  '''
  def __init__(self, args=None):
    if args is None:
      args = []
    super().__init__(Marking, args)

  def __hash__(self):
    r = 0
    for p, token_counter in self.items():
      for token, count in token_counter.items():
        r += 31 * hash(p) * hash(token) * count
    return r

  def __eq__(self, other):
    for place, token_marking in self.items():
      for token, count in token_marking.items():
        if count != other[place][token]:
          return False

    for place, token_marking in other.items():
      for token, count in token_marking.items():
        if count != self[place][token]:
          return False
    return True

  def clean(self):
    places_to_remove = []
    place_tokens_to_remove = defaultdict(list)
    for place, token_marking in self.items():
      if len(token_marking) == 0:
        places_to_remove.append(place)
        continue
      # del token_marking[('_', '_')]
      for token, count in token_marking.items():
        if count == 0:
          place_tokens_to_remove[token].append(token)
    for place in places_to_remove:
      del self[place]
    for place, tokens in place_tokens_to_remove.items():
      for token in tokens:
        del self[place][token]

  def __le__(self, other):
    if not self.keys() <= other.keys():
      return False
    for place, token_marking in self.items():
      if not token_marking <= other.get(place):
        return False
    return True

  def __add__(self, other):
    m = deepcopy(self)
    for p, token_marking in other.items():
      for token, count in token_marking.items():
        m[p][token] += count
    return m

  def __sub__(self, other):
    m = deepcopy(self)
    for p, token_marking in other.items():
      nul_tokens = []
      for token, count in token_marking.items():
        m[p][token] -= count
        if m[p][token] < 0:
          raise ValueError('marking can not be negative.')
    # self.clean()
    return m

  def __repr__(self):
    tokenstring = lambda t: ','.join(t)
    return ' '.join([f'{place.name}: {" ".join([f"{tokenstring(token)}: {self[place][token]}" for token in sorted(self[place].keys())])}'
                     for place in sorted(self.keys(), key=lambda x: x.name)])

  def to_json(self):
    fix_tuple = lambda x: ','.join(x) if isinstance(x, tuple) else x
    return {fix_tuple(place.name): {fix_tuple(token): count for token, count in sorted(token_count.items()) if count > 0}
            for place, token_count in sorted(self.items(), key=lambda x: x[0].name) if len(token_count.values()) > 0}

  def regex_ready_string(self, places):
    string = ''
    for place in sorted(self.keys(), key=lambda x: x.name):
      if place not in places:
        continue
      if isinstance(place.name, tuple):
        string = f'{string}{"SKIP".join(",".join(place.name).split(">>"))} '
      else:
        string = f'{string}{place.name} '
      for token in sorted(self[place].keys()):
        for i in range(self[place][token]):
          string = f'{string}{",".join(token)} '
    return string

  def __str__(self):
    return self.__repr__()

  def __deepcopy__(self, memodict={}):
    marking = NuMarking()
    # memodict[id(self)] = marking
    for place in self:
      place_occ = Marking()
      for token, count in self[place].items():
        place_occ[token] = count
      # new_place = memodict[id(place)] if id(place) in memodict else PetriNet.Place(place.name, properties=place.properties)
      # new_place = PetriNet.Place(place.name, properties=place.properties)
      marking[place] = place_occ
    return marking

  def flatten(self):
    flat_marking = Marking()
    for p, token_counter in self.items():
      flat_marking[p] = sum(token_counter.values())
    return flat_marking

  def remove_token(self, place_id, place_token):
    self[place_id][place_token] -= 1

  def remove_color(self, place_map=None):
    if place_map is None:
      place_map = {place: place for place in self.keys()}
    marking = Marking()
    for place, tokens in self.items():
      marking[place_map[place]] += sum(tokens.values())
    return marking

  def generalize(self):
    return GeneralizedNuMarking(self)


class GeneralizedNuMarking(NuMarking):
  '''
  Example Marking:
  { place1: 2, place2, 1 }
  Example NuMarking:
  {
    place1: {('c1', 'r1'): 1, ('c2', 'r2'): 2, ('c3', 'r3'): -1)},
    place2: ...
  }
  '''
  def __init__(self, args=None):
    if args is None:
      args = []
    super().__init__(args)

  def __sub__(self, other):
    m = deepcopy(self)
    for p, token_marking in other.items():
      for token, count in token_marking.items():
        m[p][token] -= count
    # self.clean()
    return m

  def __add__(self, other):
    return GeneralizedNuMarking(super().__add__(other))

  def __deepcopy__(self, memodict={}):
    return GeneralizedNuMarking(super().__deepcopy__())

  def degeneralize(self):
    marking = NuMarking()
    negative = False
    for place, token_count in self.items():
      if any([count < 0 for token, count in token_count.items()]):
        negative = True
      marking[place] = token_count
    if negative:
      # print(marking)
      raise ValueError('Can not degeneralize generalized marking with a negative token count.')
    return marking


class NuPetriNet(PetriNet):
  def __init__(self, name=None, places=None, transitions=None, arcs=None, properties=None):
    super().__init__(name, places, transitions, arcs, properties)

  class Arc(object):
    def __init__(self, source, target, weight=1, labels=tuple, properties=None):
      if type(source) is type(target):
        raise Exception('Petri nets are bipartite graphs!')
      self.__source = source
      self.__target = target
      self.__weight = weight
      self.__labels = labels
      self.__properties = dict() if properties is None else properties

    def __get_source(self):
      return self.__source

    def __get_target(self):
      return self.__target

    def __set_weight(self, weight):
      self.__weight = weight

    def __get_weight(self):
      return self.__weight

    def __set_labels(self, labels):
      self.__labels = labels

    def __get_labels(self):
      return self.__labels

    def __get_properties(self):
      return self.__properties

    def __repr__(self):
      if type(self.source) is PetriNet.Transition:
        if self.source.label:
          return f'(t){self.source.label}->(p){self.target.name}'
        else:
          return f'(t){self.source.name}->(p){self.target.name}'
      if type(self.target) is PetriNet.Transition:
        if self.target.label:
          return f'(p){self.source.name}->(t){self.target.label}'
        else:
          return f'(p){self.source.name}->(t){self.target.name}'

    def __hash__(self):
      return id(self)

    def __eq__(self, other):
      return self.source == other.source and self.target == other.target

    def __deepcopy__(self, memodict={}):
      if id(self) in memodict:
        return memodict[id(self)]
      new_source = memodict[id(self.source)] if id(self.source) in memodict else deepcopy(self.source, memo=memodict)
      new_target = memodict[id(self.target)] if id(self.target) in memodict else deepcopy(self.target, memo=memodict)
      memodict[id(self.source)] = new_source
      memodict[id(self.target)] = new_target
      new_arc = NuPetriNet.Arc(new_source, new_target, weight=self.weight, labels=self.labels, properties=self.properties)
      memodict[id(self)] = new_arc
      return new_arc

    source = property(__get_source)
    target = property(__get_target)
    weight = property(__get_weight, __set_weight)
    labels = property(__get_labels, __set_labels)
    properties = property(__get_properties)

  def __deepcopy__(self, memodict={}):
    from mira.rc_nu_net.nu_net_utils import add_arc_from_to
    this_copy = NuPetriNet(self.name)
    memodict[id(self)] = this_copy
    for place in self.places:
      place_copy = NuPetriNet.Place(place.name, properties=place.properties)
      this_copy.places.add(place_copy)
      memodict[id(place)] = place_copy
    for trans in self.transitions:
      trans_copy = NuPetriNet.Transition(trans.name, trans.label, properties=trans.properties)
      this_copy.transitions.add(trans_copy)
      memodict[id(trans)] = trans_copy
    for arc in self.arcs:
      add_arc_from_to(memodict[id(arc.source)], memodict[id(arc.target)], this_copy, weight=arc.weight)
    return this_copy

  def remove_color(self):
    colorless = PetriNet()
    transition_copies = {transition: PetriNet.Transition(transition.name, transition.label, properties=transition.properties) for transition in self.transitions}
    place_copies = {place: PetriNet.Place(place.name) for place in self.places}

    [colorless.transitions.add(transition) for transition in transition_copies.values()]
    [colorless.places.add(place) for place in place_copies.values()]
    for transition in self.transitions:
      for in_arc in transition.in_arcs:
        petri_utils.add_arc_from_to(place_copies[in_arc.source], transition_copies[transition], colorless)
      for out_arc in transition.out_arcs:
        petri_utils.add_arc_from_to(transition_copies[transition], place_copies[out_arc.target], colorless)

    return colorless, place_copies, transition_copies

  def view(self, m_i=None, m_f=None, debug=False, name:str=None):
    m_i = m_i if m_i is not None else NuMarking()
    m_f = m_f if m_f is not None else NuMarking()
    self.create_view(m_i, m_f, debug, name=name).view(cleanup=True)

  def create_view(self, m_i, m_f, debug=False, name:str=None):
    name = '' if name is None else f'::{name}'
    m_i = NuMarking() if m_i is None else m_i
    m_f = NuMarking() if m_f is None else m_f

    font_size = '16'
    filename = tempfile.NamedTemporaryFile(suffix=f'{name}.gv')
    viz = Digraph(self.name, filename=filename.name, engine='dot', graph_attr={'bgcolor': 'transparent', 'rankdir': 'LR'})

    nodes = {}
    viz.attr('node', shape='box')
    for transition in self.transitions:
      transition_id = str(id(transition))
      # print(transition.label)
      label = transition.name if (debug and transition.label is None) else transition.label
      # label = getattr(transition, 'name' if debug else 'label')
      if isinstance(label, tuple):
        label = label[1] if is_model_move(transition) else label[0]
      if label is not None and '_' in label:
        label = f'{label.split("_")[0]}<sub>{"_".join(label.split("_")[1:])}</sub>'
      # TODO process superscript.
      silent = label is None
      label = '' if label is None and not debug else f'<{label}>'

      args = {'style': 'filled', 'penwidth': '2', 'fillcolor': Color.BLACK if silent else Color.WHITE, 'border': '1',
              'fontcolor': Color.WHITE if silent and not debug else Color.BLACK, 'fontsize': font_size,
              'color': Color.BLACK if not isinstance(transition.label, tuple) else TRANSITION_COLOR(transition)}
      viz.node(transition_id, label, **args)
      nodes[transition] = transition_id

    order_func = {'key': lambda p: p.name}
    places_ordered = sorted(list(m_i.keys()), **order_func) + \
                     sorted(list(set(self.places) - set(m_i.keys()) - set(m_f.keys())), **order_func) + \
                     sorted(list(set(m_f.keys() - m_i.keys())), **order_func)

    if len(places_ordered) != len(self.places):
      raise ValueError('ERROR, places do not match.')

    token_str = lambda c, r: f'<td border="0"><font color="{c}">&#9686;</font></td><td border="0"><font color="{r}">&#9687;</font></td>'
    tokens_str = lambda ts: f'<table cellspacing="-5" cellpadding="0" border="0" style="rounded"><tr>' \
                            f'{"<td> </td>".join([token_str(c, r) for c, r in ts])}</tr></table>'
    for place in places_ordered:
      place_id = str(id(place))
      args = {'name': place_id, 'penwidth': '2' if place not in m_f else '5', 'fontsize': '26', 'fixedsize': 'true', 'shape': 'circle', 'color': Color.BLACK}

      if len(place.in_arcs) > 0 and list(place.in_arcs)[0].labels[1] != '_':
        args['color'] = ResourceTypeColor.get(list(place.in_arcs)[0].labels[1], Color.GREY)

      if place in m_i:
        tokens = [(CaseColor.get(id_1, Color.BLACK), ResourceColor.get(id_2, Color.BLACK))
                  for tc in [[t] * c for t, c in m_i[place].items()] for id_1, id_2 in tc][:3]
        label = f"<{tokens_str(tokens)}>" if len(tokens) > 0 else ''
        viz.node(label=label, width='0.75', **args)
      elif place in m_f:
        viz.node(label='', width='0.75', **args)
      else:
        if place.name[-len('_memory'):] == '_memory':
          args['style'] = 'dashed'
        viz.node(label='', width='0.5', **args)

      nodes[place] = place_id

    for arc in sorted(list(self.arcs), key=lambda arc: (arc.source.name, arc.target.name)):
      if arc.source in nodes and arc.target in nodes:
        label = f'({",".join(arc.labels)})' if False else ''
        color = Color.GREY if arc.labels == ('_', '_') else Color.BLACK if arc.labels[1] == '_' else ResourceTypeColor.get(arc.labels[1], Color.GREY)
        style = 'dashed' if arc.labels[0] != '_' and arc.labels[1] != '_' else ''
        viz.edge(nodes[arc.source], nodes[arc.target], label='', color=color, penwidth='1', fontsize=font_size, arrowhead='normal', style=style)

    viz.attr(overlap='false')
    viz.format = 'png'
    return viz


if __name__ == '__main__':
  from mira.rc_nu_net import nu_net_utils
  from pm4py.objects.petri_net.utils import petri_utils
  nu_net = NuPetriNet()
  places = [petri_utils.add_place(nu_net, place_name) for place_name in ['p1', 'p2']]
  transitions = [petri_utils.add_transition(nu_net, transition_name, transition_name) for transition_name in ['t1']]
  nu_net_utils.add_arc_from_to(places[0], transitions[0], nu_net, labels=('_', '_'))
  nu_net_utils.add_arc_from_to(transitions[0], places[1], nu_net, labels=('_', '_'))
