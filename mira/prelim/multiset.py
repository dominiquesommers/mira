from mira.prelim.utils import split_by_comma

from collections import Counter, defaultdict
from collections.abc import Iterable
from copy import deepcopy, copy
from itertools import chain, combinations
from colorama import Fore, Style
from typing import Optional, Callable, Any


def full_stack():
  import traceback, sys
  exc = sys.exc_info()[0]
  stack = traceback.extract_stack()[:-1]  # last one would be full_stack()
  if exc is not None:  # i.e. an exception is present
    del stack[-1]  # remove call of full_stack, the printed exception
    # will contain the caught exception caller instead
  trc = 'Traceback (most recent call last):\n'
  stackstr = trc + ''.join(traceback.format_list(stack))
  if exc is not None:
    stackstr += '  ' + traceback.format_exc().lstrip(trc)
  return stackstr

class TypedDict(defaultdict):
  def __init__(self, key_type: Optional[Callable]=None, value_type: Optional[Callable]=None, args: Any=None):
    self.key_type = key_type
    self.value_type = value_type
    if args is None:
      args = {}
    if not isinstance(args, dict):
      raise TypeError(f'Args argument ({args.__class__.__name__}) should be of type dict or None.')
    args_ = {self.__fix_type(key, self.key_type): self.__fix_type(value, self.value_type) for key, value in args.items()}
    super().__init__(value_type, args_)

  def __repr__(self):
    try:
      return f'[{",".join([f"{k}" if v == 1 else f"{v}·{k}" for k, v in sorted(self.items()) if not self.__empty_value(v)])}]'
    except TypeError:
      return f'[{",".join([f"{k}" if v == 1 else f"{v}·{k}" for k, v in self.items() if not self.__empty_value(v)])}]'

  def is_empty(self):
    return all(self.__empty_value(value) for value in self.values())

  def __empty_value(self, value):
    if isinstance(value, int):
      return value == 0
    if isinstance(value, str):
      return value == ''
    return value.is_empty()

  def __str__(self):
    return self.__repr__()

  def __eq__(self, other):
    if type(self) != type(other):
      return False
    if any(key not in other.keys() for key, value in self.items() if not self.__empty_value(value)) or \
       any(key not in self.keys() for key, value in other.items() if not self.__empty_value(value)):
      return False
    return all(v == other[k] for k, v in self.items())

  def __le__(self, other):
    if any(key not in other.keys() for key in self.keys()):
      return False
    return all(v <= other[k] for k, v in self.items())

  def le(self, other):
    if any(key not in other.keys() for key in self.keys()):
      return False
    return True

  def __gt__(self, other):
    return not (self <= other)

  def __hash__(self):
    return hash(frozenset(self.items()))

  def __getitem__(self, key):
    return super().__getitem__(self.__fix_type(key, self.key_type))

  def __setitem__(self, key, value):
    key = self.__fix_type(key, self.key_type)
    value = self.__fix_type(value, self.value_type)
    super().__setitem__(key, value)

  def __fix_type(self, value, value_type):
    if isinstance(value, value_type):
      return value
    try:
      if isinstance(value, list) or isinstance(value, tuple):
        return value_type(*value)
      elif isinstance(value, dict):
        return value_type(args=value)
      else:
        return value_type(value)
      # return value_type(*value) if isinstance(value, list) or isinstance(value, tuple) else value_type(args=value)
    except:
      raise TypeError(f'Args argument should be of type {value_type.__name__} ({type(value).__name__}).')

  def __deepcopy__(self, memodict={}):
    # TODO check if value_type should be set as well.
    c_ = type(self)(key_type=self.key_type) #, value_type=self.value_type)
    for key, value in self.items():
      c_[key] = deepcopy(value)
    return c_

  def __copy__(self):
    c_ = type(self)()
    for key, value in self.items():
      c_[key] = value
    return c_

  def __isub__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for -=: '{type(self)}' and '{type(other)}'")
    keys_to_remove = []
    for key, value in other.items():
      self[key] -= value
      if self[key] == 0:
        keys_to_remove.append(key)
    for key in keys_to_remove:
      del self[key]
    return self

  def __sub__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for -: '{type(self)}' and '{type(other)}'")
    subbed = deepcopy(self)
    subbed -= other
    return subbed

  def __iadd__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for +=: '{type(self)}' and '{type(other)}'")
    for key, value in other.items():
      self[key] += value
    return self

  def __add__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")
    added = deepcopy(self)
    added += other
    return added

  def max(self, other):
    self_copy = type(self)(key_type=self.key_type)
    for key in set(self.keys()).union(set(other.keys())):
      self_copy[key] = self[key].max(other[key])
    return self_copy

  def min(self, other):
    self_copy = type(self)(key_type=self.key_type)
    for key in set(self.keys()).union(set(other.keys())):
      self_copy[key] = self[key].min(other[key])
    return self_copy

  def intersection(self, other):
    return self - (self - other).max(type(self)(key_type=self.key_type))

  @classmethod
  def from_str(cls, string, key_type, value_type):
    if string[0] != '[' or string[-1] != ']':
      return string # TODO handle correctly.
      raise ValueError(f'Multiset string should by of form: [<els>], is {string}.')
    args = {}
    if isinstance(value_type, Iterable):
      if TypedDict not in value_type[0].mro():
        raise TypeError(f'value_type arg is an iterable, then first element of value_type argument should by a class inherited from TypedDict.')
      value_type_const, params = value_type[0], value_type[1:]
    else:
      value_type_const = value_type

    if string != '[]':
      for element in split_by_comma(string[1:-1]):
        vk = split_by_comma(element, split_char='·')
        v, k = vk if len(vk) == 2 else (1, vk[0])
        value = value_type_const.from_str(v, *params) if isinstance(value_type, Iterable) else value_type(v)
        try:
          key = key_type.from_str(k)
        except AttributeError:
          key = key_type(k)
        args[key] = value
    return cls(key_type=key_type, value_type=value_type_const, args=args)


class Multiset(TypedDict):
  value_type = int
  def __init__(self, key_type: Any=None, value_type: Any=None, args: dict=None):
    if key_type is None:
      if args is not None and len(args) > 0:
        key_type = type(list(args.keys())[0])
        if any(type(key) != key_type for key in args.keys()):
          raise TypeError('Cannot determine key type as not all provided keys are of the same type, please explicitly add the required type.')
    if key_type is None:
      raise TypeError('key_type may not be None.')
    super().__init__(key_type, self.value_type, args)

  def powerset(self, exclude_emptyset=False):
    l = [x for key, count in self.items() for x in [key]*count]
    for elements in chain.from_iterable(combinations(l, r) for r in range(int(exclude_emptyset), len(l) + 1)):
      yield Multiset(self.key_type, args={k: elements.count(k) for k in set(elements)})

  def supp(self):
    return set(self.keys())

  def __len__(self):
    return sum(self.values())

  def is_empty(self):
    return all(value == 0 for value in self.values())

  def negative(self):
    return any(v < 0 for v in self.values())

  def max(self, other):
    self_copy = type(self)(key_type=self.key_type)
    for key in set(self.keys()).union(set(other.keys())):
      if (value := max(self[key], other[key])) != 0:
        self_copy[key] = value
    return self_copy

  def min(self, other):
    self_copy = type(self)(key_type=self.key_type)
    for key in set(self.keys()).union(set(other.keys())):
      if (value := min(self[key], other[key])) != 0:
        self_copy[key] = value
    return self_copy

  def multiply(self, value):
    if not isinstance(value, int):
      raise TypeError(f'You can only multiply a multiset by an int ({type(value)})')
    if value > 1:
      for key in self.keys():
        self[key] *= value
    return self

  @classmethod
  def from_str(cls, string, key_type, value_type=None):
    return super().from_str(string, key_type, int)

  def to_dict_str(self):
    return f"{{{','.join([f'{k}: {v}' for k, v in self.items()])}}}"

if __name__ == '__main__':
  a = Multiset(args={'a': 2})
  print([x for x in a.powerset()])

  print(alsljhfs)

  b = Multiset(args={'c': 1, 'b': 3, 'a': 1})
  c = Multiset(str, args={})
  print('a', a)
  print('b', b)
  print('a ⊓ b', a - (a - b).max(Multiset(str)))
  print('a ⊓ b', a.intersection(b))
  print('a ⊓ b', a.min(b))
  print('a ⊔ b', a.max(b))