from mira.nets.petri_net import Net
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import Arc, InhibitorNuArc
from mira.nets.firing import NuFiring
from mira.nets.marking import NuMarking
from mira.prelim.multiset import Multiset

from typing import Optional, Iterable
from collections import defaultdict
from colorama import Fore, Style
import itertools


class NuNet(Net):
  firing_type = NuFiring
  marking_type = NuMarking
  def __init__(self, places: Optional[Iterable[Place]] = None, transitions: Optional[Iterable[Transition]] = None,
               arcs: Optional[Iterable[Arc]] = None, inhibitor_arcs: Optional[Iterable[InhibitorNuArc]] = None):
    super().__init__(places, transitions, arcs, inhibitor_arcs)

  def add_inhibitor_arc(self, arc:InhibitorNuArc):
    if not (isinstance(arc.source, Place) and self._check_arc(arc)):
      return
    self.transition_inhibitor_arcs[arc.target].append(arc)
    self.inhibitor_arcs.add(arc)

  def create_marking_from_mode(self, arcs: dict, mode: dict):
    self._check_mode(mode)
    mode['_'] = '_'
    marking = NuMarking()
    for arc, weight in arcs.items():
      # TODO Handle 'nu' labels (dont have to be in mode??)
      if arc.label not in mode.keys():
        raise ValueError('Not all required labels are in mode.')
      marking[arc.place][mode[arc.label]] += weight
    return marking

  def get_transition_mode_candidates(self, marking, transition, only_vars=None, verbose=False):
    k = defaultdict(set)
    for arc in self.element_in_arcs[transition]:
      # TODO nu_arc.label will be a multiset of str instead of str in the future.
      k[arc.label].update(set(marking[arc.place].keys()))
    # for arc in self.element_out_arcs[transition]:
      # TODO nu variables.
    return k

  def enabled_modes(self, marking, transition, skip=None, only_vars=None, verbose=False):
    skip = set() if skip is None else skip
    k = self.get_transition_mode_candidates(marking, transition, only_vars=only_vars, verbose=verbose)
    if verbose:
      print(f'{k=}')
    firings = []
    nr = 0
    for mode in self._get_param_combinations(k, verbose=verbose):
      if verbose:
        print(f'{transition=} {mode=}')
      enabled, sub_marking, add_marking = self.transition_enabled(transition, marking, mode, verbose=verbose)
      nr += 1
      if enabled:
        firings.append(self.firing_type(transition, mode, sub_marking, add_marking))
    return firings, nr

  def enabled_firings(self, marking, skip=None, verbose=False):
    skip = {} if skip is None else skip
    nr = 0
    enabled_firings = []
    for transition in self.transitions:
      if verbose:
        print(f'{transition=}')
      if (skippers := skip.get(transition, set())) == 'all':
        continue
      firings, nr_ = self.enabled_modes(marking, transition, skip=skippers, verbose=verbose)
      nr += nr_
      enabled_firings.extend(firings)
    return enabled_firings, nr

  def fire_transition(self, marking: NuMarking, transition: Transition, sub_marking: NuMarking, add_marking: NuMarking, mode: dict=None):
    new_marking = marking - sub_marking + add_marking
    firing = NuFiring(transition, mode, sub_marking, add_marking)
    return new_marking, firing

  def _get_param_combinations(self, keys_ranges, verbose=False):
    for values in itertools.product(*map(keys_ranges.get, keys_ranges)):
      mode = dict(zip(keys_ranges, values))
      if verbose:
        print('mode', mode)
      try:
        self._check_mode(mode)
        yield mode
      except ValueError:
        if verbose:
          print('invalid')
        continue

  def _check_mode(self, mode: dict):
    non_empty_mode_values = [v for v in mode.values() if v != '_']
    if len(set(non_empty_mode_values)) != len(non_empty_mode_values):
      raise ValueError('Mode should be a injective mapping.')

  def blacken(self):
    arcs = [Arc(arc.source, arc.target, arc.weight) for arc in self.arcs]
    return Net(places=self.places, transitions=self.transitions, arcs=arcs)