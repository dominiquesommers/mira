from mira.nets.petri_net import MultiNuNet, System
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import MultiNuArc, VariableMultiNuArc
from mira.nets.marking import MultiNuMarking, MultiColorToken

import numpy as np


def get(view=False):
  place_names = ['oi', 'o1', 'o2', 'o3', 'pi', 'p1', 'p2', 'p3', 'di', 'd1', 'd2', 'op1', 'pd1']
  activities = ['create', 'split', 'notify', 'bill', 'load', 'retry', 'deliver', 'next', 'finish']
  p = {name: Place(name) for name in place_names}
  t = {name: Transition(name, name) for name in activities}
  n = {**p, **t}
  net = MultiNuNet(list(p.values()), list(t.values()))

  simple_arcs = {
    ('{o: 1}', '_', '_'): [('oi', 'create'), ('create', 'o1'), ('o1', 'split'), ('split', 'o2'), ('o2', 'notify'), ('notify', 'o3'), ('o3', 'bill')],
    ('_', '{p: 1}', '_'): [('p2', 'retry'), ('p2', 'deliver'), ('retry', 'p1'), ('deliver', 'p3')],
    ('_', '_', '{d: 1}'): [('di', 'load'), ('load', 'd1'), ('d1', 'retry'), ('d1', 'deliver'), ('retry', 'd2'), ('deliver', 'd2'), ('d2', 'next'), ('next', 'd1'), ('d2', 'finish')]
  }
  [net.add_arc(MultiNuArc(n[source], n[target], labels)) for labels, arcs in simple_arcs.items() for source, target in arcs]

  net.add_variable_arc(VariableMultiNuArc(n['pi'], n['split'], ('_', 'P', '_'),  [1, np.inf]))
  net.add_variable_arc(VariableMultiNuArc(n['split'], n['p1'], ('_', 'P', '_'), [1, np.inf]))
  net.add_variable_arc(VariableMultiNuArc(n['p1'], n['load'], ('_', 'P', '_'), [1, np.inf]))
  net.add_variable_arc(VariableMultiNuArc(n['load'], n['p2'], ('_', 'P', '_'), [1, np.inf]))
  net.add_variable_arc(VariableMultiNuArc(n['p3'], n['bill'], ('_', 'P', '_'), [1, np.inf]))

  net.add_arc(MultiNuArc(n['split'], n['op1'], ('{o: 1}', 'P', '_')))
  net.add_arc(MultiNuArc(n['op1'], n['bill'],  ('{o: 1}', 'P', '_')))

  net.add_arc(MultiNuArc(n['load'], n['pd1'], ('_', 'P', '{d: 1}')))
  net.add_arc(MultiNuArc(n['pd1'], n['retry'], ('_', 'P + {p: 1}', '{d: 1}')))
  net.add_arc(MultiNuArc(n['pd1'], n['deliver'], ('_', 'P + {p: 1}', '{d: 1}')))
  net.add_arc(MultiNuArc(n['retry'], n['pd1'], ('_', 'P', '{d: 1}')))
  net.add_arc(MultiNuArc(n['deliver'], n['pd1'], ('_', 'P', '{d: 1}')))

  net.add_arc(MultiNuArc(n['pd1'], n['next'], ('_', 'P + {p: 1}', '{d: 1}')))
  net.add_arc(MultiNuArc(n['next'], n['pd1'], ('_', 'P + {p: 1}', '{d: 1}')))

  net.add_arc(MultiNuArc(n['pd1'], n['finish'], ('_', '_', '{d: 1}')))


  m_i = MultiNuMarking({'oi': {MultiColorToken(3, [{'o1': 1},{},{}]): 1},
                        'pi': {MultiColorToken(3, [{},{'p1': 1},{}]): 1, MultiColorToken(3, [{},{'p2': 1},{}]): 1},
                        'di': {MultiColorToken(3, [{},{},{'d1': 1}]): 1}})
  m_f = MultiNuMarking({})#'o2': {MultiColorToken(3, [{'o1': 1},{},{}]): 1},
                        # 'p1': {MultiColorToken(3, [{},{'p1': 1},{}]): 1, MultiColorToken(3, [{},{'p2': 1},{}]): 1},
                        # 'di': {MultiColorToken(3, [{},{},{'d1': 1}]): 1}})

  sn = System(net, m_i, m_f)
  if view:
    sn.view()
  return sn