from mira.simulation.ground_truth_simulation import GroundTruthSimulation
from mira.nets.system import System
from mira.nets.marking import MultiNuMarking
from mira.nets.token import MultiColorToken
from mira.alignments.exporter import NoIndent, MyEncoder

from dateutil.relativedelta import *
from copy import deepcopy
import numpy as np
np.random.seed(0)
import json
from colorama import Fore, Style


pattern_base_dir = './datasets/deviation_patterns'
class RecordingDeviations:
  _1 = {'incorrect position': (f'{pattern_base_dir}/incorrect_position.json', {'x': 'D_complete', 'y': 'E_start'})}

class BehavioralDeviations:
  _1 = {'switch role': (f'{pattern_base_dir}/switch_role.json', {'px': 'p_o2', 'py': 'p_o3', 'x': 'O2', 'y': 'O3'})}
  _2 = {'multitask': (f'{pattern_base_dir}/multitask.json', {'p1': 'p_c_2', 'p2': 'p_o3', 'x': 'P', 'y': 'O3'})}
  _3 = {'increase capacity': (f'{pattern_base_dir}/increase_capacity_inf.json', {'c': 'p_e_c', 'r': 'C'})}

def create_synthetic_data(dev_combination, n_products=None, verbose=False, visualize=False, export=''):
  with open('./datasets/nets/omron_simple.json', 'r') as json_file:
    net_data = json.load(json_file)
    system = System.from_json_dict(net_data)

  if n_products is not None:
    system.net.object_types['P'] = set(sorted(list(system.net.object_types['P']))[:n_products])

  patterns_ = {k: v for x in dev_combination for k, v in x.items()}
  patterns = {}
  for pattern_name, (pattern_filename, mapping) in patterns_.items():
    with open(pattern_filename, 'r') as json_file:
      patterns[pattern_name] = {'net_data': json.load(json_file), 'mapping': mapping}

  # Expected objects (scheduled)
  schedules = {
  }
  # Spontaneous objects
  arrivals = {'P': lambda dt: np.random.exponential(1.0)} # TODO arrival rate.

  transition_weights = {
    t.name: (lambda dt: -1) if t.label in ['create'] else
            (lambda dt: 1)
    for t in system.transitions
  }
  transition_weights.update({tname: lambda dt: -1 for tname in ['tau_A', 'tau_B', 'tau_C', 'tau_D', 'tau_E', 'tau_F', 'tau_G']})

  # Add weights of pattern transitions
  weights = {
    'E_start_pos_D_complete': lambda dt: 0.05, 'D_complete_pos_E_start': lambda dt: 1,  # incorrect position
    'tau_switch_role_O2_O3': lambda dt: -1, 'tau_switch_role_back_O2_O3': lambda dt: -1, # switch roles
    'tau_early_release_A': lambda dt: 0.1, 'tau_late_claim_A': lambda dt: 0.0002,  # multitasking
    'tau_p_e_c_+': lambda dt: -1, 'tau_p_e_c_+_undo': lambda dt: -1,  # increase capacity
  }
  transition_weights.update(weights)

  # Add schedules for pattern transitions
  if 'increase capacity' in patterns_:
    schedules.update({
      'tau_p_e_c_+': {'c_e': ([MO, TU, WE, TH, FR], [9])},
      'tau_p_e_c_+_undo': {'c_e': ([MO, TU, WE, TH, FR], [12])}
    })
  if 'switch role' in patterns_:
    schedules.update({
      'tau_switch_role_O2_O3': {('o2', 'o2(o3)'): ([MO, TU, WE, TH, FR], [9])},
      'tau_switch_role_back_O2_O3': {('o2', 'o2(o3)'): ([MO, TU, WE, TH, FR], [10])}
    })

  durations = {(arc.source.name, arc.target.name): (lambda: (0, '')) for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}
  durations[('A_start', 'p_a_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('A_start', 'p_a_2')] = durations[('A_start', 'p_a_1')]
  durations[('B_start', 'p_b_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('B_start', 'p_b_2')] = durations[('B_start', 'p_b_1')]
  durations[('C_start', 'p_c_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('C_start', 'p_c_2')] = durations[('C_start', 'p_c_1')]
  durations[('D_start', 'p_d_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('D_start', 'p_d_2')] = durations[('D_start', 'p_d_1')]
  durations[('E_start', 'p_e_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('E_start', 'p_e_2')] = durations[('E_start', 'p_e_1')]
  durations[('F_start', 'p_f_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('F_start', 'p_f_2')] = durations[('F_start', 'p_f_1')]
  durations[('G_start', 'p_g_1')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('G_start', 'p_g_2')] = durations[('G_start', 'p_g_1')]

  sim = GroundTruthSimulation(system, patterns, transition_weights, durations, schedules, arrivals)

  # Add durations for pattern transitions
  if 'incorrect position' in patterns_:
    sim.arc_durations[('E_start_pos_D_complete', 'p_d_c')] = sim.arc_durations[('E_start_pos_D_complete', 'p_d_c')]
    sim.arc_durations[('E_start_pos_D_complete', 'p_o3_incp_')] = sim.arc_durations[('E_start_pos_D_complete', 'p_o3_incp_')]
    sim.arc_durations[('E_start_pos_D_complete', 'p_e_incp_')] = sim.arc_durations[('E_start_pos_D_complete', 'p_e_incp_')]
    sim.arc_durations[('D_complete_pos_E_start', 'p_e_1')] = sim.arc_durations[('E_start', 'p_e_1')]
    sim.arc_durations[('D_complete_pos_E_start', 'p_e_2')] = sim.arc_durations[('E_start', 'p_e_1')]

  if 'switch role' in patterns_:
    # To keep the net a valid tPNID, the objects that switch roles should be aliased and added to the object types.
    sim.system.net.object_types['O3'].add('o2(o3)')

  log = sim.simulate_log(start_time='2024-04-08T08:59:00', nr_days=1, min_log_length=1, max_log_length=800, verbose=verbose)

  pattern_counts = {}
  base_tnames = [t.name for t in sim.base_system.transitions]
  for pattern, pn in sim.patterns.items():
    if pattern == 'multitask':
      # Count how many events occur with d1/d2 and not p when early_release with d1/d2-p is fired, before late_claim with d1/d2-p is fired.
      x = 0
      taus = log.projection(lambda f: f.transition.name in ['tau_early_release_O3', 'tau_late_claim_O3'])
      taus = taus.traverse()
      for a, b in zip(taus[::2], taus[1::2]):
        d, p = a.mode['v_o3'], a.mode['v_p']
        for firing in log[log.index(a) + 1:log.index(b)]:
          if firing.mode.get('o3', None) == d and firing.mode.get('p', None) != p:
            x += 1
          break
    elif pattern == 'switch role':
      # Count how many events occur with d1 and not p when early_release with d1/d2-p is fired, before late_claim with d1/d2-p is fired.
      x = 0
      taus = log.projection(lambda f: f.transition.name in ['tau_switch_role_O2_O3', 'tau_switch_role_back_O2_O3'])
      taus = taus.traverse()
      for a, b in zip(taus[::2], taus[1::2]):
        for firing in log[log.index(a) + 1:log.index(b) + 1]:
          if 'o3' in firing.mode.keys() and firing.mode['o3'].id == 'o2(o3)':
            x += 1
      x = int(x / 2)
    elif pattern == 'incorrect position':
      x = int(len([f for f in log if f.transition.name == 'E_start_pos_D_complete']))
    elif pattern == 'increase capacity':
      # Count how many times m[p_w][w1] = 0, inbetween increase_warehouse events.
      x = check_zero_token_between_events(sim.system.m_i, log, 'tau_p_e_c_+', 'tau_p_e_c_+_undo', 'p_e_c', '([c_e])')
    else:
      add_ts = set([t.name for t in pn.transitions if t.name not in base_tnames])
      x = int(len([f for f in log if f.transition.name in add_ts]) / len(add_ts))
    pattern_counts[pattern] = x
    print(f'{pattern:<33}: {Fore.GREEN if x > 0 else Fore.RED}{x}{Style.RESET_ALL}')

  created_products = {firing.mode['p'] for firing in log if firing.transition.name == 'create'}
  destroyed_products = {firing.mode['p'] for firing in log if firing.transition.name == 'tau_end'}
  if created_products == destroyed_products:
    print(f'{Fore.GREEN}Processed all {len(created_products)} products.{Style.RESET_ALL}')
  else:
    ratio = len(destroyed_products) / len(created_products)
    print(f'{Fore.RED if ratio < 0.9 else Fore.GREEN}Did not manage to process all products ({ratio:%}): {sorted(list(created_products - destroyed_products))} are missing.{Style.RESET_ALL}')

  if visualize:
    move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.name} @ {x.timestamp}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
    log.view(element_str=move_str)

  if export != '':
    log_json = log.to_json()
    log_json['Pi'] = pattern_counts
    log_json['M'] = [NoIndent(move) for move in log_json['M']]
    log_json['r'] = [NoIndent(row) for row in log_json['r']]
    log_json['m_i'] = str(sim.base_system.m_i)
    with open(export, 'w') as json_file:
      json.dump(log_json, json_file, cls=MyEncoder, sort_keys=False, indent=2)

  return log

def check_zero_token_between_events(m_i, log, between_event_a, between_event_b, place, token_str):
  # Count how many times m[place][token_str] = 0 inbetween events.
  x = 0
  taus = log.projection(lambda f: f.transition.name in [between_event_a, between_event_b])
  taus = taus.traverse()
  for a, b in zip(taus[::2], taus[1::2]):
    left = log.prefix([a])
    m = deepcopy(m_i) - sum([f.sub_marking for f in left], start=MultiNuMarking()) + sum([f.add_marking for f in left], start=MultiNuMarking())
    v1 = [m[place][MultiColorToken.from_str(token_str)]]
    for firing in log[log.index(a) + 1:log.index(b)]:
      m = m - firing.sub_marking + firing.add_marking
      v1.append(m[place][MultiColorToken.from_str(token_str)])
    for v1_a, v1_b in zip(v1[:-1], v1[1:]):
      if v1_a != 0 and v1_b == 0:
        x += 1
  return x


if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('-no', '--n_objects', help='Number of spontaneous objects to spawn and simulate.', type=int)
  parser.add_argument('-vis', '--visualize', help='Visualize simulated log.', action='store_true')
  parser.add_argument('-e', '--export', help='./datasets/logs/omron/log_synthetic_<export>.json', type=str)
  parser.add_argument('-v', '--verbose', help='Verbose.', action='store_true')
  args = parser.parse_args()

  export_filename = f'./datasets/logs/omron/log_synthetic_{args.export}.json' if args.export is not None else ''

  Pi_L = [RecordingDeviations._1]
  Pi_S = [BehavioralDeviations._1, BehavioralDeviations._2, BehavioralDeviations._3]
  log = create_synthetic_data(Pi_L + Pi_S, n_products=args.n_objects, verbose=args.verbose, visualize=args.visualize, export=export_filename)
