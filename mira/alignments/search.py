from mira.alignments import utils
from mira.alignments.firing import Firing
from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.rc_nu_net import nu_net_utils as nu_utils

from pm4py.objects.petri_net.utils.incidence_matrix import construct as construct_incidence_matrix
from pm4py.util.lp import solver as lp_solver

from typing import Union, List
import numpy as np
import heapq
import time
import sys

class State:
  def __init__(self, marking:NuMarking, costs:dict, last_firing:Union[Firing,None], T_vec:Union[List[float],None],
               trustable:bool, parent, enabled_firings:List[Firing]):
    self.marking = marking
    self.costs = {'c': costs['c1'] + costs['c2'], **costs} # m_i -c1-> m -c2-> m_f
    self.last_firing = last_firing
    self.T_vec = T_vec
    self.trustable = trustable
    self.parent = parent
    self.enabled_firings = enabled_firings

  def __lt__(self, other):
    if self.costs['c'] != other.costs['c']:
      return self.costs['c'] < other.costs['c']
    if self.trustable and not other.trustable:
      return True
    return self.costs['c2'] < other.costs['c2']

  def __get_firing_sequence(self):
    sequence = []
    if self.parent is not None:
      sequence.extend(self.parent.__get_firing_sequence())
    if self.last_firing is not None:
      sequence.append((self.last_firing.transition, self.last_firing.mode))
    return sequence

  def get_possible_next_transitions(self):
    if self.last_firing is None:
      return list({p_out_arc.target for p in self.marking for p_out_arc in p.out_arcs})
    return list({p_out_arc.target for t_out_arc in self.last_firing.transition.out_arcs for p_out_arc in t_out_arc.target.out_arcs})

  def update_cost_to_m_f(self, cost_to_m_f):
    self.costs['c'] = self.costs['c'] - self.costs['c2'] + cost_to_m_f
    self.costs['c2'] = cost_to_m_f

  def __repr__(self):
    return f'STATE\n{self.marking}\nm_i -{self.costs["c1"]}-> m -{self.costs["c2"]}-> m_f ({"" if self.trustable else "not "}trustable)\n{self.last_firing}\n' + \
           f'prev firing: {self.parent.last_firing if self.parent is not None else "None"}\n'


class Search:
  def __init__(self, nu_net:NuPetriNet, m_i:NuMarking, m_f:NuMarking, costs:dict=None, skip:str=utils.SKIP):
    self.nu_net = nu_net
    self.m_i = m_i
    self.m_f = m_f
    self.skip = skip
    if costs is None:
      costs = {t: utils.STD_TAU_COST if utils.is_model_move(t, self.skip) and t.label[1] is None else
                  utils.STD_MODEL_LOG_MOVE_COST if utils.is_model_move(t, self.skip) or utils.is_log_move(t, self.skip) else
                  utils.STD_SYNC_COST for t in nu_net.transitions}
    self.costs = costs

  def check(self):
    # TODO check for easy soundness.
    ...

  def __prepare(self):
    self.start_time = time.time()
    nu_utils.decorate_transitions_prepostset(self.nu_net)
    nu_utils.decorate_places_preset_trans(self.nu_net)

    self.black_nu_net, self.place_map, self.transition_map = self.nu_net.remove_color()
    self.black_costs = {self.transition_map[t]: cost for t, cost in self.costs.items()}
    self.black_m_i, self.black_m_f = self.m_i.remove_color(self.place_map), self.m_f.remove_color(self.place_map)

    self.incidence_matrix = construct_incidence_matrix(self.black_nu_net)
    self.m_i_vec, self.m_f_vec, self.cost_vec = self.__vectorize_initial_final_cost(self.incidence_matrix, self.black_m_i,
                                                                                    self.black_m_f, self.black_costs)

    self.a_matrix = np.asmatrix(self.incidence_matrix.a_matrix).astype(np.float64)
    self.g_matrix = -np.eye(len(self.nu_net.transitions))
    self.h_cvx = np.matrix(np.zeros(len(self.nu_net.transitions))).transpose()
    self.cost_vec = [x * 1.0 for x in self.cost_vec]

    self.use_cvxopt = lp_solver.DEFAULT_LP_SOLVER_VARIANT in \
                        [lp_solver.CVXOPT_SOLVER_CUSTOM_ALIGN, lp_solver.CVXOPT_SOLVER_CUSTOM_ALIGN_ILP]

    if self.use_cvxopt:
      from cvxopt import matrix
      self.a_matrix = matrix(self.a_matrix)
      self.g_matrix = matrix(self.g_matrix)
      self.h_cvx =    matrix(self.h_cvx)
      self.cost_vec = matrix(self.cost_vec)

  def __check_break(self, timeout:float, memory_threshold:float):
    return None

  def __check_still_enabled(self, marking, firing):
    try:
      marking - firing.sub_marking
    except ValueError:
      return False
    return True

  def compute_cheapest_run(self, max_search_time:float=np.inf, min_memory_available:float=0):
    self.__prepare()

    closed_markings = set()
    cost_to_m_f, T_vec = self.__compute_exact_heuristic_new_version(self.black_m_i)

    trans_empty_preset = [t for t in self.nu_net.transitions if len(t.in_arcs) == 0]

    open_set = [State(self.m_i, {'c1': 0, 'c2': cost_to_m_f}, last_firing=None, T_vec=T_vec, trustable=True, parent=None,
                      enabled_firings=[])]

    heapq.heapify(open_set)
    search_stats = {'visited': 0, 'queued': 0, 'traversed': 0, 'lp_solved': 1}

    current_state = None
    while len(open_set) > 0:
      if (break_reason := self.__check_break(max_search_time, min_memory_available)) is not None:
        return {'state': current_state, 'error': break_reason, 'stats': {**search_stats, 'duration': time.time() - self.start_time}}

      current_state = heapq.heappop(open_set)

      # Get the next trusted state if current is not trusted
      while not current_state.trustable:
        if (break_reason := self.__check_break(max_search_time, min_memory_available)) is not None:
          return {'state': current_state, 'error': break_reason, 'stats': {**search_stats, 'duration': time.time() - self.start_time}}

        if current_state.marking in closed_markings:
          current_state = heapq.heappop(open_set)
          continue

        cost_to_m_f, T_vec = self.__compute_exact_heuristic_new_version(current_state.marking.remove_color(self.place_map))
        search_stats['lp_solved'] += 1

        current_state.trustable = True
        current_state.update_cost_to_m_f(cost_to_m_f)
        current_state = heapq.heappushpop(open_set, current_state)

      if current_state.costs['c2'] > lp_solver.MAX_ALLOWED_HEURISTICS or current_state.marking in closed_markings:
        continue

      if current_state.costs['c2'] < 0.01:
        if current_state.marking == self.m_f:
          return {'state': current_state, 'stats': {**search_stats, 'duration': time.time() - self.start_time}}

      closed_markings.add(current_state.marking)
      search_stats['visited'] += 1

      # enabled_firings = parent's still enabled firings + new firings
      parents_enabled_firings = [] if current_state.parent is None else current_state.parent.enabled_firings
      enabled_firings = [firing for firing in parents_enabled_firings if self.__check_still_enabled(current_state.marking, firing)]

      for transition in current_state.get_possible_next_transitions() + trans_empty_preset:
        ignore_modes = [firing.mode for firing in enabled_firings if firing.transition == transition]
        modes = nu_utils.find_matches(transition, current_state.marking, ignore_modes=ignore_modes, ret_add=True)
        for (mode, sub_marking, add_marking) in modes:
          enabled_firings.append(Firing(transition, mode, sub_marking, add_marking, self.costs[transition]))

      current_state.enabled_firings = enabled_firings
      for enabled_firing in enabled_firings:
        search_stats['traversed'] += 1

        new_marking = enabled_firing.get_new_marking(current_state.marking)
        if new_marking in closed_markings:
          continue

        search_stats['queued'] += 1

        cost_to_m_f, T_vec = self.__derive_heuristic(current_state.T_vec, self.transition_map[enabled_firing.transition], current_state.costs['c2'])
        trustable = self.__trust_solution(T_vec)

        new_state = State(new_marking, {'c1': current_state.costs['c1'] + enabled_firing.cost, 'c2': cost_to_m_f},
                          enabled_firing, T_vec, trustable, current_state, enabled_firings=[])
        heapq.heappush(open_set, new_state)

    raise ValueError('Alignment failed due to unknown reason.')

  def __vectorize_initial_final_cost(self, incidence_matrix:np.matrix, m_i:NuMarking, m_f:NuMarking, costs:dict):
    m_i_vec = incidence_matrix.encode_marking(m_i)
    m_f_vec = incidence_matrix.encode_marking(m_f)
    cost_vec = [0] * len(costs)
    for t in costs.keys():
      cost_vec[incidence_matrix.transitions[t]] = costs[t]
    return m_i_vec, m_f_vec, cost_vec

  def __compute_exact_heuristic_new_version(self, marking:NuMarking, strict:bool=True):
    m_vec = self.incidence_matrix.encode_marking(marking)
    b_term = [i - j for i, j in zip(self.m_f_vec, m_vec)]
    b_term = np.matrix([x * 1.0 for x in b_term]).transpose()

    if not strict:
      self.g_matrix = np.vstack([self.g_matrix, self.a_matrix])
      self.h_cvx = np.vstack([self.h_cvx, b_term])
      self.a_matrix = np.zeros((0, self.a_matrix.shape[1]))
      b_term = np.zeros((0, b_term.shape[1]))

    if self.use_cvxopt:
      # not available in the latest version of PM4Py
      from cvxopt import matrix
      b_term = matrix(b_term)

    sol = lp_solver.apply(self.cost_vec, self.g_matrix, self.h_cvx, self.a_matrix, b_term,
                          parameters={"solver": "glpk"}, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)
    prim_obj = lp_solver.get_prim_obj_from_sol(sol, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)
    points = lp_solver.get_points_from_sol(sol, variant=lp_solver.DEFAULT_LP_SOLVER_VARIANT)

    prim_obj = prim_obj if prim_obj is not None else sys.maxsize
    points = points if points is not None else [0.0] * len(self.black_nu_net.transitions)

    return prim_obj, points

  def __derive_heuristic(self, x, t, h):
    x_prime = x.copy()
    x_prime[self.incidence_matrix.transitions[t]] -= 1
    return max(0, h - self.cost_vec[self.incidence_matrix.transitions[t]]), x_prime

  def __trust_solution(self, x):
    for v in x:
      if v < -0.001:
        return False
    return True
