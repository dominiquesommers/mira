from itertools import chain, combinations, product


def powerset(iterable, exclude_emptyset=True):
  s = list(iterable)
  return chain.from_iterable(combinations(s, r) for r in range(int(exclude_emptyset), len(s) + 1))

def partitions(collection):
  if len(collection) == 1:
    yield [collection]
    return

  first = collection[0]
  for smaller in partitions(collection[1:]):
    # insert `first` in each of the subpartition's subsets
    for n, subset in enumerate(smaller):
      yield smaller[:n] + [[first] + subset] + smaller[n + 1:]
    # put `first` in its own subset
    yield [[first]] + smaller

def values_combinations(d):
  # keys_ranges = {v[0]: v[1] for v in d.items()}
  keys = list(d)
  for values in product(*map(d.get, keys)):
    yield dict(zip(keys, values))


if __name__ == '__main__':
  from mira.nets.arc import Var
  V = {'P': {Var('p'), Var('q')}, 'D': {Var('d')}}
  xx = values_combinations({r: powerset(s) for r, s in V.items()})
  print(list(xx))
  print([set.union(*[set(x) for x in pc.values()]) for pc in xx])