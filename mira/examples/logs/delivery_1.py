from mira.logs.event_log import Log
from mira.logs.event import Event
from mira.prelim.multiset import Multiset

import numpy as np


def get(example=1, view=False):
  events = [
    Event('create', Multiset(args={'p1': 1})),
    Event('order home', Multiset(args={'p1': 1})),
    Event('register depot', Multiset(args={'p1': 1, 'w1': 1})),
    Event('deliver depot', Multiset(args={'p1': 1, 'd1': 1, 'w1': 1})),
    Event('collect', Multiset(args={'p1': 1, 'w1': 1})),
    Event('create', Multiset(args={'p2': 1})),
    Event('order depot', Multiset(args={'p2': 1})),
    Event('ring', Multiset(args={'p2': 1, 'd1': 1})),
    Event('register depot', Multiset(args={'p2': 1, 'w1': 1})),
    Event('deliver depot', Multiset(args={'p2': 1, 'd1': 1, 'w1': 1})),
    Event('collect', Multiset(args={'p2': 1, 'w1': 1})),
  ]
  log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'p1', 'p2'}, {'d1'}, {'w1'}))
  if view:
    log.view()

  return log


if __name__ == '__main__':
  log = get(view=True)
