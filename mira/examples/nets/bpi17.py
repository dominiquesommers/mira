from mira.nets.petri_net import MultiNuNet, System
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import MultiNuArc, VariableMultiNuArc
from mira.nets.marking import MultiNuMarking, MultiColorToken

import numpy as np
from copy import deepcopy

def get(entities=None, view=False):
  if entities is None:
    entities = (set(), set())

  place_names = ['a1', 'a2', 'a3', 'a4', 'o1', 'o2', 'o3', 'o4']
  activities = ['Create application', 'Accept', 'Create offer', 'Validate', 'Send (mail and online)', 'Call']
  p = {name: Place(name) for name in place_names}
  t = {name: Transition(name, name) for name in activities}
  t.update({name: Transition(name, None) for name in ['s1', 's2']})
  n = {**p, **t}
  net = MultiNuNet(list(p.values()), list(t.values()))

  simple_arcs = {
    ('{a: 1}', '_'): [('a1', 'Create application'), ('Create application', 'a2'), ('a2', 'Accept'), ('Accept', 'a3'), ('a3', 'Call'), ('Call', 'a3'),
                      ('a3', 'Create offer'), ('Create offer', 'a3'), ('a3', 'Validate'), ('Validate', 'a4'), ('a4', 's2'), ('s2', 'a3')],
    ('_', '{o: 1}'): [('o1', 'Create offer'), ('Create offer', 'o2'),
                      ('o2', 'Send (mail and online)'), ('Send (mail and online)', 'o3'),
                      ('o3', 's1'), ('o3', 'Call'), ('s1', 'o4'), ('Call', 'o4')]
  }

  [net.add_arc(MultiNuArc(n[source], n[target], labels)) for labels, arcs in simple_arcs.items() for source, target in arcs]
  # net.add_variable_arc(VariableMultiNuArc(n['o1'], n['Create offer'], ('_', 'O'), [1, np.inf]))
  # net.add_variable_arc(VariableMultiNuArc(n['Create offer'], n['o2'], ('_', 'O'), [1, np.inf]))

  m_i = MultiNuMarking({'a1': {MultiColorToken(2, [{entity: 1},{}]): 1 for entity in entities[0]},
                        'o1': {MultiColorToken(2, [{},{entity: 1}]): 1 for entity in entities[1]}})
  m_f = MultiNuMarking({'a3': deepcopy(m_i['a1']), 'o4': deepcopy(m_i['o1'])})

  sn = System(net, m_i, m_f)
  if view:
    sn.view()
  return sn
