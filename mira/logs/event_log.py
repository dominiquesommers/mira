from mira.logs.event import Event
from mira.prelim.poset import POSet
from mira.prelim.multiset import Multiset

from typing import Iterable, Union
import numpy as np


class Log(POSet):
  def __init__(self, events:Iterable[Event]=None, r:np.matrix=None, is_already_closed:bool=False, object_types:Union[set,dict]=None):
    super().__init__(events, r, is_already_closed)

    self.object_types = {role: set(role_objects) for role, role_objects in object_types.items()} if object_types is not None else {}

  def view(self, minimal:bool=True, element_str=lambda x: f'{x}', colors=lambda x: 'black', name:str='log', rankdir:str='LR'):
    # TODO proper element_str and colors functions.
    super().view(minimal, element_str=element_str, colors=colors, name=name, rankdir=rankdir)

  @classmethod
  def from_json_dict(cls, data):
    events = [Event(event[0], Multiset.from_str(event[1], str)) for event in data['events']]
    nr_events = len(events)
    if (partial_order := data.get('r', None)) is not None:
      partial_order = np.matrix(partial_order)
    elif (directly_follows := data.get('<', 'all')) == 'all':
      partial_order = np.matrix(1 - np.tri(nr_events, nr_events, dtype=int))
    else:
      partial_order = np.matrix(np.zeros((nr_events, nr_events), dtype=int))
      for i, j in directly_follows:
        partial_order[i, j] = 1

    for i, j in data.get('||', []):
      partial_order[i, j] = 0

    already_closed = ('r' in data.keys()) or ((directly_follows == 'all') and (len(data.get('||', [])) == 0))

    return Log(events, partial_order, already_closed, data['O'])
