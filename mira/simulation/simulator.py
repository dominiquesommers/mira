from datetime import datetime, timedelta
from collections import defaultdict
from copy import deepcopy
import numpy as np
import random
import simpy
from colorama import Fore, Style


class PetrinetSimulator:
  def __init__(self, system, repeated_firings=None, arc_durations=None):
    self.system = system
    self.repeated_firings = [] if repeated_firings is None else repeated_firings
    self.arc_durations = arc_durations
    self.simulation_environment = None
    self.freed = 0

  def simulate_log(self, start_time, nr_days=1, min_log_length=0, max_log_length=np.inf, verbose=0, visualize=False):
    self.start_time = datetime.fromisoformat(start_time) if isinstance(start_time, str) else start_time
    self.min_log_length = min_log_length
    self.max_log_length = max_log_length
    self.__reset_simulation()
    self.verbose = verbose

    self.simulate_repeated_firings()
    self.simulation_environment.process(self.__simulate_step())
    self.simulation_environment.run(until=86400*nr_days)
    return self.system.firings

  def simulate_repeated_firings(self):
    for firing, arguments, duration_until_next in self.repeated_firings:
      self.simulation_environment.process(self.__simulate_repeating_firing(firing, arguments, duration_until_next))

  def __simulate_repeating_firing(self, firing, arguments, duration_until_next):
    current_time = self.start_time + timedelta(seconds=self.simulation_environment.now)
    duration = duration_until_next(current_time)
    yield self.simulation_environment.timeout(duration)
    try:
      firings = firing(*arguments)
    except StopIteration as e:
      # Repeated firings have stopped as the iterator for sampling the firings is emptied.
      return

    if len(firings) == 0:
      # When the firing is not enabled, it should be fired at the first point when it is enabled.
      #   (e.g., by keeping track of a list of firings that are usually skipped in self.__simulate_step().)
      self.delayed_firings.add((firing, tuple(arguments)))



    firings = [f for f in firings if f.sub_marking <= self.system.marking]
    if self.verbose:
      print('Enabled firings:\n', firings)
    if len(firings) > 0:
      self.simulation_environment.process(self.__simulate_firing(firings))
      self.simulation_environment.process(self.__simulate_step())
    self.simulation_environment.process(self.__simulate_repeating_firing(firing, arguments, duration_until_next))

  def __simulate_step(self, verbose=False):
    current_time = self.start_time + timedelta(seconds=self.simulation_environment.now)
    for delayed_firing, arguments in self.delayed_firings:
      firings = delayed_firing(*arguments)
      if len(firings) > 0:
        self.delayed_firings.remove((delayed_firing, arguments))
        self.simulation_environment.process(self.__simulate_firing(firings, verbose=verbose))
        return

    enabled_firings, nr = self.system.enabled_firings(verbose=False)
    enabled_firings = [firing for firing in enabled_firings if firing.transition.weight(current_time) >= 0]

    if len(enabled_firings) == 0:
      return
    self.simulation_environment.process(self.__simulate_firing(enabled_firings, verbose=verbose))
    yield self.simulation_environment.timeout(0)
    self.simulation_environment.process(self.__simulate_step())

  def __simulate_firing(self, firings, verbose=False):
    if len(firings) == 0:
      return

    current_time = self.start_time + timedelta(seconds=self.simulation_environment.now)

    # Sample firing based on transition weights
    weights = [firing.transition.weight(current_time) for firing in firings]
    if all(w == 0 for w in weights) or all(w < 0 for w in weights):
      weights = [1] * len(firings)

    firing = random.choices(firings, weights=weights)[0]
    if self.verbose:
      print(f'{Fore.CYAN}{self.start_time + timedelta(seconds=self.simulation_environment.now)}: fire: {firing=}{Style.RESET_ALL}')

    zero_add_marking = type(firing.add_marking)()
    # Sample firing duration per arc
    duration_arcs = defaultdict(set)
    for arc in self.system.net.element_out_arcs[firing.transition]:
      dur_sample, expl = self.arc_durations.get((arc.source.name, arc.target.name), lambda: (0, ''))()
      duration = max(0, dur_sample) * 60
      if duration == 0:
        zero_add_marking += type(firing.add_marking)(args={arc.target: firing.add_marking[arc.target]})
      else:
        duration_arcs[duration].add(arc)

    firing.duration = duration_arcs
    firing.timestamp = self.start_time + timedelta(seconds=self.simulation_environment.now)

    if firing.sub_marking > self.system.marking:
      return

    self.system.fire(firing, self.system.marking - firing.sub_marking + zero_add_marking)

    yield self.simulation_environment.timeout(0)
    for index, (duration, arcs) in enumerate(duration_arcs.items()):
      # N.B. For this to work, arcs should be a set (not multiset) (which is in definition of tPNID) but not enforced in code.
      add_marking = type(firing.add_marking)(args={arc.target: firing.add_marking[arc.target] for arc in arcs})
      self.simulation_environment.process(self.__simulate_token_production(add_marking, duration, from_firing=firing, visualize=False))
    self.simulation_environment.process(self.__simulate_step())

  def __simulate_token_production(self, add_marking, wait_time, from_firing=None, visualize=False):
    yield self.simulation_environment.timeout(wait_time)
    self.system.marking = self.system.marking + add_marking

    if len(self.system.firings) > self.min_log_length and self.system.m_f == self.system.marking:
      print('Final marking reached.')
      return

    if len(self.system.firings) >= self.max_log_length:
      print('Aborting due to max trace length reached.')
      return

    self.simulation_environment.process(self.__simulate_step())

  def __reset_simulation(self):
    self.simulation_environment = simpy.Environment()
    self.system.marking = deepcopy(self.system.m_i)
    self.system.reset_firings()
    self.delayed_firings = set()
