from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.prelim.multiset import Multiset
from mira.prelim.utils import split_by_comma
from mira.prelim.colors import Color

import re
from typing import Union


class Arc:
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], weight: int=1):
    if type(source) == type(target):
      raise TypeError(f'Petri net is a bipartite graph, i.e., source and target must be of different types ({type(source)}).')
    self.__source = source
    self.__target = target
    self.__place, self.__transition = (source, target) if isinstance(source, Place) else (target, source)
    self.weight = weight

  @property
  def source(self):
    return self.__source

  @source.setter
  def source(self, source):
    if type(source) != type(self.__source):
      raise TypeError(f'New source should be same as same previous source: {type(self.__source).__name__} not {type(source).__name__}.')

    self.__source = source
    if isinstance(source, Place):
      self.__place = source
    else:
      self.__transition = source

  @property
  def target(self):
    return self.__target

  @target.setter
  def target(self, target):
    if type(target) != type(self.__target):
      raise TypeError(f'New target should be same as same previous target: {type(self.__target).__name__} not {type(target).__name__}.')
    self.__target = target
    if isinstance(target, Place):
      self.__place = target
    else:
      self.__transition = target

  @property
  def place(self):
    return self.__place

  @place.setter
  def place(self, place):
    if not isinstance(place, Place):
      raise TypeError(f'New place should be of type Place, is {type(place).__name__}.')
    self.__place = place
    if isinstance(self.target, Place):
      self.__target = place
    else:
      self.__source = place

  @property
  def transition(self):
    return self.__transition

  @transition.setter
  def transition(self, transition):
    if not isinstance(transition, Transition):
      raise TypeError(f'New transition should be of type Transition, is {type(transition).__name__}.')
    self.__transition = transition
    if isinstance(self.target, Transition):
      self.__target = transition
    else:
      self.__source = transition

  def __repr__(self):
    return f'{self.source} -> {self.target}'

  def __str__(self):
    return self.__repr__()

  def __eq__(self, other):
    return self.source == other.source and self.target == other.target and self.weight == other.weight

  def __hash__(self):
    return hash(self.source) + hash(self.target) + hash(self.weight)

  def blank_label(self):
    return True

  def view(self, source_node, target_node, viz, font_size=16, debug=False):
    label = '' if self.weight == 1 else f'{self.weight}'
    viz.edge(source_node, target_node, label=label, color=Color.BLACK, penwidth='1', fontsize=str(font_size), arrowhead='normal', style='')


class InhibitorArc(Arc):
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition]):
    super().__init__(source, target, 1)

  def __eq__(self, other):
    return super().__eq__(other) and isinstance(other, InhibitorArc)

  def __hash__(self):
    return super().__hash__() + hash('inhibitor')

  def __repr__(self):
    return f'{str(self.source):<20} -o {self.target}'

  def view(self, source_node, target_node, viz, font_size='12', debug=False):
    viz.edge(source_node, target_node, label='', color=Color.BLACK, penwidth='1', fontsize=str(font_size), arrowhead='odot', style='')


class NuArc(Arc):
  # TODO maybe change to multiset of labels and weight = len(labels).
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], label: str, weight: int=1):
    super().__init__(source, target, weight)
    self.label = label

  def __repr__(self):
    return f'{self.source} -{self.label}-> {self.target}'

  def __eq__(self, other):
    return super().__eq__(other) and self.label == other.label

  def __hash__(self):
    return super().__hash__() + hash(self.label)

  def blank_label(self):
    # TODO when label is a multiset of labels (future) this has to be updated.
    return self.label == '_'

  def view(self, source_node, target_node, viz, font_size='12', debug=False):
    # TODO weight in nuarc will be determined by labels in the future.
    label = f'{self.label}' if self.weight == 1 else f"{self.weight}·{self.label}"
    viz.edge(source_node, target_node, label=label, color=Color.BLACK, penwidth='1', fontsize=str(font_size),
             arrowhead='normal', style='')


class InhibitorNuArc(NuArc):
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], label: str):
    super().__init__(source, target, label, 1)

  def __eq__(self, other):
    return super().__eq__(other) and isinstance(other, InhibitorArc)

  def __hash__(self):
    return super().__hash__() + hash('inhibitor')

  def __repr__(self):
    return f'{str(self.source):<20} -o {self.target}'

  def view(self, source_node, target_node, viz, font_size='12', debug=False, **kwargs):
    # TODO color based on for which indices the label is not '_'.
    label = f'{self.label}'
    parameters = {'penwidth': '1', 'color': Color.BLACK, **kwargs}
    viz.edge(source_node, target_node, label=label, fontsize=str(font_size), arrowhead='odot', style='', **parameters)


class MultiNuArc(Arc):
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], labels: Multiset):
    super().__init__(source, target, 1)
    self.labels = labels

  @property
  def labels(self):
    return self.__labels

  @labels.setter
  def labels(self, labels):
    if labels.key_type == MultiVarExpr and all(isinstance(label, VarExpr) for labels_ in labels.keys() for label in labels_):
      self.__labels = labels
    else:
      self.__labels = Multiset(args={MultiVarExpr(size=len(l), varexprs=l): n for l, n in labels.items()})

  def __repr__(self):
    return f'{str(self.source):<21} -{str(self.labels):<13}-> {self.target}'

  def __eq__(self, other):
    return super().__eq__(other) and self.labels == other.labels

  def __hash__(self):
    return super().__hash__() + hash(self.labels)

  def blank_label(self):
    return all(expr.empty() for expr in self.labels)

  def view(self, source_node, target_node, viz, font_size='12', debug=False, **kwargs):
    # TODO color based on for which indices the label is not '_'.
    label = f'{self.labels}'
    parameters = {'penwidth': '1', 'color': Color.BLACK, **kwargs}
    viz.edge(source_node, target_node, label=label, fontsize=str(font_size), arrowhead='normal', style='', **parameters)


class InhibitorMultiNuArc(MultiNuArc):
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], labels: Multiset):
    super().__init__(source, target, labels)

  def __eq__(self, other):
    return super().__eq__(other) and isinstance(other, InhibitorArc)

  def __hash__(self):
    return super().__hash__() + hash('inhibitor')

  def __repr__(self):
    return f'{str(self.source):<20} -o {self.target}'

  def view(self, source_node, target_node, viz, font_size='12', debug=False, **kwargs):
    # TODO color based on for which indices the label is not '_'.
    label = f'{self.labels}'
    parameters = {'penwidth': '1', 'color': Color.BLACK, **kwargs}
    viz.edge(source_node, target_node, label=label, fontsize=str(font_size), arrowhead='odot', style='', **parameters)


class VariableMultiNuArc(MultiNuArc):
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], labels: Union[tuple, str], cardinality):
    super().__init__(source, target, labels)
    self.cardinality = cardinality

  def view(self, source_node, target_node, viz, font_size='16', debug=False, **kwargs):
    super().view(source_node, target_node, viz, font_size, debug, penwidth='1', color='black:white:black', arrowsize='1.2')


class Var:
  value_check = staticmethod(str.lower)
  def __init__(self, var: str):
    self._check_type(var)
    self._check_value(var)
    self.var = var

  def _check_type(self, var):
    if not isinstance(var, str):
      raise TypeError(f'{type(self).__name__} should be str ({type(var).__name__}).')

  def _check_value(self, var):
    if var != var.lower():
      raise ValueError(f'{type(self).__name__} should be all lowercase.')

  def __repr__(self):
    return self.var
    # return 'ε' if self.var == '_' else self.var

  def __str__(self):
    return self.__repr__()

  def is_empty(self):
    return self.var == '_'

  def __eq__(self, other):
    return self.var == other.var

  def __hash__(self):
    return hash(self.var)

  def __lt__(self, other):
    return self.var < other.var


class SetVar(Var):
  value_check = staticmethod(str.upper)
  def __init__(self, var: str):
    super().__init__(var)

  def to_var(self):
    return Var(f'{self.var[0].lower()}{self.var[1:]}')

  def _check_value(self, var):
    if var[0] != var[0].upper():
      raise ValueError(f'{type(self).__name__} should start with uppercase.')

  def __eq__(self, other):
    return self.var == other.var

  def __hash__(self):
    return hash(self.var)

  def is_empty(self):
    return self.var == '_'



class VarMultiset(Multiset):
  def __init__(self, args=None):
    super().__init__(Var, args)


class VarExpr:
  def __init__(self, expression):
    self.expression_str = expression
    self.setvars, self.vars = self.extract_vars()
    self.expression = expression
    self.fix_expression()
    # TODO check whether expression is safe (should come from extract_vars already).

  def fix_expression(self):
    for repl, by in [('{', 'Multiset(args={'), ('}', '})'), ('cup', '.max'), ('cap', '.min'), ('⊔', '.max'), ('⊓', '.min')]:
      self.expression = self.expression.replace(repl, by)
    # TODO add brackets for max and min

  def extract_vars(self):
    elements = re.split('\+|\-|⊔|⊓|cap|cup', re.sub('\(|\)|\s', '', self.expression_str))
    setvars, vars = set(), set()
    for element in elements:
      if element[0] == '{' and element[-1] == '}':
        vars.update({Var(x.split(':')[0]) for x in element[1:-1].split(',')})
      else:
        setvars.add(SetVar(element))
    return setvars, vars

  def to_var(self):
    if len(self.setvars) > 1 or len(self.vars) > 0:
      raise ValueError(f'Can only convert VarExpr to a var when it contains a single SetVar.')
    return list(self.setvars)[0].to_var()

  def evaluate(self, mode):
    res = eval(self.expression, {}, {'Multiset': Multiset, **mode})
    if not isinstance(res, Multiset) or res.negative():
      # TODO create special error for this.
      raise ValueError('Evaluated multiset has negative values.')
    return res

  def __repr__(self):
    mode = {v.var: v.var for v in self.setvars.union(self.vars)}
    exprs = '+'.join(str(eval(e, {}, {'Multiset': Multiset, **mode})) for e in self.expression.split('+'))
    # TODO split on '-' as well.
    return exprs

  def __str__(self):
    return self.__repr__()

  def __hash__(self):
    return hash(str(self))

  def __eq__(self, other):
    # TODO check
    return str(self) == str(other)

  def __add__(self, other):
    if other.empty() or self.expression_str == other.expression_str:
      return VarExpr(self.expression_str)
    if self.empty():
      return VarExpr(other.expression_str)
    return VarExpr(f'({self.expression_str}) ⊔ ({other.expression_str})')

  def empty(self):
    return re.sub('\(|\)|\s', '', self.expression_str) == '_'

  def projection(self, vars):
    vars_proj = set(var for var in self.vars if var in vars)
    setvars_proj = set(var for var in self.setvars if var in vars)

    mode = {**{str(var): var if var in vars_proj else '_' for var in self.vars},
            **{str(var): var if var in setvars_proj else '_' for var in self.setvars}}
    ee = self.evaluate(mode)
    ee.pop('_', None)
    if len(ee.keys()) == 0:
      return None

    return VarExpr(ee.to_dict_str())

  def add_postfixes(self, postfix='_'):
    if postfix == '':
      return self
    mode = {**{str(var): f'{var}{postfix}' for var in self.vars},
            **{str(var): f'{var}{postfix}' for var in self.setvars}}
    ee = self.evaluate(mode)
    return VarExpr(ee.to_dict_str())

class MultiVarExpr(tuple):
  def __new__(self, size, varexprs=None):
    if len(varexprs) != size:
      raise ValueError(f'Varexprs incorrect size ({len(varexprs)}), should be {size}.')

    multisets = [m if isinstance(m, VarExpr) else VarExpr(m) for m in varexprs]
    return super().__new__(self, multisets)

  @classmethod
  def from_str(cls, string):
    if string[0] != '(' or string[-1] != ')':
      raise ValueError(f'Tuple string should by of form: (<els>), is {string}.')
    multisets = []
    for element in split_by_comma(string[1:-1]):
      xx = []
      for element2 in split_by_comma(element, '+'): # TODO split on - too?
        element_ms_str = Multiset.from_str(element2, str)
        if isinstance(element_ms_str, Multiset):
          element_ms_str = element_ms_str.to_dict_str()
        xx.append(element_ms_str)
      multisets.append('+'.join(xx))

    return MultiVarExpr(size=len(multisets), varexprs=tuple(multisets))

  def __repr__(self):
    tuple_str = super().__repr__()
    if tuple_str[-2:] == ',)':
      return f'{tuple_str[:-2]})'
    return tuple_str

  def __str__(self):
    return self.__repr__()





class MultiNuArcDep(Arc):
  def __init__(self, source: Union[Place, Transition], target: Union[Place, Transition], labels: Union[tuple, str], weight: int=1):
    super().__init__(source, target, weight)
    if not (isinstance(labels, list) or isinstance(labels, tuple)):
      labels = [labels]
    self.labels = tuple([VarExpr(str(label)) for label in labels])

  def __repr__(self):
    return f'{str(self.source):<21} -{str(self.labels):<13}-> {self.target}'

  def __eq__(self, other):
    return super().__eq__(other) and self.labels == other.labels

  def __hash__(self):
    return super().__hash__() + hash(self.labels)

  def blank_label(self):
    return all(expr.empty() for expr in self.labels)

  def view(self, source_node, target_node, viz, font_size='12', debug=False, **kwargs):
    # TODO color based on for which indices the label is not '_'.
    label = f'{self.labels[0]}' if len(self.labels) == 1 else f'{self.labels}'
    label = f'{self.weight}·{label}' if self.weight != 1 else label
    parameters = {'penwidth': '1', 'color': Color.BLACK, **kwargs}
    viz.edge(source_node, target_node, label=label, fontsize=str(font_size), arrowhead='normal', style='', **parameters)

if __name__ == '__main__':
  from mira.nets.place import Place

  variable_types = {'P': {Var('nu_p'), Var('p')}, 'D': {Var('d')}, 'W': {Var('w')}}
  labels = Multiset(args={('{d: 1}', '{p: 1}'): 2})
  labels = Multiset(args={tuple([VarExpr(str(label)) for label in l]): n for l, n in labels.items()})
  print(labels)

  a = variable_types['W']
  labels_proj = Multiset(args={l_tuple: n for l, n in labels.items() if len(l_tuple := tuple([ll_proj for ll in l if (ll_proj := ll.projection(a)) is not None])) > 0})
  print(labels_proj)
  print(bhs)

  ab = Multiset(args={'a': 1, 'b': 2})
  a  = Multiset(args={'a': 1})
  b  = Multiset(args={'b': 2})
  c = Multiset(args={'c': 1})

  mode = {'_': Multiset(str, {}), 'XY': ab, 'X': a, 'Y': b, 'Z': c}
  print(VarExpr(f'XY - X + Y').evaluate(mode))
  print(VarExpr('XY - (X + Y)').evaluate(mode))

  print(VarExpr('XY').evaluate(mode))
  print(VarExpr('XY + XY').evaluate(mode))
  print(VarExpr('(X + Y) ⊔ (X)').evaluate(mode))

  print(VarExpr('(_) ⊔ (X)').evaluate(mode))
