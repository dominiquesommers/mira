from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.rc_nu_net.nu_net_utils import add_arc_from_to
from copy import deepcopy

class Relaxations:
  def __init__(self, sync_net):
    self.sync_net = sync_net
    self.transitions = {transition.name: transition for transition in sync_net.transitions}


  def add_relaxations(self):
    for transition_name, transition in self.transitions.items():
      synchronous = False
      if transition_name[0] == '>>':
        relaxed_name = (f"{transition.name[0]}", f"{transition.name[1]}'")
        relaxed_label = (f"{transition.label[0]}", f"{transition.label[1]}'")
      elif transition_name[1] == '>>':
        relaxed_name = (f"{transition.name[0]}'", f"{transition.name[1]}")
        relaxed_label = (f"{transition.label[0]}'", f"{transition.label[1]}")
      else:
        synchronous = True
        relaxed_name = (f"{transition.name[0]}'", f"{transition.name[1]}'")
        relaxed_label = (f"{transition.label[0]}'", f"{transition.label[1]}'")

      if not synchronous:
        relaxed_model_move = NuPetriNet.Transition(relaxed_name, relaxed_label)
        incoming_places = [arc.source for arc in transition.in_arcs]
        if any('resource' in place.reference.properties for place in incoming_places):
          relaxed_model_move.reference = transition
          self.sync_net.transitions.add(relaxed_model_move)
          relaxed_model_move.properties[transition] = deepcopy(transition.properties)
          for a in transition.in_arcs:
            place = a.source
            if 'resource' not in place.reference.properties:
              add_arc_from_to(place, relaxed_model_move, self.sync_net, labels=a.labels)
          for a in transition.out_arcs:
            place = a.target
            if 'resource' not in place.reference.properties:
              add_arc_from_to(relaxed_model_move, place, self.sync_net, labels=a.labels)
      else:
        print('TODO Make case relaxed and resource relaxed sync move from this one.')



    # for place in self.nu_sync_net.places:
    #   if 'resource' in place.reference.properties:
    #     print('res', place.reference.properties)
    #   else:
    #     print('no res', place.reference.properties)
    # print(fdsa)