from mira.prelim.linear_extensions import LinearExtensions

from pm4py.visualization.transition_system.visualizer import view as view_transition_system

import numpy as np
from collections.abc import Iterable
from itertools import product
import tempfile
from graphviz import Digraph
from collections import defaultdict
import random
import tqdm


class POSet(list):
  def __init__(self, elements:list=None, r:np.matrix=None, is_already_closed:bool=False):
    if elements is None:
      elements = []

    super().__init__(elements)
    self.n = len(elements)
    if r is None:
      self.r = np.matrix(np.zeros((self.n, self.n), dtype=int))
    else:
      if r.shape != (self.n, self.n):
        raise ValueError(f'Partial order matrix has incorrect shape: {r.shape} iso. {(self.n, self.n)}')
      self.r = r if isinstance(r, np.matrix) else np.matrix(r)

    self.set_indices()
    if not is_already_closed:
      self.close_transitive()

    if np.any(np.multiply(self.r, self.r.T) == 1):
      self.view(element_str=lambda x: x.short_str())
      raise ValueError('POSet has a loops.')

  def set_indices(self):
    self.indices = {element: index for index, element in enumerate(self)}

  def check_loops(self, view_if_loops:bool=False):
    if np.any(np.multiply(self.r, self.r.T) == 1):
      if view_if_loops:
        self.view(element_str=lambda x: x.short_str())
      raise ValueError('POSet has a loops.')

  def append(self, element):
    super().append(element)
    self.indices[element] = len(self) - 1
    self.r = np.matrix(np.pad(self.r, [(0,1), (0,1)], 'constant'))
    self.n += 1

  def extend(self, elements):
    before_size = len(self)
    super().extend(elements)
    self.indices.update({element: before_size + index for index, element in enumerate(elements)})
    self.r = np.matrix(np.pad(self.r, [(0, len(elements)), (0, len(elements))], 'constant'))
    self.n += len(elements)

  def get_R_indices(self, r:np.matrix=None):
    if r is None:
      r = self.r
    return np.transpose(r.nonzero())

  def union(self, other):
    elements           = list(self)
    elements_set       = set(elements)
    other_elements_set = set(other)

    new_elements = list(other_elements_set - elements_set)
    new_elements_indices = [other.indices[element] for element in new_elements]

    intersection_elements = list(elements_set.intersection(other_elements_set))
    intersection_indices = [self.indices[element] for element in intersection_elements]
    other_intersection_indices = [other.indices[element] for element in intersection_elements]

    union_poset = POSet(elements, self.r, is_already_closed=True)
    union_poset.extend(list(new_elements))

    union_poset.r[np.ix_(intersection_indices, intersection_indices)] = \
      np.maximum(self.r[np.ix_(intersection_indices, intersection_indices)], other.r[np.ix_(other_intersection_indices, other_intersection_indices)])

    union_poset.r[self.n:union_poset.n,self.n:union_poset.n] = other.r[np.ix_(new_elements_indices, new_elements_indices)]

    if len(intersection_elements) > 0:
      union_poset.r[np.ix_(intersection_indices, range(self.n, union_poset.n))] = other.r[np.ix_(other_intersection_indices,new_elements_indices)]
      union_poset.r[np.ix_(range(self.n, union_poset.n), intersection_indices)] = other.r[np.ix_(new_elements_indices,other_intersection_indices)]

      union_poset.close_transitive()

    union_poset.check_loops()
    return union_poset

  def intersection(self, other):
    elements = list(set(self).intersection(set(other)))
    element_indices       = [self.indices[element] for element in elements]
    other_element_indices = [other.indices[element] for element in elements]

    r_ = self.r[np.ix_(element_indices, element_indices)]
    if np.any(r_ + other.r[np.ix_(other_element_indices, other_element_indices)] == 1):
      raise ValueError('Can not intersect POSets where partial orders disagree.')
    return POSet(elements, r_, is_already_closed=True)

  def get_R_value(self, element1, element2):
    return self.r[self.indices[element1],self.indices[element2]] == 1

  def detach_unconnected_subposets(self):
    element_min_max = [(set(self.indices[e] for e in self.prefix([element]).min()),
                        set(self.indices[e] for e in self.postfix([element]).max())) for element in self]
    element_sets = []
    for index, element in enumerate(self):
      found_component = False
      for element_set in element_sets:
        for element2 in element_set:
          index2 = self.indices[element2]
          if len(element_min_max[index][0].intersection(element_min_max[index2][0])) > 0 or \
             len(element_min_max[index][1].intersection(element_min_max[index2][1])) > 0:
            element_set.append(element)
            found_component = True
            break
        if found_component:
          break
      if not found_component:
        element_sets.append([element])
    return [self.projection(lambda el: el in element_set, is_already_closed=True) for element_set in element_sets]

  def prefix(self, antichain):
    self.__check_antichain(antichain)
    lte_antichain = lambda element: element in antichain or (any([self.get_R_value(element, antichain_element) for antichain_element in antichain]))
    return self.projection(lte_antichain, is_already_closed=True)

  def prefixopen_indices(self, antichain_indices):
    self.__check_antichain([self[i] for i in antichain_indices])
    return

  def prefixopen(self, antichain):
    self.__check_antichain(antichain)
    lt_antichain = lambda element: any([self.get_R_value(element, antichain_element) for antichain_element in antichain])
    return self.projection(lt_antichain, is_already_closed=True)

  def postfix(self, antichain):
    self.__check_antichain(antichain)
    gte_antichain = lambda element: element in antichain or (any([self.get_R_value(antichain_element, element) for antichain_element in antichain]))
    return self.projection(gte_antichain, is_already_closed=True)

  def postfixopen(self, antichain):
    self.__check_antichain(antichain)
    gt_antichain = lambda element: any([self.get_R_value(antichain_element, element) for antichain_element in antichain])
    return self.projection(gt_antichain, is_already_closed=True)

  def projection(self, func, is_already_closed=False):
    elements = [element for element in self if func(element)]
    element_indices = [self.indices[element] for element in elements]
    return POSet(elements, self.r[np.ix_(element_indices, element_indices)], is_already_closed=is_already_closed)

  def decompose_on_attribute(self, attribute:str):
    posets_elements = defaultdict(lambda: {'elements': [], 'indices': []})
    for element in self:
      posets_elements[element.get(attribute, None)]['elements'].append(element)
      posets_elements[element.get(attribute, None)]['indices'].append(self.indices[element])

    return {key: POSet(elements_indices['elements'], self.r[np.ix_(elements_indices['indices'], elements_indices['indices'])], is_already_closed=True)
            for key, elements_indices in posets_elements.items()}

  def projection_on_attribute(self, attribute:str, values:Iterable):
    if not isinstance(values, Iterable):
      return self.projection(lambda el: el.get(attribute, None) == values, is_already_closed=True)
    else:
      return self.projection(lambda el: el.get(attribute, None) in values, is_already_closed=True)

  @staticmethod
  def close_transitive_matrix(r):
    if not isinstance(r, np.matrix):
      r = np.matrix(r)
    ones = np.ones(r.shape, dtype=int)
    r_ = np.minimum(r + r@r, ones)
    while (r != r_).any():
      r = r_
      r_ = np.minimum(r + r@r, ones)
    return r_

  def close_transitive(self):
    self.r = self.close_transitive_matrix(self.r)

  def get_minimal_partial_order(self):
    return np.maximum(self.r - self.r@self.r, np.zeros((len(self), len(self)), dtype=int))

  def min(self):
    return [self[index] for index in self.min_indices()]

  def min_indices(self):
    return np.where((np.array(self.r) == 0).all(axis=0))[0]

  def max(self):
    return [self[index] for index in self.max_indices()]

  def max_indices(self):
    return np.where((np.array(self.r) == 0).all(axis=1))[0]

  def add_children_parents(self):
    r_min = self.get_minimal_partial_order()
    for index, move in enumerate(self):
      move.children = {self[i] for i in r_min[index].nonzero()[1]}
      move.parents  = {self[i] for i in r_min[:,index].T.nonzero()[1]}

  def traverse_yield(self, forward=True, shuffle=False, return_indices=False):
    r = np.array(self.r, dtype=int)
    degrees = np.sum(r, axis=0) if forward else np.sum(r, axis=1)

    queue = list(np.where(degrees == 0)[0])
    while queue:
      if shuffle:
        random.shuffle(queue)
      element_index = queue.pop()
      yield element_index if return_indices else self[element_index]
      arcs = r[element_index] if forward else r[:, element_index]
      degrees -= arcs
      queue.extend(list(np.where((degrees == 0) & arcs)[0]))

  def traverse(self, forward=True, shuffle=False, return_indices=False):
    return [element for element in self.traverse_yield(forward, shuffle, return_indices)]

  def get_total_order_permutations(self, number_of_perms:int, verbose=False):
    if number_of_perms == 'inf':
      le = LinearExtensions(self)
      return le.list()

    permutations = []
    iters = range(number_of_perms) if not verbose else tqdm.tqdm(range(number_of_perms), postfix=f'{"Retrieving permutations":>40}')
    for seed in iters:
      new_permutation = self.traverse(shuffle=True)
      if not any(all(a == b for a, b in zip(new_permutation, permutation)) for permutation in permutations):
        permutations.append(new_permutation)

    return permutations

  def to_json(self):
    return {'M': [(element.to_json() if callable(getattr(element, 'to_json', None)) else element) for element in self],
            'r': self.r.tolist()}

  def view(self, minimal:bool=True, element_str=lambda x: f'{x}', colors=lambda x: 'black', fill_colors=lambda x: 'white', name:str=None, rankdir:str='LR'):
    if len(self) == 0:
      print(f'Nothing to view, {type(self).__name__} is empty.')
      return
    view_transition_system(self.create_view(minimal, element_str, colors, fill_colors, name=name, rankdir=rankdir))

  def create_view(self, minimal:bool=True, element_str=lambda x: f'{x}', colors=lambda x: 'black',
                  fill_colors=lambda x: 'white', name:str=None, rankdir:str='LR'):
    postfix = '' if name is None else f'::{name}'
    font_size = 18

    filename = tempfile.NamedTemporaryFile(suffix=f'{postfix}.gv')
    viz = Digraph('', filename=filename.name, engine='dot', graph_attr={'bgcolor': 'transparent', 'rankdir': rankdir})

    viz.attr('node')
    for index, element in enumerate(self):
      fillcolor = fill_colors(element)
      style = {'style': 'filled,rounded', 'color': colors(element), 'penwidth': '3', 'fillcolor': fillcolor,
               'fontcolor': 'white' if fillcolor == 'black' else 'black'}
      string = element_str(element)
      viz.node(str(id(element)), string, fontsize=str(font_size), **style, shape='box')

    for i, j in self.get_R_indices(self.get_minimal_partial_order() if minimal else self.r):
      viz.edge(str(id(self[i])), str(id(self[j])))

    viz.attr(overlap='false')
    viz.format = 'png'
    return viz

  def __check_antichain(self, antichain):
    for el1, el2 in product(antichain, antichain):
      if self.get_R_value(el1, el2):
        raise ValueError('All elements in the antichain should be incomparable.')
