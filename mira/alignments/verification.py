from mira.alignments import utils
from mira.rc_nu_net.nu_net import NuMarking
from mira.prelim.poset import POSet

import numpy as np
from copy import deepcopy
import time
import tqdm


def verify(m_m_i, m_m_f, alignment:POSet, event_log:POSet, number_of_permutations:int):
  t = time.time()
  alignment_verification = AlignmentVerification(m_m_i, m_m_f, alignment, event_log, number_of_permutations)
  alignment_verification.verify()
  alignment_verification.result['t']['total'] = time.time() - t
  return alignment_verification.result


class AlignmentVerification:
  def __init__(self, m_m_i, m_m_f, alignment:POSet, event_log:POSet, number_of_permutations:int):
    self.m_m_i = m_m_i
    self.m_m_f = m_m_f
    self.alignment = alignment
    self.event_log = event_log
    self.number_of_permutations = number_of_permutations
    self.result = {'t': {}}

  def verify(self):
    t = time.time()
    gamma_L = self.alignment.projection(lambda element: not utils.is_model_move(element.transition), is_already_closed=True)
    log_indices = [self.event_log.indices[move.transition.event] for move in gamma_L]
    # Check if <_L \subseteq <_\gamma_L
    log_correct = bool((gamma_L.r >= self.event_log.r[np.ix_(log_indices, log_indices)]).all())
    self.result['t']['log'] = time.time() - t
    t = time.time()

    gamma_T = self.alignment.projection(lambda element: not utils.is_log_move(element.transition), is_already_closed=True)
    sigmas = gamma_T.get_total_order_permutations(number_of_perms=self.number_of_permutations, verbose=True)
    self.result['t']['permutations'] = time.time() - t
    t = time.time()

    # Check for each \sigma \in \gamma_T^< if m_i -\sigma-> m_f
    rejected_sigma = None
    for sigma in tqdm.tqdm(sigmas, postfix=f'{"Verifying sigmas in model":>40}'):
      if not self.check_sigma(sigma, verbose=False):
        rejected_sigma = sigma
        break
    self.result['t']['check_sigmas'] = time.time() - t
    self.result.update({'log': log_correct, 'model': rejected_sigma is None, 'number_of_permutations': len(sigmas),
                        **({} if rejected_sigma is None else {'rejected_sigma': [m.to_json() for m in rejected_sigma]})})

  def check_sigma(self, sigma, verbose=False):
    m = deepcopy(self.m_m_i)
    for i, move in enumerate(sigma):
      sub_marking_m = NuMarking({place.reference: token_count for place, token_count in move.sub_marking.items() if place.name[1] != '>>'})
      add_marking_m = NuMarking({place.reference: token_count for place, token_count in move.add_marking.items() if place.name[1] != '>>'})
      try:
        m = m - sub_marking_m + add_marking_m
      except ValueError:
        if verbose:
          [print(el.short_str(18)) for el in sigma[: i + 1]]
        return False

    return m == self.m_m_f
