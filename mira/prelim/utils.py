from collections import defaultdict


def split_by_comma(string, split_char=','):
  brackets = {']': '[', ')': '(', '}': '{'}
  bracket_count = defaultdict(int)
  els = []
  string_so_far = ''
  for letter in string:
    if letter in brackets.values():
      bracket_count[letter] += 1
    elif letter in brackets.keys():
      bracket_count[brackets[letter]] -= 1
    if letter == split_char and all(v == 0 for v in bracket_count.values()):
      els.append(string_so_far.strip())
      string_so_far = ''
    else:
      string_so_far += letter
  els.append(string_so_far.strip())
  return els