from mira.nets.petri_net import MultiNuNet, System
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import MultiNuArc
from mira.nets.marking import MultiNuMarking, MultiColorToken

from copy import deepcopy

def get(entities=None, view=False):
  if entities is None:
    entities = ({'c1', 'c2'}, {'r_b': {'b1', 'b2'}, 'r_g': {'g1'}})

  place_names = ['pi', 'p1', 'p2', 'pf', 'p_b_ava', 'p_b_busy', 'p_g_ava', 'p_g_busy']
  activities = ['a', 'b', 'c']
  p = {name: Place(name) for name in place_names}
  t = {name: Transition(name, name) for name in activities}
  n = {**p, **t}
  net = MultiNuNet(list(p.values()), list(t.values()))

  labeled_arcs = {
    ('{c: 1}', '_'): [('pi', 'a'), ('a', 'p1'), ('p1', 'b'), ('b', 'p2'), ('p2', 'c'), ('c', 'pf')],
    ('_', '{b: 1}'): [('p_b_ava', 'a'), ('b', 'p_b_ava')],
    ('{c: 1}', '{b: 1}'): [('a', 'p_b_busy'), ('p_b_busy', 'b')],
    ('_', '{g: 1}'): [('p_g_ava', 'a'), ('c', 'p_g_ava')],
    ('{c: 1}', '{g: 1}'): [('a', 'p_g_busy'), ('p_g_busy', 'b'), ('b', 'p_g_busy'), ('p_g_busy', 'c')]
  }

  [net.add_arc(MultiNuArc(n[source], n[target], labels)) for labels, arcs in labeled_arcs.items() for source, target in arcs]

  m_i = MultiNuMarking({'pi':      {MultiColorToken(2, [{entity: 1},{}]): 1 for entity in entities[0]},
                        'p_b_ava': {MultiColorToken(2, [{},{entity: 1}]): 1 for entity in entities[1]['r_b']},
                        'p_g_ava': {MultiColorToken(2, [{}, {entity: 1}]): 1 for entity in entities[1]['r_g']}})
  m_f = MultiNuMarking({'pf': deepcopy(m_i['pi']), 'p_b_ava': deepcopy(m_i['p_b_ava']), 'p_g_ava': deepcopy(m_i['p_g_ava'])})

  sn = System(net, m_i, m_f)
  if view:
    sn.view()
  return sn
