from mira.logs.event_log import Log

import numpy as np


def get(n_entities=2, view=False):
  events = []
  log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, tuple([{}]*n_entities))
  return log
