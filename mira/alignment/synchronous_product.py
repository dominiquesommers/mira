from mira.prelim.multiset import Multiset
from mira.nets.system import System, NuNet
from mira.nets.marking import NuMarking
from mira.nets.transition import Transition
from mira.nets.arc import NuArc

from typing import Union
from copy import copy


class SynchronousProductSystem(System):
  def __init__(self, system1:System, system2:System):
    if not system1.same_type(system2):
      raise TypeError(f"Cannot create synchronous product of two different types: '{type(system1)}' and '{type(system2)}'.")
    self.system1, self.system2 = system1, system2
    net, m_i, m_f = self.synchronous_product()
    super().__init__(net, m_i, m_f)

  def synchronous_product(self):
    net = self.system1.net + self.system2.net
    self._add_synchronous_transitions(net)
    return net, self.system1.m_i + self.system2.m_i, self.system1.m_f + self.system2.m_f

  def _add_synchronous_transitions(self, net):
    # TODO create correct labels on arcs (i.e., match the labels from the log net and model).
    syncs = Multiset(str)
    for transition in self.system1.transitions:
      for match_transition in self.system2.transitions:
        continue
        if match_transition.label != transition.label:
          continue
        sync_name = f'{transition.name}-{match_transition.name}'
        syncs[sync_name] += 1
        sync_name = f'{sync_name}{"" if syncs[sync_name] == 1 else f"_{syncs[sync_name]}"}'
        sync_transition = net.add_transition(Transition(sync_name, transition.label, move_type='synchronous'))

        model_transition, log_transition = (transition, match_transition) if transition.move_type == 'model' else (match_transition, transition)

        for arc in net.transition_inhibitor_arcs[model_transition]:
          self.__add_arc(net, copy(arc), new_target=sync_transition)

        for arc in net.element_in_variable_arcs[model_transition]:
          self.__add_variable_arc(net, copy(arc), new_target=sync_transition)
        for arc in net.element_out_variable_arcs[model_transition]:
          self.__add_variable_arc(net, copy(arc), new_source=sync_transition)

        labels = None
        for arc in net.element_in_arcs[model_transition]:
          self.__add_arc(net, copy(arc), new_target=sync_transition)
          # TODO this only works for multi nu nets currently.
          if len([l for l in arc.labels if str(l) != '_']) <= 1:
            labels = arc.labels if labels is None else tuple(label + other_label for label, other_label in zip(labels, arc.labels))

        for arc in net.element_in_arcs[log_transition]:
          # There's two in arcs for each log transition, one with label ('_',...,'_') and one with non-empty labels.
          arc_ = copy(arc)
          arc_.labels = arc_.labels if arc.blank_label() else labels
          self.__add_arc(net, arc_, new_target=sync_transition)

        for arc in net.element_out_arcs[model_transition] + net.element_out_arcs[log_transition]:
          new_arc = copy(arc)
          new_arc.source = sync_transition
          net.add_arc(new_arc)

  def __add_arc(self, net, arc, new_source=None, new_target=None):
    arc.source = arc.source if new_source is None else new_source
    arc.target = arc.target if new_target is None else new_target
    net.add_arc(arc)

  def __add_variable_arc(self, net, arc, new_source=None, new_target=None):
    arc.source = arc.source if new_source is None else new_source
    arc.target = arc.target if new_target is None else new_target
    net.add_variable_arc(arc, substitute=False)


class tPNIDSynchronousProductSystem(SynchronousProductSystem):
  def __init__(self, system1:System, system2:System):
    super().__init__(system1, system2)

  def _add_synchronous_transitions(self, net):
    syncs = Multiset(str)
    for transition in self.system1.transitions:
      for match_transition in self.system2.transitions:
        if transition.label is None or match_transition.label is None or match_transition.label.split('↾')[0] != transition.label.split('↾')[0]:
          continue
        vars1, vars2 = transition.vars, match_transition.vars
        # vars1, vars2 = self.system1.net.vars(transition), self.system2.net.vars(match_transition)
        if any(len(vars1.get(k, set())) != len(vars2.get(k, set())) for k in set(vars1.keys()).union(set(vars2.keys()))):
          continue

        sync_name = f'{transition.name}-{match_transition.name}'
        syncs[sync_name] += 1
        sync_name = f'{sync_name}{"" if syncs[sync_name] == 1 else f"_{syncs[sync_name]}"}'
        sync_transition = net.add_transition(Transition(sync_name, transition.label, move_type='synchronous'))
        if hasattr(transition, 'vars_original'):
          sync_transition.vars_original = transition.vars_original
        model_transition, log_transition = (transition, match_transition) if transition.move_type == 'model' else (match_transition, transition)

        for arc in net.element_in_arcs[log_transition]:
          new_arc = copy(arc)
          new_arc.transition = sync_transition
          role = arc.source.alpha[0]
          role_vars_model = model_transition.vars
          new_arc.labels = Multiset(args={(f'{{{v}: 1}}',): 1 for v in role_vars_model[role]})
          net.add_arc(new_arc)
          for marc in net.element_out_arcs[log_transition]:
            if marc.target.alpha == arc.source.alpha:
              new_marc = copy(marc)
              new_marc.transition = sync_transition
              new_marc.labels = new_arc.labels
              net.add_arc(new_marc)

        for arc in net.element_in_arcs[model_transition] + net.element_out_arcs[model_transition]:
          new_arc = copy(arc)
          new_arc.transition = sync_transition
          net.add_arc(new_arc)


if __name__ == '__main__':
  from mira.logs.event_log import Log
  from mira.alignment.log_net import tPNIDLogSystem
  from mira.logs.event import Event
  from mira.nets.petri_net import *
  from mira.nets.arc import Arc, NuArc, MultiNuArc, SetVar, Var, VarExpr
  from mira.nets.utils import powerset
  from mira.alignment.search import Search
  from mira.prelim.colors import Color
  import numpy as np
  from copy import deepcopy

  test = 1
  if test == 3:
    object_types = {'P': {'p'}}
    variable_types = {r: set([Var(id) for id in ids]) for r, ids in object_types.items()}
    events = [
      Event('a', Multiset(args={'p': 1})),
      Event('b', Multiset(args={'p': 2}))
    ]
    log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'p1'}, {'d1'}))
    log_system = tPNIDLogSystem(log, object_types, variable_types)
    log_system.view()
    ps = {name: Place(name) for name in ['p1', 'p2']}
    ts = {t: Transition(t, t) for t in ['a', 'b']}
    variable_types = {'P': {Var('x')}}
    net = tPNID(list(ps.values()), list(ts.values()), object_types=object_types, variable_types=variable_types)
    els = {**ps, **ts}

    [net.add_arc(MultiNuArc(els[f], els[t], Multiset(args={(f'{{{v}: 1}}',): 1}))) for f, t, v in [
      ('p1', 'a', 'x'), ('a', 'p2', 'x'), ('p2', 'b', 'x'), ('b', 'p1', 'x')]]
    net.add_arc(MultiNuArc(els['p1'], els['b'], Multiset(args={('{x: 1}',): 2})))

    m_i = MultiNuMarking({ps['p1']: {MultiColorToken(1, [{'p': 1}]): 2}})
    model_system = System(net, m_i, m_i)
    # model_system.view()

    sync_product = tPNIDSynchronousProductSystem(model_system, log_system)
    sync_product.view()

  elif test == 1:
    from mira.examples.logs.delivery_1 import get as get_log
    from mira.examples.nets.delivery import get as get_net

    object_types = {'P': {'p1', 'p2'}, 'D': {'d1', 'd2'}, 'W': {'w1', 'w2'}}
    variable_types = {r: set([Var(id) for id in ids]) for r, ids in object_types.items()}
    log = get_log()
    log_system = tPNIDLogSystem(log, object_types, variable_types)
    # log_system.view()

    log_system.net = log_system.net.relax(skip_silents=True)

    model_system = get_net()
    model_system.net = model_system.net.relax()
    model_system.view()

    sync_product = tPNIDSynchronousProductSystem(model_system, log_system)
    sync_product.view()

    # TODO fix cost function for relaxed moves?
    cost_function = lambda t: (10000 if t.label is not None and t.move_type in ['log', 'model'] else 1)*len(set().union(*t.vars.values()))

    search = Search(sync_product, cost_function)
    run, stats = search.compute_cheapest_run()

    print('cost', stats['cost'])
    print('run', run)
    TRANSITION_COLOR = lambda \
      m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN

    run = run.projection(lambda x: not (x.transition.move_type == 'log' and x.transition.label is None))

    run.view(element_str=lambda x: f'{x.transition}', colors=TRANSITION_COLOR)
    print(Bslto)

  elif test == 2:
    object_types = {'P': {'p1', 'p2'}, 'D': {'d1', 'd2'}, 'W': {'w1', 'w2'}}
    variable_types = {r: set([Var(id) for id in ids]) for r, ids in object_types.items()}
    events = [
      Event('create', Multiset(args={'p1': 1})),
      Event('a', Multiset(args={'p1': 1, 'd1': 1})),
      Event('b', Multiset(args={'p1': 1}))
    ]
    log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'p1'}, {'d1'}))
    log_system = tPNIDLogSystem(log, object_types, variable_types)
    # log_system.view()

    # objects = set.union(*object_types.values())
    # net_add = deepcopy(log_system.net)
    # for s in powerset(objects):
    #   net_p = log_system.net.projection(set(s), object_types, variable_types)
    #   net_add = net_add.union(net_p)
    # net_add.set_alphas(variable_types)
    #
    # net_add.add_correlation_cd_net()

    # rel_mnet = System(net_add, log_system.m_i, None)
    # rel_mnet.view(debug=False)

    # object_types = {'P': {'p1'},  # Multiset(args={'p1': 1}),
    #                 'D': {'d1', 'd2'},  # Multiset(args={'d1': 1, 'd2': 1}),
    #                 'W': {'w1', 'w2'}}  # Multiset(args={'w1': 2, 'w2': 1})}
    variable_types = {'P': {Var('nu_p'), Var('p')}, 'D': {Var('d')}, 'W': {Var('w')}}

    ps = {name: Place(name) for name in [#'pp',
                                         'p1', 'p2', 'p3', 'pd']}
    ts = {**{t: Transition(t, t) for t in ['a', 'b', 'create']}, **{t: Transition(t, None) for t in ['destroy']}}
    net = tPNID(list(ps.values()), list(ts.values()), object_types=object_types, variable_types=variable_types)
    els = {**ps, **ts}

    [net.add_arc(MultiNuArc(els[f], els[t], Multiset(args={(f'{{{v}: 1}}',): 1}))) for f, t, v in [
      # ('pp', 'create', 'p'), ('create', 'pp', 'p'),
      ('create', 'p1', 'p'), ('p1', 'a', 'p'), ('a', 'p2', 'p'), ('p2', 'b', 'p'), ('b', 'p3', 'p'), ('p3', 'destroy', 'p'),
      ('pd', 'a', 'd'), ('a', 'pd', 'd')
    ]]

    m_i = MultiNuMarking({
      # ps['pp']: {MultiColorToken(1, [{package: 1}]): 1 for package in object_types['P']},
      ps['pd']: {MultiColorToken(1, [{'d1': 1}]): 1, MultiColorToken(1, [{'d2': 1}]): 1},
    })
    model_system = System(net, m_i, m_i)
    # objects = set.union(*object_types.values())
    # net_add = deepcopy(net)
    # for s in powerset(objects):
    #   net_p = net.projection(set(s), object_types, variable_types)
    #   net_add = net_add.union(net_p)
    # net_add.set_alphas(variable_types)
    #
    # net_add.add_correlation_cd_net()

    # rel_mnet = System(net_add, m, None)
    # model_system.view(debug=False)

    sync_product = tPNIDSynchronousProductSystem(model_system, log_system)

    # mode = {'p1': 'p2', 'd': 'd1'}
    # arc = MultiNuArc(els['p1'], els['a'], Multiset(args={('{p1: 1}', '{d: 1}'): 1}))
    # print(arc.labels)
    # for label, n in arc.labels.items():
    #   print(MultiColorToken(len(label), [var_expr.evaluate(mode) for var_expr in label]))

    # print(bs)

    # MultiColorToken(2, [var_expr.evaluate(mode) for var_expr in labels]

    sync_product.view()

    # print('check')
    # modes = sync_product.net.enabled_modes(sync_product.marking, els['create'])
    # print(modes)
    # print(buih)

    # ppp = [p for p in sync_product.places if p.name == 'p_P^(0)'][0]
    # pdd = [p for p in sync_product.places if p.name == 'p_D^(1)'][0]
    # mode = {'p1': sync_product.marking[ppp], 'p': sync_product.marking[pdd]}
    # mode = {'p': ColorToken('p1')}
    # enabled, sub_marking, add_marking = sync_product.net.transition_enabled(els['create'], sync_product.marking, mode, verbose=True)
    #
    # print(enabled)
    # print(sub_marking)
    # print(add_marking)
    # print(bls)

    cost_function = lambda t: 10000 if t.label is not None and t.move_type in ['log', 'model'] else 1

    search = Search(sync_product, cost_function)
    run, stats = search.compute_cheapest_run()

    print('cost', stats['cost'])
    print('run', run)
    TRANSITION_COLOR = lambda m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN
    run.view(element_str=lambda x: f'{x.transition}', colors=TRANSITION_COLOR)