from mira.simulation.ground_truth_simulation import GroundTruthSimulation
from mira.nets.system import System
from mira.nets.arc import MultiNuArc, MultiVarExpr
from mira.prelim.multiset import Multiset
from mira.alignments.exporter import NoIndent, MyEncoder
from mira.nets.marking import MultiNuMarking
from mira.nets.token import MultiColorToken

from dateutil.relativedelta import *
from datetime import datetime
import random
from copy import deepcopy
import numpy as np
np.random.seed(0)
import json
from colorama import Fore, Style


pattern_base_dir = './datasets/deviation_patterns'
class RecordingDeviations:
  _1 = {'incorrect event order home->depot': (f'{pattern_base_dir}/incorrect_event.json', {'x': 'order home', 'y': 'order depot'})}
  _2 = {'missing event load': (f'{pattern_base_dir}/missing_event.json', {'x': 'load in truck'})}
  _3 = {'missing object v': (f'{pattern_base_dir}/missing_object.json', {'t': 'load in truck', 'Y': 'V'})}
  _4 = {'incorrect V object': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'load in truck', 'R': 'V'})}
  _5 = {'incorrect D object': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'ring', 'R': 'D'})}
  _6 = {'incorrect position': (f'{pattern_base_dir}/incorrect_position.json', {'x': 'ring', 'y': 'deliver home'})}
  _7 = {'missing position': (f'{pattern_base_dir}/missing_position.json', {'x': 'deliver depot', 'y': 'collect'})}
  _8 = {'incorrect W object': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'deliver depot', 'R': 'W'})}
  _9 = {'incorrect event order depot->home': (f'{pattern_base_dir}/incorrect_event.json', {'x': 'order depot', 'y': 'order home'})}

class BehavioralDeviations:
  _1 = {'overtake queue': (f'{pattern_base_dir}/overtake_queue.json', {'q1': 'q1_', 'q2': 'q2_', 'q': 'Y', 'y': 'P'})}
  _2 = {'switch role': (f'{pattern_base_dir}/switch_role.json', {'px': 'p_d_on', 'py': 'p_we', 'x': 'D', 'y': 'E'})}
  _3 = {'change correlation': (f'{pattern_base_dir}/change_correlation.json', {'p': 'p4', 'py': 'p_v_ava', 'x': 'P', 'y': 'V'})} # TODO tempfixed always enabled transition}
  _4 = {'neglect batching': (f'{pattern_base_dir}/neglect_batching.json', {'c': 'p_v_ava', 'r': 'V'})}
  _5 = {'swap correlationPD': (f'{pattern_base_dir}/swap_correlation.json', {'p': 'p6', 'x': 'P', 'y': 'D'}),
        'swap correlationPV': (f'{pattern_base_dir}/swap_correlation.json', {'p': 'p_v_occ', 'x': 'P', 'y': 'V'})}
  _6 = {'skip ring':  (f'{pattern_base_dir}/skip_activity.json', {'x': 'ring'})}
  _7 = {'multitask': (f'{pattern_base_dir}/multitask.json', {'p1': 'p5', 'p2': 'p_d_on', 'x': 'P', 'y': 'D'})} # TODO tempfixed always enabled transition
  _8 = {'neglect warehouse': (f'{pattern_base_dir}/neglect_objects.json', {'t': 'register depot', 'Y': 'W'})} # TODO tempfixed always enabled transition
  _9 = {'diff res mem warehouse': (f'{pattern_base_dir}/different_resource_memory.json', {'p1': 'p7', 'p2': 'p_w', 'x': 'P', 'y': 'W'})} # TODO tempfixed always enabled transition
  _10 = {'neglect deliverer deliver depot': (f'{pattern_base_dir}/neglect_objects.json', {'t': 'deliver depot', 'Y': 'D'})} # TODO tempfixed (with bi-arc to/from p12) (!!BROKEN!!)
  _11 = {'increase warehouse cap': (f'{pattern_base_dir}/increase_capacity_inf.json', {'c': 'p_w', 'r': 'W'})}
  _12 = {'decrease warehouse cap': (f'{pattern_base_dir}/decrease_capacity_inf.json', {'c': 'p_w', 'r': 'W'})}

def create_synthetic_data(dev_combination, n_packages=None, verbose=False, visualize=False, export=''):
  with open('./datasets/nets/delivery_extended.json', 'r') as json_file:
    data = json.load(json_file)
    system = None
    for net_data in data:
      system_ = System.from_json_dict(net_data)
      if system is None:
        system = system_
      else:
        system = system.union(system_)

  if n_packages is not None:
    system.net.object_types['P'] = set(sorted(list(system.net.object_types['P']))[:n_packages])

  print('DEVIATIONS:', sorted(list(set().union(*[list(d.keys()) for d in dev_combination]))))
  patterns_ = {k: v for x in dev_combination for k, v in x.items()}
  patterns = {}
  for pattern_name, (pattern_filename, mapping) in patterns_.items():
    with open(pattern_filename, 'r') as json_file:
      patterns[pattern_name] = {'net_data': json.load(json_file), 'mapping': mapping}

  work_schedule = False
  if work_schedule:
    # Expected objects (scheduled)
    schedules = {
      'start': {'d1': ([MO, TU, WE, TH, FR], [9]), 'd2': ([MO, TU, WE, TH], [9])},
      'stop':  {'d1': ([MO, TU, WE, TH, FR], [18]), 'd2': ([MO, TU, WE, TH], [18])}
    }
    # Spontaneous objects
    arrivals = {'P': lambda dt: random.expovariate(1/(((np.sin((((dt - datetime(dt.date().year, dt.date().month, dt.date().day)).total_seconds() / 86400)-0.15)*2*np.pi)+1)/(2/.9)+.1)*3600)) }
  else:
    # TODO fire start for d1 and d2.
    schedules = {}
    # Spontaneous objects
    arrivals = {'P': lambda dt: int((dt - datetime(dt.date().year, dt.date().month, dt.date().day)).total_seconds() < (9*60*60))*60} #random.expovariate(1/1800)}

  transition_weights = {
    t.name: (lambda dt: -1) if t.label in ['start', 'stop', 'create'] else
            (lambda dt: 1)
    for t in system.transitions
  }
  transition_weights['tau_free'] = lambda dt: 100

  weights = {
    'order home': lambda dt: 1, 'order depot': lambda dt: 1, 'tau_2': lambda dt: 1,
    # behavioral deviations
    'tau_swap_correlation_PDV': lambda dt: 100, #np.random.binomial(1, 0.5),
    'tau_change_correlation_p4_PV': lambda dt: 100,
    'tau_early_release_D': lambda dt: 100, 'tau_late_claim_D': lambda dt: 0.0002,
    'tau_skip_ring': lambda dt: 100,
    'tau_create^p7↾{W}_negl_{P}': lambda dt: 100, 'register depot_neglect_W': lambda dt: 100, 'tau_register depot_W_negl': lambda dt: 100,
    'tau_destroy^p6↾{D}_negl_{P}': lambda dt: 100, 'deliver depot_neglect_D': lambda dt: 100, 'tau_deliver depot_D_negl': lambda dt: 100,
    'tau_overtake_Y': lambda dt: 100,
    'tau_p_w_-': lambda dt: -1, 'tau_p_w_-_undo': lambda dt: -1,
    'tau_p_w_+': lambda dt: -1, 'tau_p_w_+_undo': lambda dt: -1,
    'tau_switch_role_D_E': lambda dt: -1, 'tau_switch_role_back_D_E': lambda dt: -1,
    'tau_change_memory_P_W': lambda dt: 100,
    'tau_p_v_ava_-': lambda dt: -1, 'tau_p_v_ava_-_undo': lambda dt: -1,
    # recording deviations
    'tau_missing_load in truck': lambda dt: 100,
    'order depot_incorrect_order home': lambda dt: 100, 'order home_incorrect_order depot': lambda dt: 100,
    'load in truck_missing_V': lambda dt: 100, 'tau1_load in truck_missing_V': lambda dt: 100, 'tau2_load in truck_missing_V': lambda dt: 100,
    'tau_deliver depot_incorrect_W': lambda dt: 100, 'deliver depot_incorrect_W': lambda dt: 100, 'tau2_deliver depot_incorrect_W': lambda dt: 100,
    'tau_load in truck_incorrect_V': lambda dt: 100, 'load in truck_incorrect_V': lambda dt: 100, 'tau2_load in truck_incorrect_V': lambda dt: 100,
    'tau_ring_incorrect_D': lambda dt: 100, 'ring_incorrect_D': lambda dt: 100, 'tau2_ring_incorrect_D': lambda dt: 100,
    'deliver depot_parallel_collect': lambda dt: 100, 'collect_parallel_deliver depot': lambda dt: 100, 'tau_1_parallel_deliver depot_collect': lambda dt: 100, 'tau_2_parallel_deliver depot_collect': lambda dt: 100,
    'deliver home_pos_ring': lambda dt: 100, 'ring_pos_deliver home': lambda dt: 100
  }
  transition_weights.update(weights)

  if 'increase warehouse cap' in patterns_:
    schedules.update({
      'tau_p_w_+':      {'w1': ([MO, TU, WE, TH, FR], [9])},
      'tau_p_w_+_undo': {'w1': ([MO, TU, WE, TH, FR], [12])}
    })
  if 'decrease warehouse cap' in patterns_:
    schedules.update({
      'tau_p_w_-':      {'w1': ([MO, TU, WE, TH, FR], [13])},
      'tau_p_w_-_undo': {'w1': ([MO, TU, WE, TH, FR], [17])}
    })
  if 'switch role' in patterns_:
    schedules.update({
      'tau_switch_role_D_E': {('d1', 'd1(e)'): ([MO, TU, WE, TH, FR], [9])},
      'tau_switch_role_back_D_E': {('d1', 'd1(e)'): ([MO, TU, WE, TH, FR], [10])}
    })
  if 'neglect batching' in patterns_:
    schedules.update({
      'tau_p_v_ava_-': {'v1': ([MO, TU, WE, TH, FR], [9])},
      'tau_p_v_ava_-_undo': {'v1': ([MO, TU, WE, TH, FR], [17])}
    })

  order_type_based_on_time = False
  if order_type_based_on_time:
    hour = 5
    transition_weights['order depot'] = lambda dt: int((dt - (dt + relativedelta(hour=0,minute=0,second=0,microsecond=0))).total_seconds() < (hour*3600)) # int(before hour o'clock)
    transition_weights['order home'] = lambda dt: int((dt - (dt + relativedelta(hour=0,minute=0,second=0,microsecond=0))).total_seconds() >= (hour*3600)) # int(after hour o'clock)

  durations = {(arc.source.name, arc.target.name): (lambda: (0, '')) for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}
  durations[('tau_queue1', 'q2_')] = lambda: (1 / 60 / 10000000, '')
  durations[('start', 'p_d_on')] = lambda: (1/60/10000000, '')
  durations[('pick package', 'p3')] = lambda: (np.random.normal(15, 6), 'Time to walk package to delivery van')
  durations[('tau_d_1', 'p6')] = lambda: (np.random.normal(30, 10), 'Time to drive to first destination')
  # durations[('tau_d_2', 'p6')] = lambda: np.random.normal(30, 10) # Time to drive to next destination(s)
  durations[('ring', 'p5')] = lambda: (np.random.normal(3, 1), 'Time to wait for answer')
  durations[('tau_2', 'p6')] = lambda: (np.random.normal(10, 5), 'Time to drive from house to nearest depot')
  durations[('deliver depot', 'p9')] = lambda: (np.random.normal(360, 90), 'Time for clients to reach depot')
  durations[('deliver home', 'p_d_on')] = lambda: (np.random.normal(10, 3), 'Time to drive to next destination(s) or back to warehouse')
  durations[('deliver home', 'p_v_free')] = lambda: (np.random.normal(10, 3), 'Time to drive to next destination(s) or back to warehouse')
  durations[('deliver depot', 'p_d_on')] = lambda: (np.random.normal(10, 3), 'Time to drive to next destination(s) or back to warehouse')
  durations[('deliver depot', 'p_v_free')] = lambda: (np.random.normal(10, 3), 'Time to drive to next destination(s) or back to warehouse')

  # Add durations for pattern transitions
  from copy import deepcopy
  copiers = [('tau_missing_load in truck', 'load in truck'),
             ('order depot_incorrect_order home', 'order home'), ('order home_incorrect_order depot', 'order depot'),
             ('tau2_load in truck_missing_V', 'load in truck'), ('tau2_deliver depot_incorrect_W', 'deliver depot'),
             ('tau2_load in truck_incorrect_V', 'load in truck'), ('tau2_ring_incorrect_D', 'ring'),
             ('deliver depot_parallel_collect', 'deliver depot'), ('collect_parallel_deliver depot', 'collect')]
  for (dev_t, original_t) in copiers:
    durations.update({(dev_t, p): deepcopy(v) for (t, p), v in durations.items() if t == original_t})
  durations[('deliver depot_neglect_D', 'p_v_free')] = durations[('deliver depot', 'p_v_free')]
  durations[('deliver depot_neglect_D', 'p9')] = durations[('deliver depot', 'p9')]
  durations[('deliver home__pos_ring', 'p5__')] = durations[('deliver home', 'p_d_on')]
  durations[('deliver depot_parallel_collect', 'p_v_free')] = durations[('deliver depot', 'p_v_free')]
  durations[('deliver depot_parallel_collect', 'p_d_on')] = durations[('deliver depot', 'p_d_on')]
  durations[('deliver depot_parallel_collect', 'p_d_on__')] = durations[('deliver depot', 'p_d_on')]
  durations[('deliver depot_parallel_collect', 'p_v_free__')] = durations[('deliver depot', 'p_v_free')]
  durations[('collect_parallel_deliver depot', 'p_w__')] = durations[('collect', 'p_w')]
  durations[('collect_parallel_deliver depot', 'p_10__')] = durations[('collect', 'p10')]
  durations[('tau_1_parallel_deliver depot_collect', 'p_9__')] = durations[('deliver depot', 'p9')]
  durations[('ring_pos_deliver home', 'p_v_free')] = durations[('ring', 'p5')]
  durations[('ring_pos_deliver home', 'p10')] = durations[('ring', 'p5')]
  durations[('ring_pos_deliver home', 'p_d_on')] = durations[('ring', 'p5')]

  sim = GroundTruthSimulation(system, patterns, transition_weights, durations, schedules, arrivals)

  # N.B. These are the only three non-standard operations necessary to make the deviation patterns work.
  if 'swap correlationPD' in patterns_ and 'swap correlationPV' in patterns_:
    # There is correlation between 3 objects, divided over two places, so we combine the transitions to swap the correlation correctly.
    # TODO fix: renaming the merged transition does not seem to work.
    t = sim.system.net.merge_transitions([next(t for t in sim.system.net.transitions if t.name == tname) for tname in
                                         ['tau_swap_correlation_p6_PD', 'tau_swap_correlation_p_v_occ_PV']], None) #'tau_swap_correlation_PDV')
    sim.arc_durations.update({('tau_swap_correlation_p6_PD', arc.target.name): (lambda: (0, '')) for arc in sim.system.net.element_out_arcs[t]})
  if 'change correlation' in patterns_:
    p_v_occ = next(p for p in sim.system.places if p.name == 'p_v_occ')
    p_v_occ_v = next(p for p in sim.system.places if p.name == 'p_v_occ_v')
    t = next(t for t in sim.system.transitions if t.name == 'tau_change_correlation_p4_PV')
    sim.system.net.add_arc(MultiNuArc(p_v_occ, t, Multiset.from_str('[([v_p],[v_v1])]', MultiVarExpr)))
    sim.system.net.add_arc(MultiNuArc(t, p_v_occ, Multiset.from_str('[([v_p],[v_v2])]', MultiVarExpr)))
    sim.system.net.add_arc(MultiNuArc(p_v_occ_v, t, Multiset.from_str('[([v_v1])]', MultiVarExpr)))
    sim.system.net.add_arc(MultiNuArc(t, p_v_occ_v, Multiset.from_str('[([v_v2])]', MultiVarExpr)))
    sim.arc_durations[('tau_change_correlation_p4_PV', 'p_v_occ')] = lambda: (0, '')
    sim.arc_durations[('tau_change_correlation_p4_PV', 'p_v_occ_v')] = lambda: (0, '')
  if 'neglect deliverer deliver depot' in patterns_:
    # These arcs are necessary since the base net is not free-choice.
    p = next(p for p in sim.system.places if p.name == 'p12')
    t = next(t for t in sim.system.transitions if t.name == 'tau_destroy^p6↾{D}_negl_{P}')
    sim.system.net.add_arc(MultiNuArc(p, t, Multiset.from_str('[([p])]', MultiVarExpr)))
    sim.system.net.add_arc(MultiNuArc(t, p, Multiset.from_str('[([p])]', MultiVarExpr)))
    sim.arc_durations[('tau_destroy^p6↾{D}_negl_{P}', 'p12')] = lambda: (0, '')
  if 'switch role' in patterns_:
    # To keep the net a valid tPNID, the objects that switch roles should be aliased and added to the object types.
    sim.system.net.object_types['E'].add('d1(e)')

  log = sim.simulate_log(start_time='2024-04-08T08:59:00', nr_days=1, min_log_length=1, max_log_length=6000, verbose=verbose)

  pattern_counts = {}
  base_tnames = [t.name for t in sim.base_system.transitions]
  for pattern, pn in sim.patterns.items():
    if pattern == 'neglect batching':
      # Count how many times m[p_v_ava][v1] = 0, inbetween neglect batching events.
      x = check_zero_token_between_events(sim.system.m_i, log, 'tau_p_v_ava_-', 'tau_p_v_ava_-_undo', 'p_v_ava', '([v1])')
    elif pattern == 'decrease warehouse cap':
      # Count how many times m[p_w][w1] = 0, inbetween decrease_warehouse events.
      x = check_zero_token_between_events(sim.system.m_i, log, 'tau_p_w_-', 'tau_p_w_-_undo', 'p_w', '([w1])')
    elif pattern == 'increase warehouse cap':
      # Count how many times m[p_w][w1] = 0, inbetween increase_warehouse events.
      x = check_zero_token_between_events(sim.system.m_i, log, 'tau_p_w_+', 'tau_p_w_+_undo', 'p_w', '([w1])')
    elif pattern == 'multitask':
      # Count how many events occur with d1/d2 and not p when early_release with d1/d2-p is fired, before late_claim with d1/d2-p is fired.
      x = 0
      taus = log.projection(lambda f: f.transition.name in ['tau_early_release_D', 'tau_late_claim_D'])
      taus = taus.traverse()
      for a, b in zip(taus[::2], taus[1::2]):
        d, p = a.mode['v_d'], a.mode['v_p']
        for firing in log[log.index(a) + 1:log.index(b)]:
          if firing.mode['d'] == d and firing.mode['p'] != p:
            x += 1
          break
    elif pattern == 'switch role':
      # Count how many events occur with d1 and not p when early_release with d1/d2-p is fired, before late_claim with d1/d2-p is fired.
      x = 0
      taus = log.projection(lambda f: f.transition.name in ['tau_switch_role_D_E', 'tau_switch_role_back_D_E'])
      taus = taus.traverse()
      for a, b in zip(taus[::2], taus[1::2]):
        for firing in log[log.index(a) + 1:log.index(b)+1]:
          if 'e' in firing.mode.keys() and firing.mode['e'].id == 'd1(e)':
            x += 1
      x = int(x/2)
    else:
      add_ts = set([t.name for t in pn.transitions if t.name not in base_tnames])
      x = int(len([f for f in log if f.transition.name in add_ts])/len(add_ts))
    pattern_counts[pattern] = x
    print(f'{pattern:<33}: {Fore.GREEN if x > 0 else Fore.RED}{x}{Style.RESET_ALL}')

  created_packages = {list(firing.objects())[0] for firing in log if firing.transition.name == 'create'}
  destroyed_packages = {list(firing.objects())[0] for firing in log if firing.transition.name == 'destroy'}
  ratio = 1
  if created_packages == destroyed_packages:
    print(f'{Fore.GREEN}Processed all packages.{Style.RESET_ALL}')
  else:
    ratio = len(destroyed_packages)/len(created_packages)
    print(f'{Fore.RED if ratio < 0.9 else Fore.GREEN}Did not manage to process all packages ({ratio:%}): {sorted(list(created_packages - destroyed_packages))} are missing.{Style.RESET_ALL}')

  remove_silent_transition_firings = False
  if remove_silent_transition_firings:
    log = log.projection(lambda f: f.transition.label is not None or True, is_already_closed=True)

  if visualize:
    move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.name} @ {x.timestamp}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
    log.view(element_str=move_str)

  if export != '':
    log_json = log.to_json()
    log_json['Pi'] = pattern_counts
    log_json['M'] = [NoIndent(move) for move in log_json['M']]
    log_json['r'] = [NoIndent(row) for row in log_json['r']]
    log_json['m_i'] = str(sim.base_system.m_i)
    with open(export, 'w') as json_file:
      json.dump(log_json, json_file, cls=MyEncoder, sort_keys=False, indent=2)
  return ratio

def check_zero_token_between_events(m_i, log, between_event_a, between_event_b, place, token_str):
  # Count how many times m[place][token_str] = 0 inbetween events.
  x = 0
  taus = log.projection(lambda f: f.transition.name in [between_event_a, between_event_b])
  taus = taus.traverse()
  for a, b in zip(taus[::2], taus[1::2]):
    left = log.prefix([a])
    m = deepcopy(m_i) - sum([f.sub_marking for f in left], start=MultiNuMarking()) + sum([f.add_marking for f in left], start=MultiNuMarking())
    v1 = [m[place][MultiColorToken.from_str(token_str)]]
    for firing in log[log.index(a) + 1:log.index(b)]:
      m = m - firing.sub_marking + firing.add_marking
      v1.append(m[place][MultiColorToken.from_str(token_str)])
    for v1_a, v1_b in zip(v1[:-1], v1[1:]):
      if v1_a != 0 and v1_b == 0:
        x += 1
  return x


if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('-dev_i', '--deviation_index', help='Deviation index: R=[0..5], B=[6..11]', type=int)
  parser.add_argument('-vis', '--visualize', help='Visualize simulated log.', action='store_true')
  parser.add_argument('-e', '--export', help='./datasets/logs/energy/log_{deviation}_<export>.json', type=str)
  parser.add_argument('-v', '--verbose', help='Verbose.', action='store_true')
  args = parser.parse_args()

  # Note that these deviations are used for alignments, even though more are implemented for simulation.
  R, B = RecordingDeviations, BehavioralDeviations
  Pi_L = [R._1, R._9, R._2, R._3, R._5, R._7]
  Pi_S = [B._1, B._2, B._4, B._6, B._9, B._7]

  for i, dev in enumerate(Pi_L + Pi_S):
    if args.deviation_index is not None and i != args.deviation_index:
      continue
    export_filename = f'./datasets/logs/delivery_extended/per_deviation/log_{sorted(list(dev.keys()))[0]}_{args.export}.json' if args.export is not None else ''
    log = create_synthetic_data(dev_combination=[dev], n_packages=2, verbose=args.verbose, export=export_filename, visualize=args.visualize)
