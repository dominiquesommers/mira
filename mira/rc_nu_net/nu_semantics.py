from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking

import re
from copy import deepcopy


def fire_transition(transition:NuPetriNet.Transition, mode, marking:NuMarking, sub_marking:NuMarking):
  new_marking = sub_nu_marking(transition, mode, marking, sub_marking)
  return add_nu_marking(transition, mode, new_marking)


def get_sub_marking(transition:NuPetriNet.Transition, mode, marking:NuMarking):
  sub_marking = NuMarking()
  for place in transition.incoming_arc_labels:
    var_c, var_r = transition.incoming_arc_labels[place]
    token = ('_' if var_c == '_' else mode[var_c], '_' if var_r == '_' else mode[var_r])
    sub_marking[place][token] += 1
  return sub_marking


def get_add_marking(transition:NuPetriNet.Transition, mode, marking:NuMarking):
  add_marking = NuMarking()
  for place_id, labels in transition.outgoing_arc_labels.items():
    var_c, var_r = labels
    id_c = '_' if var_c == '_' else mode.get(var_c, 'nu')
    id_r = '_' if var_r == '_' else mode.get(var_r, 'nu')
    add_marking[place_id][(id_c, id_r)] += 1
  return add_marking


def add_nu_marking(transition:NuPetriNet.Transition, mode, marking:NuMarking):
  new_marking = deepcopy(marking)
  for place_id, labels in transition.outgoing_arc_labels.items():
    var_c, var_r = labels
    id_c = '_' if var_c == '_' else mode.get(var_c, 'nu')
    id_r = '_' if var_r == '_' else mode.get(var_r, 'nu')
    new_marking[place_id][(id_c, id_r)] += 1
  return new_marking


def sub_nu_marking(transition:NuPetriNet.Transition, mode, marking:NuMarking, sub_tokens:NuMarking):
  new_marking = deepcopy(marking)
  for place, tokens in sub_tokens.items():
    for token in tokens:
      new_marking[place][token] -= 1
      if new_marking[place][token] == 0:
        del new_marking[place][token]
        if len(new_marking[place]) == 0:
          del new_marking[place]
  return new_marking
