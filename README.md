This repository contains the source code for multiple papers:

1. ### Aligning Event Logs to Resource-Constrained nu-Petri nets (Petri nets 2022)
2. ### Exact and Approximated Log Alignments for Processes with Inter-case Dependencies (Petri nets 2023)
3. ### Conformance Checking with Model Projections: Rethinking Log-Model Alignments for Processes with Interacting Objects (Petri nets 2024)
4. ### Assessing Process Mining Techniques: a Ground Truth Approach (submitted ICPM 2024)
#### By Dominique Sommers, Natalia Sidorova, Boudewijn van Dongen


This document contains usage for the methods described in (1), (2), and (3). For (4), we refer to `mira/simulation/README.md`.

## Usage
Run with --help to show the usage, i.e. which additional arguments should be used for the script, i.e., `python3 -m mira --help`:

```angular2html
$ python3 -m mira --help
usage: __main__.py [-h] -en EXAMPLE_NET -el EXAMPLE_LOG [-a] [-s SPLIT] [-vn] [-vl] [-vs] [-va] [-ver VERIFY_ALIGNMENT] [-e EXPORT]

optional arguments:
  -h, --help            show this help message and exit
  -en EXAMPLE_NET, --example_net EXAMPLE_NET
                        str (required).: Path to the Petri net (.json). See ./examples/nets/ for examples.
  -el EXAMPLE_LOG, --example_log EXAMPLE_LOG
                        str (required).: Path to the event log (.json). See ./examples/logs/ for examples.
  -a, --approximate     Whether to approximate the alignment or not.
  -s SPLIT, --split SPLIT
                        int or "inf": Split the composed alignment into parts having maximum size of "split".
  -vn, --visualize_net  Visualize the Petri net.
  -vl, --visualize_event_log
                        Visualize the event log.
  -vs, --visualize_synchronous
                        Visualize the synchronous product model.
  -va, --visualize_alignment
                        Visualize the resulting alignment.
  -ver VERIFY_ALIGNMENT, --verify_alignment VERIFY_ALIGNMENT
                        int: Verify the resulting alignment, by checking \prec_L \subseteq \prec_\gamma_L and m_i -\sigma-> m_f for "ver"
                        permutations of \gamma.
  -e EXPORT, --export EXPORT
                        str: Export filename.
```

Example usage:
`python3 -m mira -en ./examples/nets/simple.json -a -ver 10 -el ./examples/nets/simple_1.json -e ./examples/nets/simple.json -va`

Which approximates the alignment for a simple Petri net and event log, verifies it by checking 10 permutations of the alignment in the Petri net,
exports it to a file and visualizes the result.
