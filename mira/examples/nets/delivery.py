from mira.nets.petri_net import MultiNuNet, System, tPNID
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import MultiNuArc, Var
from mira.nets.marking import MultiNuMarking, MultiColorToken
from mira.prelim.multiset import Multiset

from copy import deepcopy


def get(view=False):
  object_types = {'P': {'p1', 'p2'}, # Multiset(args={'p1': 1}),
                  'D': {'d1', 'd2'}, # Multiset(args={'d1': 1, 'd2': 1}),
                  'W': {'w1', 'w2'}} # Multiset(args={'w1': 2, 'w2': 1})}
  variable_types = {'P': {Var('nu_p'), Var('p')}, 'D': {Var('d')}, 'W': {Var('w')}}

  place_names = [f'p{i}' for i in range(1, 13)]
  activities = ['start', 'stop', 'create', 'order home', 'order depot', 'ring', 'deliver home', 'deliver depot', 'register depot', 'collect', 'destroy', 'tau_1', 'tau_2']
  p = {name: Place(name) for name in place_names}
  t = {name: Transition(name, name if name[:3] != 'tau' and name not in ['start', 'stop', 'destroy'] else None) for name in activities}
  n = {**p, **t}
  net = tPNID(list(p.values()), list(t.values()), object_types=object_types, variable_types=variable_types)

  arcs = {
    Multiset(args={('{p: 1}',): 1}): [#('pp', 'create'), ('create', 'pp'),
                                      ('create', 'p1'),
                                      ('p1', 'order home'), ('order home', 'p2'), ('p2', 'ring'), ('deliver home', 'p10'), ('p1', 'order depot'),
                                      ('order depot', 'p3'), ('p3', 'tau_1'), ('tau_2', 'p4'), ('collect', 'p10'),
                                      ('order depot', 'p4'), ('p4', 'register depot'), ('p10', 'destroy')], # 12
    Multiset(args={('{d: 1}',): 1}): [('p12', 'start'), ('start', 'p11'), ('p11', 'stop'), ('stop', 'p12'), ('p11', 'ring'), ('p11', 'tau_1'),
                                      ('deliver home', 'p11'), ('deliver depot', 'p11')], # 8
    Multiset(args={('{w: 1}',): 1}): [('p8', 'register depot'), ('register depot', 'p8'), ('p8', 'deliver depot'), ('collect', 'p8')], # 4
    Multiset(args={('{p: 1}', '{d: 1}'): 1}): [('ring', 'p5'), ('p5', 'deliver home'), ('p5', 'tau_2'), ('tau_2', 'p6'), ('tau_1', 'p6'), ('p6', 'deliver depot')], # 6
    Multiset(args={('{p: 1}', '{w: 1}'): 1}): [('register depot', 'p7'), ('p7', 'deliver depot'), ('deliver depot', 'p9'), ('p9', 'collect')] # 4
  }
  [net.add_arc(MultiNuArc(n[source], n[target], labels)) for labels, label_arcs in arcs.items() for source, target in label_arcs]

  m_i = MultiNuMarking({#'pp': {MultiColorToken(1, [{package: 1}]): 1 for package in object_types['P']},
                        'p12': {MultiColorToken(1, [{'d1': 1}]): 1, MultiColorToken(1, [{'d2': 1}]): 1},
                        'p8': {MultiColorToken(1, [{'w1': 1}]): 2, MultiColorToken(1, [{'w2': 1}]): 1}})
  m_f = deepcopy(m_i)
  sn = System(net, m_i, m_f)
  if view:
    sn.view(debug=False)
  return sn


if __name__ == '__main__':
  from copy import deepcopy
  from mira.nets.utils import powerset

  sn = get(view=True)

  net_rel = sn.net.relax()
  rel_mnet = System(net_rel, sn.m_i, sn.m_f)
  rel_mnet.view(debug=False)