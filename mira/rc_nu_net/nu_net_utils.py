from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.rc_nu_net.nu_semantics import get_sub_marking

from pm4py.objects.petri_net.utils import petri_utils
import re
from copy import deepcopy
import itertools
from collections import defaultdict
import numpy as np


def add_arc_from_to(fr, to, net, weight=1, labels=None):
  if labels is None:
    labels = ('_', '_')
  a = NuPetriNet.Arc(fr, to, weight, labels)
  net.arcs.add(a)
  fr.out_arcs.add(a)
  to.in_arcs.add(a)

  return a


def add_resources_to_nu_net(nu_net, m_i, m_f, resources, resource_memories, transitions):
  resources_places, resource_transitions = {}, {}
  for rtype, (resource_instances, claim_transitions, release_transitions, corr_transitions) in resources.items():
    aval_place = petri_utils.add_place(nu_net, f'p_{rtype}')
    aval_place.properties['resource'] = 'available'
    aval_place.properties['resource_type'] = rtype
    busy_place = petri_utils.add_place(nu_net, f'p_{rtype}_')
    busy_place.properties['resource'] = 'busy'
    busy_place.properties['resource_type'] = rtype
    resources_places.update({aval_place.name: aval_place, busy_place.name: busy_place})

    for name, capacity in resource_instances.items():
      m_i[aval_place][('_', name)] += capacity
      m_f[aval_place][('_', name)] += capacity

    for t_name in corr_transitions:
      transition = transitions[t_name]
      if t_name in claim_transitions and t_name in release_transitions:
        add_arc_from_to(aval_place, transition, nu_net, labels=('_', rtype))
        add_arc_from_to(transition, aval_place, nu_net, labels=('_', rtype))
      elif t_name in claim_transitions:
        add_arc_from_to(aval_place, transition, nu_net, labels=('_', rtype))
        add_arc_from_to(transition, busy_place, nu_net, labels=('c', rtype))
      elif t_name in release_transitions:
        add_arc_from_to(busy_place, transition, nu_net, labels=('c', rtype))
        add_arc_from_to(transition, aval_place, nu_net, labels=('_', rtype))
      else:
        add_arc_from_to(busy_place, transition, nu_net, labels=('c', rtype))
        add_arc_from_to(transition, busy_place, nu_net, labels=('c', rtype))

    if (resource_memory := resource_memories.get(rtype, None)) is not None:
      memory_place = petri_utils.add_place(nu_net, f'p_{rtype}_memory')
      resources_places[memory_place.name] = memory_place
      from_transition, to_transition = transitions[resource_memory[0]], transitions[resource_memory[1]]
      add_arc_from_to(from_transition, memory_place, nu_net, labels=('c', rtype))
      add_arc_from_to(memory_place, to_transition, nu_net, labels=('c', rtype))

  return resources_places, resource_transitions


def decorate_places_preset_trans(nu_net):
  for place in nu_net.places:
    place.ass_trans = {arc.target for arc in place.out_arcs}


def decorate_transitions_prepostset(nu_net):
  for transition in nu_net.transitions:
    transition.incoming_arc_labels = {arc.source: arc.labels for arc in transition.in_arcs}
    transition.outgoing_arc_labels = {arc.target: arc.labels for arc in transition.out_arcs}


def undecorate_transitions_prepostset(nu_net):
  for transition in nu_net.transitions:
    for attr in ['incoming_arc_labels', 'outgoing_arc_labels']: #, 'regex', 'unique_vars']:
      delattr(transition, attr)


def decorate_transitions(nu_net):
  nu_net.places_ordered = sorted(nu_net.places, key=lambda p: p.name)
  for t in nu_net.transitions:
    t.v =  {0: defaultdict(list), 1: defaultdict(list)}
    t.v2 = defaultdict(list)
    for arc in sorted(t.in_arcs, key=lambda a: a.source.name):
      source_index = nu_net.places_ordered.index(arc.source)
      for i, var in enumerate(arc.labels):
        t.v[i][f'{var}{i if var == "_" else ""}'].append(source_index)

      t.v2[arc.labels].append(source_index)


def find_matches(transition, marking, ignore_modes=None, ret_add=False, verbose=False):
  if ignore_modes is None:
    ignore_modes = []
  marking_copy = deepcopy(marking)
  case_vars = sorted(set([label[0] for label in transition.incoming_arc_labels.values() if label[0] != '_']))
  resource_vars = sorted(set([label[1] for label in transition.incoming_arc_labels.values() if label[1] != '_']))
  case_colors = sorted(set(token[0] for place, token_count in marking_copy.items() for token in token_count if token[0] != '_' if place in transition.incoming_arc_labels))
  resource_colors = sorted(set(token[1] for place, token_count in marking_copy.items() for token in token_count if token[1] != '_' if place in transition.incoming_arc_labels))

  enabled_modes = []
  modes = {**{case_var: case_colors for case_var in case_vars}, **{resource_var: resource_colors for resource_var in resource_vars}}
  for mode in get_param_combinations(modes):
    if mode not in ignore_modes:
      enabled, sub_marking, add_marking = transition_enabled(marking_copy, transition, mode)
      if enabled:
        enabled_modes.append((mode, sub_marking, add_marking) if ret_add else (mode, sub_marking))

  return enabled_modes


def transition_enabled(marking, transition, mode):
  sub_marking, add_marking = NuMarking(), NuMarking()
  for place, (case_var, resource_var) in transition.incoming_arc_labels.items():
    case_color = mode[case_var] if case_var != '_' else '_'
    resource_color = mode[resource_var] if resource_var != '_' else '_'
    for token in [(case_color, resource_color), (case_color, '_'), ('_', resource_color), ('_', '_')]:
      if token in marking[place] and marking[place][token] > 0:
        sub_marking[place][token] += 1
        break
    else:
      return False, None, None

  # TODO fix? var may be mapped to a '_' in the sub_marking, which should probably stay that way.
  for place, (case_var, resource_var) in transition.outgoing_arc_labels.items():
    case_color = mode[case_var] if case_var != '_' else '_'
    resource_color = mode[resource_var] if resource_var != '_' else '_'
    add_marking[place][(case_color, resource_color)] += 1

  return True, sub_marking, add_marking


def get_param_combinations(keys_ranges):
  keys = list(keys_ranges)
  for values in itertools.product(*map(keys_ranges.get, keys)):
    mode = dict(zip(keys, values))
    valid = True
    for key1, value1 in mode.items():
      for key2, value2 in mode.items():
        if key1 != key2 and value1 == value2:
          valid = False
          break
      if not valid:
        break
    yield mode


class Mode(dict):
  def __hash__(self):
    return hash(tuple(sorted(self.items())))
