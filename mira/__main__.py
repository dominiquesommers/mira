from mira.alignments.align import align
from mira.alignments.verification import verify
from mira.alignments.exporter import export, print_info_verification
from mira.alignments import utils as align_utils
from mira.get_example import get_example
from mira.prelim.colors import Color, ResourceColor, CaseColor

import argparse
import time
import json
from pprint import pprint


parser = argparse.ArgumentParser()
requiredNamed = parser.add_argument_group('required named arguments')
parser.add_argument('-en', '--example_net', help='str (required).: Path to the Petri net (.json). See ./examples/nets/ for examples.', type=str, required=True)
parser.add_argument('-el', '--example_log', help='str (required).: Path to the event log (.json). See ./examples/logs/ for examples.', type=str, required=True)

parser.add_argument('-a', '--approximate', help='Whether to approximate the alignment or not.', action='store_true')
parser.add_argument('-s', '--split', help='int or "inf": Split the composed alignment into parts having maximum size of "split".', type=str)

parser.add_argument('-ipre', '--individual_prefix', help='str: directory where individual alignments are stored.', type=str)

parser.add_argument('-vn', '--visualize_net', help='Visualize the Petri net.', action='store_true')
parser.add_argument('-vl', '--visualize_event_log', help='Visualize the event log.', action='store_true')
parser.add_argument('-vs', '--visualize_synchronous', help='Visualize the synchronous product model.', action='store_true')
parser.add_argument('-va', '--visualize_alignment', help='Visualize the resulting alignment.', action='store_true')
parser.add_argument('-ver', '--verify_alignment', help='int: Verify the resulting alignment, by checking \prec_L \subseteq \prec_\gamma_L '
                                                       'and m_i -\sigma-> m_f for "ver" permutations of \gamma.', type=int)
parser.add_argument('-e', '--export', help='str: Export filename.', type=str)

args = parser.parse_args()

args.split = 'inf' if args.split is None or not args.split.isdecimal() else int(args.split)

nu_net, m_i, m_f, event_log, net_filename, log_filename = get_example(args.example_net, args.example_log)

m_m_i, m_m_f = align_utils.create_model_markings(m_i, m_f, event_log)

if args.visualize_net:
  nu_net.view(m_m_i, m_m_f, name='Resource Constrained nu-Petri net')

if args.visualize_event_log:
  def EVENT_STR(e):
    color = lambda s, c: f'<FONT COLOR="{c}">{s}</FONT>'
    resource_string = ','.join([color(r.rtype, ResourceColor[r.name]) for r in e['resources']])
    resource_string = f'<SUP>{resource_string}</SUP>' if resource_string != '' else ''
    activity = e['concept:name']
    if '_' in activity:
      activity = f'{activity.split("_")[0]}<SUB>{"_".join(activity.split("_")[1:])}</SUB>'
    return f'<{color(activity, CaseColor[e["case_id"]])}{resource_string}>'

  event_log.view(element_str=EVENT_STR, name='event log')

start_time = time.time()
align_parameters = {'approximation': args.approximate, 'visualize_sync_net': args.visualize_synchronous, 'split': args.split,
                    'individual_prefix': args.individual_prefix}
alignment, info = align(event_log, nu_net, m_m_i, m_m_f, align_parameters)

verification_results = {}
if args.verify_alignment is not None:
  verification_results = verify(m_m_i, m_m_f, alignment, event_log, number_of_permutations=args.verify_alignment)
print_info_verification(info, verification_results)

if args.export is not None:
  export_filename = f'./examples/alignments/{args.export}' if 'alignments/' not in args.export else args.export
  export_filename = export_filename if export_filename[-5:] == '.json' else f'{export_filename}.json'
  export(export_filename, net_filename, log_filename, alignment, verification_results, not args.approximate, info)

if args.visualize_alignment:
  alignment.view(element_str=align_utils.MOVE_STR, colors=align_utils.MOVE_COLOR, name='alignment')
