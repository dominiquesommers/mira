import numpy as np


class Firing:
  def __init__(self, transition, mode, sub_marking, add_marking, cost):
    self.transition = transition
    self.mode = mode
    self.sub_marking = sub_marking
    self.add_marking = add_marking
    self.cost = cost
    self.id = np.random.rand()

  def get_new_marking(self, marking):
    new = marking - self.sub_marking + self.add_marking
    return new

  def short_str(self, act_width:int=None, include_mode:bool=False):
    mode_str = ' {' + ', '.join([f'{k}: {v}' for k, v in sorted(self.mode.items()) if k != 'c']) + '}' if include_mode else ''
    t = self.transition.label if isinstance(self.transition.label, str) else f'{self.transition.label[0]},{self.transition.label[1]}'
    if act_width is None:
      return f'{t} ({self.mode["c"]}){mode_str}'
    return f'{t:<{act_width}} ({self.mode["c"]}){mode_str}'

  def to_json(self):
    return [(self.transition.name, self.transition.label), {var: self.mode[var] for var in sorted(self.mode.keys())},
             self.sub_marking.to_json(), self.add_marking.to_json()]

  def __repr__(self):
    return f'{self.transition.label} / {self.transition.name} ({self.mode}) m-({self.sub_marking}) m+({self.add_marking})'

  def __hash__(self):
    return hash(self.id)
