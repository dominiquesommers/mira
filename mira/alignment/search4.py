from mira.prelim.poset import POSet
from mira.nets.system import System
from mira.nets.marking import Marking, MultiNuMarking
from mira.nets.firing import Firing
from mira.nets.place import Place

from pm4py.visualization.transition_system.visualizer import view as view_transition_system
import tempfile
from graphviz import Digraph
from pm4py.util.lp import solver as lp_solver
from bidict import bidict
from collections import defaultdict
from cvxopt import matrix
from typing import Union, List
import numpy as np
import heapq
import time
import sys
import psutil
from datetime import datetime, timedelta
from typing import Callable


class State:
  def __init__(self, markings, costs:dict, last_firing:Union[Firing,None], T_vec:Union[List[float],None],
               trustable:bool, parent, enabled_firings:List[Firing]):
    self.markings = markings
    self.costs = {'c': costs['c1'] + costs['c2'], **costs} # m_i -c1-> m -c2-> m_f
    self.last_firing = last_firing
    self.T_vec = T_vec
    self.trustable = trustable
    self.parent = parent
    self.enabled_firings = enabled_firings

  def __lt__(self, other):
    # TODO prefer unpack transitions, (pack transitions), unprefer non-unpack void transitions.
    if self.costs['c'] != other.costs['c']:
      return self.costs['c'] < other.costs['c']
    if self.trustable and not other.trustable:
      return True
    return self.costs['c2'] < other.costs['c2']

  def __get_firing_sequence(self):
    sequence = []
    if self.parent is not None:
      sequence.extend(self.parent.__get_firing_sequence())
    if self.last_firing is not None:
      sequence.append((self.last_firing.transition, self.last_firing.mode))
    return sequence

  def get_possible_next_transitions(self):
    # TODO implement
    ...
  #   if self.last_firing is None:
  #     return list({p_out_arc.target for p in self.marking for p_out_arc in p.out_arcs})
  #   return list({p_out_arc.target for t_out_arc in self.last_firing.transition.out_arcs for p_out_arc in t_out_arc.target.out_arcs})

  def update_cost_to_m_f(self, cost_to_m_f):
    self.costs['c'] = self.costs['c'] - self.costs['c2'] + cost_to_m_f
    self.costs['c2'] = cost_to_m_f

  def __repr__(self):
    return f'STATE\n{self.markings}\nm_i -{self.costs["c1"]}-> m -{self.costs["c2"]}-> m_f ({"" if self.trustable else "not "}trustable)\n{self.last_firing}\n' + \
           f'prev firing: {self.parent.last_firing if self.parent is not None else "None"}\n'

  def __hash__(self):
    return hash(self.markings)

  def __eq__(self, other):
    return self.markings == other.markings


# class Node:
#   def __init__(self, submarking):
#     self.submarking = submarking

class Edge:
  def __init__(self, firing, sub_indices, add_indices, index=0):
    self.firing = firing
    self.sub_indices = sub_indices
    self.add_indices = add_indices
    self.index = index

# class Node:
#   def __init__(self, sub_marking, index=0):
#     self.sub_marking = sub_marking
#     self.index = index

class TransitionGraph:
  def __init__(self, nodes):
    self.nodes = nodes
    self.edges = set()

  def get_leafs(self):
    return frozenset([(i, node) for i, node in enumerate(self.nodes) if not any(i in edge.sub_indices for edge in self.edges)])

  def view(self, sub_markings=None, minimal:bool=True, element_str=lambda x: f'{x}', colors=lambda x: 'black', fill_colors=lambda x: 'white', name:str=None, rankdir:str='LR'):
    if len(self.nodes) == 0:
      print(f'Nothing to view, {type(self).__name__} is empty.')
      return
    view_transition_system(self.create_view(sub_markings, minimal, element_str, colors, fill_colors, name=name, rankdir=rankdir))

  def create_view(self, sub_markings=None, minimal:bool=True, element_str=lambda x: f'{x}', colors=lambda x: 'black',
                  fill_colors=lambda x: 'white', name:str=None, rankdir:str='LR'):
    postfix = '' if name is None else f'::{name}'
    sub_markings = {} if sub_markings is None else sub_markings
    font_size = 18

    filename = tempfile.NamedTemporaryFile(suffix=f'{postfix}.gv')
    viz = Digraph('', filename=filename.name, engine='dot', graph_attr={'bgcolor': 'transparent', 'rankdir': rankdir})

    viz.attr('node')
    for index, sm_i in enumerate(self.nodes):
      # fillcolor = fill_colors(element)
      style = {'style': 'filled,rounded', 'color': 'black', 'penwidth': '3', 'fillcolor': 'white', 'fontcolor': 'black'}
      string = element_str(sub_markings[sm_i])
      print(string)
      viz.node(str(index), string, fontsize=str(font_size), **style, shape='box')

    for edge in self.edges:
      style = {'style': 'filled', 'color': 'black', 'penwidth': '3', 'fillcolor': 'white', 'fontcolor': 'black'}
      string = f'{edge.firing.transition.name}\n{edge.firing.objects()}'
      viz.node(f'f{edge.index}', string, fontsize=str(font_size), **style, shape='box')
      for i in edge.sub_indices:
        viz.edge(str(i), f'f{edge.index}')
      for j in edge.add_indices:
        viz.edge(f'f{edge.index}', str(j))

    viz.attr(overlap='false')
    viz.format = 'png'
    return viz


class PrioritySet(object):
  def __init__(self):
    self.heap = []
    heapq.heapify(self.heap)
    self.set_ = set()

  def __len__(self):
    return len(self.heap)

  def add(self, d):
    # heapq.heappush(self.heap, d)
    if not d in self.set_:
      heapq.heappush(self.heap, d)
      self.set_.add(d)

  def pop(self):
    d = heapq.heappop(self.heap)
    self.set_.remove(d)
    return d


class Search:
  def __init__(self, system:System, costs:Callable=None, variant='a_star'):
    self.system = system
    self.black_system = self.system.blacken()
    self.costs = costs

  def compute_cheapest_run(self, max_search_time:float=np.inf, min_memory_available:float=0):
    self.start_time = time.time()
    print(f'Starting at {datetime.now()}')
    self.max_search_time = max_search_time
    self.min_memory_available = min_memory_available
    self.process = psutil.Process()

    print(f'aligning from\n{self.system.m_i}\nto\n{self.system.m_f}')

    for place in self.system.m_i.keys():
      place.alpha = self.system.net.alpha(place, self.system.net.variable_types)

    # self.submarkings = bidict(enumerate(self.system.m_i.split_tokens()))

    transition_graph = TransitionGraph(list(self.system.m_i.split_tokens()))

    open_states = [transition_graph.get_leafs()]
    while len(open_states) > 0:
      leafs = open_states.pop(0)
      print(f'\n{leafs=}')
      marking = sum([sm for i, sm in leafs], start=MultiNuMarking())
      print(f'{marking=}')

      enabled_firings, n = self.system.net.enabled_firings(marking=marking)
      for f in enabled_firings:
        print('firing', f)

        sub_is = []
        for sub_sub_marking in f.sub_marking.split_tokens():
          sub_is.append([i for i, sm in leafs if sm == sub_sub_marking][0])

        add_is = []


        for sub_add_marking in f.add_marking.split_tokens():
          if (sm_i := self.submarkings.inverse.get(sub_add_marking, None)) is not None:
            print('looking for', sm_i, 'in', leafs)
            add_is.append([i for i, sm in leafs if sm == sm_i][0])
          else:
            new_i = len(self.submarkings.keys())
            print('new', new_i)
            self.submarkings[new_i] = sub_add_marking
            transition_graph.nodes.append(new_i)
            add_is.append(len(transition_graph.nodes) - 1)

        transition_graph.edges.add(Edge(f, set(sub_is), set(add_is), index=len(transition_graph.edges)))
        open_states.append(transition_graph.get_leafs())
        print(transition_graph.get_leafs())
        # print(stop)

    transition_graph.view(self.submarkings)
    print(fdslfllsls)
    # open_states = PrioritySet()
    # leafs = transition_graph.get_leafs()
    # open_states.add(State(frozenset([sm for i, sm in leafs]), {'c1': 0, 'c2': 0}, last_firing=None, T_vec=None, trustable=True, parent=None, enabled_firings=[]))
    # closed_markings = set()
    #
    # i = 0
    # while len(open_states) > 0:
    #   i += 1
    #   state = open_states.pop()
    #
    #   if state.markings in closed_markings:
    #     continue
    #
    #   closed_markings.add(state.markings)
    #   marking = sum([self.submarkings[i] for i in state.markings], start=MultiNuMarking())
    #   print('\ncurrent marking', state.costs['c1'], marking, ' last firing ', state.last_firing)
    #
    #   self.system.marking = marking
    #
    #   if self.system.marking == self.system.m_f:
    #     print('final marking reached. '*5)
    #     transition_graph.view(self.submarkings)
    #     return #self.get_run(state), {'cost': state.costs['c1']}
    #
    #   enabled_firings, n = self.system.enabled_firings(last_firing=state.last_firing)
    #
    #   for f in enabled_firings:
    #     print(f)
    #     sub_is = []
    #     for sub_sub_marking in f.sub_marking.split_tokens():
    #       sub_is.append(self.submarkings.inverse[sub_sub_marking])
    #     add_is = []
    #     for sub_add_marking in f.add_marking.split_tokens():
    #       add_is.append(self.submarkings.inverse.get(sub_add_marking, len(self.submarkings.keys())))
    #       self.submarkings[add_is[-1]] = sub_add_marking
    #       transition_graph.nodes.append(add_is[-1])
    #
    #     # if exists:
    #     #   print('already have this one!!!')
    #     #   continue
    #
    #     new_state = State(state.marking - f.sub_marking + f.add_marking, {'c1': state.costs['c1'] + self.costs(f.transition), 'c2': 0},
    #                           last_firing=f, T_vec=None, trustable=True, parent=state, enabled_firings=[])
    #     print('---- marking', new_state.costs['c1'], new_state.marking, ' last firing ', new_state.last_firing)
    #     open_states.add(new_state)
    #
    #
    #     if not exists:
    #       transition_graph.edges.add(Edge(f, set(sub_is), set(add_is), index=len(transition_graph.edges)))

      # if i == 2:
      #   break
    transition_graph.view()
    print(stop)

    return None, None

  def get_run(self, state):
    if state is None:
      return None, None
    self.system.marking = self.system.m_i
    if state.last_firing is None:
      return POSet()
    reversed_firings = [(state.last_firing, state.marking)]
    if state.parent is not None and state.last_firing is not None:
      parent = state.parent
      while parent.parent is not None:
        reversed_firings.append((parent.last_firing, parent.marking))
        parent = parent.parent
    for firing, new_marking in reversed(reversed_firings):
      self.system.fire(firing, new_marking)
    return self.system.firings



def main():
  from mira.nets.system import System
  from mira.alignment.log_net import create_log_system
  from mira.alignment.synchronous_product import tPNIDSynchronousProductSystem
  from mira.alignment.cost import Cost
  from mira.logs.event_log import Log
  from mira.prelim.colors import Color
  import json

  # data = {
  #   'type': 'tPNID',
  #   'T': ['a', 'b', 'c'], 'P': ['p1', 'p2', 'p3', 'p4', 'pd'],
  #   'F': {
  #     '[([p])]': [['p1', 'a'], ['a', 'p2'], ['p2', 'b'], ['c', 'p4']],
  #     '[([d])]': [['pd', 'b'], ['c', 'pd']],
  #     '[([p], [d])]': [['b', 'p3'], ['p3', 'c']],
  #   },
  #   'O': {'P': ['x', 'y'], 'D': ['v']},
  #   'V': {'P': ['p'], 'D': ['d']},
  #   'm_i': '[[([x]),([y])]·p1,[([v])]·pd]',
  #   'm_f': '[[([x]),([y])]·p4,[([v])]·pd]',
  # }
  # system = System.from_json_dict(data)
  # # system.view(debug=True)

  with open('./tests/nets/search.json', 'r') as json_file:
    data = json.load(json_file)
    system = System.from_json_dict(data)
    # system.view()
  with open(f'./tests/logs/search_1.json', 'r') as json_file:
    data = json.load(json_file)
    log = Log.from_json_dict(data)

  log_system = create_log_system(log, type(system.net))
  sync_product = tPNIDSynchronousProductSystem(log_system, system)
  # sync_product.view(debug=True)

  cost = lambda t: {'tau_a_0': 0, 'a_0': 1, 'a_0-a': 0, 'a': 1, 'c': 1, 'b': 10}[t.name]
  search = Search(sync_product, cost)
  run, info = search.compute_cheapest_run()

  for state in run:
    print(state)

  print(info)
  print(f'Cost: {info["cost"]:_}')
  view = True
  if view:
    TRANSITION_COLOR = lambda m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN
    # run = run.projection(lambda x: not (x.transition.move_type == 'log' and x.transition.label is None))
    move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
    fill_color = lambda x: 'gray' if x.transition.label is None else 'white'
    run.view(element_str=move_str, colors=TRANSITION_COLOR, fill_colors=fill_color)


if __name__ == '__main__':
  main()
  exit()
  from mira.nets.petri_net import Net, NuNet, MultiNuNet, System
  from mira.nets.marking import Marking, NuMarking, MultiNuMarking
  from mira.nets.place import Place
  from mira.nets.transition import Transition
  from mira.nets.token import MultiColorToken, ColorToken
  from mira.nets.arc import Arc, NuArc, MultiNuArc, InhibitorArc, VariableMultiNuArc

  test = 'multi4'
  if test == 'nu':
    ps = [Place(name) for name in ['pi', 'p1', 'pf']]
    ts = [Transition(name, name) for name in ['a', 'b', 'c']]
    net = NuNet(ps, ts)
    [net.add_arc(NuArc(s, t, 'x')) for s, t in [(ps[0], ts[0]), (ps[0], ts[1]), (ts[0], ps[2]), (ts[1], ps[1]), (ps[1], ts[2]), (ts[2], ps[2])]]

    m_i = NuMarking({'pi': {ColorToken('a'): 1}})
    m_f = NuMarking({'pf': {ColorToken('a'): 1}})
    costs = {ts[0]: 3, ts[1]: 1, ts[2]: 1}

  elif test == 'multi1':
    ps = [Place(name) for name in ['p1', 'p2', 'p3']]
    ts = [Transition(name, name) for name in ['t', 't_']]
    net = MultiNuNet(ps, ts)
    net.add_arc(MultiNuArc(ps[0], ts[0], ('X', '_')))
    net.add_arc(MultiNuArc(ps[1], ts[0], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[0], ('X - {x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[2], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ps[0], ts[1], ('_', '_')))

    m_i = MultiNuMarking({
      ps[0]: {MultiColorToken(2, [{'a': 1, 'b': 1}, {}]): 1},
      ps[1]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    m_f = MultiNuMarking({
      ps[2]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    costs = {ts[0]: 1, ts[1]: 1}
  elif test == 'nu2':
    ps = [Place(name) for name in ['pi', 'p1', 'pf']]
    ts = [Transition(name, name) for name in ['a', 'b', 'c']]
    net = NuNet(ps, ts)
    [net.add_arc(NuArc(s, t, 'x')) for s, t in [(ps[0], ts[0]), (ps[0], ts[1]), (ts[0], ps[2]), (ts[1], ps[1]), (ps[1], ts[2]), (ts[2], ps[2])]]

    m_i = NuMarking({'pi': {ColorToken('a'): 1}})
    m_f = NuMarking({'pf': {ColorToken('a'): 1}})
    costs = {ts[0]: 3, ts[1]: 1, ts[2]: 1}

  elif test == 'multi1':
    ps = [Place(name) for name in ['p1', 'p2', 'p3']]
    ts = [Transition(name, name) for name in ['t', 't_']]
    net = MultiNuNet(ps, ts)
    net.add_arc(MultiNuArc(ps[0], ts[0], ('X', '_')))
    net.add_arc(MultiNuArc(ps[1], ts[0], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[0], ('X - {x: 1}', '_')))
    net.add_arc(MultiNuArc(ts[0], ps[2], ('{x: 1}', '_')))
    net.add_arc(MultiNuArc(ps[0], ts[1], ('_', '_')))

    m_i = MultiNuMarking({
      ps[0]: {MultiColorToken(2, [{'a': 1, 'b': 1}, {}]): 1},
      ps[1]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    m_f = MultiNuMarking({
      ps[2]: {MultiColorToken(2, [{'a': 1}, {}]): 1, MultiColorToken(2, [{'b': 1}, {}]): 1}
    })
    costs = {ts[0]: 1, ts[1]: 1}
  elif test == 'multi2':
    ps = {name: Place(name) for name in ['pi', 'p1', 'p2', 'p10', 'pf']}
    ts = {name: Transition(name, name) for name in ['t_c', 't_sp', 't_p', 'a']}
    net = MultiNuNet(list(ps.values()), list(ts.values()))
    [net.add_arc(MultiNuArc(source, target, ('X',))) for source, target in [(ps['pi'], ts['t_c']), (ts['t_c'], ps['p1']),
                                                                                (ps['p1'], ts['t_p']), (ps['p1'], ts['t_sp']),
                                                                                (ts['a'], ps['pf']), (ps['p10'], ts['a']),
                                                                                (ps['p2'], ts['a']), (ts['t_sp'], ps['p2'])
                                                                            ]]
    net.add_arc(MultiNuArc(ps['p2'], ts['t_p'], ('Y',)))
    net.add_arc(MultiNuArc(ts['t_p'], ps['p2'], ('X + Y',)))
    [net.add_arc(InhibitorArc(ps[place], ts[transition])) for place, transition in [('p2', 't_c'), ('p2', 't_sp'), ('p1', 'a')]]

    m_i = MultiNuMarking({
      ps['pi']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1},
      ps['p10']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1},
    })
    m_f = MultiNuMarking({
      ps['pf']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1},
    })
    costs = {t: 1 for t in ts.values()}

  elif test == 'multi3':
    ps = {name: Place(name) for name in ['pi', 'p1', 'p2', 'p10', 'pf']}
    ts = {name: Transition(name, name) for name in ['t_c', 't_ep', 't_p', 'a']}
    net = MultiNuNet(list(ps.values()), list(ts.values()))
    [net.add_arc(MultiNuArc(source, target, ('X',))) for source, target in [(ps['pi'], ts['t_c']), (ts['t_c'], ps['p1']),
                                                                                (ps['p1'], ts['t_p']),
                                                                                (ts['a'], ps['pf']), (ps['p10'], ts['a']),
                                                                                (ps['p2'], ts['a'])
                                                                            ]]
    net.add_arc(MultiNuArc(ts['t_ep'], ps['p2'], ('_',)))
    net.add_arc(MultiNuArc(ps['p2'], ts['t_p'], ('Y',)))
    net.add_arc(MultiNuArc(ts['t_p'], ps['p2'], ('X + Y',)))
    [net.add_arc(InhibitorArc(ps[place], ts[transition])) for place, transition in [('p2', 't_c'), ('p2', 't_ep'), ('p1', 'a')]]

    m_i = MultiNuMarking({
      ps['pi']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1},
      ps['p10']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{}]): 1},
    })
    m_f = MultiNuMarking({
      ps['pf']: {MultiColorToken(1, [{'a': 1, 'b': 1}]): 1, MultiColorToken(1, [{}]): 1},
    })
    costs = {t: 1 for t in ts.values()}
  elif test == 'multi4':
    # TODO MONDAY, FIX FOR THIS.

    ps = {name: Place(name) for name in ['i', 'f']}
    t = Transition('a', 'a')
    net = MultiNuNet(list(ps.values()), [t])
    net.add_variable_arc(VariableMultiNuArc(ps['i'], t, ('X',), (1, np.inf)))
    net.add_variable_arc(VariableMultiNuArc(t, ps['f'], ('X',), None))

    print('places')
    for place in net.places:
      print(place)
    print('\ntransitions')
    for transition in net.transitions:
      print(transition)
    print('\narcs')
    for arc in net.arcs:
      print(arc)
    print('\ninhibit arcs')
    for arc in net.inhibitor_arcs:
      print(arc)
    # print(buh)

    m_i = MultiNuMarking({
      ps['i']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1}
    })
    m_f = MultiNuMarking({
      ps['f']: {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1}
    })
    cost_function = lambda t: 10000 if t.label is not None else 1

  else:
    raise ValueError()

  system = System(net, m_i, m_f)

  search = Search(system, cost_function)
  run, stats = search.compute_cheapest_run()

  print('cost', stats['cost'])
  print('run', run)
  run.view(element_str=lambda x: f'{x.transition}')