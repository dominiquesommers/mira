import json
from pm4py import read_xes
from pm4py.objects.log.obj import Event
import argparse
import datetime
import time
import tqdm
from heapq import merge
import os
from copy import copy


parser = argparse.ArgumentParser()
parser.add_argument('-xes', '--xes_file', help='', type=str)

parser.add_argument('-n', '--number_of_traces', help='', type=int)

parser.add_argument('-fact', '--filter_activities', help='', type=str)
parser.add_argument('-kact', '--keep_activities', help='', type=str)
args = parser.parse_args()

max_traces = args.number_of_traces

if max_traces is None:
  max_traces = 'all'


filename = args.xes_file[:-4] if args.xes_file[-4:] == ".xes" else args.xes_file
from pm4py.objects.log.importer.xes import importer as xes_importer
xes_import_param = {} if max_traces == 'all' else {'max_traces': max_traces}
xes_log = xes_importer.apply(f'{filename}.xes', xes_import_param)

class OverlappingCases:
  def __init__(self, cases=None, timestamp_key='time:timestamp'):
    if cases is None:
      cases = []
    self.cases = cases
    self.timestamp_key = timestamp_key
    self.start_time = None if len(cases) == 0 else min(trace[ 0][timestamp_key] for trace in self.cases)
    self.end_time   = None if len(cases) == 0 else max(trace[-1][timestamp_key] for trace in self.cases)
    self.flattened_trace = None

  def has_overlap(self, case):
    return not (self.end_time < case[0][self.timestamp_key] or case[-1][self.timestamp_key] < self.start_time)

  def add_case(self, case):
    self.cases.append(case)
    self.start_time = min(case[ 0][self.timestamp_key], self.start_time)
    self.end_time   = max(case[-1][self.timestamp_key], self.end_time)

  def flatten(self, add_case_id_to_event=True, case_key='case_id', sort=True):
    cases = [[Event(**event, **{case_key: case.attributes['concept:name']}) for event in case] for case in self.cases]
    # Automatically only sort across different cases!
    if sort:
      flattened_trace = list(merge(*cases, key=lambda e: e[self.timestamp_key])) #sorted([event for case in cases for event in case], )
      print()
    else:
      flattened_trace = [event for case in cases for event in case]
    self.flattened_trace = flattened_trace

  def export_to_json(self, filename):
    if self.flattened_trace is None:
      raise ValueError('Flatten first.')

    filename = filename if filename[-5:] == '.json' else f'{filename}.json'
    with open(filename, 'w') as json_file:
      json.dump({
        'n_events': len(self.flattened_trace),
        'n_cases': len(self.cases),
        'events': [[f'{event["concept:name"]}:{event["lifecycle:transition"]}', event['case_id'], [event['org:resource']]] for event in self.flattened_trace],
        "<": "all",
        "||": [],
        "start_time": str(min(event[self.timestamp_key] for event in self.flattened_trace)),
        "end_time": str(max(event[self.timestamp_key] for event in self.flattened_trace))
      }, json_file)

individual = False

overlaps = []
for index, case in tqdm.tqdm(enumerate(xes_log)):
  for overlap in overlaps:
    if (not individual) and overlap.has_overlap(case):
      overlap.add_case(case)
      break
  else:
    overlaps.append(OverlappingCases([case]))

print(len(overlaps), len(overlaps[0].cases), sum(len(case) for case in overlaps[0].cases))

if individual:
  filename = f"{'/'.join(filename.split('/')[:-1])}/individual/{filename.split('/')[-1]}"

if individual or len(overlaps) > 1:
  for index, overlap in tqdm.tqdm(enumerate(overlaps)):
    overlap.flatten(add_case_id_to_event=True, sort=not individual)
    identifier = overlap.flattened_trace[0]['case_id'] if individual else index + 1
    overlap.export_to_json(filename=f'{filename}_{identifier}.json')
else:
  overlaps[0].flatten(add_case_id_to_event=True)
  print('exporting!')
  overlaps[0].export_to_json(filename=f'{filename}_{max_traces}.json')
