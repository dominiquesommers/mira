import json
from _ctypes import PyObj_FromPtr  # see https://stackoverflow.com/a/15012814/355230
import re


class NoIndent(object):
    """ Value wrapper. """
    def __init__(self, value):
        if not isinstance(value, (list, tuple, dict)):
            raise TypeError(f'Only lists, tuples and dicts can be wrapped, not {value.__class__}')
        self.value = value


class MyEncoder(json.JSONEncoder):
    FORMAT_SPEC = '@@{}@@'  # Unique string pattern of NoIndent object ids.
    regex = re.compile(FORMAT_SPEC.format(r'(\d+)'))  # compile(r'@@(\d+)@@')

    def __init__(self, **kwargs):
        # Keyword arguments to ignore when encoding NoIndent wrapped values.
        ignore = {'cls', 'indent'}

        # Save copy of any keyword argument values needed for use here.
        self._kwargs = {k: v for k, v in kwargs.items() if k not in ignore}
        super(MyEncoder, self).__init__(**kwargs)

    def default(self, obj):
        return (self.FORMAT_SPEC.format(id(obj)) if isinstance(obj, NoIndent)
                    else super(MyEncoder, self).default(obj))

    def iterencode(self, obj, **kwargs):
        format_spec = self.FORMAT_SPEC  # Local var to expedite access.

        # Replace any marked-up NoIndent wrapped values in the JSON repr
        # with the json.dumps() of the corresponding wrapped Python object.
        for encoded in super(MyEncoder, self).iterencode(obj, **kwargs):
            match = self.regex.search(encoded)
            if match:
                id = int(match.group(1))
                no_indent = PyObj_FromPtr(id)
                json_repr = json.dumps(no_indent.value, **self._kwargs)
                # Replace the matched id string with json formatted representation
                # of the corresponding Python object.
                encoded = encoded.replace('"{}"'.format(format_spec.format(id)), json_repr)

            yield encoded


def __info_indent(key, subinfo):
  if not isinstance(subinfo, (list, tuple, dict)):
    return subinfo
  second_order_indent = ['individual', 'ilp']
  if key not in second_order_indent:
    return NoIndent(subinfo)
  if isinstance(subinfo, (list, tuple)):
    return [NoIndent(subsubinfo) for subsubinfo in subinfo]
  if isinstance(subinfo, dict):
    return {subkey: NoIndent(subsubinfo) for subkey, subsubinfo in subinfo.items()}
  raise TypeError(f'Only lists, tuples, dicts and values can be wrapped, not {subinfo.__class__}')


def __verification_indent(key, subinfo):
  if not isinstance(subinfo, (list, tuple, dict)):
    return subinfo
  if isinstance(subinfo, dict):
    return NoIndent(subinfo)
  if key == 'rejected_sigma':
    return [NoIndent(move) for move in subinfo]


def get_info_verification(info, verification_results):
  info_json = {key: __info_indent(key, subinfo) for key, subinfo in info.items()}
  verification_json = {key: __verification_indent(key, subresult) for key, subresult in verification_results.items()}
  data = {'info': info_json, 'verification': verification_json}
  return data

def print_info_verification(info, verification_results):
  data = get_info_verification(info, verification_results)
  print(json.dumps({key: subinfo for key, subinfo in data.items() if key in ['info', 'verification']}, cls=MyEncoder, sort_keys=False, indent=2))

def export(filename, net_filename, log_filename, alignment, verification_results, exact, info):
  alignment_json = alignment.to_json()
  alignment_json['M'] = [NoIndent(move) for move in alignment_json['M']]
  alignment_json['r'] = [NoIndent(row) for row in alignment_json['r']]

  data = {'N': net_filename, 'L': log_filename, **get_info_verification(info, verification_results), 'exact': exact, **alignment_json}
  with open(filename, 'w') as json_file:
    json.dump(data, json_file, cls=MyEncoder, sort_keys=False, indent=2)