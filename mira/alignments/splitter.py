from mira.alignments import utils

from collections import defaultdict
from copy import copy
import tqdm
import numpy as np

class Splitter:
  def __init__(self, composed_alignment, idr_capacities):
    self.composed_alignment = composed_alignment
    self.idr_capacities = idr_capacities

  def __set_claim_release_counts(self, al):
    for index, move in enumerate(al):
      move.claims, move.releases = defaultdict(int), defaultdict(int)
      if utils.is_log_move(move.transition):
        continue
      for arc in move.transition.reference.in_arcs:
        if arc.source.properties.get('resource', '-') == 'available':
          move.claims[move.mode[arc.labels[1]]] += int(arc.weight)
      for arc in move.transition.reference.out_arcs:
        if arc.target.properties.get('resource', '-') == 'available':
          move.releases[move.mode[arc.labels[1]]] += int(arc.weight)

  def split_prep(self, al):
    self.__set_claim_release_counts(al)

    r = np.array(al.r)
    for move_index in tqdm.tqdm(al.traverse_yield(return_indices=True), total=len(al), postfix=f'{"Setting up for splitting compal":>40}'):
      current_move = al[move_index]
      pre_indices = r[:, move_index].nonzero()[0]
      parents = [al[pre_index] for pre_index in pre_indices if not (r[pre_index] * r[:, move_index].T).any()]
      pres = set(al[i] for i in pre_indices)
      if len(pres) == 0:
        current_move.before_capacities = copy(self.idr_capacities)
        current_move.last_status = {idr: 'full' for idr in self.idr_capacities.keys()}
        current_move.previous_moves = pres
      else:
        current_move.previous_moves = pres

        current_move.before_capacities = {idr: cap - sum([move.claims[idr] for move in current_move.previous_moves])
                                                   + sum([move.releases[idr] for move in current_move.previous_moves])
                                          for idr, cap in self.idr_capacities.items()}
        current_move.last_status = {}

        for idr, cap in self.idr_capacities.items():
          if current_move.before_capacities[idr] < 0:
            current_move.last_status[idr] = 'violation'
          elif current_move.before_capacities[idr] == cap:
            current_move.last_status[idr] = 'full'
          else:
            current_move.last_status[idr] = 'violation' if any(parent.last_status[idr] == 'violation' for parent in parents) else 'full'

  def split(self, al, accepted_nr_elements=1, first=True):
    if accepted_nr_elements == 'inf' or len(al) <= accepted_nr_elements:
      return [al]

    if first:
      self.split_prep(al)

    l = np.sum(al.r, axis=0)
    candidates = np.array(np.argsort(np.abs(0.5 - np.maximum(l, 0.01) / al.r.shape[0]))).reshape(-1)
    al_min, al_max = al.min(), al.max()
    for candidate in candidates:
      candidate = al[candidate]
      if sum(candidate.claims.values()) == 0:
        continue
      if candidate in al_min or candidate in al_max:
        continue

      if all(candidate.last_status[idr] == 'full' for idr in self.idr_capacities.keys()):
        left = al.prefixopen([candidate])
        right = al.projection(lambda x: x not in left, is_already_closed=True)
        return self.split(left, accepted_nr_elements=accepted_nr_elements, first=False) + self.split(right, accepted_nr_elements=accepted_nr_elements, first=False)

    print('no valid candidates found.')
    return [al]

  def export(self):
    ...
