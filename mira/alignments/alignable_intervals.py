from mira.alignments import utils
from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.prelim.poset import POSet

import numpy as np
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, LpAffineExpression, LpInteger
from collections import defaultdict
from itertools import product
import time
import tqdm


def get_minimal_alignable_intervals(composed_alignment:POSet, m_i:NuMarking, k:dict, name:str=''):
  ilp_solver = ILPSolver(composed_alignment, m_i, k, name=name)
  return ilp_solver.solve()


class ILPSolver:
  def __init__(self, composed_alignment:POSet, m_i:NuMarking, k:dict, name:str=''):
    self.clm_rls_move = lambda move: any([in_arc.source in m_i and in_arc.labels[1] != '_' for in_arc in move.transition.reference.in_arcs]) or \
                                     any([out_arc.target in m_i and out_arc.labels[1] != '_' for out_arc in move.transition.reference.out_arcs])
    self.clm_move = lambda move: any([in_arc.source in m_i and in_arc.labels[1] != '_' for in_arc in move.transition.reference.in_arcs])

    self.composed_alignment = composed_alignment
    self.m_i = m_i
    self.k = k
    self.name = name

    self.prepare()
    self.setup()

  def prepare(self):
    self.clm_rls_indices = [i for i, move in enumerate(self.composed_alignment) if self.clm_rls_move(move)]
    # print(f'Reduced composed alignment from {len(self.composed_alignment)} to {len(self.clm_rls_indices)} by only keeping claim and release moves.')

    self.R = self.composed_alignment.r
    self.R_cr = self.R[np.ix_(self.clm_rls_indices, self.clm_rls_indices)]
    self.__compute_Cs_k()

    self.R_cr = np.pad(self.R_cr, [(0,1), (0,1)], 'constant', constant_values=1)
    self.R_cr[:,-1] = 0

    self.C_in = np.pad(self.C_in, [(0, 1), (0, 0)])
    for place, token_count in self.m_i.items():
      if place.properties.get('resource', '-') != 'available':
        continue
      for (_, idr), count in token_count.items():
        idr_index = self.idrs.index(idr)
        self.C_in[-1,idr_index] = self.k[idr_index] - count

    self.C_out = np.pad(self.C_out, [(0, 1), (0, 0)])
    self.info = {'n': len(self.composed_alignment), 'n_rc': len(self.clm_rls_indices)}

  def setup(self):
    t1 = time.time()
    self.problem = LpProblem('', LpMinimize)
    n = len(self.R_cr)
    self.I = range(n)

    self.X = np.matrix(LpVariable.matrix('X', (self.I, self.I), lowBound=0, upBound=1))#, cat='Binary'))

    for i, j in product(self.I, self.I):
      self.X[i,j].setInitialValue(self.R_cr[i,j])
      if (j == n - 1 or i == n - 1) or self.composed_alignment[self.clm_rls_indices[i]].mode.get('c', 1) == self.composed_alignment[self.clm_rls_indices[j]].mode.get('c', 2):
        self.X[i, j].fixValue()

    self.problem.setObjective(LpAffineExpression([
      (self.X[i, j], 10000 * self.R_cr[j, i] * (1 - self.R_cr[i, j]) + 1 * (1 - self.R_cr[j, i]) * (1 - self.R_cr[i, j]))
       for i, j in product(self.I, self.I)
    ]))

    consts = 0
    for i in tqdm.tqdm(self.I, postfix=f'{f"Setting up the ILP problem {self.name}":>40}'):
      if i == n - 1:
        continue
      move = self.composed_alignment[self.clm_rls_indices[i]]
      case_i = move.mode.get('c', 1)
      for j in self.I:
        if j == n - 1 or i == j:
          continue

        same_case = case_i == self.composed_alignment[self.clm_rls_indices[j]].mode.get('c', 2)
        if self.R_cr[i,j] == 1:
          self.problem.addConstraint(self.X[i,j] + self.X[j,i] >= 1) # Constraint 12 (can only reverse arcs, not just remove them)
          consts += 1

        for k in self.I:
          if k == n - 1 or (same_case and case_i == self.composed_alignment[self.clm_rls_indices[k]].mode.get('c', 3)):
            continue
          self.problem.addConstraint(self.X[i,j] + self.X[j,k] - self.X[i,k] <= 1)  # Constraint 13 (transitive closure)
          consts += 1

      if self.clm_move(move):
        for idr_i, idr in enumerate(self.idrs):
          if self.C_in[i,idr_i] == 0:
            continue
          self.problem.addConstraint(((1 - self.X[i]) @ self.C_in[:, idr_i])[0, 0] - (self.X[:,i].T @ self.C_out[:, idr_i])[0, 0] <= self.k[idr_i])  # Constraint 14 (no violations)
          consts += 1

    self.info.update({'constraints': consts, 't': {'setup': time.time() - t1}})

  def compute_cost(self, R, X):
    return sum([10000 *      R[j, i]  * (1 - R[i, j]) * X[i, j] + \
                    1 * (1 - R[j, i]) * (1 - R[i, j]) * X[i, j] for i, j in product(self.I, self.I)])

  def solve(self, again_after_non_binary=True):
    if not again_after_non_binary:
      self.info['non_binary'] = True

    t1 = time.time()
    self.problem.solve()
    self.info['t']['solve'] = time.time() - t1
    self.R_cr = self.R_cr[:-1,:-1]

    if LpStatus[self.problem.status] == 'Infeasible':
      raise ValueError('Infeasible!')
    self.info.update({'status': LpStatus[self.problem.status], 'cost': self.problem.objective.value()})

    X_ = np.matrix([[self.X[i,j].value() for j in self.I] for i in self.I])[:-1, :-1]
    if np.any((0.001 < X_) & (X_ < 0.999)):
      if again_after_non_binary:
        print('Result contains non-binary values, solving again with integers.')
        for i, j in product(self.I, self.I):
          self.X[i,j].cat = LpInteger
        return self.solve(again_after_non_binary=False)
      else:
        raise ValueError('LP problem solution contains non-integers.')

    X_ = np.matrix(X_, dtype=int)

    new_R = np.matrix(np.copy(self.R))
    new_R[np.ix_(self.clm_rls_indices, self.clm_rls_indices)] = X_

    if len(self.clm_rls_indices) != len(self.R):
      # Propagate the reversed arcs to non-clm/rls moves.
      removed_arcs = np.transpose(np.multiply(self.R, 1 - new_R).nonzero())
      for i, j in removed_arcs:
        newrow = np.multiply(new_R[i], new_R[:,j].T)
        new_R[:,j] -= newrow.T
        if np.any(new_R[:,j] < 0):
          raise ValueError('R is negative.')

      if np.any((new_R + new_R.T) == 2):
          raise ValueError('Loops are not allowed in POSets')

      new_R = POSet.close_transitive_matrix(new_R)

    poset = POSet([el for el in self.composed_alignment], new_R, is_already_closed=True)
    extra_partial_order = [(self.composed_alignment[i], self.composed_alignment[j])
                           for i, j in np.transpose(np.multiply(np.multiply(1 - self.R.T, 1 - self.R), new_R).nonzero())]

    if np.any(new_R.diagonal() != 0):
      raise ValueError('All elements must be incomparable with themselves in POSets.')

    new_partial_order = np.multiply(np.multiply(self.R.T, (1 - self.R)), new_R)
    intervals = self.__get_intervals(new_partial_order, poset)

    return extra_partial_order, intervals, self.info

  def __get_intervals(self, m_new, new_poset):
    intervals = POSet()
    for i, j in product(range(len(m_new)), range(len(m_new))):
      if m_new[i,j] == 1:
        from_move, to_move = new_poset[i], new_poset[j]
        if not self.composed_alignment.get_R_value(to_move, from_move):
          raise ValueError('This should not happen.')
        interval = new_poset.postfix([from_move]).intersection(new_poset.prefix([to_move]))
        intervals = intervals.union(interval)
    return intervals.detach_unconnected_subposets()

  def __compute_Cs_k(self):
    self.I_case = defaultdict(list)
    C_in, C_out = [], []
    aval_places = defaultdict(set)
    for move_index in self.clm_rls_indices:
      move = self.composed_alignment[move_index]
      move_C_in, move_C_out = defaultdict(int), defaultdict(int)
      transition, mode = move.transition, move.mode

      self.I_case[mode['c']].append(move_index)
      if not utils.is_log_move(transition):
        reference_transition = transition.reference
        for arc in reference_transition.in_arcs:
          if arc.source.properties.get('resource', '-') == 'available':
            idr = mode[arc.labels[1]]
            aval_places[idr].add(arc.source)
            move_C_in[idr] += int(arc.weight)
        for arc in reference_transition.out_arcs:
          if arc.target.properties.get('resource', '-') == 'available':
            move_C_out[mode[arc.labels[1]]] += int(arc.weight)

        # print('1len in ', len(reference_transition.in_arcs))
        # print('1len out', len(reference_transition.out_arcs))
        #
        #
        # print('2len in ', len(transition.in_arcs))
        # print('2len out', len(transition.out_arcs))
        # for arc in transition.in_arcs:
        #   # if arc.source.properties.get('resource', '-') == 'available':
        #   if arc.source.name[0] == utils.SKIP and arc.labels[1] != '_' and arc.source.name[1][-1] != '_' and arc.source.name[1][-len('_memory'):] != '_memory':
        #     idr = mode[arc.labels[1]]
        #     aval_places[idr].add(arc.source)
        #     move_C_in[idr] += int(arc.weight)
        # for arc in transition.out_arcs:
        #   # if arc.target.properties.get('resource', '-') == 'available':
        #   if arc.target.name[0] == utils.SKIP and arc.labels[1] != '_' and arc.target.name[1][-1] != '_' and arc.target.name[1][-len('_memory'):] != '_memory':
        #     move_C_out[mode[arc.labels[1]]] += int(arc.weight)
      C_in.append(move_C_in)
      C_out.append(move_C_out)

    self.idrs = sorted(self.k.keys())
    self.C_in = np.matrix(np.zeros((len(self.clm_rls_indices), len(self.idrs))), dtype=int)
    self.C_out = np.matrix(np.zeros((len(self.clm_rls_indices), len(self.idrs))), dtype=int)
    for move_index, (c_in, c_out) in enumerate(zip(C_in, C_out)):
      for idr, count in c_in.items():
        self.C_in[move_index, self.idrs.index(idr)] = count
      for idr, count in c_out.items():
        self.C_out[move_index, self.idrs.index(idr)] = count

    self.k = np.array([self.k[idr] for idr in self.idrs], dtype=int)
