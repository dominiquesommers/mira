from mira.prelim.poset import POSet
from mira.rc_nu_net.nu_net import NuPetriNet, NuMarking
from mira.rc_nu_net.resource import Resource
from mira.rc_nu_net import nu_net_utils

from pm4py.objects.petri_net.utils import petri_utils
from pm4py.objects.log.obj import Trace, Event
from pm4py import read_pnml
from collections import namedtuple

import json
import numpy as np


def get_example(net_filename:str, log_filename:str):
  example = Example(net_filename, log_filename)
  return *example.get(), example.net_filename, example.log_filename


class Example:
  RESOURCE_TYPE = namedtuple('resource_type', ['instances', 'claim_activities', 'release_activities', 'activities'])

  def __init__(self, net_filename:str, log_filename:str):
    net_filename = f'./examples/nets/{net_filename}' if 'nets/' not in net_filename else net_filename
    self.net_filename = net_filename if net_filename[-5:] == '.json' else f'{net_filename}.json'
    log_filename = f'./examples/logs/{log_filename}' if 'logs/' not in log_filename else log_filename
    self.log_filename = log_filename if log_filename[-5:] == '.json' else f'{log_filename}.json'

  def get(self):
    nu_net, m_i, m_f, resources = self.__get_net()
    event_log = self.__get_log(resources)

    return nu_net, m_i, m_f, event_log

  @staticmethod
  def silent(name):
    if name == 'tau':
      return True
    if name[:3] == 'tau':
      return True
    if name.isdecimal():
      return True
    if name[:2] == 'st':
      return True
    return False

  def __get_net(self):
    with open(self.net_filename, 'r') as net_file:
      net_data = json.load(net_file)
      resource_types = {rtype: self.RESOURCE_TYPE(inst, clm, rls, {*set(clm), *set(rls), *set(act)})
                        for rtype, (inst, clm, rls, act) in net_data.get('R', {}).items()}
      resources = {name: Resource(name, rtype, capacity, acts, clm_acts, rls_acts)
                   for rtype, (resource_instances, clm_acts, rls_acts, acts) in resource_types.items()
                   for name, capacity in resource_instances.items()}
      resource_memory = {rtype: (from_acts, to_acts) for rtype, (from_acts, to_acts) in net_data.get('R_memory', {}).items()}

      if 'pnml_filename' in net_data.keys():
        net, im, fm = read_pnml(net_data['pnml_filename'], auto_guess_final_marking=True)
        # print(net, im, fm)
        if len(im.keys()) != 1 or len(fm.keys()) != 1:
          raise ValueError('Petri net should have single source and sink place.')
        net_data['P'] = [p.name for p in net.places]
        initial_place, final_place = list(im.keys())[0].name, list(fm.keys())[0].name
        net_data['T'] = [[t.name, t.label] for t in net.transitions]
        net_data['F'] = [[arc.source.name, arc.target.name] for arc in net.arcs]
      else:
        initial_place, final_place = 'i', 'f'

      net_data['T'] = [t if isinstance(t, list) else [t, t] for t in net_data['T']]

      if net_data.get('split_transitions', False):
        tnames = [t_name for t_name, _ in net_data['T']]
        net_data['F'] = [[f'{source_name}:COMPLETE' if source_name in tnames else source_name,
                          f'{target_name}:START'    if target_name in tnames else target_name] for source_name, target_name in net_data['F']]
        net_data['P'].extend([f'p_{t_name}' for t_name in tnames])
        net_data['T'] = [[f'{name}:{postfix}', f'{label}:{postfix}'] for name, label in net_data['T'] for postfix in ['START', 'COMPLETE']]
        net_data['F'].extend([[f'{t_name}:START', f'p_{t_name}'] for t_name in tnames])
        net_data['F'].extend([[f'p_{t_name}', f'{t_name}:COMPLETE'] for t_name in tnames])

      nu_net = NuPetriNet()
      places = {name: petri_utils.add_place(nu_net, f'p{name}') for name in net_data['P']}

      transitions = {name: petri_utils.add_transition(nu_net, name, None if self.silent(label) else label) for name, label in net_data['T']}
      elements = {**places, **transitions}

      additional_arcs = self.__add_arcs(nu_net, elements, net_data['F'])

      m_i = NuMarking({places[initial_place]: {('_', '_'): 0}})
      m_f = NuMarking({places[final_place]: {('_', '_'): 0}})

      resource_places, resource_transitions = nu_net_utils.add_resources_to_nu_net(nu_net, m_i, m_f, resource_types, resource_memory, transitions)
      elements.update({**resource_places, **resource_transitions})

      left_over_arcs = self.__add_arcs(nu_net, elements, additional_arcs)
      if len(left_over_arcs) > 0:
        raise ValueError(f'Could not add all arcs: {left_over_arcs}')

      return nu_net, m_i, m_f, resources

  def __get_log(self, resources):
    with open(self.log_filename, 'r') as log_file:
      log_data = json.load(log_file)
      event_log = []
      [event_log.append(Event({'concept:name': event[0], 'case_id': f'{event[1]}',
                               'resources': self.__parse_resources(event[2], resources) if len(event) == 3 else {},
                               'index': index}))
       for index, event in enumerate(log_data['events'])]

      if (directly_follows := log_data.get('<', 'all')) == 'all':
        r = np.matrix(1 - np.tri(len(event_log), len(event_log)))
      else:
        r = np.matrix(np.zeros((len(event_log), len(event_log)), dtype=int))
        for i, j in directly_follows:
          r[i,j] = 1

      for i, j in log_data.get('||', []):
        r[i,j] = 0

      event_log = POSet(event_log, r)

      return event_log

  def __add_arcs(self, nu_net, nodes, arcs):
    failed_arcs = []
    for source, target in arcs:
      if source in nodes and target in nodes:
        nu_net_utils.add_arc_from_to(nodes[source], nodes[target], nu_net, labels=('c', '_'))
      else:
        failed_arcs.append((source, target))
    return failed_arcs

  def __parse_resources(self, resource_list, resources):
    return {resources[name]: resource_list.count(name) for name in set(resource_list)}

if __name__ == '__main__':
  net_filename = '/mnt/c/Users/s140511/tue/phd/gits/git_mira/examples/nets/bpi_17_.json'
  # net_filename = '/mnt/c/Users/s140511/tue/phd/gits/git_mira/examples/nets/omron.json'
  log_filename = '/mnt/c/Users/s140511/tue/phd/gits/git_mira/examples/logs/bpi_17/individual/bpi_17_220112.json'
  # log_filename = '/mnt/c/Users/s140511/tue/phd/gits/git_mira/examples/logs/omron/omron_1.json'

  net, m_i, m_f, log, _, _ = get_example(net_filename, log_filename)
  print(type(net))

  net.view(debug=True)