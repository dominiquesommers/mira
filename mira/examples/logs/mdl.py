from mira.logs.event_log import Log
from mira.logs.event import Event
from mira.prelim.multiset import Multiset

import numpy as np
import pandas as pd
import ast
import pickle

def get(example=1, view=False):
  with open('./mira/examples/logs/mdl.csv', 'rb') as file:
    data = pd.read_csv(file)
    for col in ['orders','items','packages']:
      data[col] = data[col].apply(ast.literal_eval)
    print(data[['event_activity','orders','items','packages']])

  # events = [
  #   Event('create', Multiset(args={'o1': 1}))]

  order_data = data[data['orders'].apply(lambda orders: orders[0]) == '990001']
  identifiers = (set(np.concatenate(order_data['orders'].values)) - {''}, set(np.concatenate(order_data['items'].values)) - {''}, set(np.concatenate(order_data['packages'].values)) - {''})

  all_ids = set()
  events = []
  for _, row in data.iterrows():
    orders = set(row['orders']) - {''}
    items = set(row['items']) - {''}
    packages = set(row['packages']) - {''}
    if len(orders.union(items).union(packages) - identifiers[0].union(identifiers[1]).union(identifiers[2])) == 0:
    # if len(orders.intersection(identifiers[0])) > 0 or len(items.intersection(identifiers[1])) > 0 or len(packages.intersection(identifiers[2])) > 0:
      events.append(Event(row['event_activity'], Multiset(args={identifier: 1 for identifier in orders.union(items).union(packages)})))
      all_ids.update(set(events[-1].identifiers.keys()))


  # get_identifiers = lambda row: {identifier: 1 for identifier in row['orders'] + row['items'] + row['packages'] if identifier != ''}
  # events = [Event(row['event_activity'], Multiset(args=get_identifiers(row))) for _, row in data.iterrows()]

  print(all_ids - identifiers[0].union(identifiers[1]).union(identifiers[2]))
  [print(e) for e in events]
  print(len(events))

  # print(buh)

  # print(events[0])
  # print(bj)
  # with open('./mira/examples/logs/bpi17_variants.pkl', 'rb') as file:
  #   variants = pickle.load(file)
  #
  # examples = []
  # for variant_key, variant in variants.items():
  #   events = [Event(activity, Multiset(args={entity: 1 for entity in entities}), timestamp=dt) for activity, entities, dt in variant]
  #   entity_set = set([entity for _, entities, _ in variant for entity in entities])
  #   identifiers = tuple([{entity for entity in entity_set if entity[:len(object)] == object} for object in ['Application', 'Offer']])
  #   examples.append({'events': events, 'identifiers': identifiers, 'variant_key': variant_key})
  #
  # default_example = {'events': [], 'identifiers': tuple([set()]*len(examples[0]['identifiers'])), 'variant_key': example}
  # example = examples[example] if isinstance(example, int) else next((x for x in examples if x['variant_key'] == example), default_example)
  #
  # print(f"{example['variant_key']}: {len(example['events'])} events, {sum(len(v) for v in example['identifiers'])} objects.")

  # events = []
  # identifiers = ({'o1'}, {'i1'}, {'p1'})
  # events, identifiers = example['events'], example['identifiers']
  log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, identifiers)
  if view:
    log.view()
  return log


if __name__ == '__main__':
  get()
