from mira.nets.petri_net import Net
from mira.nets.nu_net import NuNet
from mira.nets.multi_nu_net import MultiNuNet
from mira.nets.tPNID import tPNID
from mira.nets.transition import Transition
from mira.nets.marking import Marking, NuMarking, MultiNuMarking
from mira.prelim.poset import POSet

from typing import Optional, Union
import numpy as np
from copy import deepcopy
from collections import defaultdict
from pprint import pformat


class MarkedNet:
  def __init__(self, net: Union[Net, NuNet, MultiNuNet], marking: Union[Marking, NuMarking, MultiNuMarking]):
    self.net = net
    self.marking = marking
    self.firings = POSet()

  def enabled_firings(self, parent_enabled_firings=None, last_firing=None, verbose=False):
    parent_enabled_firings = [] if parent_enabled_firings is None else parent_enabled_firings
    enabled_firings = [firing for firing in parent_enabled_firings if firing.sub_marking <= self.marking]
    enabled_firings = [f for f in enabled_firings if
                       all(self.marking[place][token] == 0 for place, inh_place_marking in
                           self.net.create_marking_from_mode({arc: 1 for arc in self.net.transition_inhibitor_arcs[f.transition]}, f.mode).items()
                           for token in inh_place_marking.keys())]
    skip_checks = defaultdict(list)
    [skip_checks[firing.transition].append(firing.mode) for firing in parent_enabled_firings]
    if last_firing is not None:
      add_places = set(last_firing.add_marking.keys())
      for transition in self.net.transitions:
        transition_preset = set(arc.place for arc in self.net.element_in_arcs[transition])
        if len(transition_preset & add_places) == 0:
          skip_checks[transition] = 'all'

    new_enabled_firings, nr = self.net.enabled_firings(self.marking, skip=skip_checks, verbose=verbose)
    return enabled_firings + new_enabled_firings, nr

  @property
  def transitions(self):
    return self.net.transitions

  @property
  def places(self):
    return self.net.places

  @property
  def arcs(self):
    return self.net.arcs

  @property
  def inhibitor_arcs(self):
    return self.net.inhibitor_arcs

  def fire(self, firing, new_marking):
    self.firings.append(firing)
    n = len(self.firings)
    index = self.firings.indices[firing]
    inhibited_places = [arc.source for arc in self.net.transition_inhibitor_arcs[firing.transition]]
    for other_firing in self.firings[:-1]:
      other_inhibited_places = [arc.source for arc in self.net.transition_inhibitor_arcs[firing.transition]]
      if not other_firing.add_marking.intersection(firing.sub_marking).is_empty() or \
         any(not other_firing.sub_marking[place].is_empty() for place in inhibited_places) or \
         any(not firing.add_marking[place].is_empty() for place in other_inhibited_places):
        self.firings.r[:,index] = np.minimum(np.ones((n, 1), dtype=int), self.firings.r[:,index] + (self.firings.r + np.eye(n, dtype=int))[:,self.firings.indices[other_firing]])

    self.marking = new_marking

  def fire_transition(self, transition: Transition, sub_marking: MultiNuMarking, add_marking: MultiNuMarking, mode: dict=None):
    new_marking, firing = self.net.fire_transition(self.marking, transition, sub_marking, add_marking, mode)
    self.fire(firing, new_marking)
    return self.marking, firing

  def dryfire_transition(self, transition: Transition, sub_marking: MultiNuMarking, add_marking: MultiNuMarking, mode: dict = None):
    # Same as fire_transition but does not update its marking and firing history
    return self.net.fire_transition(self.marking, transition, sub_marking, add_marking, mode)

  def reset_firings(self):
    self.firings = POSet()

  def projection(self, objects, object_types, variable_types):
    return MarkedNet(self.net.projection(objects, object_types, variable_types), self.marking.projection(objects, object_types))

  def blacken(self):
    return MarkedNet(self.net.blacken(), self.marking.blacken())

  def view(self, debug=False, name=''):
    self.net.view(self.marking, None, debug, name)


class System(MarkedNet):
  def __init__(self, net: Union[Net, NuNet, MultiNuNet, tPNID], m_i: Union[Marking, NuMarking, MultiNuMarking],
                     m_f: Optional[Union[Marking, NuMarking, MultiNuMarking]]=None):
    super().__init__(net, deepcopy(m_i))
    self.m_i = m_i
    self.m_f = m_f

  @classmethod
  def from_json_dict(cls, data, silent=lambda name: name[:3] == 'tau', relaxed=False, verbose=False):
    net_class = globals()[data['type']]
    if Net not in net_class.mro():
      raise TypeError(f'Net class should be inherited from type Net, is {net_class} with inheritance from {net_class.mro()}.')

    net = net_class.from_json_dict(data, silent=silent, relaxed=relaxed, verbose=verbose)
    m_i = net_class.marking_type.from_str(data['m_i'])
    m_f = net_class.marking_type.from_str(data.get('m_f', data['m_i']))
    return System(net, m_i, m_f)

  def to_dict(self, save_filename=None):
    data = {
      'type': type(self.net).__name__,
      **self.net.to_dict(),
      'm_i': str(self.m_i),
      'm_f': str(self.m_f)
    }
    if save_filename is None:
      return data
    with open(save_filename, 'w') as json_file:
      json_file.write(pformat(data, compact=True, width=150).replace("'", '"'))

  def blacken(self):
    return System(self.net.blacken(), self.m_i.blacken(), self.m_f.blacken())

  def same_type(self, other):
    return all(type(x) == type(y) for x, y in [(self.net, other.net), (self.m_f, other.m_f), (self.m_f, other.m_f)])

  def projection(self, objects, object_types, variable_types, skip_dups=True):
    objects_roles = set(role for role, role_objects in object_types.items() if len(role_objects.intersection(objects)) > 0)
    for place in set(self.m_i.keys()).union(set(self.m_f.keys())):
      place.alpha = self.net.alpha(place, self.net.variable_types)
    return System(self.net.projection(objects_roles, object_types, variable_types, skip_dups=skip_dups), self.m_i.projection(objects, object_types),
                  None if self.m_f is None else self.m_f.projection(objects, object_types))

  def union(self, other):
    m_f = None if self.m_f is None and other.m_f is None else \
          self.m_f if other.m_f is None else \
          other.m_f if self.m_f is None else \
          self.m_f.union(other.m_f)
    return System(self.net.union(other.net), self.m_i.union(other.m_i), m_f)

  def __sub__(self, other):
    m_f = None if self.m_f is None and other.m_f is None else \
          self.m_f if other.m_f is None else \
          other.m_f if self.m_f is None else \
          self.m_f - other.m_f
    return System(self.net - other.net, self.m_i - other.m_i, m_f)


  def view(self, debug=False, name=''):
    self.net.view(self.marking, self.m_f, debug, name)

  def easy_soundness(self):
    # TODO implement.
    return True