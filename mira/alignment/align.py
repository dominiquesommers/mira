from mira.logs.event_log import Log
from mira.nets.system import System
from mira.alignment.log_net import create_log_system
from mira.alignment.cost import Cost
from mira.alignment.synchronous_product import SynchronousProductSystem, tPNIDSynchronousProductSystem
from mira.alignment.search import Search

from tqdm import tqdm
import numpy as np


def align(event_log:Log, system:System, parameters=None):
  mira_alignment = MiraAlignment(event_log, system, parameters=parameters)
  alignment = mira_alignment.align(cost_function=parameters.get('cost', Cost.cost))
  # mira_alignment.info['t']['total'] = time.time() - t
  return alignment, mira_alignment.info


class MiraAlignment:
  def __init__(self, event_log:Log, system:System, parameters=None):
    self.event_log = event_log
    self.system = system
    self.parameters = {} if parameters is None else parameters
    self.info = {'t': {}}
    self.check_easy_soundness()

  def check_easy_soundness(self):
    if not bool(self.parameters.get('skip_easy_soundness_check', False)):
      # TODO check easy soundness of self.system.
      if not self.system.easy_soundness():
        raise ValueError('System is not easy sound.')

  def align(self, cost_function=Cost.cost):
    if bool(self.parameters.get('approximation', False)):
      return self.align_approximation()

    self.log_system = create_log_system(self.event_log, type(self.system.net))
    print('create log net', self.log_system.variable_types)
    if bool(self.parameters.get('visualize_log_net', False)):
      self.log_system.view(debug=True)
    if self.parameters.get('relax', False):
      self.log_system.net = self.log_system.net.relax(skip_silents=True)
    # TODO create correct sync product system.
    print('create sync net')
    self.sync_product = tPNIDSynchronousProductSystem(self.log_system, self.system)
    if bool(self.parameters.get('visualize_sync_net', False)):
      # Commented out: show only log+synchronous moves.
      # transitions_to_remove = set([t for t in self.sync_product.net.transitions if t.move_type == 'model'])
      # arcs_to_remove = set([arc for t in transitions_to_remove for arc in self.sync_product.net.element_in_arcs[t]] + [arc for t in transitions_to_remove for arc in self.sync_product.net.element_out_arcs[t]])
      # places_to_remove = set([arc.source for t in transitions_to_remove for arc in self.sync_product.net.element_in_arcs[t]] + [arc.target for t in transitions_to_remove for arc in self.sync_product.net.element_out_arcs[t]])
      # for t in transitions_to_remove:
      #   self.sync_product.net.remove_transition(t)
      # for p in places_to_remove:
      #   self.sync_product.net.remove_place(p)
      # for a in arcs_to_remove:
      #   self.sync_product.net.remove_arc(a)

      self.sync_product.view(debug=True)
      print(stop)

    # transitions_to_remove = set([t for t in self.sync_product.net.transitions if t.move_type == 'log' and t.label == 'deliver depot'])
    # arcs_to_remove = set([arc for t in transitions_to_remove for arc in self.sync_product.net.element_in_arcs[t]] + [arc for t in transitions_to_remove for arc in self.sync_product.net.element_out_arcs[ t]])
    # for t in transitions_to_remove:
    #   self.sync_product.net.remove_transition(t)
    # for a in arcs_to_remove:
    #   self.sync_product.net.remove_arc(a)
    # self.sync_product.view(debug=True)
    # print(stoppp)


    # self.costs = {'synchronous': lambda t: 0,
    #               'log':         lambda t: 10000,
    #               'model':       lambda t: 1 if t.label is None else 10000}
    # cost_function = lambda t: self.costs[t.move_type](t)
    searcher = Search(self.sync_product, cost_function, variant=self.parameters.get('variant', 'a_star'))
    run, stats = searcher.compute_cheapest_run(max_search_time=self.parameters.get('max_search_time', np.inf), min_memory_available=self.parameters.get('min_memory_available', 0))
    if self.parameters.get('filter_variable_subs', False):
      run = self.filter_variable_subs(run)
    print(stats)
    self.info = stats
    return None if 'error' in stats else run

  def align_approximation(self):
    # TODO decompose log
    # TODO align individual traces
    # TODO compose alignment
    # TODO split composed alignment
    # TODO find intervals in sub-compals
    # TODO extract corresponding sub-logs
    # TODO align sub-logs
    # TODO replace intervals by aligned sub-logs
    ...

  def filter_variable_subs(self, run):
    return run.projection(lambda move: not move.transition.parameters.get('variable_sub', False))


if __name__ == '__main__':
  from mira.logs.event_log import Log
  from mira.nets.petri_net import Net, NuNet, MultiNuNet, System
  from mira.nets.marking import Marking, NuMarking, MultiNuMarking
  from mira.nets.place import Place
  from mira.nets.transition import Transition
  from mira.nets.token import ColorToken, MultiColorToken
  from mira.nets.arc import Arc, NuArc, MultiNuArc, InhibitorArc, VariableMultiNuArc
  from mira.logs.event import Event
  from mira.prelim.multiset import Multiset
  import numpy as np
  from mira.prelim.colors import Color

  test = 'mdl'
  if test == 'nu':
    # log = Log([Event('a', Multiset(args={0: 1})), Event('b', Multiset(args={0: 1}))], np.matrix([[0, 1], [0, 0]]))
    log = Log([Event('a', Multiset(args={0: 1}))], np.matrix([[0]]))
    places = {name: Place(name) for name in ['pi', 'p1', 'pf']}
    transitions = {name: Transition(name, name) for name in ['a', 'b']}
    net = NuNet(list(places.values()), list(transitions.values()))
    [net.add_arc(NuArc(s, t, 'x')) for s, t in
      [(places['pi'], transitions['a']), (transitions['a'], places['p1']), (places['p1'], transitions['b']), (transitions['b'], places['pf'])]]
    m_i = NuMarking({'pi': {ColorToken(0): 1}})
    m_f = NuMarking({'pf': {ColorToken(0): 1}})
    sn = System(net, m_i, m_f)

  elif test == 'multi':
    from mira.logs.event import Event
    import numpy as np

    events = [
      Event('a', Multiset(args={'x': 1, 'y': 1})),
      Event('b', Multiset(args={'x': 1, 'y': 1}))
    ]
    log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'x'}, {'y'}))
    # log_system = MultiNuLogSystem(log)


    # log = Log([Event('a', Multiset(args={0: 1})), Event('b', Multiset(args={0: 1}))], np.matrix([[0, 1], [0, 0]]))
    places = {name: Place(name) for name in ['pi', 'p1', 'p2', 'pf']}
    transitions = {name: Transition(name, name) for name in ['a', 'b']}
    net = MultiNuNet(list(places.values()), list(transitions.values()))
    net.add_arc(MultiNuArc(places['pi'], transitions['a'], ('A', 'B')))
    net.add_arc(MultiNuArc(transitions['a'], places['p1'], ('A', '_')))
    net.add_arc(MultiNuArc(transitions['a'], places['p2'], ('_', 'B')))
    net.add_arc(MultiNuArc(places['p1'], transitions['b'], ('A', '_')))
    net.add_arc(MultiNuArc(places['p2'], transitions['b'], ('_', 'B')))
    net.add_arc(MultiNuArc(transitions['b'], places['pf'], ('A', 'B')))

    m_i = MultiNuMarking({'pi': {MultiColorToken(2, [{'x': 1}, {'y': 1}]): 1}})
    m_f = MultiNuMarking({'pf': {MultiColorToken(2, [{'x': 1}, {'y': 1}]): 1}})
    sn = System(net, m_i, m_f)
  elif test == 'multi2':
    from mira.logs.event import Event
    import numpy as np

    events = [
      Event('a', Multiset(args={'c1': 1, 'd': 1})),
      Event('a', Multiset(args={'c2': 1, 'd': 1})),
      Event('b', Multiset(args={'c1': 1, 'd': 1})),
      Event('b', Multiset(args={'c2': 1, 'd': 1}))
    ]
    log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'c1', 'c2'}, {'d'}))
    # log_system = MultiNuLogSystem(log)

    # log = Log([Event('a', Multiset(args={0: 1})), Event('b', Multiset(args={0: 1}))], np.matrix([[0, 1], [0, 0]]))
    places = {name: Place(name) for name in ['i', '1', 'f', 'd', 'd_']}
    transitions = {name: Transition(name, name) for name in ['a', 'b']}
    net = MultiNuNet(list(places.values()), list(transitions.values()))
    net.add_arc(MultiNuArc(places['i'], transitions['a'], ('{a:1}', '_')))
    net.add_arc(MultiNuArc(transitions['a'], places['1'], ('{a:1}', '_')))
    net.add_arc(MultiNuArc(places['1'], transitions['b'], ('A', '_')))
    net.add_arc(MultiNuArc(transitions['b'], places['f'], ('A', '_')))

    net.add_arc(MultiNuArc(places['d'], transitions['a'], ('_', 'B')))
    net.add_arc(MultiNuArc(transitions['b'], places['d'], ('_', 'B')))

    net.add_arc(MultiNuArc(transitions['a'], places['d_'], ('{a:1}', 'B')))
    net.add_arc(MultiNuArc(places['d_'], transitions['b'], ('A', 'B')))

    m_i = MultiNuMarking({'i': {MultiColorToken(2, [{'c1': 1}, {}]): 1, MultiColorToken(2, [{'c2': 1}, {}]): 1},
                          'd': {MultiColorToken(2, [{}, {'d': 1}]): 1}})
    m_f = MultiNuMarking({'f': {MultiColorToken(2, [{'c1': 1}, {}]): 1, MultiColorToken(2, [{'c2': 1}, {}]): 1},
                          'd': {MultiColorToken(2, [{}, {'d': 1}]): 1}})
    sn = System(net, m_i, m_f)
  elif test == 'multi3':
    from mira.logs.event import Event
    import numpy as np

    events = [
      Event('t', Multiset(args={'a': 1, 'c': 1})),
      Event('t', Multiset(args={'b': 1}))
    ]
    log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'a', 'b', 'c'},))
    # log_system = MultiNuLogSystem(log)

    # log = Log([Event('a', Multiset(args={0: 1})), Event('b', Multiset(args={0: 1}))], np.matrix([[0, 1], [0, 0]]))
    places = {name: Place(name) for name in ['i', 'f']}
    transitions = {name: Transition(name, name) for name in ['t']}
    net = MultiNuNet(list(places.values()), list(transitions.values()))
    net.add_variable_arc(VariableMultiNuArc(places['i'], transitions['t'], ('V',), [1, np.inf]))
    net.add_variable_arc(VariableMultiNuArc(transitions['t'], places['f'], ('V',), [1, np.inf]))

    m_i = MultiNuMarking({'i': {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1}})
    m_f = MultiNuMarking({'f': {MultiColorToken(1, [{'a': 1}]): 1, MultiColorToken(1, [{'b': 1}]): 1, MultiColorToken(1, [{'c': 1}]): 1}})
    sn = System(net, m_i, m_f)

  elif test == 'packing':
    from mira.examples.nets.packing import get as get_net
    from mira.examples.logs.packing import get as get_log
    sn = get_net(view=False)
    log = get_log()

  elif test == 'bpi17':
    from mira.examples.nets.bpi17 import get as get_net
    from mira.examples.logs.bpi17 import get as get_log

    for i in tqdm(range(1)):
      log = get_log(example=i, view=True)
      sn = get_net(entities=log.identifiers, view=True)
      run, _ = align(log, sn, parameters={'visualize_sync_net': False, 'filter_variable_subs': True})

      colors = {'synchronous': Color.GREEN, 'log': Color.YELLOW, 'model': Color.PURPLE}
      mode_str = lambda mode: '{' + f'{",".join([f"{key}:{value}" for key, value in mode.items() if key != "_"])}' + '}'
      run.view(element_str=lambda x: f'{x.transition}\n{mode_str(x.mode)}', colors=lambda t: colors[t.transition.move_type])
    print(done)

  elif test == 'mdl':
    from mira.examples.nets.mdl import get as get_net
    from mira.examples.logs.mdl import get as get_log

    for i in tqdm(range(1)):
      log = get_log(example=i, view=False)
      sn = get_net(entities=log.identifiers, view=False)
      run, _ = align(log, sn, parameters={'visualize_sync_net': False, 'filter_variable_subs': True})

      colors = {'synchronous': Color.GREEN, 'log': Color.YELLOW, 'model': Color.PURPLE}
      mode_str = lambda mode: '{' + f'{",".join([f"{key}:{value}" for key, value in mode.items() if key != "_"])}' + '}'
      run.view(element_str=lambda x: f'{x.transition}\n{mode_str(x.mode)}', colors=lambda t: colors[t.transition.move_type])
    print(done)

  else:
    raise TypeError()

  # sn.view()
  run, _ = align(log, sn, parameters={'visualize_sync_net': False, 'filter_variable_subs': True})


  colors = {'synchronous': Color.GREEN, 'log': Color.YELLOW, 'model': Color.PURPLE}
  mode_str = lambda mode: '{' + f'{",".join([f"{key}:{value}" for key, value in mode.items() if key != "_"])}' + '}'
  run.view(element_str=lambda x: f'{x.transition}\n{mode_str(x.mode)}', colors=lambda t: colors[t.transition.move_type])
