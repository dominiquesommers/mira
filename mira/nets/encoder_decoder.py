import json
from mira.nets.marking import MultiNuMarking, Marking, NuMarking
from mira.nets.place import Place
from mira.nets.token import MultiColorToken, ColorToken
from mira.prelim.multiset import Multiset, TypedDict
from colorama import Fore, Style
from mira.nets.petri_net import *
from mira.nets.arc import MultiNuArc, Var, MultiVarExpr, VarExpr
from mira.alignment.search import Search



with open('examples/nets/omron_relaxed.json', 'r') as json_file:
  data = json.load(json_file)

# data = {
#   'type': 'tPNID',
#   'P': ['p1', 'p2'],
#   'T': ['a', 'b'],
#   'F': {'[([p])]': [('p1', 'a'), ('p1', 'b'), ('a', 'p2'), ('b', 'p2')]},
#   'O': {'P': ['x', 'y']},
#   'V': {'P': ['p'], 'Q': ['q']},
#   'm_i': '[[([p])]·p1]',
#   'm_f': '[[([p])]·p2]'
# }
import time

t1 = time.time()
print('start loading.')
system = System.from_json_dict(data)
t2 = time.time()
print(f'loaded in {t2 - t1:.1f} seconds, view now.')
system.view()
print(f'view in {time.time() - t2:.1f} seconds.')
print(bsldf)

system.net = system.net.relax()
system.to_dict('examples/nets/omron_relaxed.json')
print(bsl)

# system = System(net, MultiNuMarking.from_str(data['m_i']), MultiNuMarking.from_str(data.get('m_f', data['m_i'])))
# system.view(debug=True)
# print(stoo)

pmode = {'p': 'p1'}
cp1mode = {**pmode, 'c': 'c1'}
cpo1mode = {**cp1mode, 'o': 'o1'}
cp2mode = {**pmode, 'c': 'c2'}
cpo2mode = {**cp2mode, 'o': 'o2'}
cp3mode = {**pmode, 'c': 'c3'}
cpo3mode = {**cp3mode, 'o': 'o3'}
firings = [
  ('422', pmode),
  ('CBP-WPPs', cp1mode),
  ('CBP-WPPc', cpo1mode),
  ('CBP-FS', cpo1mode), ('CBP-AP', cpo1mode), ('CBP-Lf1', cpo1mode), ('CBP-STL', cpo1mode),
  ('CBP-WSFs', cpo1mode),
  ('CBP-WSFc', cp1mode),

  ('CB-WSs', cp2mode),
  ('CB-WSc', cpo2mode),
  ('CB-AP', cpo2mode), ('CB-STL', cpo2mode), ('CB-WCSs', cpo2mode), ('CB-WCSc', cpo2mode),
  ('CB-WSFs', cpo2mode),
  ('CB-WSFc', cp2mode),

  ('IA-WSs', cp3mode),
  ('IA-WSc', cpo3mode),
  ('IA-AP', cpo3mode), ('IA-STL', cpo3mode), ('IA-WCSs', cpo3mode), ('IA-WCSc', cpo3mode),
  ('IA-WSFs', cpo3mode),
  ('IA-WSFc', cp3mode)
]

transitions = {t.name: t for t in system.net.transitions}
for tname, mode in firings:
  print(tname, mode)
  transition = transitions[tname]
  mode = {k: ColorToken(v) for k, v in mode.items()}
  enabled, sub_marking, add_marking = system.net.transition_enabled(transition, system.marking, mode, verbose=True)
  if enabled:
    firing = MultiNuFiring(transition, mode, sub_marking, add_marking)
    system.fire(firing, system.marking - sub_marking + add_marking)
  else:
    print(f'{sub_marking=}')
    print(f'{system.marking=}')
    raise ValueError('not enabled')

cost_function = lambda t: 10000

system.reset_firings()
search = Search(system, cost_function)
run, stats = search.compute_cheapest_run()

print('cost', stats['cost'])
print('run', run)

run = run.projection(lambda x: not (x.transition.move_type == 'log' and x.transition.label is None))

run.view(element_str=lambda x: f'{x.transition}')

print(testtestest)



ps = {name: Place(name) for name in ['p1', 'p2']}
ts = {t: Transition(t, t) for t in ['a', 'b']}
variable_types = {'P': {Var('x')}}
net = tPNID(list(ps.values()), list(ts.values()), object_types=object_types, variable_types=variable_types)
els = {**ps, **ts}

[net.add_arc(MultiNuArc(els[f], els[t], Multiset(args={(f'{{{v}: 1}}',): 1}))) for f, t, v in [
  ('p1', 'a', 'x'), ('a', 'p2', 'x'), ('p2', 'b', 'x'), ('b', 'p1', 'x')]]
net.add_arc(MultiNuArc(els['p1'], els['b'], Multiset(args={('{x: 1}',): 2})))

m_i = MultiNuMarking({ps['p1']: {MultiColorToken(1, [{'p': 1}]): 2}})

print(bsdfdsfdsafds)


# test(TypedDict.from_str('[a]', str, int))
test(TypedDict.from_str('[[a]·b]', Place, (Multiset, ColorToken)))

test(Marking.from_str('[a]'))

test(NuMarking.from_str('[[b]·a]'))

test(MultiNuMarking.from_str('[[([b])]·a]'))

print(bsdfds)

def decode_multiset(m_str, key_type=str):
  elements = m_str[1:-1].split(',')
  args = defaultdict(int)
  for element in elements:
    v, k = element.split('·') if '·' in element else (1, element)
    args[key_type(k)] += int(v)

  return Multiset(key_type=key_type, args=args)


dm = decode_multiset(str(Multiset(args={Place('y'): 2, Place('x'): 1})), key_type=Place)
print('dm', dm)
print(dm.key_type)
for k, v in dm.items():
  print('k', k, type(k))
  break


# m = MultiNuMarking(args={Place('p1'): MultiNuMarking.MultiColorTokenMultiset(args=Multiset(args={MultiColorToken(1, [{'p': 1}]): 1}))})
# print('m', m, type(m))



# m_dict = m.to_dict()
# print(json.dumps(m_dict))
