class Resource:
  def __init__(self, name, rtype, capacity, activities, claim_activities, release_activities):
    self.name = name
    self.rtype = rtype
    self.capacity = capacity
    self.activities = activities
    self.claim_activities = claim_activities
    self.release_activities = release_activities