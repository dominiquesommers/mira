from mira.alignments.firing import Firing
from mira.alignments import utils
from mira.rc_nu_net.nu_net import NuMarking
from mira.prelim.poset import POSet
from mira.get_example import Example

from pm4py.objects.petri_net.obj import PetriNet

import numpy as np
import json


def __get_marking(marking, places):
  nu_marking = NuMarking()
  for p, tokens in marking.items():
    place = PetriNet.Place(tuple(p.split(',')))
    place.reference = places.get(p.split(',')[1], None)
    for token, count in tokens.items():
      token_key = token.split(',')
      # TODO this is temporary hack.
      if token_key[0][0] == 'c':
        token_key[0] = token_key[0][1:]

      nu_marking[place][tuple(token_key)] = count

  return nu_marking

def __parse_mode(keys, transition, sub_marking):
  mode = {}
  for place, token_count in sub_marking.items():
    if len(token_count) > 1:
      raise ValueError('Do not know what to do here (yet).')
    if place.reference is None:
      continue
    for arc in place.reference.out_arcs:
      if arc.target == transition.reference:
        for i, label in enumerate(arc.labels):
          if label == '_':
            continue
          token = list(token_count.keys())[0]
          mode[label] = token[i]
  # if set(keys) != set(mode.keys()):
  #   raise ValueError('Something went wrong parsing the mode.')
  return mode

def __get_move_cost(transition_name):
  if transition_name[0] == utils.SKIP:
    if Example.silent(transition_name[1]):
      return utils.STD_TAU_COST
    return utils.STD_MODEL_LOG_MOVE_COST
  if transition_name[1] == utils.SKIP:
    return utils.STD_MODEL_LOG_MOVE_COST
  return utils.STD_SYNC_COST

def __get_event(t_name, event_log):
  ...

def import_individual_alignment_from_file(case_id, alignment_fn, petri_net, event_log):
  trace = [event for event in event_log if event['case_id'] == case_id]
  events = {f"{event['concept:name']}_c{event['case_id']}_{index}": event for index, event in enumerate(trace)}
  transitions = {t.name: t for t in petri_net.transitions}
  places = {p.name: p for p in petri_net.places}

  with open(alignment_fn, 'r') as file:
    data = json.load(file)

  info = data['info']
  moves = []
  for (t_name, t_label), mode, sub_marking, add_marking in data['M']:
    model_transition = transitions.get(t_name[1], PetriNet.Transition(tuple(t_name), tuple(t_label)))
    transition = PetriNet.Transition(tuple(t_name), tuple(t_label))
    transition.reference = model_transition
    transition.event = events.get(t_name[0])
    if t_name[0] != utils.SKIP and transition.event is None:
      print()
      print(t_name[0])
      print(transition.event)
    sub_marking = __get_marking(sub_marking, places)
    mode = __parse_mode(mode, transition, sub_marking)
    moves.append(Firing(transition, mode, sub_marking, __get_marking(add_marking, places), __get_move_cost(t_name)))

  prec = data['r']
  alignment = POSet(moves, np.matrix(prec))
  return alignment, info
