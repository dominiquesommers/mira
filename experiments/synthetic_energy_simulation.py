from mira.simulation.ground_truth_simulation import GroundTruthSimulation
from mira.nets.system import System
from mira.nets.arc import Var, MultiNuArc, MultiVarExpr
from mira.prelim.multiset import Multiset
from mira.alignments.exporter import NoIndent, MyEncoder
from mira.nets.marking import MultiNuMarking
from mira.nets.token import MultiColorToken

from dateutil.relativedelta import *
from copy import deepcopy
import numpy as np
np.random.seed(0)
import json
from colorama import Fore, Style


pattern_base_dir = './datasets/deviation_patterns'
class RecordingDeviations:
  _1 = {'missing event acd': (f'{pattern_base_dir}/missing_event.json', {'x': 'add customer details'})}
  _2 = {'incorrect position batch': (f'{pattern_base_dir}/incorrect_position_batch.json', {'x': 'start signing', 'y': 'approve contract', 'r': 'M', 'p': 'p_m_on'})}
  # _3 = {'incorrect object data': (f'{pattern_base_dir}/incorrect_object_data.json', {'x': 'cancel previous contract', 'p1': 'p_true', 'p2': 'p_false'})} # TODO
  _4 = {'incorrect object agent': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'open file', 'R': 'A'})}

class BehavioralDeviations:
  _1 = {'switch role': (f'{pattern_base_dir}/switch_role.json', {'px': 'p_a_on', 'py': 'p_m_on', 'x': 'A', 'y': 'M'})}
  _2 = {'multitask': (f'{pattern_base_dir}/multitask.json', {'p1': 'p12', 'p2': 'p_a_on', 'x': 'I', 'y': 'A'})}
  _3 = {'ignore batch': (f'{pattern_base_dir}/break_batching.json', {'p_batch': 'p_approve', 'p_batched': 'p_to_sign', 'p_empty': 'p_empty', 'a': 'I', 'r': 'M'})}
  _4 = {'diff res mem agent': (f'{pattern_base_dir}/different_resource_memory.json', {'p1': 'p_res_mem', 'p2': 'p_a_on', 'x': 'I', 'y': 'A'})}
  _5 = {'long duration': (f'{pattern_base_dir}/long_duration.json', {'x': 'add meter details'})}

def create_synthetic_data(dev_combination, n_objects=None, verbose=False, visualize=False, export=''):
  with open('./datasets/nets/energy.json', 'r') as json_file:
    data = json.load(json_file)
    system = None
    for net_data in data:
      if net_data["comment"] == "":
        continue
      system_ = System.from_json_dict(net_data)
      if system is None:
        system = system_
      else:
        system = system.union(system_)

    system.net.set_alphas(system.net.variable_types)

  if n_objects is not None:
    system.net.object_types['I'] = set(sorted(list(system.net.object_types['I']))[:n_objects])

  patterns_ = {k: v for x in dev_combination for k, v in x.items()}
  patterns = {}
  for pattern_name, (pattern_filename, mapping) in patterns_.items():
    with open(pattern_filename, 'r') as json_file:
      patterns[pattern_name] = {'net_data': json.load(json_file), 'mapping': mapping}

  # Expected objects (scheduled)
  schedules = {
    'start_manager': {'Joey': ([MO, TU, WE, TH, FR], [10])},
    'stop_manager':  {'Joey': ([MO, TU, WE, TH, FR], [18])},
    'start_agent': {'Charles': ([MO, TU, WE, TH, FR], [9]), 'Mary': ([MO, TU, WE, TH, FR], [10])},
    'stop_agent': {'Charles': ([MO, TU, WE, TH, FR], [17]), 'Mary': ([MO, TU, WE, TH, FR], [17])},
  }
  # Spontaneous objects
  arrivals = {'I': lambda dt: np.random.exponential(5.0)}

  transition_weights = {
    t.name: (lambda dt: -1) if t.label in ['start_agent', 'stop_agent', 'start_manager', 'stop_manager', 'create'] else
            (lambda dt: 1)
    for t in system.transitions
  }
  transition_weights['skip_cancel'] = lambda dt: 100

  # Add weights of pattern transitions
  weights = {
    'tau_missing_add customer details': lambda dt: 0.1, # R_mi^e acd
    'start signing_ib': lambda dt: 0.1, 'approve contract_ib': lambda dt: 1, # incorrect position batch
    'tau_open file_incorrect_A': lambda dt: 0.1, 'open file_incorrect_A': lambda dt: 1, 'tau2_open file_incorrect_A': lambda dt: 1, # 'incorrect object agent'
    'tau_switch_role_A_M': lambda dt: -1, 'tau_switch_role_back_A_M': lambda dt: -1, # switch roles
    'tau_early_release_A': lambda dt: 0.1, 'tau_late_claim_A': lambda dt: 0.0002, # multitasking
    'tau_I_M_break_batch': lambda dt: 0.1, 'tau_I_M_break_batch2': lambda dt: 0.1, # ignore batch
    'tau_change_memory_I_A': lambda dt: 0.1, # diff res mem agent
    'add meter details_long_duration': lambda dt: 0.1 # long duration
  }
  weights = {
    'tau_missing_add customer details': lambda dt: 1.1,  # R_mi^e acd
    'start signing_ib': lambda dt: 1.1, 'approve contract_ib': lambda dt: 1,  # incorrect position batch
    'tau_open file_incorrect_A': lambda dt: 1.1, 'open file_incorrect_A': lambda dt: 1,
    'tau2_open file_incorrect_A': lambda dt: 1,  # 'incorrect object agent'
    'tau_switch_role_A_M': lambda dt: -1, 'tau_switch_role_back_A_M': lambda dt: -1,  # switch roles
    'tau_early_release_A': lambda dt: 1.1, 'tau_late_claim_A': lambda dt: 0.0002,  # multitasking
    'tau_I_M_break_batch': lambda dt: 1.1, 'tau_I_M_break_batch2': lambda dt: 1.1,  # ignore batch
    'tau_change_memory_I_A': lambda dt: 1.1,  # diff res mem agent
    'add meter details_long_duration': lambda dt: 1.1  # long duration
  }
  transition_weights.update(weights)

  # Add schedules for pattern transitions
  if 'switch role' in patterns_:
    schedules.update({
      'tau_switch_role_A_M': {('Charles', 'Charles(m)'): ([MO, TU, TH], [10])},
      'tau_switch_role_back_A_M': {('Charles', 'Charles(m)'): ([MO, TU, TH], [12])}
    })

  durations = {(arc.source.name, arc.target.name): (lambda: (0, '')) for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}
  durations[('add meter details', 'p6')] = lambda: (max(0, np.random.normal(2, 2)), '')
  durations[('add customer details', 'p7')] = lambda: (max(0, np.random.normal(5, 5)), '')
  durations[('cancel previous contract', 'p8')] = lambda: (max(0, np.random.normal(4, 10)), '')
  durations[('cancel previous contract2', 'p13')] = lambda: (max(0, np.random.normal(4, 10)), '')
  durations[('approve contract', 'p_end')] = lambda: (max(0, np.random.exponential(.3)), '')
  durations[('approve contract', 'p_to_sign')] = lambda: (max(0, np.random.exponential(.3)), '')

  sim = GroundTruthSimulation(system, patterns, transition_weights, durations, schedules, arrivals)

  # Add durations for pattern transitions
  sim.arc_durations[('add customer details_long_duration', 'p7')] = lambda: (max(0, np.random.normal(8, 5)), '')
  sim.arc_durations[('approve contract_ib', 'p_end')] = lambda: (0, '')
  sim.arc_durations[('approve contract_ib', 'p_end')] = lambda: (0, '')
  sim.arc_durations[('start_signing_ib', 'p_to_sign')] = lambda: (max(0, sum(np.random.exponential(.3) for _ in 5)), '')
  sim.arc_durations[('tau_missing_add_customer_details', 'p_7')] = durations[('add customer details', 'p7')]

  if 'switch role' in patterns_:
    # To keep the net a valid tPNID, the objects that switch roles should be aliased and added to the object types.
    sim.system.net.object_types['M'].add('Charles(m)')
    sim.system.net.variable_types['M'].add(Var('v_nu_m'))
  if 'incorrect position batch' in patterns_:
    # TODO tempfix the choice (lt dependency) for the ignore batching pattern, i.e., something like
    #  ss_or -> p_or -> ac_or, and ss_ib -> p_ib -> ac_ib
    ss_or = next(t for t in sim.system.transitions if t.name == 'start signing')
    ac_or = next(t for t in sim.system.transitions if t.name == 'approve contract')
    ss_ib = next(t for t in sim.system.transitions if t.name == 'start signing_ib')
    ac_ib = next(t for t in sim.system.transitions if t.name == 'approve contract_ib')
    # p_or = sim.system.net.add_place(Place('p_inc_pos_batch1'))
    # p_ib = sim.system.net.add_place(Place('p_inc_pos_batch2'))
    # sim.system.net.add_arc(MultiNuArc(ss_or, p_or, Multiset.from_str('[([m])]', MultiVarExpr)))

  log = sim.simulate_log(start_time='2024-04-08T08:59:00', nr_days=1, min_log_length=1, max_log_length=800, verbose=verbose)

  pattern_counts = {}
  base_tnames = [t.name for t in sim.base_system.transitions]
  for pattern, pn in sim.patterns.items():
    if pattern == 'multitask':
      # Count how many events occur with d1/d2 and not p when early_release with d1/d2-p is fired, before late_claim with d1/d2-p is fired.
      x = 0
      taus = log.projection(lambda f: f.transition.name in ['tau_early_release_A', 'tau_late_claim_A'])
      taus = taus.traverse()
      for a, b in zip(taus[::2], taus[1::2]):
        d, p = a.mode['v_a'], a.mode['v_i']
        for firing in log[log.index(a) + 1:log.index(b)]:
          if firing.mode.get('a', None) == d and firing.mode.get('i', None) != p:
            x += 1
          break
    elif pattern == 'switch role':
      # Count how many events occur with d1 and not p when early_release with d1/d2-p is fired, before late_claim with d1/d2-p is fired.
      x = 0
      taus = log.projection(lambda f: f.transition.name in ['tau_switch_role_A_M', 'tau_switch_role_back_A_M'])
      taus = taus.traverse()
      for a, b in zip(taus[::2], taus[1::2]):
        for firing in log[log.index(a) + 1:log.index(b) + 1]:
          if 'm' in firing.mode.keys() and firing.mode['m'].id == 'Charles(m)':
            x += 1
      x = int(x / 2)
    elif pattern == 'incorrect position batch':
      x = int(len([f for f in log if f.transition.name == 'start signing_ib']))
    elif pattern == 'ignore batch':
      x = int(len([f for f in log if f.transition.name in ['tau_I_M_break_batch', 'tau_I_M_break_batch2']]))
    else:
      add_ts = set([t.name for t in pn.transitions if t.name not in base_tnames])
      x = int(len([f for f in log if f.transition.name in add_ts]) / len(add_ts))
    pattern_counts[pattern] = x
    print(f'{pattern:<33}: {Fore.GREEN if x > 0 else Fore.RED}{x}{Style.RESET_ALL}')

  created_packages = {firing.mode['i'] for firing in log if firing.transition.name == 'create'}
  destroyed_packages = {firing.mode['i'] for firing in log if firing.transition.name in ['approve contract', 'approve contract_ib']}
  if created_packages == destroyed_packages:
    print(f'{Fore.GREEN}Processed all {len(created_packages)} packages.{Style.RESET_ALL}')
  else:
    ratio = len(destroyed_packages) / len(created_packages)
    print(f'{Fore.RED if ratio < 0.9 else Fore.GREEN}Did not manage to process all packages ({ratio:%}): {sorted(list(created_packages - destroyed_packages))} are missing.{Style.RESET_ALL}')

  if visualize:
    move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.name} @ {x.timestamp}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
    log.view(element_str=move_str)

  if export != '':
    log_json = log.to_json()
    log_json['Pi'] = pattern_counts
    log_json['M'] = [NoIndent(move) for move in log_json['M']]
    log_json['r'] = [NoIndent(row) for row in log_json['r']]
    log_json['m_i'] = str(sim.base_system.m_i)
    with open(export, 'w') as json_file:
      json.dump(log_json, json_file, cls=MyEncoder, sort_keys=False, indent=2)

  return log


def check_zero_token_between_events(m_i, log, between_event_a, between_event_b, place, token_str):
  # Count how many times m[place][token_str] = 0 inbetween events.
  x = 0
  taus = log.projection(lambda f: f.transition.name in [between_event_a, between_event_b])
  taus = taus.traverse()
  for a, b in zip(taus[::2], taus[1::2]):
    left = log.prefix([a])
    m = deepcopy(m_i) - sum([f.sub_marking for f in left], start=MultiNuMarking()) + sum([f.add_marking for f in left], start=MultiNuMarking())
    v1 = [m[place][MultiColorToken.from_str(token_str)]]
    for firing in log[log.index(a) + 1:log.index(b)]:
      m = m - firing.sub_marking + firing.add_marking
      v1.append(m[place][MultiColorToken.from_str(token_str)])
    for v1_a, v1_b in zip(v1[:-1], v1[1:]):
      if v1_a != 0 and v1_b == 0:
        x += 1
  return x


if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('-no', '--n_objects', help='Number of spontaneous objects to spawn and simulate.', type=int)
  parser.add_argument('-vis', '--visualize', help='Visualize simulated log.', action='store_true')
  parser.add_argument('-e', '--export', help='./datasets/logs/energy/log_<export>.json', type=str)
  parser.add_argument('-v', '--verbose', help='Verbose.', action='store_true')
  args = parser.parse_args()

  export_filename = f'./datasets/logs/energy/log_{args.export}.json' if args.export is not None else ''

  Pi_L = [RecordingDeviations._1, RecordingDeviations._2, RecordingDeviations._4]
  Pi_S = [BehavioralDeviations._1, BehavioralDeviations._2, BehavioralDeviations._3, BehavioralDeviations._4, BehavioralDeviations._5]
  log = create_synthetic_data(Pi_L + Pi_S, n_objects=args.n_objects, verbose=args.verbose, visualize=args.visualize, export=export_filename)
