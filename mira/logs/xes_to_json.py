import json
from pm4py import read_xes
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-xes', '--xes_file', help='', type=str)
parser.add_argument('-a', '--activity', help='', type=str, const='concept:name', nargs='?', default='concept:name')
parser.add_argument('-c', '--case_id', help='', type=str)
parser.add_argument('-r', '--resources', help='', type=str)

parser.add_argument('-fact', '--filter_activities', help='', type=str)
parser.add_argument('-kact', '--keep_activities', help='', type=str)
args = parser.parse_args()

filename = args.xes_file[:-4] if args.xes_file[-4:] == ".xes" else args.xes_file
xes_log = read_xes(f'{filename}.xes')

filter_activities = [] if args.filter_activities is None else args.filter_activities.split(',')
keep_activities = [] if args.keep_activities is None else args.keep_activities.split(',')


for index, trace in enumerate(xes_log):
  if index <= 50:
    continue
  event_list = []
  cases = set()
  for event in trace:
    activity = event[args.activity]
    if activity in filter_activities or (len(keep_activities) > 0 and activity not in keep_activities):
      continue

    case_id = event[args.case_id]
    event_list.append([activity, case_id, []])
    cases.add(case_id)
  print(index + 1, len(event_list), len(cases))
  with open(f'{filename}_{index+1}.json', 'w') as json_file:
    json.dump({
      'n_events': len(event_list),
      'n_cases': len(cases),
      'events': event_list,
      "<": "all",
      "||": []},
      json_file)
