from mira.nets.multi_nu_net import MultiNuNet
from mira.prelim.multiset import Multiset
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import Arc, InhibitorArc, MultiNuArc, InhibitorMultiNuArc, VariableMultiNuArc, Var, MultiVarExpr
from mira.nets.firing import MultiNuFiring
from mira.nets.marking import MultiNuMarking
from mira.nets.utils import powerset, values_combinations, partitions

from typing import Optional, Iterable
from copy import deepcopy
from collections import defaultdict
from tqdm import tqdm
import re
from pprint import pformat


class tPNID(MultiNuNet):
  firing_type = MultiNuFiring
  marking_type = MultiNuMarking
  def __init__(self, places: Optional[Iterable[Place]] = None, transitions: Optional[Iterable[Transition]] = None,
               arcs: Optional[Iterable[Arc]] = None, inhibitor_arcs: Optional[Iterable[InhibitorArc]] = None,
               object_types=None, variable_types=None, relaxed=False):
    super().__init__(places, transitions, arcs, inhibitor_arcs, object_types=object_types, variable_types=variable_types)
    # self.object_types = {r: set(rO) for r, rO in object_types.items()}
    # self.variable_types = {r: set(Var(v) if isinstance(v, str) else v for v in rvars) for r, rvars in variable_types.items()}

  @classmethod
  def from_json_dict(cls, data, silent=lambda name: name[:3] == 'tau', relaxed=False, verbose=False):
    name_label = lambda namelabel: (name[0], label[0]) if (len(name := re.findall('(.*?)\(', namelabel)) == 1 and len(label := re.findall('\((.*?)\)', namelabel)) == 1) else (namelabel, namelabel)
    places = {p: Place(p) for p in data['P']}
    transitions = {}
    for t in data['T']:
      name, label = name_label(t)
      if verbose:
        print(f'{t=} {name=}, {label=}')
      transitions[t] = Transition(name, None if silent(label) else label)

    elements = {**places, **transitions}

    object_types = {}
    for role, role_objects in data.get('O', {}).items():
      # Pattern should be of sort: ['xi_start', '..', 'xi_end'] to generate [f'{x}{i} for i in range(i_start, i_end+1)]
      if '..' in role_objects and len(role_objects) == 3 and all(v[-1].isnumeric for v in [role_objects[0], role_objects[2]]):
        # TODO parse e.g., [p1,..,p10,q1,..,q10]
        name_i_split = next((i for i, x in enumerate(reversed(role_objects[0])) if not x.isnumeric()), 0)
        prefix_zeros = int(role_objects[0][-name_i_split]) == 0
        i_start = int(role_objects[0][-name_i_split:])
        name = role_objects[0][:-name_i_split]
        name_i_split2 = next((i for i, x in enumerate(reversed(role_objects[2])) if not x.isnumeric()), 0)
        i_end = int(role_objects[2][-name_i_split2:])

        if name != role_objects[2][:-name_i_split2]:
          raise ValueError('')

        max_length = max([len(role_objects[0][-name_i_split:]), len(role_objects[2][-name_i_split2:])])
        object_types[role] = [f'{name}{i:0{max_length if prefix_zeros else 0}d}' for i in range(i_start, i_end+1)]
      else:
        object_types[role] = role_objects

    net = tPNID(list(places.values()), list(transitions.values()), object_types=object_types, variable_types=data.get('V', {}))

    for labels, arcs in data['F'].items():
      for source, target in arcs:
        if isinstance(labels, Multiset):
          labels = str(labels)
        labels = Multiset.from_str(labels, MultiVarExpr)
        net.add_arc(MultiNuArc(elements[source], elements[target], labels))

    [net.add_inhibitor_arc(InhibitorMultiNuArc(elements[source], elements[target], Multiset.from_str(labels, MultiVarExpr)))
     for labels, arcs in data.get('F_inh', {}).items() for source, target in arcs]

    if relaxed:
      for transition in net.transitions:
        try:
          reference = transitions[transition.name[:transition.name.index('↾')]]
          transition.vars_original = reference.vars
        except ValueError:
          transition.vars_original = transition.vars

    return net

  def to_dict(self, save_filename=None):
    net_data = {
      'P': [p.name for p in self.places],
      'T': [t.name for t in self.transitions]
    }
    arc_data = defaultdict(list)
    for arc in self.arcs:
      arc_data[str(arc.labels)].append([arc.source.name, arc.target.name])
    inh_arc_data = defaultdict(list)
    for arc in self.inhibitor_arcs:
      inh_arc_data[str(arc.labels)].append([arc.source.name, arc.target.name])

    data = {**net_data, 'F': dict(arc_data), 'F_inh': dict(inh_arc_data), 'O': {r: list(rO) for r, rO in self.object_types.items()},
            'V': {r: [str(v) for v in rvars] for r, rvars in self.variable_types.items()}}
    if save_filename is None:
      return data

    with open(save_filename, 'w') as json_file:
      json_file.write(pformat(data, compact=True, width=150).replace("'", '"'))


  def add_arc(self, arc: Arc, verbose: bool = False):
    super().add_arc(arc, verbose)
    arc.place.alpha = self.alpha(arc.place, self.variable_types)
    arc.transition.vars = self.vars(arc.transition)
    arc.transition.in_vars = self.in_vars(arc.transition)
    arc.transition.out_vars = self.out_vars(arc.transition)

  def set_alphas(self, variable_types):
    for place in self.places:
      place.alpha = self.alpha(place, variable_types)

  def add_variable_arc(self, arc:VariableMultiNuArc, substitute=True):
    raise NotImplementedError('Variable arcs are not supported for tPNIDs, use MultiNuNet instead.')
    return None

  def alpha(self, place: Place, variable_types: dict):
    types = None
    arc_type = None
    for arc in self.arcs:
      if arc.place == place:
        for labels in arc.labels:
          if any(len(varexpr.vars) + len(varexpr.setvars) != 1 for varexpr in labels): # TODO check if setvars is handled correctly here.
            raise ValueError(f't-PNID should have beta defined differently, beta({arc}) = {labels}.')

          label_types = []
          for varexpr in labels:
            tt = [t for t, vs in variable_types.items() if list(varexpr.vars)[0] in vs]
            if len(tt) == 1:
              label_types.append(tt[0])
          label_types = tuple(label_types)
          if types is None:
            types = label_types
            arc_type = arc
          if types != label_types:
            raise ValueError(f't-PNID should have beta defined such that it matches alpha, beta({arc}) = {label_types} != beta({arc_type}) = {types}.')
    return types

  def beta(self, arc):
    return arc.labels

  def projection(self, roles, object_types, variable_types, skip_silents=False, skip_dups=True):
    roles_str = lambda rols: '{' + ','.join(rols) + '}'
    places = {}
    for place in self.places:
      if '↾' in place.name:
        continue
      place_roles = set(place.alpha)
      place_roles_proj = place_roles.intersection(roles)
      if len(place_roles_proj) > 0:
        places[place] = Place(f'{place.name}' + ('' if place_roles_proj == place_roles else f'↾{roles_str(place_roles_proj)}'))

    variables = set.union(*[vars for role, vars in variable_types.items() if role in roles])
    vars_str = lambda vars: '{' + ','.join([var.var for var in vars]) + '}'
    transitions = {}
    for transition in self.transitions:
      if transition.label is None and skip_silents:
        continue
      if '↾' in transition.name:
        continue
      transitions[transition] = {}
      transition_variables = set.union(*[varexpr.vars for arc in self.arcs for labels in arc.labels.keys() if transition == arc.transition for varexpr in labels])
      transition_variables_proj = transition_variables.intersection(variables)
      if len(transition_variables_proj) > 0:

        transition_variables_per_role = {role: powerset(rolevars_proj) for role, vars in variable_types.items() if len(rolevars_proj := vars.intersection(transition_variables_proj)) > 0}
        for vars in [set.union(*[set(x) for x in pc.values()]) for pc in values_combinations(transition_variables_per_role)]:
          if len(vars) > 0:
            tname = f'{transition.name}' + ('' if set(vars) == transition_variables else f'↾{vars_str(vars)}')
            tlabel = transition.label
            # tlabel = None if transition.label is None else (f'{transition.label}' + ('' if set(vars) == transition_variables else f'↾{vars_str(vars)}'))
            new_transition = Transition(tname, tlabel, move_type=transition.move_type)
            new_transition.vars_original = transition.vars
            transitions[transition][tuple(sorted(vars, key=lambda var: var.var))] = new_transition

    tpnid = tPNID(set(places.values()), set.union(*[set(transition_vars.values()) for transition_vars in transitions.values()]),
                  object_types=self.object_types, variable_types=self.variable_types)

    for arc in self.arcs:
      if arc.transition in transitions.keys() and arc.place in places.keys():
        for V, transition in transitions[arc.transition].items():
          V = set(V)
          source, target = (transition, places[arc.place]) if arc.transition == arc.source else (places[arc.place], transition)

          largs = defaultdict(int)
          for labels, n in arc.labels.items():
            if any(len(varexpr.vars.intersection(V)) > 0 for varexpr in labels):
              labels_proj = [varexpr.expression_str for varexpr in labels if len(varexpr.vars.intersection(V)) > 0]
              if len(labels_proj) != len(set(arc.place.alpha).intersection(roles)):
                continue
              largs[MultiVarExpr(len(labels_proj), labels_proj)] += n
          if len(largs) > 0:
            if skip_dups and any(arc.source.name == source.name and arc.target.name == target.name for arc in self.arcs):
              continue
            else:
              tpnid.add_arc(MultiNuArc(source, target, Multiset(args=largs)))

    for arc in self.inhibitor_arcs:
      if arc.transition in transitions.keys() and arc.place in places.keys():
        for V, transition in transitions[arc.transition].items():
          V = set(V)
          source, target = (places[arc.place], transition)

          largs = defaultdict(int)
          for labels, n in arc.labels.items():
            if any(len(varexpr.vars.intersection(V)) > 0 for varexpr in labels):
              labels_proj = [varexpr.expression_str for varexpr in labels if len(varexpr.vars.intersection(V)) > 0]
              if len(labels_proj) != len(set(arc.place.alpha).intersection(roles)):
                continue
              largs[MultiVarExpr(len(labels_proj), labels_proj)] += n
          if len(largs) > 0:
            if not any(arc.source.name == source.name and arc.target.name == target.name for arc in self.inhibitor_arcs):
              tpnid.add_inhibitor_arc(InhibitorMultiNuArc(source, target, Multiset(args=largs)))

    return tpnid

  def union(self, other):
    places = {place: deepcopy(place) for place in set(self.places).union(other.places)}
    transitions = {transition: deepcopy(transition) for transition in set(self.transitions).union(other.transitions)}
    elements = {**places, **transitions}
    merger = lambda a, b: {k: a.get(k, set()).union(b.get(k, set())) for k in set(a.keys()).union(b.keys())}
    net = tPNID(list(places.values()), list(transitions.values()),
                object_types=merger(self.object_types, other.object_types), variable_types=merger(self.variable_types, other.variable_types))

    for arc in set(self.arcs).union(other.arcs):
      net.add_arc(MultiNuArc(elements[arc.source], elements[arc.target], arc.labels))
    for arc in set(self.inhibitor_arcs).union(other.inhibitor_arcs):
      net.add_inhibitor_arc(InhibitorMultiNuArc(elements[arc.source], elements[arc.target], arc.labels))
    return net

  def __sub__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")
    places = {place: deepcopy(place) for place in set(self.places) - set(other.places)}
    transitions = {transition: deepcopy(transition) for transition in set(self.transitions) - set(other.transitions)}
    elements = {**places, **transitions}
    merger = lambda a, b: {k: a.get(k, set()).union(b.get(k, set())) for k in set(a.keys()).union(b.keys())}
    net = tPNID(list(places.values()), list(transitions.values()),
                object_types=merger(self.object_types, other.object_types),
                variable_types=merger(self.variable_types, other.variable_types))

    for arc in set(self.arcs) - set(other.arcs):
      net.add_arc(MultiNuArc(elements[arc.source], elements[arc.target], arc.labels))
    return net

  def __add__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")
    return self.union(other)

  def relax(self, skip_silents=False):
    object_subsets = set()
    for t in self.transitions:
      t_roles = set([role for role, role_vars in self.variable_types.items() if len(t.vars.get(role, [])) > 0])
      object_subsets.update(set(powerset(t_roles)))

    net_add = deepcopy(self)
    for t in net_add.transitions:
      t.vars_original = t.vars
    for s in tqdm(object_subsets):
      net_p = net_add.projection(set(s), self.object_types, self.variable_types, skip_silents=skip_silents)
      net_add = net_add.union(net_p)
    net_add.set_alphas(self.variable_types)
    net_add.add_correlation_cd_net()
    return net_add

  def add_correlation_cd_net(self):
    for place in [p for p in self.places if '↾' not in p.name and len(p.alpha) > 1]:
      proj_places = {p.alpha: p for p in self.places if '↾' in p.name and p.name.split('↾')[0] == place.name}
      for B_alpha in [B_alpha for B_alpha in partitions(list(place.alpha)) if len(B_alpha) > 1]:
        B_alpha_str = '{' + ','.join([f'{{{",".join(v)}}}' for v in B_alpha]) + '}'
        t_create = self.add_transition(Transition(f't_create^{place.name},{B_alpha_str}', None))
        self.add_arc(MultiNuArc(t_create, place, Multiset(args={tuple(f'{{{x.lower()}: 1}}' for x in place.alpha): 1})))
        t_destroy = self.add_transition(Transition(f't_destroy^{place.name},{B_alpha_str}', None))
        self.add_arc(MultiNuArc(place, t_destroy, Multiset(args={tuple(f'{{{x.lower()}: 1}}' for x in place.alpha): 1})))
        for sub_alpha in B_alpha:
          self.add_arc(MultiNuArc(proj_places[tuple(sub_alpha)], t_create,
                                  Multiset(args={MultiVarExpr(size=len(sub_alpha), varexprs=[f'{{{x.lower()}: 1}}' for x in sub_alpha]): 1})))
          self.add_arc(MultiNuArc(t_destroy, proj_places[tuple(sub_alpha)],
                                  Multiset(args={MultiVarExpr(size=len(sub_alpha), varexprs=[f'{{{x.lower()}: 1}}' for x in sub_alpha]): 1})))

  def vars(self, transition):
    if transition not in self.transitions:
      raise ValueError(f'Transition {transition} not in this net.')
    transition_variables = set.union(*[varexpr.vars for arc in self.arcs for labels in arc.labels.keys() if transition == arc.transition for varexpr in labels], set())
    return {role: role_vars for role, vars in self.variable_types.items() if len(role_vars := vars.intersection(transition_variables)) > 0}

  def in_vars(self, transition):
    if transition not in self.transitions:
      raise ValueError(f'Transition {transition} not in this net.')
    transition_variables = set.union(*[varexpr.vars for arc in self.element_in_arcs[transition] for labels in arc.labels.keys() for varexpr in labels], set())
    return {role: role_vars for role, vars in self.variable_types.items() if len(role_vars := vars.intersection(transition_variables)) > 0}

  def out_vars(self, transition):
    if transition not in self.transitions:
      raise ValueError(f'Transition {transition} not in this net.')
    transition_variables = set.union(*[varexpr.vars for arc in self.element_out_arcs[transition] for labels in arc.labels.keys() for varexpr in labels], set())
    return {role: role_vars for role, vars in self.variable_types.items() if len(role_vars := vars.intersection(transition_variables)) > 0}
