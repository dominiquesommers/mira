import json
import os
import tqdm


def t_str(x):
  h = int(x / 3600)
  m = int((x - h * 3600) / 60)
  s = int((x - h * 3600 - m * 60))
  ms = round((x - int(x))*1000)
  return f'{h}:{m:02d}:{s:02d}.{ms}'

dir = './omron'

filenames = [f for f in os.listdir(dir) if f[-5:] == '.json']
filenames = sorted(filenames, key=lambda s: int(s.split('.json')[0].split('_')[1]))

with open('../examples/alignments/omron/omron_8.json', 'r') as json_file:
  a = json.load(json_file)
with open('../examples/alignments/omron/omron_8_.json', 'r') as json_file:
  b = json.load(json_file)

sigma_1  = a['M']
sigma_2  = b['M']




# for filename in tqdm.tqdm(filenames):
#   if filename[-5:] != '.json':
#     continue
#   filename = f'{dir}/{filename}'
#   print(filename)
#   with open(filename, 'r') as json_file:
#     data = json.load(json_file)
#   print(f'n: {len(data["M"])}')
#   print(f't: {t_str(data["info"]["t"]["total"])}')
#   print(f'L: {data["verification"]["log"]}')
#   print(f'M: {data["verification"]["model"]}')
#   if not data["verification"]["model"]:
#     print('='*120)
