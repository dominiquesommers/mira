from mira.nets.petri_net import *
from mira.nets.arc import Arc, NuArc, MultiNuArc, SetVar, Var, VarExpr
from mira.nets.utils import powerset

test = 'tpnid0'

if test == 'tpnid0':
  object_types = {'P': {'p1', 'p2'}}
  variable_types = {'P': {Var('p')}}
  ps = {name: Place(name) for name in ['pi', 'pf', 'px', 'py']}
  ts = {name: Transition(name, name) for name in ['t1', 't2', 't3']}
  els = {**ps, **ts}
  net = tPNID(list(ps.values()), list(ts.values()), object_types=object_types, variable_types=variable_types)
  net.add_arc(MultiNuArc(els['pi'], els['t1'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(els['t1'], els['pf'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(els['pf'], els['t2'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(els['t2'], els['px'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(els['py'], els['t3'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(els['pi'], els['t3'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(els['t3'], els['pf'], Multiset(args={('{p: 1}',): 1})))
  m = MultiNuMarking.from_str('[[([p1]),([p2])]·pi]')
  system = System(net, m, None)

  f1, nr = system.enabled_firings(parent_enabled_firings=[], last_firing=None)
  print('enabled:')
  [print(f) for f in f1]

  last_firing = f1[0]
  system.fire(last_firing, last_firing.new_marking(system.marking))

  f2, nr2 = system.enabled_firings(parent_enabled_firings=f1, last_firing=last_firing)

  print('still enabled:')
  [print(f) for f in f2]
  print(nr + nr2)
  # system.view()

if test == 'tpnid1':
  object_types = {'P': {'p1'},  # Multiset(args={'p1': 1}),
                  'D': {'d1', 'd2'},  # Multiset(args={'d1': 1, 'd2': 1}),
                  'W': {'w1', 'w2'}}  # Multiset(args={'w1': 2, 'w2': 1})}
  variable_types = {'P': {Var('nu_p'), Var('p')}, 'D': {Var('d')}, 'W': {Var('w')}}

  ps = {name: Place(name) for name in ['pi', 'pd', 'pp']}
  t = Transition('t', 't')
  net = tPNID(list(ps.values()), [t], object_types=object_types, variable_types=variable_types)

  net.add_arc(MultiNuArc(ps['pi'], t, Multiset(args={('{p: 1}', '{d: 1}'): 1})))
  net.add_arc(MultiNuArc(t, ps['pp'], Multiset(args={('{p: 1}',): 1})))
  net.add_arc(MultiNuArc(t, ps['pd'], Multiset(args={('{d: 1}',): 1})))

  m = MultiNuMarking({
    ps['pi']: {MultiColorToken(2, [{'p1': 1}, {'d1': 1}]): 1},
  })
  objects = set.union(*object_types.values())
  net_add = deepcopy(net)
  for s in powerset(objects):
    net_p = net.projection(set(s), object_types, variable_types)
    net_add = net_add.union(net_p)
  net_add.set_alphas(variable_types)

  net_add.add_correlation_cd_net()

  rel_mnet = System(net_add, m, None)
  rel_mnet.view(debug=False)


if test == 'tpnid':
  object_types = {'P': {'p1', 'p2'}, 'D': {'d1'}}
  variable_types = {'P': {Var('p'), Var('q')}, 'D': {Var('d')}}

  ps = {name: Place(name) for name in ['p']}
  t = Transition('t', 't')
  net = tPNID(list(ps.values()), [t], object_types=object_types, variable_types=variable_types)
  labels = Multiset(args={('{p: 1}', '{d: 1}'): 1, ('{q: 1}', '{d: 1}'): 1})
  arc = MultiNuArc(ps['p'], t, labels)
  net.add_arc(arc)

  m = MultiNuMarking({
    ps['p']: {MultiColorToken(2, [{'p1': 1}, {'d1': 1}]): 1, MultiColorToken(2, [{'p2': 1}, {'d1': 1}]): 1},
  })

  net.set_alphas(variable_types)

  objects = set.union(*object_types.values())
  net_add = deepcopy(net)
  for s in powerset(objects):
    net_p = net.projection(set(s), object_types, variable_types)
    net_add = net_add.union(net_p)
  net_add.set_alphas(variable_types)

  net_add.add_correlation_cd_net()

  rel_mnet = System(net_add, m, None)

  ff = rel_mnet.enabled_firings()
  [print(f) for f in ff]
  for f in ff:
    if 'destroy' in f.transition.name:
      rel_mnet.fire_transition(f.transition, f.sub_marking, f.add_marking, f.mode)
  print('fired')
  [print(f) for f in rel_mnet.enabled_firings()]

  rel_mnet.view(debug=True)
  print(stop)

elif test == 'nu':
  ps = [Place(name) for name in ['p1', 'p2']]
  t = Transition('t', 't')
  t_ = Transition('t_', 't_')
  net = NuNet(ps, [t, t_])
  net.add_arc(NuArc(ps[0], t, 'x'))
  net.add_arc(NuArc(ps[1], t, 'y'))
  net.add_arc(NuArc(ps[1], t_, 'x'))

  m = NuMarking({
    'p1': {ColorToken('a'): 2, ColorToken('b'): 1},
    'p2': {ColorToken('a'): 1, ColorToken('b'): 1}
  })

  enabled, sub, add = net.transition_enabled(t, m, {'x': 'a', 'y': 'b'})
  print(enabled)
  print(sub)
  print(add)



elif test == 'multi':
  ps = [Place(name) for name in ['p1', 'p2', 'p3']]
  t = Transition('t', 't')
  t_ = Transition('t\'', 't\'')
  net = MultiNuNet(ps, [t, t_])
  net.add_arc(MultiNuArc(ps[0], t, ('X', '_')))
  net.add_arc(MultiNuArc(ps[1], t, ('{x: 1}', '_')))
  net.add_arc(MultiNuArc(t, ps[0], ('X - {x: 1}', '_')))
  net.add_arc(MultiNuArc(t, ps[2], ('{x: 1}', '_')))

  net.add_arc(MultiNuArc(ps[0], t_, ('_', '_')))

  m = MultiNuMarking({
    'p1': {MultiColorToken(2, [{'a': 1, 'b': 1}, {}]): 1},
    'p2': {MultiColorToken(2, [{'a': 1}, {}]): 1,
           MultiColorToken(2, [{'b': 1}, {}]): 1,
           MultiColorToken(2, [{'c': 1}, {}]): 1}
  })

  net.view(m)

  print('ENABLED FIRINGS:')
  [print(f) for f in net.enabled_firings(m)]
  print(fds)

  print(f'Initial marking: {m}')
  firings = [
    (t, {'X': Multiset(ColorToken, {'a': 1, 'b': 1}), 'x': ColorToken('c')}),
    # (t, {'X': Multiset(ColorToken, {'b': 1}), 'x': ColorToken('b')}),
    # (t_, {}) #'X': Multiset(ColorToken, {})})
  ]

  for transition, mode in firings:
    enabled, sub, add = net.transition_enabled(transition, m, mode)
    print(f'{"E" if enabled else "Not e"}nabled.')
    if enabled:
      m, firing = net.fire_transition(m, transition, sub, add, mode)
      print(firing)
      print(f'New marking: {m}')
