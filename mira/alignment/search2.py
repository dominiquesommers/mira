from mira.nets.marking import Marking
from mira.nets.petri_net import System
from mira.alignment.log_net import create_log_system
from mira.alignment.synchronous_product import tPNIDSynchronousProductSystem
from mira.alignment.cost import Cost
from mira.logs.event_log import Log

from collections import defaultdict
import json
import heapq
from typing import Union, List


class State:
  def __init__(self, marking:Marking, cost:float, last_firing=None, parent=None):
    self.marking = marking
    self.cost = cost
    self.parent = parent
    self.last_firing = last_firing
    self.next_states = None

  def __lt__(self, other):
    return self.cost < other.cost

  def __eq__(self, other):
    return self.marking == other.marking

  def __hash__(self):
    return hash(self.marking)

  def __repr__(self):
    firing_str = f' {self.last_firing.transition} {self.last_firing.mode}' if self.last_firing is not None else ''
    return f'STATE {self.marking}{firing_str}'

  def __str__(self):
    return self.__repr__()

def rec(state):
  states = [state]
  while state.parent is not None:
    states.append(state.parent)
    state = state.parent
  [print(state) for state in reversed(states)]

def search(system, costs):
  initial_state = State(system.m_i, 0)
  states = {initial_state}
  open_set = [initial_state]
  heapq.heapify(open_set)

  while len(open_set) > 0:
    current_state = heapq.heappop(open_set)
    print(f'{current_state=}')

    if current_state.marking == system.m_f:
      print('Found final marking.')
      rec(current_state)
      break

    if current_state.next_states is None:
      current_state.next_states = set()
      for enabled_firing in system.net.enabled_firings(current_state.marking)[0]:
        new_cost = current_state.cost + costs(enabled_firing.transition)
        new_marking = enabled_firing.new_marking(current_state.marking)
        if (new_state := next((state for state in states if hash(state) == hash(new_marking)), None)) is not None:
          print(f'already saw this state! {new_state=}')
          if new_cost < new_state.cost:
            new_state.parent = current_state
        else:
          new_state = State(new_marking, new_cost, enabled_firing, current_state)
        print(f'New state: {new_state=}')
        states.add(new_state)
        heapq.heappush(open_set, new_state)
  else:
    print('Did not find run.')



def main():
  with open('./tests/nets/search.json', 'r') as json_file:
    data = json.load(json_file)
    system = System.from_json_dict(data)
    # system.view()
  with open(f'./tests/logs/search_1.json', 'r') as json_file:
    data = json.load(json_file)
    log = Log.from_json_dict(data)

  log_system = create_log_system(log, type(system.net))
  sync_product = tPNIDSynchronousProductSystem(log_system, system)
  # sync_product.view(debug=True)

  search(sync_product, Cost.cost)

if __name__ == '__main__':
  main()