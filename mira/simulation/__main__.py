from mira.simulation.simulator import PetrinetSimulator
from mira.simulation.ground_truth_simulation import GroundTruthSimulation
from mira.nets.system import System
from mira.nets.token import ColorToken
from mira.nets.arc import MultiNuArc, MultiVarExpr
from mira.prelim.multiset import Multiset

from dateutil.relativedelta import *
import random
import numpy as np
import json
from copy import deepcopy
from datetime import datetime, timedelta

with open('./datasets/nets/delivery_extended.json', 'r') as json_file:
  data = json.load(json_file)
  system = None
  for net_data in data:
    # if net_data['comment'] not in ['delivery']:
    #   continue
    system_ = System.from_json_dict(net_data)
    if system is None:
      system = system_
    else:
      system = system.union(system_)
  # system.view(debug=True)
  # exit()

pattern_base_dir = './datasets/deviation_patterns'
patterns_ = {
  'change correlation': (f'{pattern_base_dir}/change_correlation.json', {'p': 'p4', 'py': 'p_v_ava', 'x': 'P', 'y': 'V'}), # TODO tempfixed always enabled transition
  'swap correlationPD': (f'{pattern_base_dir}/swap_correlation.json', {'p': 'p6', 'x': 'P', 'y': 'D'}),
  'swap correlationPV': (f'{pattern_base_dir}/swap_correlation.json', {'p': 'p_v_occ', 'x': 'P', 'y': 'V'}),
  'multitask': (f'{pattern_base_dir}/multitask.json', {'p1': 'p5', 'p2': 'p_d_on', 'x': 'P', 'y': 'D'}), # TODO tempfixed always enabled transition
  'skip ring':  (f'{pattern_base_dir}/skip_activity.json', {'x': 'ring'}),
  'neglect warehouse': (f'{pattern_base_dir}/neglect_objects.json', {'t': 'register depot', 'Y': 'W'}), # TODO tempfixed always enabled transition
  'neglect deliverer deliver depot': (f'{pattern_base_dir}/neglect_objects.json', {'t': 'deliver depot', 'Y': 'D'}), # TODO tempfixed (with bi-arc to/from p12)
  'overtake queue': (f'{pattern_base_dir}/overtake_queue.json', {'q2': 'q2_', 'q3': 'q3_', 'q': 'Y', 'y': 'P'}),
  'decrease warehouse cap': (f'{pattern_base_dir}/decrease_capacity_inf.json', {'c': 'p_w', 'r': 'W'}),
  'increase warehouse cap': (f'{pattern_base_dir}/increase_capacity_inf.json', {'c': 'p_w', 'r': 'W'}),
  'switch role': (f'{pattern_base_dir}/switch_role.json', {'px': 'p_d_on', 'py': 'p_we', 'x': 'D', 'y': 'E'}),
  'diff res mem warehouse': (f'{pattern_base_dir}/different_resource_memory.json', {'p1': 'p7', 'p2': 'p_w', 'x': 'P', 'y': 'W'}), # TODO tempfixed always enabled transition
  'neglect batching': (f'{pattern_base_dir}/neglect_batching.json', {'c': 'p_v_ava', 'r': 'V'}), #

  'missing event load': (f'{pattern_base_dir}/missing_event.json', {'x': 'load in truck'}),
  'incorrect event order home->depot': (f'{pattern_base_dir}/incorrect_event.json', {'x': 'order home', 'y': 'order depot'}),
  'incorrect event order depot->home': (f'{pattern_base_dir}/incorrect_event.json', {'x': 'order depot', 'y': 'order home'}),
  'missing object v': (f'{pattern_base_dir}/missing_object.json', {'t': 'load in truck', 'Y': 'V'}),
  'incorrect W object': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'deliver depot', 'R': 'W'}),
  'incorrect V object': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'load in truck', 'R': 'V'}),
  'incorrect D object': (f'{pattern_base_dir}/incorrect_object.json', {'t': 'ring', 'R': 'D'}),
  'missing position': (f'{pattern_base_dir}/missing_position.json', {'x': 'deliver depot', 'y': 'collect'}),
  'incorrect position': (f'{pattern_base_dir}/incorrect_position.json', {'x': 'ring', 'y': 'deliver home'})
}
patterns = {}
for pattern_name, (pattern_filename, mapping) in patterns_.items():
  with open(pattern_filename, 'r') as json_file:
    patterns[pattern_name] = {'net_data': json.load(json_file), 'mapping': mapping}

# Expected objects (scheduled)
schedules = {
  'start': {'d1': ([MO, TU, WE, TH, FR], [8, 13]), 'd2': ([MO, TU, WE, TH], [8])},
  'stop':  {'d1': ([MO, TU, WE, TH, FR], [12, 17]), 'd2': ([MO, TU, WE, TH], [17])}
}

# Spontaneous objects
arrivals = {'P': lambda dt: random.expovariate(1/600)} #1/3600)}

transition_weights = {
  t.name: (lambda dt: -1) if t.label in ['start', 'stop', 'create'] else
          (lambda dt: 1)
  for t in system.transitions
}

weights = {
  # behavioral deviations
  'tau_swap_correlation_PDV': lambda dt: np.random.binomial(1, 0.5),
  'tau_change_correlation_p4_PV': lambda dt: 1,
  'tau_early_release_D': lambda dt: 1, 'tau_late_claim_D': lambda dt: 0.0002,
  'tau_skip_ring': lambda dt: 1,
  'tau_create^p7↾{W}_{P}': lambda dt: 1, 'register depot_neglect_W': lambda dt: 1, 'tau_register depot_W': lambda dt: 0,
  'tau_destroy^p6↾{D}_{P}': lambda dt: 1, 'deliver depot_neglect_D': lambda dt: 1, 'tau_deliver depot_D': lambda dt: 100,
  'tau_overtake_Y': lambda dt: 1,
  'tau_p_w_-': lambda dt: -1, 'tau_p_w_-_undo': lambda dt: -1,
  'tau_p_w_+': lambda dt: -1, 'tau_p_w_+_undo': lambda dt: -1,
  'tau_switch_role_D_E': lambda dt: -1, 'tau_switch_role_back_D_E': lambda dt: -1,
  'tau_change_memory_P_W': lambda dt: 1,
  'tau_p_v_ava_-': lambda dt: -1, 'tau_p_v_ava_-_undo': lambda dt: -1,
  # recording deviations
  'tau_missing_load in truck': lambda dt: 1,
  'order depot_incorrect_order home': lambda dt: 1, 'order home_incorrect_order depot': lambda dt: 1,
  'load in truck_missing_V': lambda dt: 1, 'tau1_load in truck_missing_V': lambda dt: 1, 'tau2_load in truck_missing_V': lambda dt: 1,
  'tau_deliver depot_incorrect_W': lambda dt: 1, 'deliver depot_incorrect_W': lambda dt: 1, 'tau2_deliver depot_incorrect_W': lambda dt: 1,
  'tau_load in truck_incorrect_V': lambda dt: -1, 'load in truck_incorrect_V': lambda dt: -1, 'tau2_load in truck_incorrect_V': lambda dt: -1,
  'tau_ring': lambda dt: -1, 'ring_incorrect_D': lambda dt: -1, 'tau2_ring': lambda dt: -1,
  'deliver depot_parallel_collect': lambda dt: 1, 'collect_parallel_deliver depot': lambda dt: 1, 'tau_1_parallel_deliver depot_collect': lambda dt: 1, 'tau_2_parallel_deliver depot_collect': lambda dt: 1,
  'deliver home_pos_ring': lambda dt: 1, 'ring_pos_deliver home': lambda dt: 1
}
transition_weights.update(weights)

if 'increase warehouse cap' in patterns_:
  schedules.update({
    'tau_p_w_+':      {'w1': ([MO, TU, WE, TH, FR], [12])},
    'tau_p_w_+_undo': {'w1': ([MO, TU, WE, TH, FR], [13])}
  })
if 'decrease warehouse cap' in patterns_:
  schedules.update({
    'tau_p_w_-':      {'w1': ([MO, TU, WE, TH, FR], [8])},
    'tau_p_w_-_undo': {'w1': ([MO, TU, WE, TH, FR], [11])}
  })
if 'switch role' in patterns_:
  schedules.update({
    'tau_switch_role_D_E': {('d1', 'd1(e)'): ([MO, TU, WE, TH, FR], [3])},
    'tau_switch_role_back_D_E': {('d1', 'd1(e)'): ([MO, TU, WE, TH, FR], [4])}
  })
if 'neglect batching' in patterns_:
  schedules.update({
    'tau_p_v_ava_-': {'v1': ([MO, TU, WE, TH, FR], [18])},
    'tau_p_v_ava_-_undo': {'v1': ([MO, TU, WE, TH, FR], [20])}
  })

x = 5
transition_weights['order depot'] = lambda dt: 1 # int((dt - (dt + relativedelta(hour=0,minute=0,second=0,microsecond=0))).total_seconds() < (x*3600)) # int(before x o'clock)
transition_weights['order home'] = lambda dt: 1  # int((dt - (dt + relativedelta(hour=0,minute=0,second=0,microsecond=0))).total_seconds() >= (x*3600)) # int(after x o'clock)
# transition_weights['tau_2'] = lambda dt: 1

durations = {(arc.source.name, arc.target.name): (lambda: 0) for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}
durations[('start', 'p_d_on')] = lambda: 1/60/10000000
durations[('pick package', 'p3')] = lambda: np.random.normal(15, 6) # Time to walk package to delivery van
durations[('tau_d_1', 'p6')] = lambda: np.random.normal(30, 10) # Time to drive to first destination
# durations[('tau_d_2', 'p6')] = lambda: np.random.normal(30, 10) # Time to drive to next destination(s)
durations[('ring', 'p5')] = lambda: np.random.normal(3, 1) # Time to wait for answer
durations[('tau_2', 'p6')] = lambda: np.random.normal(10, 5) # Time to drive from house to nearest depot
durations[('deliver depot', 'p9')] = lambda: np.random.normal(360, 90) # Time for clients to reach depot
durations[('deliver home', 'p_d_on')] = lambda: np.random.normal(10, 3) # Time to drive to next destination(s) or back to warehouse
durations[('deliver home', 'p_v_free')] = lambda: np.random.normal(10, 3) # Time to drive to next destination(s) or back to warehouse
durations[('deliver depot', 'p_d_on')] = lambda: np.random.normal(10, 3) # Time to drive to next destination(s) or back to warehouse
durations[('deliver depot', 'p_v_free')] = lambda: np.random.normal(10, 3) # Time to drive to next destination(s) or back to warehouse


copiers = [('tau_missing_load in truck', 'load in truck'),
           ('order depot_incorrect_order home', 'order home'), ('order home_incorrect_order depot', 'order depot'),
           ('tau2_load in truck_missing_V', 'load in truck'), ('tau2_deliver depot_incorrect_W', 'deliver depot'),
           ('tau2_load in truck_incorrect_V', 'load in truck'), ('tau2_ring_incorrect_D', 'ring'),
           ('deliver depot_parallel_collect', 'deliver depot'), ('collect_parallel_deliver depot', 'collect')]
for (dev_t, original_t) in copiers:
  durations.update({(dev_t, p): v for (t, p), v in durations.items() if t == original_t})
durations[('deliver depot_neglect_D', 'p_v_free')] = durations[('deliver depot', 'p_v_free')]
durations[('deliver depot_neglect_D', 'p9')] = durations[('deliver depot', 'p9')]
durations[('deliver home__pos_ring', 'p5__')] = durations[('deliver home', 'p_d_on')]
durations[('deliver depot_parallel_collect', 'p_v_free')] = durations[('deliver depot', 'p_v_free')]
durations[('deliver depot_parallel_collect', 'p_d_on')] = durations[('deliver depot', 'p_d_on')]
durations[('deliver depot_parallel_collect', 'p_d_on__')] = durations[('deliver depot', 'p_d_on')]
durations[('deliver depot_parallel_collect', 'p_v_free__')] = durations[('deliver depot', 'p_v_free')]
durations[('collect_parallel_deliver depot', 'p_w__')] = durations[('collect', 'p_w')]
durations[('collect_parallel_deliver depot', 'p_10__')] = durations[('collect', 'p10')]
durations[('tau_1_parallel_deliver depot_collect', 'p_9__')] = durations[('deliver depot', 'p9')]
durations[('ring_pos_deliver home', 'p_v_free')] = durations[('ring', 'p5')]
durations[('ring_pos_deliver home', 'p10')] = durations[('ring', 'p5')]
durations[('ring_pos_deliver home', 'p_d_on')] = durations[('ring', 'p5')]

durations = {(arc.source.name, arc.target.name): durations.get((arc.source.name, arc.target.name), (lambda: 0)) for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}

sim = GroundTruthSimulation(system, patterns, transition_weights, durations, schedules, arrivals)

# N.B. These are the only three non-standard operations necessary to make the deviation patterns work.

if 'swap correlationPD' in patterns_ and 'swap correlationPV' in patterns_:
  # There is correlation between 3 objects, divided over two places, so we combine the transitions to swap the correlation correctly.
  t = sim.system.net.merge_transitions([next(t for t in sim.system.net.transitions if t.name == tname) for tname in
                                       ['tau_swap_correlation_p6_PD', 'tau_swap_correlation_p_v_occ_PV']], 'tau_swap_correlation_PDV')
  sim.arc_durations.update({('tau_swap_correlation_PDV', arc.target.name): (lambda: 0) for arc in sim.system.net.element_out_arcs[t]})
if 'neglect deliverer deliver depot' in patterns_:
  # These arcs are necessary since the base net is not free-choice.
  p = next(p for p in sim.system.places if p.name == 'p12')
  t = next(t for t in sim.system.transitions if t.name == 'tau_destroy^p6↾{D}_{P}')
  sim.system.net.add_arc(MultiNuArc(p, t, Multiset.from_str('[([p])]', MultiVarExpr)))
  sim.system.net.add_arc(MultiNuArc(t, p, Multiset.from_str('[([p])]', MultiVarExpr)))
  sim.arc_durations[('tau_destroy^p6↾{D}_{P}', 'p12')] = lambda: 0
if 'switch role' in patterns_:
  # To keep the net a valid tPNID, the objects that switch roles should be aliased and added to the object types.
  sim.system.net.object_types['E'].add('d1(e)')

log = sim.simulate_log(start_time='2024-04-08T02:00:00', min_log_length=1, max_log_length=600, verbose=1)

log = log.projection(lambda f: f.transition.label is not None or True, is_already_closed=True)
[print(f'{str(sim.start_time + timedelta(seconds=firing.time)):<26}: {firing.short_str()}') for firing in log]
print()
[print(f'{str(sim.start_time + timedelta(seconds=firing.time)).split(".")[0]}: {firing.transition.label:<16} {firing.objects()}') for firing in log if firing.transition.label is not None]

move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label} @ {int(x.time / 3600)}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
# log.view(element_str=move_str)

exit()








add_pattern = False
if add_pattern:
  tx = {t.name: t for t in system.net.transitions}
  patterns = [('./tests/nets/skip_transition.json', {'x': tx['order home']}),
              ('./tests/nets/skip_transition.json', {'x': tx['ring']})]
  pattern_systems = []
  for filename, map in patterns:
    with open(filename, 'r') as json_file:
      data = json.load(json_file)
      data.update(pattern(data, system, map))
      pattern_systems.append(System.from_json_dict(data))
      # pattern_systems[-1].view(debug=True)

  full = deepcopy(system)
  for pattern_system in pattern_systems:
    full = full.union(pattern_system)

if True:
  schedule = lambda days, hours: (lambda dt: (min([xs for s in [relativedelta(weekday=day, hour=hour, minute=0, second=0, microsecond=0) for day in days for hour in hours] if (xs := dt+s) > dt]) - dt).total_seconds())

  packages = iter(system.net.object_types['P'])
  package_create_transition = next(t for t in system.transitions if t.name == 'create')
  deliverer_start_transition = next(t for t in system.transitions if t.name == 'start')
  deliverer_stop_transition = next(t for t in system.transitions if t.name == 'stop')

  package_create_firing = lambda: system.net.enabled_modes(system.marking, package_create_transition, only_vars={ColorToken(next(packages))})[0]
  deliverer_start_firing = lambda d: (lambda:  system.net.enabled_modes(system.marking, deliverer_start_transition, only_vars={ColorToken(d)})[0])
  deliverer_stop_firing = lambda d: (lambda:  system.net.enabled_modes(system.marking, deliverer_stop_transition, only_vars={ColorToken(d)})[0])


  arrival_rates = [(package_create_firing, lambda dt: random.expovariate(1/3600))]

  start_schedules = {'d1': ([MO, TU, WE, TH, FR], [9]), 'd2': ([MO, TU, WE, TH], [8])}
  stop_schedules = {'d1': ([MO, TU, WE, TH, FR], [17]), 'd2': ([MO, TU, WE, TH], [17])}
  start_schedules = [(deliverer_start_firing(k), schedule(*s)) for k, s in start_schedules.items()]
  stop_schedules = [(deliverer_stop_firing(k), schedule(*s)) for k, s in stop_schedules.items()]

  repeats = [*arrival_rates, *start_schedules, *stop_schedules]

  activity_durations = {n: lambda: np.random.normal(3600, 20) for n in
    ['order home', 'order depot', 'ring', 'deliver home', 'register depot', 'deliver depot', 'collect']}

  # activity_durations = {n: lambda: 3600 for n in ['a', 'b', 'c']}

  activity_durations = {(arc.source.name, arc.target.name): lambda: 1800
                        for transition in system.net.transitions for arc in system.net.element_out_arcs[transition]}

transition_weights = {t.name: 0 if t.name in ['start', 'stop', 'create'] else 1 for t in system.transitions}

# system.view(debug=True)
sim = PetrinetSimulator(system, {
  'repeated_firings': repeats, 'activity_durations': activity_durations, 'transition_weights': transition_weights})

print('Simulate the Petri net until final marking is reached:')
log = sim.simulate_log(start_time=datetime.fromisoformat('2024-04-08T07:00:00'), min_log_length=1, max_log_length=40, verbose=1)
[print(f'{str(sim.start_time + timedelta(seconds=firing.time))}, {firing.short_str()}') for firing in log]

move_str = lambda x: f'{x.transition.name if x.transition.label is None else x.transition.label} @ {int(x.time / 3600)}\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
log.view(element_str=move_str)

# print('\nSimulate the Petri net until max log length of 2 is reached:')
# log = sim.simulate_log(max_log_length=2, verbose=1)
# [print(f'{firing.time:.2f}, {firing.short_str()}') for firing in log]
# log.view()
