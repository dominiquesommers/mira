from mira.prelim.multiset import Multiset, TypedDict
from mira.nets.token import Token, ColorToken, MultiColorToken
from mira.nets.place import Place

from copy import deepcopy
from collections import defaultdict
import json


class Marking(TypedDict):
  value_type = int
  def __init__(self, key_type=None, value_type=None, args=None):
    super().__init__(Place, self.value_type, args=args)

  def to_dict(self):
    return {'class': type(self).__name__, 'args': {place.name: tokens.to_dict()
            for place, tokens in self.items()}}

  def vectorize(self, places):
    x = [0 for _ in range(len(places))]
    for p in self:
      x[places.index(p)] = self[p]
    return x

  @classmethod
  def from_str(cls, string, key_type=None, value_type=None):
    if string == '[]' or string == '':
      return Marking(args=None)
    return super().from_str(string, Place, cls.value_type if value_type is None else value_type)


class NuMarking(Marking):
  class ColorTokenMultiset(Multiset):
    def __init__(self, key_type=None, value_type=None, args=None):
      super().__init__(**{'key_type': ColorToken, 'args':args})
  value_type = ColorTokenMultiset

  def __init__(self, key_type=None, value_type=None, args=None):
    super().__init__(args=args)

  def blacken(self):
    return Marking(args={p: sum(tokens.values()) for p, tokens in self.items()})

  def get_distinct_tokens(self):
    tokensets = list(self.values())
    if len(tokensets) == 0:
      return self.value_type()
    distinct_tokens = deepcopy(tokensets[0])
    for tokenset in tokensets[1:]:
      distinct_tokens = distinct_tokens.max(tokenset)
    return distinct_tokens

  @classmethod
  def from_str(cls, string, key_type=None, value_type=None):
    if string == '[]' or string == '':
      return NuMarking(args=None)
    return super().from_str(string, Place, (cls.value_type, ColorToken, int) if value_type is None else value_type)

class MultiNuMarking(NuMarking):
  class MultiColorTokenMultiset(Multiset):
    def __init__(self, key_type=None, value_type=None, args=None):
      super().__init__(MultiColorToken, args=args)
  value_type = MultiColorTokenMultiset

  def __init__(self, key_type=None, value_type=None, args=None):
    super().__init__(args=args)

  @classmethod
  def from_str(cls, string, key_type=None, value_type=None):
    if string == '[]' or string == '':
      return MultiNuMarking(args=None)
    return super().from_str(string, Place, (cls.value_type, MultiColorToken, int))

  def projection(self, objects, object_types):
    roles = set(role for role, role_objects in object_types.items() if len(role_objects.intersection(objects)) > 0)
    roles_str = lambda rols: '{' + ','.join(rols) + '}'
    places = {}
    for place in self.keys():
      place_roles = set(place.alpha)
      place_roles_proj = place_roles.intersection(roles)
      if len(place_roles_proj) > 0:
        places[place] = Place(f'{place.name}' + ('' if place_roles_proj == place_roles else f'↾{roles_str(place_roles_proj)}'))

    tokens = {}
    for place, place_proj in places.items():
      tokens[place_proj] = defaultdict(int)
      for multicolortoken, n in self[place].items():
        multicolortoken_proj = multicolortoken.projection(objects, object_types)
        if len(multicolortoken_proj) == len(set(place.alpha).intersection(roles)):
          tokens[place_proj][multicolortoken_proj] += n
    return MultiNuMarking(args=tokens)

  def split_tokens(self):
    submarkings = set()
    for place, place_marking in self.items():
      for multicolortoken, n in place_marking.items():
        submarkings.add(MultiNuMarking(args={place: self.MultiColorTokenMultiset(args={multicolortoken: n})}))
    return submarkings

  def union(self, other):
    # TODO this is not union, this is addition! Should be max(a, b).
    m = deepcopy(self)
    for place, tokens in other.items():
      if place in m:
        for token, n in tokens.items():
          m[place][token] = max(m[place][token], n)
      else:
        m[place] = tokens
    return m


if __name__ == '__main__':
  m1 = MultiNuMarking({'p': {MultiColorToken(2, [{'a': 2}, {}]): 1}})
  m2 = MultiNuMarking({'p': {MultiColorToken(2, [{'a': 2, 'b': 1}, {}]): 2}})
  print('m1', m1)
  print('m2', m2)
  print(m1.intersection(m2))
  print(m1.intersection(m2).is_empty())

  m1 = NuMarking({'p': {ColorToken('a'): 2, ColorToken('b'): 1}})
  m2 = NuMarking({'p': {ColorToken('c'): 2}})
  print('m1', m1)
  print('m2', m2)
  print(m1.intersection(m2))

