from mira.nets.petri_net import MultiNuNet, System
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import MultiNuArc, VariableMultiNuArc
from mira.nets.marking import MultiNuMarking, MultiColorToken

import numpy as np
from copy import deepcopy

def get(entities=None, view=False):
  if entities is None:
    entities = (set(), set(), set())

  place_names = [f'o{index}' for index in range(1, 8)]
  place_names.extend([f'i{index}' for index in range(1, 16)])
  place_names.extend([f'p{index}' for index in range(1, 8)])
  activities = ['place order', 'item out of stock', 'confirm order', 'reorder item', 'payment reminder', 'pick item', 'pay order',
                'create package', 'send package', 'failed delivery', 'package delivered']
  activities.extend([f'tau_o_{index}' for index in range(1, 5)])
  activities.extend([f'tau_i_{index}' for index in range(1, 8)])
  activities.extend([f'tau_p_{index}' for index in range(1, 5)])

  p = {name: Place(name) for name in place_names}
  t = {name: Transition(name, None if name[:3] == 'tau' else name) for name in activities}
  n = {**p, **t}
  net = MultiNuNet(list(p.values()), list(t.values()))

  simple_arcs = {
    ('{o: 1}', '_', '_'): [('o1', 'place order'), ('place order', 'o2'), ('o2', 'confirm order'), ('confirm order', 'o3'),
                           ('o3', 'tau_o_1'), ('tau_o_1', 'o4'), ('o4', 'pay order'), ('pay order', 'o5'), ('o3', 'tau_o_2'),
                           ('tau_o_2', 'o6'), ('o6', 'payment reminder'), ('payment reminder', 'o7'), ('o7', 'tau_o_3'), ('tau_o_3', 'o6'),
                           ('o7', 'tau_o_4'), ('tau_o_4', 'o4')],
    ('_', '{i: 1}', '_'): [('i2', 'tau_i_1'), ('tau_i_1', 'i3'), ('i4', 'tau_i_2'), ('tau_i_2', 'i5'), ('tau_i_1', 'i6'),
                           ('i6', 'tau_i_3'), ('tau_i_3', 'i8'), ('i6', 'item out of stock'), ('item out of stock', 'i7'),
                           ('i7', 'reorder item'), ('reorder item', 'i8'), ('i8', 'pick item'), ('pick item', 'i9'),
                           ('i11', 'tau_i_4'), ('tau_i_4', 'i12'), ('i13', 'tau_i_2'), ('i11', 'tau_i_5'), ('tau_i_5', 'i14'),
                           ('i15', 'tau_i_6'), ('tau_i_6', 'i14'), ('i15', 'tau_i_7'), ('tau_i_7', 'i12')],
    ('_', '_', '{p: 1}'): [('p1', 'create package'), ('create package', 'p2'), ('p2', 'send package'), ('send package', 'p3'),
                           ('p3', 'tau_p_1'), ('tau_p_1', 'p4'), ('p4', 'package delivered'), ('package delivered', 'p5'),
                           ('p3', 'tau_p_2'), ('tau_p_2', 'p6'), ('p6', 'failed delivery'), ('failed delivery', 'p7'),
                           ('p7', 'tau_p_3'), ('tau_p_3', 'p6'), ('p7', 'tau_p_4'), ('tau_p_4', 'p4')]
  }
  [net.add_arc(MultiNuArc(n[source], n[target], labels)) for labels, arcs in simple_arcs.items() for source, target in arcs]
  variable_arcs = {
    ('_', 'I', '_'): [('i1', 'place order'), ('place order', 'i2'), ('i3', 'confirm order'), ('confirm order', 'i4'),
                      ('i9', 'create package'), ('create package', 'i10'), ('i10', 'send package'), ('send package', 'i11'),
                      ('i12', 'package delivered'), ('package delivered', 'i13'), ('i14', 'failed delivery'), ('failed delivery', 'i15')]
  }
  [net.add_variable_arc(VariableMultiNuArc(n[source], n[target], labels, [1, np.inf])) for labels, arcs in variable_arcs.items() for source, target in arcs]

  m_i = MultiNuMarking({'o1': {MultiColorToken(3, [{entity: 1},{},{}]): 1 for entity in entities[0]},
                        'i1': {MultiColorToken(3, [{},{entity: 1},{}]): 1 for entity in entities[1]},
                        'p1': {MultiColorToken(3, [{},{},{entity: 1}]): 1 for entity in entities[2]}})
  m_f = MultiNuMarking({'o5': deepcopy(m_i['o1']), 'i5': deepcopy(m_i['i1']), 'p5': deepcopy(m_i['p1'])})

  sn = System(net, m_i, m_f)
  if view:
    sn.view()
  return sn
