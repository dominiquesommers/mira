from mira.nets.petri_net import Net
from mira.nets.nu_net import NuNet
from mira.prelim.multiset import Multiset
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.arc import Arc, InhibitorArc, MultiNuArc, InhibitorMultiNuArc, VariableMultiNuArc, MultiVarExpr, Var, SetVar
from mira.nets.firing import MultiNuFiring
from mira.nets.marking import MultiNuMarking
from mira.nets.token import ColorToken, MultiColorToken
from mira.nets.utils import powerset

from typing import Optional, Iterable
from copy import copy, deepcopy
from collections import defaultdict
import re


class MultiNuNet(NuNet):
  firing_type = MultiNuFiring
  marking_type = MultiNuMarking
  def __init__(self, places: Optional[Iterable[Place]] = None, transitions: Optional[Iterable[Transition]] = None,
               arcs: Optional[Iterable[Arc]] = None, inhibitor_arcs: Optional[Iterable[InhibitorMultiNuArc]] = None, variable_arcs: Optional[Iterable[VariableMultiNuArc]] = None,
               object_types=None, variable_types=None):
    super().__init__(places, transitions, arcs, inhibitor_arcs)
    self.variable_arcs = set()
    self.__element_in_variable_arcs =  defaultdict(list)
    self.__element_out_variable_arcs = defaultdict(list)
    if variable_arcs is not None:
      [self.add_variable_arc(arc) for arc in variable_arcs]
    self.object_types = {r: set(rO) for r, rO in (object_types if object_types is not None else {}).items()}
    self.variable_types = {}
    if variable_types is not None:
      for r, rvars in variable_types.items():
        self.variable_types[r] = set()
        for v in rvars:
          if isinstance(v, str):
            self.variable_types[r].add(Var(v) if v.upper() != v else SetVar(v))
          elif isinstance(v, Var) or isinstance(v, SetVar):
            self.variable_types[r].add(v)
          else:
            raise TypeError(f"Unexpected type {type(v)}")

  @classmethod
  def from_json_dict(cls, data, silent=lambda name: name[:3] == 'tau', relaxed=False, verbose=False):
    name_label = lambda namelabel: (name[0], label[0]) if (len(name := re.findall('(.*?)\(', namelabel)) == 1 and len(
      label := re.findall('\((.*?)\)', namelabel)) == 1) else (namelabel, namelabel)
    places = {p: Place(p) for p in data['P']}
    transitions = {}
    for t in data['T']:
      name, label = name_label(t)
      if verbose:
        print(f'{t=} {name=}, {label=}')
      transitions[t] = Transition(name, None if silent(label) else label)

    elements = {**places, **transitions}

    object_types = {}
    for role, role_objects in data.get('O', {}).items():
      # Pattern should be of sort: ['xi_start', '..', 'xi_end'] to generate [f'{x}{i} for i in range(i_start, i_end+1)]
      if '..' in role_objects and len(role_objects) == 3 and all(
          v[-1].isnumeric for v in [role_objects[0], role_objects[2]]):
        # TODO parse e.g., [p1,..,p10,q1,..,q10]
        name_i_split = next((i for i, x in enumerate(reversed(role_objects[0])) if not x.isnumeric()), 0)
        prefix_zeros = int(role_objects[0][-name_i_split]) == 0
        i_start = int(role_objects[0][-name_i_split:])
        name = role_objects[0][:-name_i_split]
        name_i_split2 = next((i for i, x in enumerate(reversed(role_objects[2])) if not x.isnumeric()), 0)
        i_end = int(role_objects[2][-name_i_split2:])

        if name != role_objects[2][:-name_i_split2]:
          raise ValueError('')

        max_length = max([len(role_objects[0][-name_i_split:]), len(role_objects[2][-name_i_split2:])])
        object_types[role] = [f'{name}{i:0{max_length if prefix_zeros else 0}d}' for i in range(i_start, i_end + 1)]
      else:
        object_types[role] = role_objects

    net = MultiNuNet(list(places.values()), list(transitions.values()), object_types=object_types, variable_types=data.get('V', {}))

    for labels, arcs in data['F'].items():
      for source, target in arcs:
        net.add_arc(MultiNuArc(elements[source], elements[target], Multiset.from_str(labels, MultiVarExpr)))

    for labels, arcs in data.get('F_inh', {}).items():
      for source, target in arcs:
        net.add_inhibitor_arc(InhibitorMultiNuArc(elements[source], elements[target], Multiset.from_str(labels, MultiVarExpr)))

    # net = MultiNuNet(list(places.values()), list(transitions.values()), object_types=object_types, variable_types=data.get('V', {}))
    #
    # for labels, arcs in data['F'].items():
    #   for source, target in arcs:
    #     if isinstance(labels, Multiset):
    #       labels = str(labels)
    #     labels = Multiset.from_str(labels, MultiVarExpr)
    #     net.add_arc(MultiNuArc(elements[source], elements[target], labels))
    # # [net.add_arc(MultiNuArc(elements[source], elements[target], Multiset.from_str(labels, MultiVarExpr)))
    # #   for labels, arcs in data['F'].items() for source, target in arcs]
    #
    # [net.add_inhibitor_arc(
    #   InhibitorMultiNuArc(elements[source], elements[target], Multiset.from_str(labels, MultiVarExpr)))
    #  for labels, arcs in data.get('F_inh', {}).items() for source, target in arcs]
    #
    # if relaxed:
    #   for transition in net.transitions:
    #     try:
    #       reference = transitions[transition.name[:transition.name.index('↾')]]
    #       transition.vars_original = reference.vars
    #     except ValueError:
    #       transition.vars_original = transition.vars

    return net

  def union(self, other):
    places = {place: deepcopy(place) for place in set(self.places).union(other.places)}
    transitions = {transition: deepcopy(transition) for transition in set(self.transitions).union(other.transitions)}
    elements = {**places, **transitions}
    merger = lambda a, b: {k: a.get(k, set()).union(b.get(k, set())) for k in set(a.keys()).union(b.keys())}
    net = MultiNuNet(list(places.values()), list(transitions.values()), object_types=merger(self.object_types, other.object_types),
                     variable_types=merger(self.variable_types, other.variable_types))

    for arc in set(self.arcs).union(other.arcs):
      net.add_arc(MultiNuArc(elements[arc.source], elements[arc.target], arc.labels))
    for arc in set(self.inhibitor_arcs).union(other.inhibitor_arcs):
      net.add_inhibitor_arc(InhibitorMultiNuArc(elements[arc.source], elements[arc.target], arc.labels))
    return net


  def get_roles(self, element):
    element_arcs = (self.element_in_arcs[element] + self.element_out_arcs[element])
    variables = set.union(*[varexpr.vars for arc in element_arcs for labels in arc.labels.keys() for varexpr in labels])
    return {role for role, role_variables in self.variable_types.items() if len(variables.intersection(role_variables)) > 0}

  @property
  def element_in_variable_arcs(self):
    return self.__element_in_variable_arcs

  @property
  def element_out_variable_arcs(self):
    return self.__element_out_variable_arcs

  def __add__(self, other):
    if type(self) != type(other):
      raise TypeError(f"unsupported operand type(s) for +: '{type(self)}' and '{type(other)}'")
    net = type(self)(copy(self.places), copy(self.transitions), copy(self.arcs), copy(self.inhibitor_arcs), copy(self.variable_arcs))
    net.places.extend(other.places)
    net.transitions.extend(other.transitions)
    [net.add_arc(arc) for arc in other.arcs]
    [net.add_arc(arc) for arc in other.inhibitor_arcs]
    [net.add_variable_arc(arc, substitute=False) for arc in other.variable_arcs]
    return net

  def add_inhibitor_arc(self, arc:InhibitorMultiNuArc):
    if not (isinstance(arc.source, Place) and self._check_arc(arc)):
      return
    self.transition_inhibitor_arcs[arc.target].append(arc)
    self.inhibitor_arcs.add(arc)

  def add_variable_arc(self, arc:VariableMultiNuArc, substitute=True):
    # TODO check whether arc.labels has one specified label, rest should be '_' (variable correlation arcs are not supported)
    self._check_arc(arc)
    self.variable_arcs.add(arc)
    self.__element_in_variable_arcs[arc.target].append(arc)
    self.__element_out_variable_arcs[arc.source].append(arc)
    if substitute:
      if isinstance(arc.source, Place):
        self.__add_variable_in_arc(arc)
      else:
        self.__add_variable_out_arc(arc)

  def __add_variable_in_arc(self, arc: VariableMultiNuArc):
    pname, tname = arc.place.name, arc.transition.name
    collect = self.add_transition(Transition(f'collect_({pname},{tname})', None, variable_sub=True))
    startpack = self.add_transition(Transition(f'startpack_({pname},{tname})', None, variable_sub=True))
    pack = self.add_transition(Transition(f'pack_({pname},{tname})', None, variable_sub=True))
    place_c = self.add_place(Place(f'c_({pname},{tname})', variable_sub=True))
    place_p = self.add_place(Place(f'p_({pname},{tname})', variable_sub=True))
    # TODO fix labels.
    arc_label = arc.labels
    arc_label_empty = ('_',)*len(arc.labels)
    arc_label_ = tuple(f'{l}_' if str(l) != '_' else '_' for l in arc_label)
    arc_label_union_ = tuple(f'{l} + {l}_' if str(l) != '_' else '_' for l in arc_label)

    self.add_arc(MultiNuArc(arc.place, collect, arc_label))
    self.add_arc(MultiNuArc(collect, place_c, arc_label))
    if arc.cardinality[0] == 0:
      self.add_arc(MultiNuArc(startpack, place_p, arc_label_empty))
    elif arc.cardinality[0] == 1:
      self.add_arc(MultiNuArc(place_c, startpack, arc_label))
      self.add_arc(MultiNuArc(startpack, place_p, arc_label))
    else:
      raise NotImplementedError(f"Variable arc cardinality 'lb={arc.cardinality[0]}' not implemented.")
    self.add_arc(MultiNuArc(place_c, pack, arc_label))
    self.add_arc(MultiNuArc(place_p, pack, arc_label_))
    self.add_arc(MultiNuArc(pack, place_p, arc_label_union_))
    self.add_arc(MultiNuArc(place_p, arc.transition, arc_label))
    self.add_inhibitor_arc(InhibitorArc(place_p, collect))
    self.add_inhibitor_arc(InhibitorArc(place_p, startpack))
    self.add_inhibitor_arc(InhibitorArc(place_c, arc.transition))

  def __add_variable_out_arc(self, arc: VariableMultiNuArc):
    # TODO ascii art showing the substitution.
    pname, tname = arc.place.name, arc.transition.name
    unpack = self.add_transition(Transition(f'unpack_({tname},{pname})', None, variable_sub=True))
    void = self.add_transition(Transition(f'void_({tname},{pname})', None, variable_sub=True))
    place = self.add_place(Place(f'p_({tname},{pname})', variable_sub=True))
    # TODO fix labels.
    arc_label = arc.labels
    arc_label_empty = ('_',) * len(arc.labels)
    arc_label_single = tuple(f'{{{l.to_var()}: 1}}' if str(l) != '_' else '_' for l in arc_label)
    arc_label_union_single = tuple(f'{l} + {{{l.to_var()}: 1}}' if str(l) != '_' else '_' for l in arc_label)

    self.add_arc(MultiNuArc(arc.transition, place, arc_label))
    self.add_arc(MultiNuArc(place, void, arc_label_empty))
    self.add_arc(MultiNuArc(place, unpack, arc_label_union_single))
    self.add_arc(MultiNuArc(unpack, place, arc_label))
    self.add_arc(MultiNuArc(unpack, arc.place, arc_label_single))

  def set_alphas(self, variable_types):
    for place in self.places:
      place.alpha = self.alpha(place, variable_types)

  def alpha(self, place: Place, variable_types: dict):
    types = None
    arc_type = None
    for arc in self.arcs:
      if arc.place == place:
        for labels in arc.labels:
          label_types = []
          for varexpr in labels:
            tt = [t for t, vs in variable_types.items() if list(varexpr.vars.union(varexpr.setvars))[0] in vs]
            if len(tt) == 1:
              label_types.append(tt[0])
          label_types = tuple(label_types)
          if types is None:
            types = label_types
            arc_type = arc
          if types != label_types:
            raise ValueError(f't-PNID should have beta defined such that it matches alpha, beta({arc}) = {label_types} != beta({arc_type}) = {types}.')
    return types

  def _check_mode(self, mode: dict):
    non_empty_mode_values = [v for k, v in mode.items() if k != '_']
    if len(set(non_empty_mode_values)) != len(non_empty_mode_values):
      raise ValueError('Mode should be a injective mapping.')

  def create_marking_from_mode(self, arcs: dict, mode: dict):
    self._check_mode(mode)
    mode['_'] = Multiset(ColorToken, {})
    marking = MultiNuMarking()
    for arc, weight in arcs.items():
      for labels, n in arc.labels.items():
        for expr in labels:
          for var in expr.vars.union(expr.setvars):
            if str(var) not in mode.keys():
              raise ValueError(f'Not all required labels are in mode, {str(var)} is missing in arc:\n{arc=}.')

        marking[arc.place][MultiColorToken(len(labels), [var_expr.evaluate(mode) for var_expr in labels])] += n*weight

    return marking

  def get_transition_mode_candidates(self, marking, transition, only_vars=None, verbose=False):
    k = defaultdict(set)
    for arc in self.element_in_arcs[transition]:
      for labels in arc.labels.keys():
        if verbose:
          print(f'{labels=}')
        for index, expr in enumerate(labels):
          for setvar in expr.setvars:
            k[str(setvar)] = set()
          for var in expr.vars:
            k[str(var)] = set()
          for token, x in marking[arc.place].items():
            if x <= 0:
              continue
            for setvar in expr.setvars:
              k[str(setvar)].update(set(token[index].powerset()))
            for var in expr.vars:
              tokens = set(token[index].keys())
              k[str(var)].update(tokens.intersection(only_vars) if only_vars is not None else tokens)

    nu_vars, nu_setvars = set(), set()
    for arc in self.element_out_arcs[transition]:
      for labels in arc.labels.keys():
        for index, expr in enumerate(labels):
          for var in expr.vars:
            if str(var) not in k.keys():
              nu_vars.add(var)
          for setvar in expr.setvars:
            if str(setvar) not in k.keys():
              nu_setvars.add(setvar)

    if len(nu_vars) == 0 and len(nu_setvars) == 0:
      return k

    ctokens = set([ctoken for tokens in marking.values() for multicolortoken, n in tokens.items() if n > 0 for mset in multicolortoken for ctoken in mset.keys()])
    for nu_var in nu_vars:
      var_type = next((role for role, role_vars in self.variable_types.items() if nu_var in role_vars), None)
      tokens = set(ColorToken(o) for o in self.object_types[var_type]) - ctokens
      k[str(nu_var)].update(tokens.intersection(only_vars) if only_vars is not None else tokens)
    for nu_setvar in nu_setvars:
      var_type = next((role for role, role_vars in self.variable_types.items() if nu_setvar in role_vars), None)
      # TODO only_vars
      k[str(nu_setvar)].update(set(powerset(set(ColorToken(o) for o in self.object_types[var_type]))) - ctokens)
    return k


  def fire_transition(self, marking: MultiNuMarking, transition: Transition, sub_marking: MultiNuMarking, add_marking: MultiNuMarking, mode: dict=None):
    new_marking = marking - sub_marking + add_marking
    firing = MultiNuFiring(transition, mode, sub_marking, add_marking)
    return new_marking, firing

  def blacken(self):
    arcs = [Arc(arc.source, arc.target, arc.weight) for arc in self.arcs]
    return Net(places=self.places, transitions=self.transitions, arcs=arcs)

  def _view_transitions(self, nodes, viz, font_size, debug, substitute=True):
    nodes.update({transition: transition.view(viz, font_size, debug) for transition in self.transitions if not substitute or not transition.parameters.get('variable_sub', False)})

  def _view_places(self, nodes, viz, m_i, m_f, font_size, debug, substitute=True):
    nodes.update({place: place.view(viz, m_i[place], m_f[place], font_size, debug) for place in self.places if not substitute or not place.parameters.get('variable_sub', False)})

  def _view_arcs(self, nodes, viz, font_size, debug, substitute=True):
    if substitute:
      [arc.view(nodes[arc.source], nodes[arc.target], viz, font_size, debug) for arc in self.variable_arcs]
    [arc.view(nodes[arc.source], nodes[arc.target], viz, font_size, debug) for arc in self.arcs if arc.source in nodes and arc.target in nodes]
    [arc.view(nodes[arc.source], nodes[arc.target], viz, font_size, debug) for arc in self.inhibitor_arcs if arc.source in nodes and arc.target in nodes]

if __name__ == '__main__':
  # from mira.nets.tPNID import tPNID
  from mira.nets.arc import Var, SetVar
  # print(MultiVarExpr.from_str('([j])'))
  # x = MultiVarExpr.from_str('(J+{j: 1})')[0]
  # print('hoi', x.vars, x.setvars)
  # print(x)

  # print(a)

  places = {name: Place(name) for name in ['p1', 'p2']}
  transitions = {name: Transition(name, name) for name in ['create', 'tau1', 'empty']}
  net = MultiNuNet(list(places.values()), list(transitions.values())) #, object_types={'P': {'p'}}, variable_types={'P': {Var('x'), SetVar('X')}})
  net.object_types = {'P': {'a'}}
  net.variable_types = {'P': {Var('x'), SetVar('X')}}
  # net.variable_types = {'P': {'x', 'X'}}
  net.add_arc(MultiNuArc(transitions['create'], places['p1'], Multiset.from_str('[([x])]', MultiVarExpr)))
  net.add_arc(MultiNuArc(places['p1'], transitions['tau1'], Multiset.from_str('[([x])]', MultiVarExpr)))
  net.add_arc(MultiNuArc(places['p2'], transitions['tau1'], Multiset.from_str('[(X)]', MultiVarExpr)))
  net.add_arc(MultiNuArc(transitions['tau1'], places['p2'], Multiset.from_str('[(X+{x: 1})]', MultiVarExpr)))

  net.add_arc(MultiNuArc(places['p2'], transitions['empty'], Multiset.from_str('[(X+{x: 1})]', MultiVarExpr)))
  net.add_arc(MultiNuArc(transitions['empty'], places['p2'], Multiset.from_str('[(X)]', MultiVarExpr)))

  # print(Multiset.from_str('[([i], [j])]', MultiVarExpr))
  # print(Multiset.from_str('[(J)]', MultiVarExpr))

  # places = {name: Place(name) for name in ['pi', 'p1', 'p2', 'pf']}
  # transitions = {name: Transition(name, name) for name in ['a', 'b']}
  # net = MultiNuNet(list(places.values()), list(transitions.values()))
  # net.add_arc(MultiNuArc(places['pi'], transitions['a'], ('A', 'B')))
  # net.add_arc(MultiNuArc(transitions['a'], places['p1'], ('A', '_')))
  # net.add_arc(MultiNuArc(transitions['a'], places['p2'], ('_', 'B')))
  # net.add_arc(MultiNuArc(places['p1'], transitions['b'], ('A', '_')))
  # net.add_arc(MultiNuArc(places['p2'], transitions['b'], ('_', 'B')))
  # net.add_arc(MultiNuArc(transitions['b'], places['pf'], ('A', 'B')))
  #
  m_i = MultiNuMarking.from_str('[[([])]·p2]')
  m_f = MultiNuMarking.from_str('[[([])]·p2]')
  from mira.nets.system import System
  sn = System(net, m_i, m_f)


  firing = sn.enabled_firings()[0][0]
  sn.fire(firing, firing.new_marking(sn.marking))
  firing = sn.enabled_firings()[0][0]
  sn.fire(firing, firing.new_marking(sn.marking))
  firing = sn.enabled_firings()[0][0]
  sn.fire(firing, firing.new_marking(sn.marking))
  sn.view(debug=True)