from mira.prelim.multiset import Multiset, TypedDict
from mira.prelim.colors import Color

from typing import Optional, Union


class Place:
  def __init__(self, name: Union[float, str], **parameters):
    self.name = name
    self.parameters = parameters
    # TODO could check for the correct type here.

  def __repr__(self):
    return f'{self.name}'

  def __str__(self):
    return self.__repr__()

  def __lt__(self, other):
    if isinstance(self.name, str) or isinstance(other.name, str):
      return str(self.name) < str(other.name)
    return self.name < other.name

  def __hash__(self):
    return hash(self.name)

  def __eq__(self, other):
    return type(self) == type(other) and self.name == other.name

  def view(self, viz, tokens, final_tokens, font_size='12', debug=False):
    place_id = str(id(self))
    place_is_final = (isinstance(final_tokens, int) and final_tokens != 0) or (isinstance(final_tokens, TypedDict) and not final_tokens.is_empty())
    place_has_tokens = place_is_final or (isinstance(tokens, int) and tokens != 0) or (isinstance(tokens, TypedDict) and not tokens.is_empty())
    args = {'name': place_id, 'penwidth': '5' if place_is_final else '2', 'fontsize': str(font_size), 'fixedsize': 'true',
            'shape': 'circle', 'color': Color.BLACK}

    empty = (tokens == 0) if isinstance(tokens, int) else tokens.is_empty()
    label = f'{self.name}' + '\n'*int(not empty)  if debug else ''
    label = label if empty else f'{label}{tokens}'.replace('),(', '),\n(')
    viz.node(label=label, width='0.75' if place_has_tokens else '0.6', **args)
    return place_id
