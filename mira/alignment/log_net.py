from mira.prelim.multiset import Multiset
from mira.nets.system import System, Net, NuNet, MultiNuNet, tPNID
from mira.nets.marking import Marking, NuMarking, MultiNuMarking
from mira.nets.token import ColorToken, MultiColorToken
from mira.nets.transition import Transition
from mira.nets.arc import Arc, NuArc, MultiNuArc, Var
from mira.nets.place import Place
from mira.logs.event_log import Log

from typing import Type, Union
from copy import copy
import string
from collections import defaultdict


class LogSystem(System):
  def __init__(self, log:Log):
    self.log = log
    self.log_to_net()
    super().__init__(self.net, self.m_i, self.m_f)

  def log_to_net(self):
    transitions = [Transition(f'{event.activity}_{index}', event.activity, move_type='log') for index, event in enumerate(self.log)]
    self._initialize(transitions)
    self._add_arcs(transitions)

  def _initialize(self, transitions):
    self.net, self.m_i, self.m_f = Net([], transitions), Marking(), Marking()

  def _add_arcs(self, transitions):
    colored_transitions = []
    for (i, j) in self.log.get_R_indices(self.log.get_minimal_partial_order()):
      place = self.net.add_place(Place(f'{transitions[i]}-{transitions[j]}'))
      self._add_arc(transitions[i], place)
      self._add_arc(place, transitions[j], self.log[j] if j not in colored_transitions else None)
      colored_transitions.append(j)
    for i in self.log.min_indices():
      place = self.net.add_place(Place(f'-{transitions[i]}'))
      self._add_arc(place, transitions[i], self.log[i])
      self._add_blank_token(self.m_i, place)
    for i in self.log.max_indices():
      place = self.net.add_place(Place(f'{transitions[i]}-'))
      self._add_arc(transitions[i], place)
      self._add_blank_token(self.m_f, place)

  def _add_arc(self, source, target, target_event=None):
    self.net.add_arc(Arc(source, target))

  def _add_blank_token(self, marking, place):
    marking[place] += 1


class NuLogSystem(LogSystem):
  def __init__(self, log: Log):
    super().__init__(log)

  def _initialize(self, transitions):
    self.net, self.m_i, self.m_f = NuNet([], transitions), NuMarking(), NuMarking()

  def _add_arc(self, source, target, target_event=None):
    self.net.add_arc(NuArc(source, target, '_'))
    if isinstance(source, Place) and target_event is not None:
      for index, (identifier, count) in enumerate(sorted(target_event.identifiers.items())):
        self.m_i[source][ColorToken(identifier)] += count
        self.net.add_arc(NuArc(source, target, string.ascii_lowercase[index])) # TODO Change to single arc with multiset of labels.

  def _add_blank_token(self, marking, place):
    marking[place][ColorToken('_')] += 1


class MultiNuLogSystem(LogSystem):
  def __init__(self, log: Log):
    super().__init__(log)

  def _initialize(self, transitions):
    self.net, self.m_i, self.m_f = MultiNuNet([], transitions), MultiNuMarking(), MultiNuMarking()

  def _add_arc(self, source, target, target_event=None):
    blank_label = ['_']*len(self.log.identifiers)
    self.net.add_arc(MultiNuArc(source, target, tuple(blank_label)))
    if isinstance(source, Place) and target_event is not None:
      token = self.__get_blank_token()
      label = copy(blank_label)
      for identifier, count in target_event.identifiers.items():
        object_index = next(index for index, ids in enumerate(self.log.identifiers) if identifier in ids)
        label[object_index] = string.ascii_uppercase[object_index]
        token[object_index][identifier] += count
      self.m_i[source][token] += 1
      self.net.add_arc(MultiNuArc(source, target, tuple(label)))

  def _add_blank_token(self, marking, place):
    marking[place][self.__get_blank_token()] += 1

  def __get_blank_token(self):
    return MultiColorToken(len(self.log.identifiers), [{}]*len(self.log.identifiers))


class tPNIDLogSystem(LogSystem):
  def __init__(self, log: Log, object_types, variable_types):
    self.object_types = object_types
    self.variable_types = variable_types
    super().__init__(log)

  def _initialize(self, transitions):
    self.net, self.m_i, self.m_f = (tPNID([], transitions, object_types=self.object_types, variable_types=self.variable_types),
                                    MultiNuMarking(), MultiNuMarking())

  def _add_arcs(self, transitions):
    roles = {o: r for r, O in self.object_types.items() for o in O}
    event_transitions = {int(t.name.split('_')[-1]): t for t in self.net.transitions}
    silent_transitions = {}
    event_roles = defaultdict(set)
    event_role_labels = defaultdict(lambda: defaultdict(dict))
    for index, event in enumerate(self.log):
      silent_transitions[index] = self.net.add_transition(Transition(f'tau_{event.activity}_{index}', None, move_type='log'))
      marked_places = {}
      for identifier, n in event.identifiers.items():
        if (role := roles[identifier]) not in marked_places:
          event_roles[index].add(role)
          marked_places[role] = self.net.add_place(Place(f'p_{role}^({index})'))
          id_ = self.parse_id_to_var(identifier)
          event_role_labels[index][role][(f'{{{id_}: {n}}}',)] = 1
        self.m_i[marked_places[role]][MultiColorToken(1, [{identifier: 1}])] += n
      for role in marked_places.keys():
        self.net.add_arc(MultiNuArc(marked_places[role], silent_transitions[index], Multiset(args=event_role_labels[index][role]))) # TODO beta
        pre_place = self.net.add_place(Place(f'p_{role}^(.{index})'))
        self.net.add_arc(MultiNuArc(pre_place, event_transitions[index], Multiset(args=event_role_labels[index][role])))  # TODO beta
        self.net.add_arc(MultiNuArc(silent_transitions[index], pre_place, Multiset(args=event_role_labels[index][role])))  # TODO beta
    for (i, j) in self.log.get_R_indices(self.log.get_minimal_partial_order()):
      for role in event_roles[i]:
        place = self.net.add_place(Place(f'p_{role}^({i},{j})'))
        self.net.add_arc(MultiNuArc(event_transitions[i], place, Multiset(args=event_role_labels[i][role]))) # TODO beta
        self.net.add_arc(MultiNuArc(place, silent_transitions[j], Multiset(args=event_role_labels[i][role])))  # TODO beta

  @staticmethod
  def parse_id_to_var(o_id):
    id_ = f'c{o_id}' if ''.join(o_id.split('_')).isnumeric() else o_id
    return '_'.join('_'.join(id_.split('(')).split(')'))


def create_log_system(log:Log, net_type:Type[Union[Net, NuNet, MultiNuNet, tPNID]]):
  log_system_types = {Net: LogSystem, NuNet: NuLogSystem, MultiNuNet: MultiNuLogSystem, tPNID: tPNIDLogSystem}
  variable_types = {role: set([Var(tPNIDLogSystem.parse_id_to_var(o)) for o in role_objects]) for role, role_objects in log.object_types.items()}
  return log_system_types[net_type](log, log.object_types, variable_types)


if __name__ == '__main__':
  from mira.logs.event import Event
  from mira.nets.utils import powerset
  from mira.nets.arc import Var
  import numpy as np
  from copy import deepcopy

  object_types = {'P': {'p1'}, 'D': {'d1', 'd2'}, 'W': {'w1', 'w2'}}
  variable_types = {r: set([Var(id) for id in ids]) for r, ids in object_types.items()}
  events = [
    Event('a', Multiset(args={'p1': 1, 'd1': 1})),
    Event('b', Multiset(args={'p1': 1}))
  ]
  log = Log(events, np.matrix(1 - np.tri(len(events), len(events), dtype=int)), True, ({'p1'}, {'d1'}))
  log_system = tPNIDLogSystem(log, object_types, variable_types)
  # log_system.view()

  objects = set.union(*object_types.values())
  net_add = deepcopy(log_system.net)
  for s in powerset(objects):
    net_p = log_system.net.projection(set(s), object_types, variable_types)
    net_add = net_add.union(net_p)
  net_add.set_alphas(variable_types)

  net_add.add_correlation_cd_net()

  rel_mnet = System(net_add, log_system.m_i, None)
  rel_mnet.view(debug=False)

  # from mira.alignment.search import Search
  #
  # s = Search(log_system, costs=lambda t: 10000)
  # run, info = s.compute_cheapest_run()
  # print(info)
  # run.view()
