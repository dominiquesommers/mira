from mira.prelim.multiset import Multiset

from typing import Union


class Token:
  def __init__(self):
    ...

class ColorToken:
  def __init__(self, id: Union[str, float]):
    self.id = id

  def is_empty(self):
    return self.id == '_'

  def to_dict(self):
    return self.__repr__()

  def __repr__(self):
    return f'{self.id}'

  def __str__(self):
    return self.__repr__()

  def __lt__(self, other):
    if isinstance(self.id, str) or isinstance(other.id, str):
      return str(self.id) < str(other.id)
    return self.id < other.id

  def __hash__(self):
    return hash(self.id)

  def __eq__(self, other):
    return type(self) == type(other) and self.id == other.id


class MultiColorToken(tuple):
  def __new__(self, size=None, multisets=None):
    if multisets is None:
      if size is None:
        raise ValueError(f'Multisets and size arguments can not both be None.')
      multisets = [Multiset(key_type=ColorToken) for _ in range(size)]
    elif size is None:
      size = len(multisets)
    elif len(multisets) != size:
      raise ValueError(f'Multisets incorrect size ({len(multisets)}), should be {size}.')

    multisets = [Multiset(key_type=ColorToken, args={} if m is None else {k: v for k, v in m.items()}) for m in multisets]
    if any(not isinstance(m, Multiset) for m in multisets):
      raise ValueError(f'Multisets should be of type Multiset or None.')
    return super().__new__(self, tuple(multisets))

  @classmethod
  def from_str(cls, string):
    if string[0] != '(' or string[-1] != ')':
      raise ValueError(f'Tuple string should by of form: (<els>), is {string}.')
    multisets = []
    for element in string[1:-1].split(','):
      multisets.append(Multiset.from_str(element, ColorToken))
    return MultiColorToken(size=len(multisets), multisets=tuple(multisets))

  def to_dict(self):
    return tuple(x.to_dict() for x in self)

  def __le__(self, other):
    if len(self) != len(other):
      return False
    return all(v1 <= v2 for v1, v2 in zip(self, other))

  def __fix_type(self, value):
    return

  def __repr__(self):
    return f'({", ".join([str(x) for x in self])})'

  def __str__(self):
    return self.__repr__()

  def projection(self, objects, object_types):
    mss = []
    for ms in self:
      argies = {k: v for k,v in ms.items() if k.id in objects}
      if len(argies) > 0:
        mss.append(Multiset(key_type=ColorToken, args=argies))
    return MultiColorToken(len(mss), mss)


if __name__ == '__main__':
  t = MultiColorToken(2)
  print(t)