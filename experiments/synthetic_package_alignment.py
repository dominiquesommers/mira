from mira.nets.system import System
from mira.nets.marking import MultiNuMarking
from mira.nets.firing import MultiNuFiring
from mira.nets.transition import Transition
from mira.logs.event_log import Log
from mira.logs.event import Event
from mira.alignment.align import align
from mira.alignment.cost import Cost
from mira.prelim.colors import Color
from mira.prelim.poset import POSet
from mira.prelim.multiset import Multiset
from mira.alignments.exporter import NoIndent, MyEncoder
import matplotlib.pyplot as plt


import json
import os
from collections import defaultdict
from copy import deepcopy
import numpy as np
from pprint import pprint


def run_all(visualize=None, relax=False, verbose=False, variant='a_star'):
  visualize = [] if visualize is None else visualize
  deviations = [int(filename.split('_')[1].split('.')[0])
                 for filename in os.listdir('datasets/logs/delivery_extended')]
  for deviation in sorted(deviations):
    run(deviation, relax=relax, visualize=visualize, verbose=verbose, variant=variant)


def get_filename(base_dir, deviation):
  return f'{base_dir}/per_deviation/log_{deviation}.json'

def get_log(deviation, visualize=None):
  visualize = [] if visualize is None else visualize
  filename = get_filename('./datasets/logs/delivery_extended', deviation)

  with open(filename, 'r') as json_file:
    data = json.load(json_file)
    log_data = {'events': [], 'O': defaultdict(set)}
    object_type = lambda o: o[0].upper()
    indices = []
    for index, (name, label, move_type, mode, sub_marking, add_marking, timestamp_str) in enumerate(data['M']):
      if label is not None:# and (len(set(mode.values()).intersection({'p_001', 'p_003', 'p_004'})) > 0 or label in ['start', 'stop']):
        indices.append(index)
        objects = f'[{", ".join(sorted(list(mode.values())))}]'
        print(f'{timestamp_str:<26} {label:<15}, {objects:<20} ({name})')
        log_data['events'].append([label, f'[{",".join(o for o in mode.values())}]'])
        [log_data['O'][object_type(o)].add(o) for o in mode.values()]
    log_data['O']['W'] = set(['w1', 'w2'])

    log_data['<'] = 'all'
    # log_data['r'] = np.array(data['r'])[np.ix_(indices, indices)].tolist()
    log = Log.from_json_dict(log_data)

    m_i = MultiNuMarking.from_str(data['m_i'])

    if 'log' in visualize:
      log.view()

  return log, m_i, filename

def get_system(relax=False, visualize=None):
  visualize = [] if visualize is None else visualize
  if relax:
    net_filename = f'./datasets/nets/delivery_extended_relaxed.json'
    with open(net_filename, 'r') as json_file:
      data = json.load(json_file)
      system = System.from_json_dict(data, relaxed=relax, silent=lambda name: name.startswith('tau') or name.startswith('t_create^') or name.startswith('t_destroy^'))
  else:
    net_filename = './datasets/nets/delivery_extended.json'
    with open(net_filename, 'r') as json_file:
      data = json.load(json_file)
      system = None
      for net_data in data:
        system_ = System.from_json_dict(net_data)
        if system is None:
          system = system_
        else:
          system = system.union(system_)

  if 'net' in visualize:
    system.view(debug=False)

  return system, net_filename

def view_alignment(alignment):
  TRANSITION_COLOR = lambda m: Color.PURPLE if m.transition.move_type == 'model' else Color.YELLOW if m.transition.move_type == 'log' else Color.GREEN
  move_str = lambda x: f'{x.transition.label}\n({x.transition.name})\n-{x.sub_marking.get_distinct_tokens()}\n+{x.add_marking.get_distinct_tokens()}'
  fill_color = lambda x: 'gray' if x.transition.label is None else 'white'
  alignment.view(element_str=move_str, colors=TRANSITION_COLOR, fill_colors=fill_color)


def run(deviation, relax=False, visualize=None, verbose=False, export=False, variant='a_star', single_object=False, **kwargs):
  visualize = [] if visualize is None else visualize
  system, net_filename = get_system(relax, visualize)
  log, m_i, log_filename = get_log(deviation)
  system.m_i = m_i
  system.marking = deepcopy(m_i)
  system.m_f = deepcopy(m_i)
  system.net.object_types = log.object_types

  if single_object:
    run = POSet()
    info = {}
    for role, object in [(role, role_object) for role, role_objects in log.object_types.items() for role_object in role_objects]:
      system_o = system.projection({object}, system.net.object_types, system.net.variable_types, skip_dups=False)
      for t in system_o.transitions:
        t.name = t.name.split('↾')[0]
        t.label = t.label if t.label is None else t.label.split('↾')[0]
      for p in system_o.places:
        p.name = p.name.split('↾')[0]
      # system_o.view(debug=True)

      log_o = deepcopy(log).projection(lambda e: object in e.identifiers.keys())
      for event in log_o:
        event.identifiers = Multiset(args={object: 1}, key_type=str)
      if len(log_o) == 0:
        print('Log does not contain any events.')
        continue
      log_o.object_types = {role: {object}}

      run_o, info[object] = align(log_o, system_o,
                        {'relax': relax, 'cost': Cost.cost_relaxed if relax else Cost.cost, 'variant': variant,
                         'visualize_sync_net': ('syncnet' in visualize) or ('sync' in visualize),
                         'visualize_log_net': 'lognet' in visualize})
      run = run.union(run_o)
      print(f'Cost {object}: {info[object]["cost"]:_}')
    total_cost = sum([info_o['cost'] for info_o in info.values()])
    total_duration = sum([info_o['duration'] for info_o in info.values()])
    info['cost'] = total_cost
    info['duration'] = total_duration
    print(f'Cost: {info["cost"]:_}')
  else:
    run, info = align(log, system, {'relax': relax, 'cost': Cost.cost_relaxed if relax else Cost.cost, 'variant': variant,
                                    'visualize_sync_net': ('syncnet' in visualize) or ('sync' in visualize),
                                    'visualize_log_net': 'lognet' in visualize})
    print(f'Cost: {info["cost"]:_}')

  if export:
    run = POSet() if run is None else run
    alignment_json = run.to_json()
    alignment_json['M'] = [NoIndent(move) for move in alignment_json['M']]
    alignment_json['r'] = [NoIndent(row) for row in alignment_json['r']]
    data = {'N': net_filename, 'L': log_filename, 'info': info, 'exact': True, 'relaxed': relax,
            'm_i': str(system.m_i), 'm_f': str(system.m_f), **alignment_json}

    al_type = "" if (not relax and not single_object) else "single_object_" if single_object else "relaxed_"
    alignment_filename = get_filename(f'./examples/{al_type}alignments/delivery_extended', deviation)
    print(f'{alignment_filename=}')
    with open(alignment_filename, 'w') as json_file:
      json.dump(data, json_file, cls=MyEncoder, sort_keys=False, indent=2)

  if ('alignment' in visualize) or ('run' in visualize):
    projection_fn = lambda f: not (f.transition.label is None and f.transition.move_type == 'log') # and not (('destroy' in f.transition.name or 'create' in f.transition.name) and f.transition.label is None)
    view_alignment(run.projection(projection_fn))


def view(deviation, relax, single_object, duration, **kwargs):
  get_log(deviation)
  al_type = "" if (not relax and not single_object) else "single_object_" if single_object else "relaxed_"
  filename = get_filename(f'./examples/{al_type}alignments/delivery_extended', deviation)
  print('alignment', filename)

  if not os.path.isfile(filename):
    return {}
  with open(filename, 'r') as json_file:
    data = json.load(json_file)

    moves = []
    for t_name, t_label, move_type, mode, sub_marking, add_marking in data['M']:
      transition = Transition(t_name, t_label)
      transition.move_type = move_type
      firing = MultiNuFiring(transition, mode, MultiNuMarking.from_str(''), MultiNuMarking.from_str('')) # TODO parse sub/add markings
      firing.objects = set(mode.values())
      firing.event = None if move_type == 'model' else Event(t_label, Multiset(args={object: 1 for object in firing.objects}, key_type=str))
      moves.append(firing)
    prec = np.matrix(data['r'])
    alignment = POSet(moves, prec)
    info = data['info']

  if not duration:
    view_alignment(alignment.projection(lambda f: not (f.transition.label is None and f.transition.move_type == 'log')))
    pprint(info)

  return data


def plot_durations(data):
  log_labels = {
    'incorrect_event_order_depot-home': r'$RI_{in}^e(1)$',
    'incorrect_event_order_home-depot': r'$RI_{in}^e(2)$',
    'missing_event_load': r'$RI_{mi}^e$',
    'missing_object_v': r'$RI_{mi}^o$',
    'incorrect_D_object': r'$RI_{in}^o$',
    'missing_position': r'$RI_{mi}^p$',
    'overtake_queue': r'$BI_5$',
    'switch_role': r'$BI_7$',
    'neglect_batching': r'$BI_{10}$',
    'skip_ring': r'$BI_3$',
    'diff_res_mem_warehouse': r'$BI_9$',
    'multitask': r'$BI_2$'
  }
  logs = ['incorrect_event_order_depot-home', 'incorrect_event_order_home-depot', 'missing_event_load', 'missing_object_v',
    'incorrect_D_object', 'missing_position', 'overtake_queue', 'switch_role', 'neglect_batching', 'skip_ring',
    'diff_res_mem_warehouse', 'multitask']

  fig = plt.figure(constrained_layout=True, figsize=(11.5, 6))
  gs = fig.add_gridspec(2, 3)
  ax_all = fig.add_subplot(gs[0, :])
  ax_all.set_title('All methods')
  ax_all.set_yscale('log')
  ax_obj = fig.add_subplot(gs[1, 0])
  ax_obj.set_title('Object')
  ax_obj.set_yscale('log')
  ax_sys = fig.add_subplot(gs[1, 1])
  ax_sys.set_title('Systemic')
  ax_sys.set_yscale('log')
  ax_rel = fig.add_subplot(gs[1, 2])
  ax_rel.set_title('Relaxed')
  ax_rel.set_yscale('log')
  axes = {'object': ax_obj, 'systemic': ax_sys, 'relaxed': ax_rel}

  bar_width = 0.8/3
  for index, (variant, color) in enumerate(zip(['object', 'systemic', 'relaxed'], ['#4394E5', '#87BB62', '#876FD4'])):
    durations = np.array([float(data[log][variant]['info']['duration']) for log in logs])
    ax_all.bar(np.arange(len(logs)) + index*bar_width, durations, width=bar_width, bottom=0, alpha=0.8, color=color, label=variant)
    axes[variant].bar(np.arange(len(logs)), durations, width=0.8, bottom=0, alpha=0.8, color=color)
    axes[variant].set_ylabel('Duration (s)')
    axes[variant].set_xticks(np.arange(len(logs)), [log_labels[l] for l in logs], rotation=45) #, ha='right')
    ax_all.set_ylabel('Duration (s)')
    ax_all.set_xticks(np.arange(len(logs)) + bar_width, [log_labels[l] for l in logs]) #, rotation=45) #, ha='right')
    ax_all.legend()

  plt.tight_layout()
  plt.show()


if __name__ == '__main__':
  import os
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('-c', '--compute', help='Compute alignment(s) (otherwise viewed).', action='store_true')
  parser.add_argument('-ad', '--all_deviations', help='Process all individual deviations.', action='store_true')
  parser.add_argument('-d', '--deviation', help='Specify a single deviation.', type=str)

  parser.add_argument('-r', '--relax', help='Relaxed alignment.', action='store_true')
  parser.add_argument('-so', '--single_object', help='Single object.', action='store_true')
  parser.add_argument('-var', '--variant', help='Variant for alignment computation: [a_star, dijkstra] default: a_star.', type=str)
  parser.add_argument('-vis', '--visualize', help='Comma separated visualization (log,net,sync/syncnet,alignment/run).', type=str)
  parser.add_argument('-e', '--export', help='Postfix of filename to export, not exported if empty.', action='store_true')
  parser.add_argument('-dur', '--duration', help='Plot durations of already computed alignments.', action='store_true')
  parser.add_argument('-v', '--verbose', help='Verbose.', action='store_true')
  args = parser.parse_args()

  args.visualize = [] if args.visualize is None else args.visualize.split(',')
  args.variant = 'a_star' if args.variant is None else args.variant

  vargs = {k: v for k, v in vars(args).items() if v is not None}

  if args.deviation is None and args.all_deviations and not args.duration:
    # Compute or view the alignment for all deviations
    #   i.e., for all logs in './datasets/logs/delivery_extended/per_deviation' or
    #         for all alignments in './experiments/<alignment_method>/delivery_extended/per_deviation'
    for log_filename in sorted(os.listdir('./datasets/logs/delivery_extended/per_deviation')):
      log_filename = log_filename[:-5] if log_filename.endswith('.json') else log_filename
      deviation = log_filename[4:] if log_filename.startswith('log_') else log_filename
      vargs['deviation'] = deviation
      if args.compute:
        run(**vargs)
      else:
        view(**vargs)
  elif args.deviation is None and args.all_deviations and args.duration:
    # Show the durations of the alignments for all methods for all deviations (i.e., for all log in './datasets/logs/delivery_extended/per_deviation')
    #  Note that the alignments must be computed and exported before running this.
    data = defaultdict(dict)
    for log_filename in sorted(os.listdir('./datasets/logs/delivery_extended/per_deviation')):
      log_filename = log_filename[:-5] if log_filename.endswith('.json') else log_filename
      deviation = log_filename[4:] if log_filename.startswith('log_') else log_filename
      vargs['deviation'] = deviation
      for variant, variant_arg in [('object', {'single_object': True, 'relax': False}),
                                   ('systemic', {'single_object': False, 'relax': False}),
                                   ('relaxed', {'single_object': False, 'relax': True})]:
        vargs.update(variant_arg)
        alignment_data = view(**vargs)
        data[deviation][variant] = alignment_data
    plot_durations(data)
  else:
    # Compute or view the alignment for one deviation, specified by args.deviation
    #  Note that the following log or alignment file must exist:
    #    './datasets/logs/delivery_extended/per_deviation/<args.deviation>.json' or
    #    './experiments/<alignment_method>/delivery_extended/per_deviation/<args.deviation>.json'
    if args.compute:
      run(**vargs)
    else:
      view(**vargs)
