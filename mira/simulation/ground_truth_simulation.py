from mira.simulation.simulator import PetrinetSimulator
from mira.nets.system import System
from mira.nets.place import Place
from mira.nets.transition import Transition
from mira.nets.token import ColorToken, MultiColorToken
from mira.nets.arc import MultiVarExpr, VarExpr, SetVar, Var
from mira.nets.marking import MultiNuMarking
from mira.prelim.multiset import Multiset, TypedDict

import re
from itertools import product
from collections import defaultdict
from dateutil.relativedelta import *
from copy import deepcopy
from colorama import Fore, Style
from pprint import pprint


class GroundTruthSimulation(PetrinetSimulator):
  def __init__(self, base_system, patterns, transition_weights, durations, schedules, arrivals):
    self.base_system = base_system
    self.patterns = {pattern_name: self.parse_pattern(base_system, pattern_data['net_data'], pattern_data['mapping']) for pattern_name, pattern_data in patterns.items()}
    self.system = deepcopy(self.base_system)
    for pattern_system in self.patterns.values():
      pattern_system.view(debug=True)
      print(slslsls)
      self.system = self.system.union(pattern_system)
    self.system.net.transition_map = {t.name: t for t in self.system.net.transitions}

    # self.system.view()
    # self.system.view(debug=True)
    # exit()

    self.add_transition_weights(transition_weights)
    arc_durations = self.get_durations(durations)
    super().__init__(self.system, self.parse_schedules({**schedules, 'create': arrivals}), arc_durations)

  def get_durations(self, durations):
    return {(arc.source.name, arc.target.name): durations.get((arc.source.name, arc.target.name), durations.get(arc.source.name, lambda: (0, '')))
            for arc in self.system.net.arcs if isinstance(arc.source, Transition)}

  def add_transition_weights(self, transition_weights):
    for transition in self.system.net.transitions:
      transition.weight = transition_weights.get(transition.name, lambda dt: 0)

  def parse_schedules(self, repeating_specification):
    repeated_firings = []
    # TODO handle variance in schedules

    # TODO fix the list in min can be empty.
    schedule_fn = lambda days, hours: (lambda dt: (min([xs for s in [relativedelta(weekday=day, hour=hour, minute=0, second=0, microsecond=0) for day in days for hour in hours] if (xs := dt + s) > dt]) - dt).total_seconds())
    for transition_name, specification in repeating_specification.items():
      for transition in self.system.net.transitions:
        if transition.name != transition_name:
          continue
        label = transition.label
        if len(roles := self.system.net.get_roles(transition)) != 1 and label == 'create':
          raise ValueError(f'Transition {transition} should have a single role, is ({roles}).')
        role = next(iter(roles))
        if label == 'create':
          if role not in specification:
            raise ValueError(f'Arrivals for role {role} is missing.')
          objects_iterator = iter(sorted(self.system.net.object_types[role]))
          t = deepcopy(transition)
          firing = lambda t, x: self.system.net.enabled_modes(self.system.marking, t, only_vars={ColorToken(next(x))})[0]
          repeated_firings.append((firing, [t, objects_iterator], specification[role]))
        else:
          for object in specification.keys():
            # TODO check validness
            # if object not in self.system.net.object_types[role]:
            #   raise ValueError(f'Object(s) {object} is missing.')
            only_vars = frozenset([ColorToken(x) for x in object]) if isinstance(object, tuple) or isinstance(object, list) else frozenset([ColorToken(object)])
            firing = lambda t, x: self.system.net.enabled_modes(self.system.marking, t, only_vars=x)[0]
            repeated_firings.append((firing, [transition, only_vars], schedule_fn(*specification[object])))

    return repeated_firings

  def __get_element_by_name(self, system, name):
    t = next((t for t in system.transitions if t.name == name), None)
    if t is not None:
      return t
    return next((p for p in system.places if p.name == name), None)

  def parse_pattern(self, system, data, map):
    pp = PatternParser(data, system, map)
    return System.from_json_dict(pp.parse(), verbose=False)


class PatternElement:
  def __init__(self, name, reference=None, projections=None):
    self.name = name
    self.reference = reference
    self.projections = set() if projections is None else projections

class PatternPlace(PatternElement):
  def __init__(self, name, reference=None, projections=None):
    super().__init__(name, reference, projections)


class PatternTransition(PatternElement):
  def __init__(self, name, reference=None, projections=None):
    super().__init__(name, reference, projections)


class PatternParser:
  def __init__(self, pattern_data, base_system, mapping):
    self.pattern_data = pattern_data
    self.base_system = base_system
    self.mapping = mapping

  def parse(self):
    transitions = {t: self.parse_element(PatternTransition(t)) for t in self.pattern_data['T']}
    places      = {p: self.parse_element(PatternPlace(p)) for p in self.pattern_data['P']}
    self.elements = {**places, **transitions}

    self.variables = {}
    self.variables_types = defaultdict(set)
    for role, new_variables in self.pattern_data['V'].items():
      role_ = list(self.parse_element(PatternElement(role)))[0].name
      xx = {}
      for variable in new_variables:
        xx[variable] = list(self.parse_element(PatternElement(variable)))[0].name
        if xx[variable][0].upper() != xx[variable][0]:
          xx[variable] = xx[variable].lower()
      self.variables.update(xx)
      self.variables_types[role_] = set(xx.values())

    self.object_types = {}
    for role, new_objects in self.pattern_data['O'].items():
      role_ = list(self.parse_element(PatternElement(role)))[0].name
      self.object_types[role_] = set([list(self.parse_element(PatternElement(new_object)))[0].name for new_object in new_objects])

    arcs = self.parse_arcs()
    arc_elements = set([element for arcs_ in arcs.values() for (source, target) in arcs_ for element in [source, target]])
    inh_arcs = self.parse_arcs(inhibitors=True)
    arc_elements.update(set([element for arcs_ in inh_arcs.values() for (source, target) in arcs_ for element in [source, target]]))

    pattern_data = deepcopy(self.pattern_data)
    pattern_data['T'] = [t.name for transition_set in transitions.values() for t in transition_set if t.name in arc_elements]
    pattern_data['P'] = [p.name for place_set in places.values() for p in place_set if p.name in arc_elements]
    pattern_data['F'] = {str(label): arcies for label, arcies in arcs.items()}
    pattern_data['F_inh'] = {str(label): arcies for label, arcies in inh_arcs.items()}
    pattern_data['V'] = self.variables_types
    pattern_data['O'] = self.object_types

    markings = {}
    for m in ['m_i', 'm_f']:
      m_i = TypedDict.from_str(pattern_data[m], key_type=str, value_type=str)
      markings[m] = MultiNuMarking()
      for k, v in m_i.items():
        placies = self.parse_element(PatternPlace(k))
        for p in placies:
          markings[m][Place(p.name)] = self.create_tokens([role for role in p.reference.alpha if role in p.projections])
      markings[m] = str(markings[m])
    pattern_data.update(markings)
    # pprint(pattern_data)
    return pattern_data

  def parse_arcs(self, inhibitors=False):
    arcs = defaultdict(list)
    for arc_labels, sub_arcs in self.pattern_data.get(f'F{"_inh" if inhibitors else ""}', {}).items():
      for sources, targets in sub_arcs:
        for source in self.elements[sources]:
          for target in self.elements[targets]:
            if '*' in arc_labels:
              place = source if isinstance(source, PatternPlace) else target
              reference_arc = ref_arc if (ref_arc := self.__get_arc(source.reference, target.reference, inhibitors)) is not None else \
                              self.__get_arc(target.reference, source.reference, inhibitors)
              if reference_arc is None:
                continue
              projected_label = deepcopy(self.label_projection(reference_arc.labels, place.projections))

              append_label = None
              if ',' in arc_labels:
                vvv = set([self.variables.get(v, None) for v in set([l.strip() for l in arc_labels.split(',')[1:]])])
                variables_to_use = {role: list(varies_)[0] for role, varies in self.variables_types.items() if len(varies_ := varies.intersection(vvv)) > 0}
                append_label = list(self.create_label(tuple(sorted(variables_to_use.keys())), variables_to_use).keys())[0][0]

              label_postfix = '_' if len(pattern := re.findall(":(.*?)'", list(self.parse_element(PatternElement(arc_labels)))[0].name)) == 1 else ''
              xroles = pattern[0].split(',') if len(pattern) == 1 else set()
              labels = list(projected_label.keys())
              for label in labels:
                new_label = []
                for varexpr, role in zip(label, [r for r in reference_arc.place.alpha if len(place.projections) == 0 or r in place.projections]):
                  new_label.append(varexpr.add_postfixes(label_postfix) if role in xroles else varexpr)
                  var_strings = set([v.var for v in new_label[-1].vars.union(new_label[-1].setvars)])
                  self.variables_types[role].update(var_strings)
                if append_label is not None:
                  new_label.append(append_label)
                new_label = MultiVarExpr(len(new_label), new_label)
                projected_label[new_label] = projected_label.pop(label)

              arcs[projected_label].append([source.name, target.name])
            else:
              place, transition = (source, target) if isinstance(source, PatternPlace) else (target, source)
              if source.reference is None and target.reference is None:
                for xxx, n in Multiset.from_str(arc_labels, key_type=str).items():
                  vvv = set([l.strip() for l in xxx[1:-1].split(',')])
                  vvv = set([self.variables.get(v, None) for v in vvv])
                  variables_to_use = {role: list(varies_)[0] for role, varies in self.variables_types.items() if len(varies_ := varies.intersection(vvv)) > 0}
                  label = self.create_label(tuple(sorted(variables_to_use.keys())), variables_to_use).multiply(n)
                  arcs[label].append([source.name, target.name])

              elif 'v' in arc_labels and source.reference == target.reference:
                reference_place = source.reference if isinstance(source.reference, Place) else target.reference
                projected_label = self.label_projection(self.create_label(reference_place.alpha), place.projections)
                arcs[projected_label].append([source.name, target.name])

              elif 'V' in arc_labels: # and source.reference == target.reference:
                reference_place = place.reference if place.reference is not None else source.reference if isinstance(source.reference, Place) else target.reference
                for xxx, n in Multiset.from_str(arc_labels, key_type=str).items():
                  vvv = set([v.strip() for l in xxx[1:-1].split(',') for v in l.strip().split('+')])
                  vvv = set([self.variables.get(v, None) for v in vvv])
                  variables_to_use = {role: next(iter(varies_)) if len(varies_) == 1 else '+'.join(varies_) for role, varies in self.variables_types.items() if
                                      len(varies_ := varies.intersection(vvv)) > 0}
                  label = self.create_label(reference_place.alpha, variables_to_use).multiply(n)
                  arcs[label].extend([[source.name, target.name]] * n)

              elif ('ν' not in arc_labels and 'V' not in arc_labels) or transition.reference is None:
                reference_place = place.reference if place.reference is not None else source.reference if isinstance(source.reference, Place) else target.reference
                for xxx, n in Multiset.from_str(arc_labels, key_type=str).items():
                  vvv = set([l.strip() for l in xxx[1:-1].split(',')])
                  vvv = set([self.variables.get(v, None) for v in vvv])
                  variables_to_use = {role: list(varies_)[0] for role, varies in self.variables_types.items() if len(varies_ := varies.intersection(vvv)) > 0}
                  label = self.create_label(reference_place.alpha, variables_to_use).multiply(n)
                  arcs[label].extend([[source.name, target.name]]*n)

    return arcs

  def create_tokens(self, roles):
    role_objects = [list(self.base_system.net.object_types[role]) for role in roles]
    tokens = {}
    for names in product(*role_objects):
      tokens[MultiColorToken.from_str('(' + ','.join([f'[{name}]' for name in names]) + ')')] = 1
    return MultiNuMarking.MultiColorTokenMultiset(args=tokens)

  def label_projection(self, labels, project_on):
    if len(project_on) == 0:
      return labels
    variables = set.union(*[self.base_system.net.variable_types[p] for p in project_on])
    proj_labels = Multiset(key_type=labels.key_type)
    for label, n in labels.items():
      varexprs = [varexpr for varexpr in label if len(varexpr.vars.intersection(variables)) > 0]
      proj_labels[MultiVarExpr(len(varexprs), varexprs)] = n
    return proj_labels

  def create_label(self, roles, variables_to_use=None):
    variables_to_use = {} if variables_to_use is None else variables_to_use
    variables = []
    for role in roles:
      if role in variables_to_use:
        variables.append(variables_to_use.get(role))
      else:
        variables.append(min(self.base_system.net.variable_types[role]))
    # print('vars', variables)
    # print(f'[(' + ','.join(f'[{v}]' for v in variables) + ')]')

    return Multiset.from_str(f'[(' + ','.join(f'[{v}]' if (isinstance(v, Var) or (isinstance(v, str) and v.upper() != v)) else f'{v}' for v in variables) + ')]', MultiVarExpr)

  def parse_element(self, pattern_element, elements=None):
    elements = set() if elements is None else elements
    string = pattern_element.name

    wildcard_matches = list(re.finditer('<(.*?)>', string))
    if len(wildcard_matches) == 0:
      if string[0] == '*':
        element = self.__get_element_by_name(string[1:])
        if element is not None:
          preset_elements = set(arc.source for arc in (list(self.base_system.net.element_in_arcs[element].keys()) + self.base_system.net.transition_inhibitor_arcs.get(element, [])))
          for preset_element in preset_elements:
            elements.update(self.parse_element(type(pattern_element)(preset_element.name, reference=preset_element)))
          return elements
      elif string[-1] == '*':
        element = self.__get_element_by_name(string[:-1])
        if element is not None:
          postset_elements = set(arc.target for arc in (list(self.base_system.net.element_out_arcs[element].keys()) + [inh_arc for inh_arc in self.base_system.inhibitor_arcs if inh_arc.source == element]))
          for postset_element in postset_elements:
            elements.update(self.parse_element(type(pattern_element)(postset_element.name, reference=postset_element)))
          return elements

    for wildcard_match in wildcard_matches:
      wildcard_indices = wildcard_match.span()
      wildcard = string[wildcard_indices[0] + 1:wildcard_indices[1] - 1]
      # Functions are: preceding * for preset, succeeding * for postset, preceding ↾ for projection, ^C for set complement
      preceding = string[wildcard_indices[0] - 1] if wildcard_indices[0] > 0 else None
      succeeding = string[wildcard_indices[1]] if wildcard_indices[1] < len(string) else None
      complement = succeeding == '^' and wildcard_indices[1] < (len(string) - 1) and string[wildcard_indices[1] + 1] == 'C'
      if preceding == '{' and succeeding == '}':
        element = self.__get_element_by_name(self.mapping[wildcard])
        name = string[:wildcard_indices[0] - 1] + string[wildcard_indices[1] + 1:]
        return elements.union(self.parse_element(type(pattern_element)(name, reference=element), elements))
      if preceding == '*':
        element = self.__get_element_by_name(self.mapping[wildcard])
        preset_elements = set(arc.source for arc in (list(self.base_system.net.element_in_arcs[element].keys()) + self.base_system.net.transition_inhibitor_arcs.get(element, [])))
        bracketed = int(wildcard_indices[0] > 1 and string[wildcard_indices[0] - 2] == '(' and wildcard_indices[1] < len(string) and string[wildcard_indices[1]] == ')')
        # TODO check set operation
        if wildcard_indices[1] + bracketed < len(string) and (set_operation := string[wildcard_indices[1] + bracketed]) in ['∩', '\\']:
          suc_string = string[wildcard_indices[1] + bracketed:]
          for wildcard_match2 in re.finditer('<(.*?)>', suc_string):
            wildcard_indices2 = wildcard_match2.span()
            wildcard2 = suc_string[wildcard_indices2[0] + 1:wildcard_indices2[1] - 1]
            element2 = self.__get_element_by_name(self.mapping[wildcard2])
            # TODO this assumes that the righthand wildcard is a postset.
            other_elements = set(arc.target for arc in self.base_system.net.element_out_arcs[element2])
            if set_operation == '∩':
              preset_elements = preset_elements.intersection(other_elements)
            elif set_operation == '\\':
              preset_elements = preset_elements - other_elements
            # Remove righthand wildcard from string
            bracketed2 = int(wildcard_indices2[0] > 0 and suc_string[wildcard_indices2[0] - 1] == '(' and wildcard_indices2[1] < (len(suc_string) - 1) and suc_string[wildcard_indices2[1] + 1] == ')')
            string = string[:wildcard_indices[1] + bracketed:] + string[wildcard_indices[1] + bracketed + 1 + wildcard_indices2[1] + bracketed2:]
            break

        for preset_element in preset_elements:
          name = string[:wildcard_indices[0] - (1 + bracketed)] + preset_element.name + string[wildcard_indices[1] + bracketed:]
          elements.update(self.parse_element(type(pattern_element)(name, reference=preset_element)))
        return elements
      elif succeeding == '*':
        element = self.__get_element_by_name(self.mapping[wildcard])
        postset_elements = set(arc.target for arc in self.base_system.net.element_out_arcs[element])
        bracketed = int(wildcard_indices[0] > 0 and string[wildcard_indices[0] - 1] == '(' and wildcard_indices[1] < (len(string) - 1) and string[wildcard_indices[1] + 1] == ')')

        if wildcard_indices[1] + bracketed + 1 < len(string) and (set_operation := string[wildcard_indices[1] + bracketed + 1]) in ['∩', '\\']:
          suc_string = string[wildcard_indices[1] + bracketed + 1:]
          for wildcard_match2 in re.finditer('<(.*?)>', suc_string):
            wildcard_indices2 = wildcard_match2.span()
            wildcard2 = suc_string[wildcard_indices2[0] + 1:wildcard_indices2[1] - 1]
            element2 = self.__get_element_by_name(self.mapping[wildcard2])
            # TODO this assumes that the righthand wildcard is a preset.
            other_elements = set(arc.source for arc in self.base_system.net.element_in_arcs[element2])
            if set_operation == '∩':
              postset_elements = postset_elements.intersection(other_elements)
            elif set_operation == '\\':
              postset_elements = postset_elements - other_elements
            # Remove righthand wildcard from string
            # bracketed2 = int(wildcard_indices2[0] > 1 and suc_string[wildcard_indices2[0] - 2] == '(' and wildcard_indices2[1] < len(suc_string) and suc_string[wildcard_indices2[1]] == ')')
            string = string[:wildcard_indices[1] + bracketed + 1:] + string[wildcard_indices[1] + bracketed + 1 + wildcard_indices2[1] + bracketed:]
            break

        for postset_element in postset_elements:
          name = string[:wildcard_indices[0] - bracketed] + postset_element.name + string[wildcard_indices[1] + (bracketed + 1):]
          elements.update(self.parse_element(type(pattern_element)(name, reference=postset_element), elements))
        return elements
      elif preceding == '↾' or complement:
        reference_roles = set(self.base_system.net.get_roles(pattern_element.reference))
        roles = set(self.mapping[wildcard].split(','))
        if complement:
          roles = set(self.base_system.net.object_types.keys()) - roles
        roles = roles.intersection(reference_roles)
        if len(roles) == 0:
          return elements
        elif roles == reference_roles:
          name = string[:wildcard_indices[0] - 1] + string[wildcard_indices[1] + (2 * complement):]
          return elements.union(self.parse_element(type(pattern_element)(name, reference=pattern_element.reference, projections=roles), elements))
        name = string[:wildcard_indices[0]] + '{' + ','.join(sorted(roles)) + '}' + string[wildcard_indices[1] + (2 * complement):]
        return elements.union(self.parse_element(type(pattern_element)(name, reference=pattern_element.reference, projections=roles), elements))
      else:
        name = string[:wildcard_indices[0]] + self.mapping[wildcard] + string[wildcard_indices[1] + (2 * complement):]
        reference = self.__get_element_by_name(self.mapping[wildcard])
        reference = pattern_element.reference if reference is None else reference
        return elements.union(self.parse_element(type(pattern_element)(name, reference=reference), elements))

    elements.add(pattern_element)
    return elements

  def __get_element_by_name(self, name):
    t = next((t for t in self.base_system.transitions if t.name == name), None)
    if t is not None:
      return t
    return next((p for p in self.base_system.places if p.name == name), None)

  def __get_arc(self, source, target, inhibitors=False):
    arcs = self.base_system.arcs if not inhibitors else self.base_system.inhibitor_arcs
    return next((arc for arc in arcs if arc.source == source and arc.target == target), None)
